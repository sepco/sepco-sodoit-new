USE [SEPCO-DB]
GO
/****** Object:  UserDefinedTableType [dbo].[MF_DealerAndAccountMapping]    Script Date: 01/19/2018 1:50:07 PM ******/
CREATE TYPE [dbo].[MF_DealerAndAccountMapping] AS TABLE(
	[SoldToParty] [nvarchar](max) NULL,
	[SoldToPartyName] [nvarchar](max) NULL,
	[SiteGroupID] [nvarchar](max) NULL,
	[SiteGroup] [nvarchar](max) NULL,
	[Dealer] [nvarchar](max) NULL,
	[GSCMAccountID] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[AP1ID] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[SimulationStatus] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_DealerAndAccountStoreMapping]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[MF_DealerAndAccountStoreMapping] AS TABLE(
	[SiteID] [nvarchar](max) NULL,
	[SiteName] [nvarchar](max) NULL,
	[SiteGroupID] [nvarchar](max) NULL,
	[SiteGroup] [nvarchar](max) NULL,
	[Dealer] [nvarchar](max) NULL,
	[GSCMAccountID] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[AP1ID] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[AP2] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_ExchangeRate]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[MF_ExchangeRate] AS TABLE(
	[Week] [nvarchar](max) NULL,
	[Month] [nvarchar](max) NULL,
	[Year] [nvarchar](max) NULL,
	[DollarExchangeRate] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_ModelAttributes]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[MF_ModelAttributes] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Attribute1] [nvarchar](max) NULL,
	[Attribute2] [nvarchar](max) NULL,
	[Attribute3] [nvarchar](max) NULL,
	[Attribute4] [nvarchar](max) NULL,
	[Attribute5] [nvarchar](max) NULL,
	[Attribute6] [nvarchar](max) NULL,
	[Attribute7] [nvarchar](max) NULL,
	[Attribute8] [nvarchar](max) NULL,
	[Attribute9] [nvarchar](max) NULL,
	[Attribute10] [nvarchar](max) NULL,
	[Attribute11] [nvarchar](max) NULL,
	[Attribute12] [nvarchar](max) NULL,
	[Attribute13] [nvarchar](max) NULL,
	[Attribute14] [nvarchar](max) NULL,
	[Attribute15] [nvarchar](max) NULL,
	[Attribute16] [nvarchar](max) NULL,
	[Attribute17] [nvarchar](max) NULL,
	[Attribute18] [nvarchar](max) NULL,
	[Attribute19] [nvarchar](max) NULL,
	[Attribute20] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_ModelManagementMapping]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[MF_ModelManagementMapping] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[ModelStatus] [nvarchar](max) NULL,
	[ModelDescription] [nvarchar](max) NULL,
	[ModelClassification] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_ProductGroupAttributes]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[MF_ProductGroupAttributes] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[AttributeColumn] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_StoreListAttributes]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[MF_StoreListAttributes] AS TABLE(
	[SiteID] [nvarchar](max) NULL,
	[SiteName] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[AttributeColumn] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_WOSTarget]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[MF_WOSTarget] AS TABLE(
	[AP1] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[Dealer] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[Week] [nvarchar](max) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[Average] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_YearDateMapping]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[MF_YearDateMapping] AS TABLE(
	[Week] [nvarchar](max) NULL,
	[SellInYearMonth] [nvarchar](max) NULL,
	[SellOutYearMonth] [nvarchar](max) NULL,
	[WeekMapping] [nvarchar](max) NULL,
	[MonthMapping] [nvarchar](max) NULL,
	[YearMapping] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_ACCOUNT_LEVEL_CHANNEL_PSI_MGMT]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_ACCOUNT_LEVEL_CHANNEL_PSI_MGMT] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[AP1] [nvarchar](150) NULL,
	[Account] [nvarchar](150) NULL,
	[Item] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_AP1_FORECAST]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_AP1_FORECAST] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[Account] [nvarchar](150) NULL,
	[Item] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_AP2_PSI]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_AP2_PSI] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[Item] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_FLOORING_SITE]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_FLOORING_SITE] AS TABLE(
	[Brand] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[ProductType] [nvarchar](150) NULL,
	[Capacity] [nvarchar](150) NULL,
	[Model] [nvarchar](150) NULL,
	[SellOut] [int] NULL,
	[Display] [int] NULL,
	[Inventory] [int] NULL,
	[StocksRequested] [int] NULL,
	[StocksDelivered] [int] NULL,
	[PromoterName] [nvarchar](300) NULL,
	[StoreCode] [nvarchar](150) NULL,
	[StoreName] [nvarchar](300) NULL,
	[Week] [nvarchar](150) NULL,
	[Agency] [nvarchar](150) NULL,
	[Dealer] [nvarchar](150) NULL,
	[Segment] [nvarchar](150) NULL,
	[TeamHead] [nvarchar](150) NULL,
	[MidasInCharge] [nvarchar](150) NULL,
	[Area] [nvarchar](150) NULL,
	[ModelStatus] [nvarchar](150) NULL,
	[BWL] [nvarchar](150) NULL,
	[AP1Tagging] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_FLOORING_TARGET]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_FLOORING_TARGET] AS TABLE(
	[StoreCode] [nvarchar](150) NULL,
	[StoreName] [nvarchar](300) NULL,
	[Dealer] [nvarchar](150) NULL,
	[Account] [nvarchar](150) NULL,
	[AP1] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[ProductType] [nvarchar](150) NULL,
	[Model] [nvarchar](150) NULL,
	[DisplayTarget] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [decimal](18, 2) NULL,
	[TransactionID] [nvarchar](250) NULL,
	[User] [nvarchar](25) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_GROSS_NET_PRICE]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_GROSS_NET_PRICE] AS TABLE(
	[Year] [int] NULL,
	[Month] [nvarchar](20) NULL,
	[AP1] [nvarchar](50) NULL,
	[GSCMAccount] [nvarchar](50) NULL,
	[Dealer] [nvarchar](50) NULL,
	[Model] [nvarchar](50) NULL,
	[ProductGroup] [nvarchar](50) NULL,
	[ProductType] [nvarchar](50) NULL,
	[ModelClassification] [nvarchar](50) NULL,
	[GrossPrice] [decimal](18, 2) NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_KPI_ACCOUNT_SELLOUT_FA]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_KPI_ACCOUNT_SELLOUT_FA] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[GSCMAccount] [nvarchar](350) NULL,
	[YearMonth] [nvarchar](150) NULL,
	[YearWeek] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_KPI_AP1_FA_PRODUCTGROUP]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_KPI_AP1_FA_PRODUCTGROUP] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[GSCMAccount] [nvarchar](150) NULL,
	[YearMonth] [nvarchar](150) NULL,
	[YearWeek] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_KPI_AP2_FA_PRODUCTGROUP]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_KPI_AP2_FA_PRODUCTGROUP] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[YearMonth] [nvarchar](150) NULL,
	[YearWeek] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SALES_TARGET]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SALES_TARGET] AS TABLE(
	[AP1] [nvarchar](350) NULL,
	[GSCMAccount] [nvarchar](150) NULL,
	[Dealer] [nvarchar](350) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[TargetName] [nvarchar](150) NULL,
	[YearMonth] [nvarchar](150) NULL,
	[Amount] [decimal](18, 2) NULL,
	[TransactionID] [nvarchar](250) NULL,
	[User] [nvarchar](25) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SELLIN_BILLING]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SELLIN_BILLING] AS TABLE(
	[BusinessGroup] [nvarchar](120) NULL,
	[YearMonth] [nvarchar](50) NULL,
	[SoldToPartyCode] [nvarchar](50) NULL,
	[Area] [nvarchar](250) NULL,
	[Dealer] [nvarchar](250) NULL,
	[AP1] [nvarchar](150) NULL,
	[SoldToPartyName] [nvarchar](250) NULL,
	[Model] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[ProductType] [nvarchar](150) NULL,
	[Description] [nvarchar](550) NULL,
	[Qty] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[AmountType] [nvarchar](5) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SELLIN_BILLING_USD]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SELLIN_BILLING_USD] AS TABLE(
	[BusinessGroup] [nvarchar](120) NULL,
	[Year_Month] [nvarchar](50) NULL,
	[SoldToPartyCode] [nvarchar](50) NULL,
	[Area] [nvarchar](250) NULL,
	[DealerName] [nvarchar](250) NULL,
	[AP1] [nvarchar](150) NULL,
	[SellOutToPartyName] [nvarchar](250) NULL,
	[Material] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[ProductType] [nvarchar](150) NULL,
	[Description] [nvarchar](550) NULL,
	[Qty] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SELLIN_SELLOUT_TARGET_AMOUNT]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SELLIN_SELLOUT_TARGET_AMOUNT] AS TABLE(
	[Dealer] [nvarchar](250) NULL,
	[Account] [nvarchar](250) NULL,
	[AP1] [nvarchar](150) NULL,
	[Type] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SELLIN_SELLOUT_TARGET_QTY]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SELLIN_SELLOUT_TARGET_QTY] AS TABLE(
	[Dealer] [nvarchar](250) NULL,
	[Account] [nvarchar](250) NULL,
	[AP1] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellInGI]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SellInGI] AS TABLE(
	[GC] [nvarchar](max) NULL,
	[AP2] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[SaleQty] [decimal](18, 2) NOT NULL,
	[AmountUSD] [decimal](18, 2) NOT NULL,
	[AmountKRW] [decimal](18, 2) NOT NULL,
	[Currency] [nvarchar](max) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[SalesDate] [datetime] NOT NULL,
	[SalesWeek] [nvarchar](max) NULL,
	[SoldToName] [nvarchar](max) NULL,
	[ShipToName1] [nvarchar](max) NULL,
	[ShipToName2] [nvarchar](max) NULL,
	[Plant] [nvarchar](max) NULL,
	[Storage] [nvarchar](max) NULL,
	[ProductCode] [nvarchar](max) NULL,
	[BillToParty] [nvarchar](max) NULL,
	[SoldToPartyCode] [nvarchar](max) NULL,
	[ShipToPartyCode] [nvarchar](max) NULL,
	[Company] [nvarchar](max) NULL,
	[SalesOrg] [nvarchar](max) NULL,
	[SalesOffice] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Attribute1] [nvarchar](max) NULL,
	[Attribute2] [nvarchar](max) NULL,
	[Attribute3] [nvarchar](max) NULL,
	[Buyer] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NULL,
	[ToolName] [nvarchar](max) NULL,
	[BasicName] [nvarchar](max) NULL,
	[Project] [nvarchar](max) NULL,
	[ModelGroup] [nvarchar](max) NULL,
	[DistributionChannel] [nvarchar](max) NULL,
	[DCDesc] [nvarchar](max) NULL,
	[SalesDocType] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellOutAndInventoryMonthly]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SellOutAndInventoryMonthly] AS TABLE(
	[DivisionGroup] [nvarchar](max) NULL,
	[Division] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[Subsidiary] [nvarchar](max) NULL,
	[ChannelCode] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[ChannelCategory] [nvarchar](max) NULL,
	[ChannelGroup] [nvarchar](max) NULL,
	[ChannelType] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[RRPPriceLoc] [decimal](18, 4) NOT NULL,
	[Month] [nvarchar](max) NULL,
	[Purchase] [int] NOT NULL,
	[Sales] [int] NOT NULL,
	[SellThru] [int] NOT NULL,
	[Inventory] [int] NOT NULL,
	[DisplayQty] [int] NOT NULL,
	[SalesPRC] [decimal](18, 4) NOT NULL,
	[AMTSRRPLoc] [decimal](18, 4) NOT NULL,
	[InvoiceAmountUSD] [decimal](18, 4) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellOutAndInventoryWeekly]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SellOutAndInventoryWeekly] AS TABLE(
	[DivisionGroup] [nvarchar](max) NULL,
	[Division] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[Subsidiary] [nvarchar](max) NULL,
	[ChannelCode] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[ChannelCategory] [nvarchar](max) NULL,
	[ChannelGroup] [nvarchar](max) NULL,
	[ChannelType] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[RRPPriceLoc] [decimal](18, 4) NOT NULL,
	[Week] [nvarchar](max) NULL,
	[Purchase] [int] NOT NULL,
	[Sales] [int] NOT NULL,
	[SellThru] [int] NOT NULL,
	[Inventory] [int] NOT NULL,
	[DisplayQty] [int] NOT NULL,
	[SalesPRC] [decimal](18, 4) NOT NULL,
	[AMTSRRPLoc] [decimal](18, 4) NOT NULL,
	[InvoiceAmountUSD] [decimal](18, 4) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellOutMonthly]    Script Date: 01/19/2018 1:50:08 PM ******/
CREATE TYPE [dbo].[TF_SellOutMonthly] AS TABLE(
	[Subsidiary] [nvarchar](max) NULL,
	[DivisionGroup] [nvarchar](max) NULL,
	[Division] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[PromoterID] [nvarchar](max) NULL,
	[Promoter] [nvarchar](max) NULL,
	[MobileNo] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[NationCD] [nvarchar](max) NULL,
	[ChannelCode] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[Contact] [nvarchar](max) NULL,
	[PhoneNo] [nvarchar](max) NULL,
	[ChannelCategory] [nvarchar](max) NULL,
	[ShipmentType] [nvarchar](max) NULL,
	[AggregationtyPE] [nvarchar](max) NULL,
	[ChannelType] [nvarchar](max) NULL,
	[ChannelGroup] [nvarchar](max) NULL,
	[Month] [nvarchar](max) NULL,
	[RRPPriceLoc] [decimal](18, 4) NOT NULL,
	[Vat] [decimal](18, 4) NOT NULL,
	[RRPLoc] [decimal](18, 4) NOT NULL,
	[RRPAmountLoc] [decimal](18, 4) NOT NULL,
	[SKUSales] [int] NOT NULL,
	[IMEISales] [int] NOT NULL,
	[Sales] [int] NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellOutWeekly]    Script Date: 01/19/2018 1:50:09 PM ******/
CREATE TYPE [dbo].[TF_SellOutWeekly] AS TABLE(
	[Subsidiary] [nvarchar](max) NULL,
	[DivisionGroup] [nvarchar](max) NULL,
	[Division] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[PromoterID] [nvarchar](max) NULL,
	[Promoter] [nvarchar](max) NULL,
	[MobileNo] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[NationCD] [nvarchar](max) NULL,
	[ChannelCode] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[Contact] [nvarchar](max) NULL,
	[PhoneNo] [nvarchar](max) NULL,
	[ChannelCategory] [nvarchar](max) NULL,
	[ShipmentType] [nvarchar](max) NULL,
	[AggregationtyPE] [nvarchar](max) NULL,
	[ChannelType] [nvarchar](max) NULL,
	[ChannelGroup] [nvarchar](max) NULL,
	[Week] [nvarchar](max) NULL,
	[RRPPriceLoc] [decimal](18, 4) NOT NULL,
	[Vat] [decimal](18, 4) NOT NULL,
	[RRPLoc] [decimal](18, 4) NOT NULL,
	[RRPAmountLoc] [decimal](18, 4) NOT NULL,
	[SKUSales] [int] NOT NULL,
	[IMEISales] [int] NOT NULL,
	[Sales] [int] NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_TIM]    Script Date: 01/19/2018 1:50:09 PM ******/
CREATE TYPE [dbo].[TF_TIM] AS TABLE(
	[Dealer] [nvarchar](150) NULL,
	[GSCMAccount] [nvarchar](150) NULL,
	[AP1] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_WOSPerAP1AndAccount]    Script Date: 01/19/2018 1:50:09 PM ******/
CREATE TYPE [dbo].[TF_WOSPerAP1AndAccount] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Category] [nvarchar](max) NULL,
	[Week] [nvarchar](max) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_WOSPerDealer]    Script Date: 01/19/2018 1:50:09 PM ******/
CREATE TYPE [dbo].[TF_WOSPerDealer] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Category] [nvarchar](max) NULL,
	[Week] [nvarchar](max) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[f_split]    Script Date: 01/19/2018 1:50:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_split]
(
@param nvarchar(max), 
@delimiter char(1)
)
returns @t table (val nvarchar(max), seq int)
as
begin
set @param += @delimiter

;with a as
(
select cast(1 as bigint) f, charindex(@delimiter, @param) t, 1 seq
union all
select t + 1, charindex(@delimiter, @param, t + 1), seq + 1
from a
where charindex(@delimiter, @param, t + 1) > 0
)
insert @t
select substring(@param, f, t - f), seq from a
option (maxrecursion 0)
return
end

GO
