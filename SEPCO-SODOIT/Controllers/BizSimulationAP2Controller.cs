﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text;
using SEPCO_SODOIT.ViewModels;
using System.Data;
using SEPCO_SODOIT.Helpers;
using SEPCO;
using System.Data.SqlClient;
using System.IO;
using ClosedXML.Excel;
using Newtonsoft.Json;

namespace SEPCO_SODOIT.Controllers
{
    public class BizSimulationAP2Controller : Controller
    {
        int DefaultPageSize = 10;
        ApplicationDbContext DB = new ApplicationDbContext();
        BizSimulationsLogic myLogic = new BizSimulationsLogic();
        // GET: BizSimulationAP2
        List<string> Category = new List<string>();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SimulationTransactions(int? page)
        {
            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            var q = (from s in DB._SYS_SimulationTrx
                    where s.Simulation == "AP2"
                    select s).OrderByDescending(O=> O.ID);
            ViewBag.OnePageOfItems = q.ToPagedList(pageNumber, DefaultPageSize); // will only contain 25 products max because of the pageSize
            return PartialView("_SimulationTransactions");
        }
        public ActionResult LoadFilters()
        {

            return PartialView("_LoadFilters");
        }
        public JsonResult LoadREFWMSimulationData(string hiddenCategory, string pGroup, string pType, string model, DateTime? dateFrom, DateTime? dateTo, string account, string columns)
        {
            Category.Add("BOH");
            Category.Add("ARRIVAL");
            Category.Add("AGING");
            Category.Add("DEMAND");
            Category.Add("AP1");
            Category.Add("AP2-AP1 BALANCE");
            Category.Add("RTF");
            Category.Add("SP2");
            Category.Add("SP1");
            Category.Add("SP2-SP1 BALANCE");
            Category.Add("INVENTORY AP2/SP2");
            Category.Add("INVENTORY AP1/SP1");
            Category.Add("WOS AP2/SP2 (F4)");
            Category.Add("WOS AP1/SP1 (F4)");
            Category.Add("RUN RATE");
            Category.Add("FLOORING TARGET");
            Category.Add("FLOORING ACTUAL");
            Category.Add("N.PRICE");
            
            List<ModelVM> data = new List<ModelVM>();
            DataTable dt = new DataTable();
            var getWeek = new List<String>();
            var allWeek = "";
            var getModelOmitZero = new List<String>();
            var getFirstCat = Category.FirstOrDefault(x => hiddenCategory.Contains(x));
            var toFilterModel = new List<String>();
            var allFilteredModel = "";
            DataAccessLayer sql = new DataAccessLayer();

               var hiddenCatList = hiddenCategory.Split(',');
            List<int> hiddenCatsForView = new List<int>();
            foreach (var col in columns.Split(','))
            {
                if (col != null)
                    dt.Columns.Add(col);
            }
            var weekFrom = int.Parse(dateFrom.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var weekTo = int.Parse(dateTo.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());

            for (var ctr = 0; weekFrom <= weekTo; ctr++)
            {
                var year = weekFrom.ToString().Substring(0, 4);
                var week = int.Parse(weekFrom.ToString().Substring(4, 2));
                var maxWeek = Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxWeek)
                {
                    weekFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxWeek).ToString().PadLeft(2, '0'));
                    dt.Columns.Add(weekFrom.ToString());
                    getWeek.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
                else
                {
                    dt.Columns.Add(weekFrom.ToString());
                    getWeek.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
            }
            // get the all week
            allWeek = string.Join(",", getWeek);

            //get the all Model except for model with EOL model status
            var getAllModel = DB._MF_ModelManagementMapping.Where(items => pGroup.Contains(items.ProductGroup) && (pType.Contains(items.ProductType) || pType == "") && (model.Contains(items.Model) || model == "") && items.ModelStatus != "EOL");

            //get the Model dont have 0 of total Values
            toFilterModel = getAllModel.Select(x => x.Model).AsEnumerable().Where(x =>
            {
                SqlParameter[] param = {
                                            new SqlParameter("@model",x),
                                            new SqlParameter("@allweek", allWeek),
                                    };
                string TotalofAllCategegory = sql.getSQLStoredProcString("spu_AP2_GET_TotalValueofAllCategories", param);
                return double.Parse(TotalofAllCategegory.ToString()) >= 1;

            }).ToList();
            //get all Filtered Model
            allFilteredModel = string.Join(",", toFilterModel);

            List<ModelVM> ModelFiltered = new List<ModelVM>();

            //add Filtered Models and Add Total
            ModelFiltered = pType.Split(',').Select(x => new ModelVM { Model = "TOTAL", Type = x }).ToList();
            ModelFiltered.AddRange(
                 DB._MF_ModelManagementMapping.Where(m => (allFilteredModel.Contains(m.Model))).Select(x => new ModelVM
                 {
                     Type = x.ProductType,
                     Model = x.Model
                 }).OrderBy(or => or.Model));


            #region formulas
            List<object[]> list = new List<object[]>();
            var ctrRowNum = 0;
            var catOrdinal = columns.Split(',').ToList().FindIndex(F => F == "Category");

            foreach (ModelVM item in ModelFiltered)
            {

                foreach (var cats in Category)
                {
                    ctrRowNum++;
                    DataRow dr = dt.NewRow();
                    var ctrCol = columns.Split(',').Length;

                    if (hiddenCatList.Where(W => W.ToUpper() == cats.ToUpper()).Count() == 0)
                    {
                        hiddenCatsForView.Add(ctrRowNum - 1);
                    }
                    switch (cats)
                    {

                        case "BOH":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        dr[cols.Ordinal] = "0";
                                    }
                                    else
                                    {
                                        if (IsEditableJS(int.Parse(cols.ColumnName), "BOH"))
                                        {
                                            var colAlphaLastWeek = HelperClass.IntToLetters(cols.Ordinal + 1);
                                            dr[ctrCol] = "=" + colAlphaLastWeek + (ctrRowNum) + "+" + colAlphaLastWeek + (ctrRowNum + 1) + "-(IF(" + colAlphaLastWeek + (ctrRowNum + 3) + "<= SUM(" + colAlphaLastWeek + (ctrRowNum) + ":" + colAlphaLastWeek + (ctrRowNum + 1) + ")," + colAlphaLastWeek + (ctrRowNum + 3) + "," + "SUM(" + colAlphaLastWeek + (ctrRowNum) + ":" + colAlphaLastWeek + (ctrRowNum + 1) + ")))";

                                        }
                                        else
                                        {
                                            var value = from t in DB._TF_AP2PSI
                                                        where t.Category == "BOH" && t.Item == item.Model && t.Week == cols.ColumnName
                                                        select t.Value;
                                            dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "ARRIVAL":
                            foreach (DataColumn cols in dt.Columns)
                            {

                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        dr[cols.Ordinal] = "0";
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AP2PSI
                                                    where t.Category.ToUpper() == "ARRIVAL" && t.Item == item.Model && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "AGING":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (cols.Ordinal > 9)
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var colAlphaNextWeekFrom = HelperClass.IntToLetters(cols.Ordinal - 6);
                                        dr[ctrCol] = "=IF(SUM(" + colAlphaNextWeekFrom + (ctrRowNum - 1) + ":" + colAlphaNextWeekFrom + (ctrRowNum - 2) + ") - SUM(" + colAlphaNextWeekFrom + (ctrRowNum + 1) + ":" + colAlpha + (ctrRowNum + 1) + ") < 0,' ',SUM(" + colAlphaNextWeekFrom + (ctrRowNum - 1) + ":" + colAlphaNextWeekFrom + (ctrRowNum - 2) + ") - SUM(" + colAlphaNextWeekFrom + (ctrRowNum + 1) + ":" + colAlpha + (ctrRowNum + 1) + "))";
                                        ctrCol++;
                                    }
                                    else
                                    {
                                        dr[ctrCol] = " ";
                                        ctrCol++;
                                    }
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "DEMAND":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (IsEditableJS(int.Parse(cols.ColumnName), "DEMAND"))
                                    {
                                        if (item.Model.ToUpper() == "TOTAL")
                                        {
                                            var value = DB._TF_AP2PSI.Where(myItem => myItem.Category == "DEMAND" &&
                                            myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                             .Select(v => v.Value);
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                        else
                                        {
                                            var value = from t in DB._TF_AP2PSI
                                                        where t.Category.ToUpper() == "DEMAND" && t.Item == item.Model && t.Week == cols.ColumnName
                                                        select t.Value;
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                     

                                    }
                                    else
                                    {
                                        if (item.Model.ToUpper() == "TOTAL")
                                        {
                                            var value = DB._TF_AP2PSI.Where(myItem => myItem.Category == "GI" &&
                                            myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                             .Select(v => v.Value);
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                        else
                                        {
                                            var value = from t in DB._TF_AP2PSI
                                                        where t.Category.ToUpper() == "GI" && t.Item == item.Model && t.Week == cols.ColumnName
                                                        select t.Value;
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                    }
                                 
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "AP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AP1Forecast.Where(myItem => myItem.Week == cols.ColumnName && myItem.Category == "AP1 FCST" && allFilteredModel.Contains(myItem.Item))
                                          .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = DB._TF_AP1Forecast.Where(myItem => myItem.Week == cols.ColumnName && myItem.Category == "AP1 FCST" && myItem.Item == item.Model)
                                             .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "AP2-AP1 BALANCE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 2) + "-" + colAlpha + (ctrRowNum - 1);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "RTF":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=IF(" + colAlpha + (ctrRowNum - 6) + "+" + colAlpha + (ctrRowNum - 5) + ">" + colAlpha + (ctrRowNum - 3) + "," + colAlpha + (ctrRowNum - 3) + "," + colAlpha + (ctrRowNum - 6) + "+" + colAlpha + (ctrRowNum - 5) + ")";
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "SP2":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (IsEditable(int.Parse(cols.ColumnName)))
                                    {
                                        if (item.Model.ToUpper() == "TOTAL")
                                        {
                                            var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "Sell-out FCST(AP2)" &&
                                            myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                            .Select(v => v.Value);
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                        else
                                        {
                                            var value = from t in DB._TF_AccountLevelPSIMgmt
                                                        where t.Category.ToUpper() == "Sell-out FCST(AP2)" && t.Item == item.Model && t.Week == cols.ColumnName
                                                        select t.Value;

                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                    }
                                    else
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        dr[ctrCol] = "=" + colAlpha + (ctrRowNum + 1);
                                    }

                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "SP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "Sell-out FCST(AP1)" &&
                                        myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                        .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Item == item.Model && t.Category.ToLower() == "Sell-out FCST(AP1)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "SP2-SP1 BALANCE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 2) + "-" + colAlpha + (ctrRowNum - 1);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "INVENTORY AP2/SP2":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (IsEditable(int.Parse(cols.ColumnName)))
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var colAlphaLastWeek = HelperClass.IntToLetters(cols.Ordinal);
                                        if(colAlphaLastWeek == "C")
                                        {
                                            dr[ctrCol] = "= 0 +" + colAlpha + (ctrRowNum - 7) + "-" + colAlpha + (ctrRowNum - 3);
                                        }
                                        else
                                        {
                                            dr[ctrCol] = "=" + colAlphaLastWeek + (ctrRowNum) + "+" + colAlpha + (ctrRowNum - 7) + "-" + colAlpha + (ctrRowNum - 3);
                                        }
                                    }
                                    else
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        dr[ctrCol] = "=" + colAlpha + (ctrRowNum + 1);
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "INVENTORY AP1/SP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "EOH_EX(AP1)" &&
                                     myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                     .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Item == item.Model && t.Category.ToLower() == "EOH_EX(AP1)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "WOS AP2/SP2 (F4)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    int colsOrdinal = cols.Ordinal + 1;
                                    int SellOutRow = ctrRowNum - 5;
                                    int InventoryRow = ctrRowNum - 2;
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {

                                        dr[ctrCol] = myLogic.getWosFormulaTotal(allFilteredModel, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP2)");
                                    }
                                    else
                                    {
                                        dr[ctrCol] = myLogic.getWosFormula(item.Model, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP2)");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "WOS AP1/SP1 (F4)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    int colsOrdinal = cols.Ordinal + 1;
                                    int SellOutRow = ctrRowNum - 5;
                                    int InventoryRow = ctrRowNum - 2;
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {

                                        dr[ctrCol] = myLogic.getWosFormulaTotal(allFilteredModel, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP1)");
                                    }
                                    else
                                    {
                                        dr[ctrCol] = myLogic.getWosFormula(item.Model, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP1))");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                              
                            }
                            break;
                        case "RUN RATE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);

                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 6) + "/" + colAlpha + (ctrRowNum + 2);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "FLOORING TARGET":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_FlooringTarget.Where(myItem => myItem.Week == cols.ColumnName &&
                                        allFilteredModel.Contains(myItem.Model)).Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_FlooringTarget
                                                    where t.Model == item.Model
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "FLOORING ACTUAL":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (IsEditable(int.Parse(cols.ColumnName)))
                                    {
                                        var colAlphaLastWeek = HelperClass.IntToLetters(cols.Ordinal);
                                        if (colAlphaLastWeek.Equals("C"))
                                        {
                                            dr[ctrCol] = "0";
                                        }
                                        else
                                        {
                                            dr[ctrCol] = "=" + colAlphaLastWeek + (ctrRowNum);
                                        }
                                    }
                                    else
                                    {
                                        if (item.Model.ToUpper() == "TOTAL")
                                        {

                                            var value = DB._TF_FlooringSite.Where(myItem => myItem.Week == cols.ColumnName &&
                                            allFilteredModel.Contains(myItem.Model)).Select(v => v.Display);
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                        else
                                        {
                                            var value = from t in DB._TF_FlooringSite
                                                        where t.Model == item.Model
                                                        && t.Week == cols.ColumnName
                                                        select t.Display;
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                    }      
                                    
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "N.PRICE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var dtY = int.Parse(cols.ColumnName.Substring(0, 4));//year
                                    var dtW = int.Parse(cols.ColumnName.Substring(4,2));//month
                                    var dtM = HelperClass.MonthOfYearByWeek(dtY,dtW);//month
                                    var value = from t in DB._TF_GrossNetPrice
                                                where t.Model == item.Model && t.ProductGroup.ToUpper() == pGroup
                                                && t.Month.ToUpper() == dtM.ToUpper() && t.Year == dtY
                                                select t.GrossPrice;
                                    var exchangeRate = from t in DB._TF_ExchangeRate
                                                       where t.Week == cols.ColumnName
                                                       select t.DollarExchangeRate;
                                    dr[ctrCol] = value.Count() == 0 ? "0" : (value.Sum() / exchangeRate.FirstOrDefault()).ToString("0"); 
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;

                    }

                    dt.Rows.Add(dr);
                    list.Add(dr.ItemArray);
                }
            }
            #endregion
            List<string> columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToList();
            List<HeadersVM> colsFormat = new List<HeadersVM>();
            foreach (DataColumn colHead in dt.Columns)
            {
                if (colHead.Caption != "Type" && colHead.Caption != "Model" && colHead.Caption != "Category")
                    colsFormat.Add(new HeadersVM
                    {
                        type = "numeric",
                        data = colHead.ColumnName
                    });
                else
                    colsFormat.Add(new HeadersVM
                    {
                        type = "text",
                        data = colHead.ColumnName
                    });
            }

            DataVM dataVM = new DataVM();
            dataVM.FirstCategory = getFirstCat.ToString();
            dataVM.Data = list;
            dataVM.Count = ModelFiltered.Count();
            dataVM.Headers = columnNames;
            dataVM.HiddenRows = hiddenCatsForView;
            dataVM.ColsFormat = colsFormat;

            return Json(dataVM, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportSellIn(string ProductGroup, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            var dtSellInActFcst = new DataTable();
            dtSellInActFcst.Columns.Add("Product Grp");
            dtSellInActFcst.Columns.Add("Item");
            dtSellInActFcst.Columns.Add("Category");

            //get the index of future weeks
            var startOfEditableCells = 0;
            var headCtr = 0;
            var colHeaders = columns.Split(',').ToList();
            bool alreadAdded = false;
            foreach (var header in colHeaders)
            {
                int week;
                if (int.TryParse(header, out week))
                {
                    if (IsEditableFuture(week))
                    {
                        if (startOfEditableCells == 0)
                            startOfEditableCells = headCtr;

                        //add month total header
                        var monthOfCurrentWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)));
                        var monthOfNextWeek = "";
                        if (int.Parse(week.ToString().Substring(4, 2)) == 52)
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)) + 1, 1);
                        else
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)) + 1);

                        dtSellInActFcst.Columns.Add(week.ToString());

                        if (monthOfCurrentWeek != monthOfNextWeek)
                        {
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                            alreadAdded = true;
                        }
                        else
                        {
                            alreadAdded = false;
                        }


                        if ((colHeaders.Count - 1) == headCtr && !alreadAdded)
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                    }
                }
                headCtr++;
            }
            //get the sell in (act + fcst)
            if (startOfEditableCells != 0)
            {
                var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    if ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")] == "DEMAND" && (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")] != "TOTAL")
                    {
                        DataRow dr = dtSellInActFcst.NewRow();
                        dr[0] = ProductGroup;
                        dr[1] = (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                        dr[2] = "AP2 FCST";

                        var ctr = 0;
                        var ctr1 = 0;
                        var ctr2 = 0;
                        var totalInMonth = 0M;
                        var alreadAddedValue = false;
                        foreach (var cols in item)
                        {
                            if (ctr1 >= startOfEditableCells)
                            {
                                //total in each month
                                var monthOfCurrentWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)));
                                var monthOfNextWeek = "";
                                if (int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) == 52)
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)) + 1, 1);
                                else
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) + 1);

                                dr[3 + ctr] = item[startOfEditableCells + ctr2];

                                ctr++;

                                //if same month, store sum
                                if (monthOfCurrentWeek == monthOfNextWeek)
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    alreadAddedValue = false;
                                }
                                else//if not same month, put the value and increment the ctr to match the week column
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    dr[3 + ctr] = totalInMonth;
                                    ctr++;
                                    alreadAddedValue = true;
                                    totalInMonth = 0;
                                }


                                //if last item but not last week of month
                                if ((item.Count - 1) == ctr1 && !alreadAddedValue)
                                    dr[3 + ctr] = totalInMonth;
                                ctr2++;
                            }
                            ctr1++;
                        }
                        dtSellInActFcst.Rows.Add(dr);
                    }
                }
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtSellInActFcst, "Export Button - Sell-In");
                var filename = "ap2simulation_sell_in" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }

                }
                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "AP2_Sell-In"
                });
                DB.SaveChanges();

                return new JsonResult()
                {
                    Data = filename,
                };

            }

        }
        public ActionResult DeleteFile(string filename)
        {
            try
            {
                DB._SYS_SimulationTrx.Remove(DB._SYS_SimulationTrx.Where(W => W.FileLocation == filename).FirstOrDefault());
                DB.SaveChanges();
                System.IO.File.Delete(Server.MapPath("~/Content/SimulationFiles/" + filename));
                return new JsonResult()
                {
                    Data = "Success"
                };
            }
            catch (Exception c)
            {
                return new JsonResult()
                {
                    Data = "Failed"
                };
            }

        }
        public ActionResult ExportSellOut(string ProductGroup, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            var dtSellInActFcst = new DataTable();
            dtSellInActFcst.Columns.Add("Product Grp");
            dtSellInActFcst.Columns.Add("Item");
            dtSellInActFcst.Columns.Add("Category");

            //get the index of future weeks
            var startOfEditableCells = 0;
            var headCtr = 0;
            var colHeaders = columns.Split(',').ToList();
            bool alreadAdded = false;
            foreach (var header in colHeaders)
            {
                int week;
                if (int.TryParse(header, out week))
                {
                    if (IsEditable(week))
                    {
                        if (startOfEditableCells == 0)
                            startOfEditableCells = headCtr;

                        //add month total header
                        var monthOfCurrentWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)));
                        var monthOfNextWeek = "";
                        if (int.Parse(week.ToString().Substring(4, 2)) == 52)
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)) + 1, 1);
                        else
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)) + 1);

                        dtSellInActFcst.Columns.Add(week.ToString());

                        if (monthOfCurrentWeek != monthOfNextWeek)
                        {
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                            alreadAdded = true;
                        }
                        else
                        {
                            alreadAdded = false;
                        }


                        if ((colHeaders.Count - 1) == headCtr && !alreadAdded)
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                    }
                }
                headCtr++;
            }
            //get the sell out (act + fcst)
            if (startOfEditableCells != 0)
            {
                var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    if ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")] == "SP2" && (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")] != "TOTAL")
                    {
                        DataRow dr = dtSellInActFcst.NewRow();
                        dr[0] = ProductGroup;
                        dr[1] = (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                        dr[2] = "Sell-out FCST(AP2)";

                        var ctr = 0;
                        var ctr1 = 0;
                        var ctr2 = 0;
                        var totalInMonth = 0M;
                        var alreadAddedValue = false;
                        foreach (var cols in item)
                        {
                            if (ctr1 >= startOfEditableCells)
                            {
                                //total in each month
                                var monthOfCurrentWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)));
                                var monthOfNextWeek = "";
                                if (int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) == 52)
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)) + 1, 1);
                                else
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) + 1);

                                dr[3 + ctr] = item[startOfEditableCells + ctr2];

                                ctr++;

                                //if same month, store sum
                                if (monthOfCurrentWeek == monthOfNextWeek)
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    alreadAddedValue = false;
                                }
                                else//if not same month, put the value and increment the ctr to match the week column
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    dr[3 + ctr] = totalInMonth;
                                    ctr++;
                                    alreadAddedValue = true;
                                    totalInMonth = 0;
                                }


                                //if last item but not last week of month
                                if ((item.Count - 1) == ctr1 && !alreadAddedValue)
                                    dr[3 + ctr] = totalInMonth;
                                ctr2++;
                            }
                            ctr1++;
                        }
                        dtSellInActFcst.Rows.Add(dr);
                    }
                }
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtSellInActFcst, "Export Button - Sell-Out");
                var filename = "ap2simulation_sell_out" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }

                }
                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "AP2_Sell-Out"
                });
                DB.SaveChanges();

                return new JsonResult()
                {
                    Data = filename
                };

            }

        }

        public ActionResult ExportREFWMSimulation(string ProductGroup, string GSCMAccount, string Dealer, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            List<string> listOfWeeks = new List<string>();
            //weeks
            var weekFrom = int.Parse(dateFrom.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var weekTo = int.Parse(dateTo.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());
            for (var ctr = 0; weekFrom <= weekTo; ctr++)
            {
                var year = weekFrom.ToString().Substring(0, 4);
                var week = int.Parse(weekFrom.ToString().Substring(4, 2));
                var maxWeek = Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxWeek)
                {
                    weekFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxWeek).ToString().PadLeft(2, '0'));
                    listOfWeeks.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
                else
                {
                    listOfWeeks.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
            }

            var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
            var colHeaders = columns.Split(',').ToList();
            DataTable dtSimulationData = new DataTable();
            List<int> notIncludedCols = new List<int>();
            var ctrCols = 0;
            foreach (var item in colHeaders)
            {
                if (item.ToLower() != "type" && item.ToLower() != "item" && item.ToLower() != "category" && !item.ToLower().Contains("20"))
                    notIncludedCols.Add(ctrCols);
                else
                    dtSimulationData.Columns.Add(item);

                ctrCols++;
            }
            DataAccessLayer sql = new DataAccessLayer();
            var listOfMonths = HelperClass.GetListOfMonths(dateFrom, dateTo);

            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            SqlParameter[] paramExchange = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks))
                    };

            //var data = System.Web.Helpers.Json.Decode(simulationData);
            DataTable dtAP2Forecast = GetExportableData_Keys(ProductGroup, listOfWeeks, "spu_GET_AP2PSITF", "Item", "Category");
            DataTable dtChannelPSI = GetExportableData_5Keys(ProductGroup, listOfWeeks, "spu_ap2_GET_ChannelPSITF", "Item", "Category", "Product Grp", "AP1", "Account");
            DataTable dtExchangeRate = sql.getDTSQLStoredProc("spu_ap2_GET_ExchangeRateMF",paramExchange);
            SqlParameter[] paramSB = {
                new SqlParameter("@YEARMONTH",string.Join(",",listOfMonths)),
                new SqlParameter("@pgroup",ProductGroup),
                new SqlParameter("@ATYPE","PHP")
                    };
            DataTable dtSellInBilling = sql.getDTSQLStoredProc("spu_GET_SellInBillingTF", paramSB);
            SqlParameter[] param1 = {
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtGrossNetPrice = sql.getDTSQLStoredProc("spu_ap2_GET_GrossNetPriceTF", param1);
            DataTable dtFlooringSite = sql.getDTSQLStoredProc("spu_GET_FlooringSiteTF", param);
            DataTable dtFlooringTarget = GetExportableData_FT(ProductGroup, listOfWeeks, "spu_ap2_GET_FlooringTargetTF", "Store Name", "Dealer", "GSCM Account", "AP1", "Product Group" ,"Model");
            //DataTable dtFlooringTarget = GetExportableData(ProductGroup, listOfWeeks, "spu_ap2_GET_FlooringTargetTF", "Model");
            DataTable dtAP1Forecast = GetExportableData_4Keys(ProductGroup, listOfWeeks, "spu_ap2_GET_AP1ForecastTF", "Item","Category","Product Grp", "Account");
            DataTable dtWOSTarget = GetExportableData_WOS(ProductGroup, listOfWeeks, "spu_ap2_GET_WOSTargetMF", "AP1", "GSCM Account", "Dealer","Product Group");
            SqlParameter[] paramST = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtSalesTarget = sql.getDTSQLStoredProc("spu_ap2_GET_SalesTargetTF", paramST);



            using (XLWorkbook wb = new XLWorkbook())
            {
                // Add a DataTable as a worksheet
                var ws = wb.Worksheets.Add("Simulation File");
                #region BuildSimulationSheet

                #region secondbox
                ws.Cell("B6").Value = "DA TOTAL";//todo
                ws.Range("B6:B7").Column(1).Merge();
                ws.Cell("B6").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("B6:B7").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                
                ws.Range("C6:C7").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D6:D7").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Range("C15:C17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D15:D17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;


                ws.Cell("B8").Value = "KPI";
                ws.Range("B8:B17").Column(1).Merge();
                ws.Range("B8:B17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C8:C17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D8:D17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E8:E17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("B18").Value = "Revenue";
                ws.Range("B18:B28").Column(1).Merge();
                ws.Range("B18:B28").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C18:C28").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D18:D28").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E18:E28").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C18:C28").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D18:D28").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E18:E28").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("B29").Value = "Sales Contribution";
                ws.Range("B29:B40").Column(1).Merge();
                ws.Range("B29:B40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C29:C40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D29:D40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E29:E40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C29:C40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D29:D40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E29:E40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Range("C40:C40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D40:D40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E40:E40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;


                ws.Cell("E6").Value = ProductGroup;
                ws.Range("E6:E7").Merge();
                ws.Range("E6:E7").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("D15").Value = "TOTAL";
                ws.Range("D15:D17").Column(1).Merge();

                ws.Cell("E8").Value = "AP2 (sell-in) FA (61%)";
                ws.Cell("E9").Value = "AP1 (sell-in) FA (51%)";
                ws.Cell("E10").Value = "Sell-out FA (61%)";
                ws.Cell("E11").Value = "EOH";
                ws.Cell("E12").Value = "TOTAL AGING";
                ws.Cell("E13").Value = "Month End Sales";
                ws.Cell("E14").Value = "TIM";

                ws.Cell("E15").Value = "Flooring target";
                ws.Cell("E16").Value = "Flooring Actual";
                ws.Cell("E17").Value = "Balance";
                ws.Range("E15:E17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("E18").Value = "LY";
                ws.Cell("E19").Value = "MP";
                ws.Cell("E20").Value = "SP2";
                ws.Cell("E21").Value = "SIRENG";
                ws.Cell("E22").Value = "AP2+Actual";
                ws.Cell("E23").Value = "Revenue / Week";
                ws.Cell("E24").Value = "vs LY";
                ws.Cell("E25").Value = "vs MP";
                ws.Cell("E26").Value = "vs SP";
                ws.Cell("E27").Value = "vs Sireng";
                ws.Cell("E28").Value = "ASP";

                ws.Cell("E29").Value = "MM1";
                ws.Cell("E30").Value = "MM2";
                ws.Cell("E31").Value = "MM3";
                ws.Cell("E32").Value = "MM4";
                ws.Cell("E33").Value = "LUZON_S";
                ws.Cell("E34").Value = "DAG";
                ws.Cell("E35").Value = "CDO";
                ws.Cell("E36").Value = "CEBU";
                ws.Cell("E37").Value = "DAVAO";
                ws.Cell("E38").Value = "ILOILO";
                ws.Cell("E39").Value = "TARGET PLANNING(GC BUFFER -B2C)";

                ws.Cell("E40").Value = "RAC ONLY";

                var qModelMix = from PT in DB._ProductType
                                join PG in DB._ProductGroup
                                on PT.ProductGroup equals PG.GroupName
                                where PG.GroupName == ProductGroup
                                select PT;
                var typeCount = qModelMix.Count();

                //after model minx starting row
                //for sell-out target achievement
                var afterModelMixRow = 40 + (typeCount) + 1;
                //simulation starting row
                var startingRow = 40 + (typeCount) + 23;
                //total row count of items in simulation
                var simulationCount = result.Count;
                //simulation ending row
                var endRow = startingRow + simulationCount;

                //get the months
                //set month header
                //generate & set formula for the monthly basis computation
                var ctrM = 0;
                var ctrMonth = 6;
                var stCtr = 3;
                var monthList = HelperClass.GetListOfMonths(dateFrom, dateTo);
                foreach (var month in monthList)
                {
                    //compute the range week of month
                    //this is used to determine which cell will be the scope of each month
                    var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                    string year = month.Substring(0, 4);
                    string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                    var letter = HelperClass.IntToLetters(ctrMonth);
                    var letterst = HelperClass.IntToLetters(stCtr+ctrM);
                    var letterTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                    //set above month
                    for (int i = 0; i <= rangeDif; i++)
                    {
                        ws.Cell(HelperClass.IntToLetters(ctrMonth + i) + "1").Value = year;
                        ws.Cell(HelperClass.IntToLetters(ctrMonth + i) + "2").Value = monthName;
                        ws.Cell(HelperClass.IntToLetters(ctrMonth + i) + "3").Value = (int.Parse(year)-1) + "" + month.Substring(4,2);
                        ws.Cell(HelperClass.IntToLetters(ctrMonth + i) + "4").Value = month;
                    }
                    //set month header styles
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "6").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "6").Value = monthName;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "6:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "6").Merge();
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "6:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "7").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "8:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "14").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "15:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "18:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "28").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "29:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "39").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "40:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "40").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                    #region revenue
                    //ly
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "18").FormulaA1 = "=SUMIFS('Sell-In_Billing_Transaction'!$M:$M,'Sell-In_Billing_Transaction'!$I:$I," + '"' + "E2" + '"' + ",'Sell-In_Billing_Transaction'!$B:$B," + "'Simulation File'!"+ letter + "$4)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "18:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "18").Merge();

                    //mp
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "19").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$" + letterst + ":$" + letterst + ",''Sales Target_Transaction'!$B:$B,Simulation File!$E19" + ",'Sales Target_Transaction'!$A:$A,Simulation File!$E$6)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "19:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "19").Merge();

                    //sp
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "20").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$" + letterst + ":$" + letterst + ",''Sales Target_Transaction'!$B:$B,Simulation File!$E20" + ",'Sales Target_Transaction'!$A:$A,Simulation File!$E$6)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "20:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "20").Merge();

                    //sireng
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "21").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$" + letterst + ":$" + letterst + ",''Sales Target_Transaction'!$B:$B,Simulation File!$E21" + ",'Sales Target_Transaction'!$A:$A,Simulation File!$E$6)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "21:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "21").Merge();

                    //ap2+actual
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "22").FormulaA1 = "=SUMIFS('Sell-In_Billing_Transaction'!$M:$M,'Sell-In_Billing_Transaction'!$I:$I," + '"' + "E2" + '"' + ",'Sell-In_Billing_Transaction'!$B:$B," + "'Simulation File'!" + letter + "$4)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "22:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "22").Merge();

                    //vs ly
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "23").FormulaA1 = "=IFERROR(" + letter + "22/" + letter + "18-1,0)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "23:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "23").Merge();

                    //vs mp
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "24").FormulaA1 = "=IFERROR(" + letter + "22/" + letter + "19,0)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "24:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "24").Merge();

                    //vs sp
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "25").FormulaA1 = "=IFERROR(" + letter + "22/" + letter + "20,0)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "25:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "25").Merge();

                    //vs sireng
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "26").FormulaA1 = "=IFERROR(" + letter + "22/" + letter + "21,0)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "26:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "26").Merge();

                    //asp todo
                    var f = "=(";
                    //var colEndOfMonth = HelperClass.IntToLetters(ctrMonth + (rangeDif));
                    //for (int i = 0; i <= rangeDif; i++)
                    //{
                    //    //alphabeth
                    //    var colAlpha = HelperClass.IntToLetters(ctrMonth + i);
                    //    var sumIfs = "SUMI(" + colAlpha + "$" + (startingRow + 4) + ":" + colEndOfMonth + "$" + (startingRow + 4) + "," + "$C$" + (startingRow + 1) + ":" + "$C$" + endRow + ",$E" + ctrCCell + "," + "$E$" + (startingRow + 1) + ":" + "$E$" + endRow + "," + '"' + "Invoice Revenue" + '"' + ")";

                    //    f += sumIfs + ",";
                    //}
                    //f = f.Remove(f.Length - 1);
                    //f = f + ")";
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "27").FormulaA1 = f;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "27:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "27").Merge();
                    #endregion

                    //increment
                    ctrM++;
                    ctrMonth += rangeDif + 1;
                }

                ws.Range("A1:AZ5").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("A1:AZ5").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                var ctr = 6;
                var index = 3;
                foreach (var week in listOfWeeks)
                {
                    //set the week
                    //set the styles
                    ws.Cell(HelperClass.IntToLetters(ctr) + "7").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                    ws.Cell(HelperClass.IntToLetters(ctr) + "5").Value = week;
                    ws.Cell(HelperClass.IntToLetters(ctr) + "7").Value = week;

                    var letters = HelperClass.IntToLetters(index);
                    var kpiLetter = HelperClass.IntToLetters(index + 1);//result
                    var kpiLetter1 = HelperClass.IntToLetters(index + 2);//week
                    var letter1 = HelperClass.IntToLetters(ctr);
                    //AP2 (sell-in) FA (61%)
                    ws.Cell(HelperClass.IntToLetters(ctr) + "8").FormulaA1 = "=SUMIFS('KPI_AP1 FA'!" + kpiLetter + ":" + kpiLetter + ",'KPI_AP1 FA'!$A:$A,'Simulation File'!$E$6)";

                    //AP1 (sell-in) FA (51%)
                    ws.Cell(HelperClass.IntToLetters(ctr) + "9").FormulaA1 = "=SUMIFS('KPI_AP1 FA'!" + kpiLetter1 + ":" + kpiLetter1 + ",'KPI_AP1 FA'!$A:$A,'Simulation File'!$E$6,'KPI_AP1 FA'!$B:$B," + '"' + "Result" + '"' + ")";

                    //Sell-out FA (61%)
                    ws.Cell(HelperClass.IntToLetters(ctr) + "10").FormulaA1 = "=SUMIFS('KPI_Account Sell-Out FA'!" + kpiLetter1 + ":" + kpiLetter1 + ",'KPI_Account Sell-Out FA'!$B:$B,'Simulation File'!$E$6,'KPI_Account Sell-Out FA'!$B:$B," + '"' + "Result" + '"' + ")";

                    //EOH todo
                    ws.Cell(HelperClass.IntToLetters(ctr) + "11").FormulaA1 = "-";
                    
                    //Total Aging
                    ws.Cell(HelperClass.IntToLetters(ctr) + "12").FormulaA1 = "=SUMIFS(" + letter1 + (startingRow + 1) + ":" + letter1 + (endRow) + ",$E$" + (startingRow + 1) + ":" + ",$E$" + (endRow) + "," + '"' + "AGING" + '"' + ")";

                    //Flooring Target todo
                    ws.Cell(HelperClass.IntToLetters(ctr) + "15").FormulaA1 = "-";//"=SUMIFS(Flooring_Target_Transaction!";

                    //Flooring Actual
                    ws.Cell(HelperClass.IntToLetters(ctr) + "2163").FormulaA1 = "=IFERROR(" + "SUMIFS('Flooring_Target_Transaction'!$G:G,'Flooring_Target_Transaction'!$N:$N,'Simulation File'" + letter1 + "$5,'Flooring_Target_Transaction'!$B:$B,'Simulation File'$E$6" + ")," + '"' + '"' + ")";

                    //Flooring balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + "17").FormulaA1 = "=IF(" + letter1 + "16=0," + '"' + '"' + "," + letter1 + "15-" + letter1 + "16)";

                    //revenue per week todo

                    ctr++;
                    index++;
                }


                //MODEL MIX

                var ctrCCell = 41;

                ws.Cell("B" + ctrCCell).Value = "Model Mix";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                var ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula todo
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = "-";
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();

                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }

                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }

                    ctrCCell++;
                    ctrModelMix++;
                }

                //after model mix
                ws.Cell("B" + ctrCCell).Value = "Target vs System";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 12)).Column(1).Merge();

                ws.Cell("C" + (ctrCCell + 1)).Value = "GI";

                ws.Cell("D" + (ctrCCell)).Value = "sireng & MP";
                ws.Cell("D" + (ctrCCell + 1)).Value = "Demand";

                ws.Cell("E" + ctrCCell).Value = "Sell-In Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-In(Act+AP2)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-In Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Sell-In Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 0)).FormulaA1 = "0";

                    //Sell-in(Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS('AP2 PSI_Transaction'!" + letters + ":" + letters + "'AP2 PSI_Transaction'!$A:$A,Simulation File!$E$6,'AP2 PSI_Transaction'!$D:$D,Simulation File!$D"+ ctrCCell + ")"  ;

                    //Sell-in Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }

                ctrCCell += 3;

                ws.Cell("D" + (ctrCCell + 1)).Value = "Sell-out FCST(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Sell-Out Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-Out(Account+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-Out Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    var psiLetter = HelperClass.IntToLetters(ctr);
                    //Sell-Out Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 0)).FormulaA1 = "";

                    //Sell-Out (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS('Channel PSI Mgmt_Transaction'!" + psiLetter + ":" + psiLetter + ",'Channel PSI Mgmt_Transaction'!$A:$A,Simulation File!$E$6,'Channel PSI Mgmt_Transaction'!$B:$B," + '"' + "Total" +'"' + ",'Channel PSI Mgmt_Transaction'!$C:$C," + '"' + "Total" +'"' + ",'Channel PSI Mgmt_Transaction'!$D:$D,"+ '"' + "Total" +'"' + ",'Channel PSI Mgmt_Transaction'!$E:$E,Simulation File!D" + (ctrCCell + 1) ;

                    //Sell-Out Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("D" + (ctrCCell + 1)).Value = "EOH_EX(AP1)";
                ws.Cell("D" + (ctrCCell + 2)).Value = "target - act";

                ws.Cell("E" + ctrCCell).Value = "Inventory Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Inventory Act+Fcst";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Inventory Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    var psiLetter = HelperClass.IntToLetters(ctr);
                    //Inventory Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "";

                    //Inventory (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS('Channel PSI Mgmt_Transaction'!" + psiLetter + ":" + psiLetter + ",'Channel PSI Mgmt_Transaction'!$A:$A,Simulation File!$E$6,'Channel PSI Mgmt_Transaction'!$B:$B," + '"' + "Total" + '"' + ",'Channel PSI Mgmt_Transaction'!$C:$C," + '"' + "Total" + '"' + ",'Channel PSI Mgmt_Transaction'!$D:$D," + '"' + "Total" + '"' + ",'Channel PSI Mgmt_Transaction'!$E:$E,Simulation File!D" + (ctrCCell + 1);

                    //Inventory Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Range("B" + (ctrCCell - 11) + ":B" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C" + (ctrCCell - 11) + ":C" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + (ctrCCell - 11) + ":D" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + (ctrCCell - 11) + ":E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("D" + (ctrCCell + 1)).Value = "WOS_EX(AP1)";

                ws.Cell("E" + ctrCCell).Value = "WOS Target(F4)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "WOS Act+Fcst(F4)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "WOS Balance(F4)";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 6;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Sell-Out Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "=SUMIFS('WOS Target_Masterfile'!" + letters + ":" + letters + ",'WOS Target_Masterfile'!$A:$A," + '"' + "Total" + '"' + ",'WOS Target_Masterfile'!$B:$B," + '"' + "Total" + '"' +",'WOS Target_Masterfile'!$C:$C," + '"' + "Total" + '"' + ",'WOS Target_Masterfile'!$D:$D,Simulation File!$E$6";

                    //Sell-Out (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell - 2) + "/AVERAGE(" + HelperClass.IntToLetters(ctr + 1) + (ctrCCell - 5) + ":" + HelperClass.IntToLetters(ctr + 4) + (ctrCCell - 5) + "),0)";

                    //Sell-Out Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1) + "/" + HelperClass.IntToLetters(ctr) + (ctrCCell) + "," + '"' + '"' + ")";

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                //after WOS(F4)
                ws.Cell("B" + ctrCCell).Value = "SCM Simulation";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 12)).Column(1).Merge();
                ws.Range("B" + (ctrCCell) + ":B" + (ctrCCell + 12)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C" + (ctrCCell) + ":C" + (ctrCCell + 12)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + (ctrCCell) + ":D" + (ctrCCell + 12)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + (ctrCCell) + ":E" + (ctrCCell + 12)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("C" + ctrCCell).Value = "Sell In";
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell + 3)).Column(1).Merge();

                ws.Cell("D" + ctrCCell).Value = "Demand";
                ws.Cell("D" + (ctrCCell + 1)).Value = "Sum of Demand";
                ws.Cell("D" + (ctrCCell + 2)).Value = "Sum of AP1";
                ws.Cell("D" + (ctrCCell + 3)).Value = "AP2-FCST";

                ws.Cell("E" + ctrCCell).Value = "Sell-In(AP2)";  
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-In(Act+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "AP1";
                ws.Cell("E" + (ctrCCell + 3)).Value = "Sell-In Balance";
                ws.Range("C" + ctrCCell + ":" + "C" + (ctrCCell + 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":" + "D" + (ctrCCell + 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                var ctrap2 = 5;
                index = 7;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);//for channel
                    var letters1 = HelperClass.IntToLetters(ctrap2);//for ap2
                    //Sell-in(AP2)
                    var f1 = "=SUMIFS('AP2 FCST_Transaction'!" + letters1 + ":" + letters1 + ",'AP2 FCST_Transaction'!$A:$A,'Simulation File'!$E$6,'AP2 FCST_Transaction'!$D:$D,'Simulation File'!$D" + (ctrCCell) + ")";
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = f1;

                    //Sell-in(Act+Fcst)
                    var f2 = "=SUMIFS(" + HelperClass.IntToLetters(ctr) + "$" + (startingRow + 1) + ":" + HelperClass.IntToLetters(ctr) +"$" + (endRow) + "," + "$E$" + (startingRow + 1) + ":$E$" + (endRow) + "," + "$D$" + (ctrCCell) + ")";
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = f2;
                    
                    //AP1
                    var f3 = "=SUMIFS(" + HelperClass.IntToLetters(ctr) + "$" + (startingRow + 1) + ":" + HelperClass.IntToLetters(ctr) + "$" + (endRow) + "," + "$E$" + (startingRow + 1) + ":$E$" + (endRow) + "," + "$E$" + (ctrCCell + 2) + ")";
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = f3;

                    //Sell-in Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 3)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 3)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 4;

                ws.Cell("C" + ctrCCell).Value = "Sell Out";
                ws.Range("C" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + ctrCCell).Value = "Sum of SP1";
                ws.Cell("D" + (ctrCCell + 1)).Value = "Sum of SP2";

                ws.Cell("E" + ctrCCell).Value = "Sell-Out(SP1)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-Out(Act+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-Out Balance";

                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 7;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Sell-out(AP1)
                    var f = "=SUMIFS(" + HelperClass.IntToLetters(ctr) + "$" + (startingRow + 1) + ":" + HelperClass.IntToLetters(ctr) + "$" + (endRow) + "," + "$E$" + (startingRow + 1) + ":$E$" + (endRow) + "," + '"' + "SP1" + '"' + ")";
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = f;

                    //Sell-out(Act+Fcst)
                    var f1 = "=SUMIFS(" + HelperClass.IntToLetters(ctr) + "$" + (startingRow + 1) + ":" + HelperClass.IntToLetters(ctr) + "$" + (endRow) + "," + "$E$" + (startingRow + 1) + ":$E$" + (endRow) + "," + '"' + "SP2" + '"' + ")";
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = f1;

                    //Sell-out Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("C" + ctrCCell).Value = "Inv.";
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + (ctrCCell + 1)).Value = "Sum of Inventory AP2/SP2";

                ws.Cell("E" + ctrCCell).Value = "Inventory (AP1/SP1)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Inventory Act+Fcst";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Inventory Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 7;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    var letters1 = HelperClass.IntToLetters(ctr);
                    //Inventory (AP1)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    //Inventory (Act+Fcst)
                    var f = "=SUMIFS(" + HelperClass.IntToLetters(ctr) + "$" + (startingRow + 1) + ":" + HelperClass.IntToLetters(ctr) + "$" + (endRow) + "," + "$E$" + (startingRow + 1) + ":$E$" + (endRow) + "," + '"' + "Inventory AP2/SP2" + '"' + ")";
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = f;

                    //Inventory Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("C" + ctrCCell).Value = "WOS (F4)";
                ws.Range("C" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();


                ws.Cell("E" + ctrCCell).Value = "WOS AP1/SP1(F4)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "WOS Act+Fcst(F4)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "WOS Balance(F4)";

                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 7;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //WOS
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell - 3) + "/AVG(" + HelperClass.IntToLetters(ctr + 1) + (ctrCCell - 8) + ":" + HelperClass.IntToLetters(ctr + 5) + (ctrCCell - 8) + "),0)";

                    //WOS(Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell - 3) + "/AVG(" + HelperClass.IntToLetters(ctr + 1) + (ctrCCell - 7) + ":" + HelperClass.IntToLetters(ctr + 5) + (ctrCCell - 7) + "),0)"; ;

                    //WOS Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                //data
                ws.Cell("C" + ctrCCell).Value = "Type";
                ws.Cell("D" + ctrCCell).Value = "Model";
                ws.Cell("C" + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Cell("D" + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                //ws.Cell("C80").InsertData(dtSimulationData.Rows);
                ctrCCell += 1;

                //ws.Columns().AdjustToContents();
                ws.Column("A").Width = 20.43;
                ws.Column("B").Width = 9.86;
                ws.Column("C").Width = 11.29;
                ws.Column("D").Width = 22;
                ws.Column("E").Width = 28;
                ws.Column("F").Width = 12;
                ws.Column("G").Width = 12;
                ws.Column("H").Width = 12;
                ws.Column("I").Width = 12;
                ws.Column("J").Width = 12;
                ws.Column("K").Width = 12;
                ws.Column("L").Width = 12;
                ws.Column("M").Width = 12;
                ws.Column("N").Width = 12;
                ws.Column("O").Width = 12;
                ws.Column("P").Width = 12;
                ws.Column("Q").Width = 12;
                ws.Column("R").Width = 12;
                ws.Column("S").Width = 12;
                ws.Column("T").Width = 12;
                ws.Column("U").Width = 12;
                ws.Column("V").Width = 12;
                ws.Column("W").Width = 12;
                ws.Column("X").Width = 12;
                ws.Column("Y").Width = 12;
                ws.Column("z").Width = 12;
                #endregion
                #region simulationdata
                var ctrResultCols = 0;
                var ctrResultCols1 = ctrCCell;
                int catIndex = colHeaders.FindIndex(F => F == "Category");
                var lastModel = "";
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    var ctrCOls = 0;
                    var ctrCOls1 = 0;
                    if (lastModel != (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")])
                    {
                        if (lastModel != "")
                            ctrResultCols += 16;//todo

                        lastModel = item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                    }
                    else
                    {
                        ws.Cell("C" + ctrResultCols1).Style.Font.FontColor = XLColor.White;
                        ws.Cell("D" + ctrResultCols1).Style.Font.FontColor = XLColor.White;
                    }
                    ws.Cell("C" + ctrResultCols1).Value = ((string)item[colHeaders.FindIndex(F => F.ToLower() == "type")]).ToString().Trim();
                    ws.Cell("D" + ctrResultCols1).Value = lastModel.Trim();
                    ws.Cell("E" + ctrResultCols1).Value = ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")]).ToString().Trim();

                    foreach (var cols in item)
                    {
                        if (notIncludedCols.Where(W => W == ctrCOls).Count() == 0)
                        {
                            var letter = "";// HelperClass.IntToLetters(6 + weekIndex);
                            var sLetter = "";// HelperClass.IntToLetters(6 + weekIndex);
                            var isEditable = false;
                            switch ((string)cols)//item[catIndex])
                            {
                                case "BOH":
                                    var we = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(5 + we);
                                        sLetter = HelperClass.IntToLetters(we + 6);//simulation letter
                                        var pastWeek = HelperClass.IntToLetters((we + 6) - 1);
                                        isEditable = IsEditableFuture(int.Parse(w));
                                        if (isEditable)
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IF(" + pastWeek + (ctrCCell + ctrResultCols) + "+" + pastWeek + ((ctrCCell + ctrResultCols) + 1) + "-" + pastWeek + ((ctrCCell + ctrResultCols) + 3) + "<0,0," + pastWeek + (ctrCCell + ctrResultCols) + "+" + pastWeek + ((ctrCCell + ctrResultCols) + 1) + "-" + pastWeek + ((ctrCCell + ctrResultCols) + 3) + ")";
                                            ws.Cell(sLetter + ctrResultCols1).Style.Fill.BackgroundColor = XLColor.Orange;
                                        }
                                        else
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('AP2 PSI_Transaction'!" + letter + ":" + letter + ",'AP2 PSI_Transaction'!$A:$A,'Simulation File'!$E$6,'AP2 PSI_Transaction'!$B:$B,'Simulation File'!$D" + (ctrCCell + ctrResultCols) + ",'AP2 PSI_Transaction'!$D:D,'Simulation File'!$E" + (ctrCCell + ctrResultCols) + ")";
                                        }
                                        we++;
                                    }
                                    break;
                                case "ARRIVAL":
                                    var we1 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(5 + we1);
                                        sLetter = HelperClass.IntToLetters(we1 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('AP2 PSI_Transaction'!" + letter + ":" + letter + ",'AP2 PSI_Transaction'!$A:$A,'Simulation File'!$E$6,'AP2 PSI_Transaction'!$B:$B,'Simulation File'!$D" + (ctrCCell + ctrResultCols) + ",'AP2 PSI_Transaction'!$D:D,'Simulation File'!$E" + (ctrCCell + ctrResultCols) + ")";

                                        we1++;
                                    }
                                    break;
                                case "AGING":
                                    //todo
                                    var we2 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we2);
                                        sLetter = HelperClass.IntToLetters(we2 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "0";
                                        we2++;
                                    }
                                    break;
                                case "DEMAND":
                                    var we3 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(5 + we3);
                                        sLetter = HelperClass.IntToLetters(we3 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('AP2 PSI_Transaction'!" + letter + ":" + letter + ",'AP2 PSI_Transaction'!$A:$A,'Simulation File'!$E$6,'AP2 PSI_Transaction'!$B:$B,'Simulation File'!$D" + (ctrCCell + ctrResultCols) + ",'AP2 PSI_Transaction'!$D:D,'Simulation File'!$E" + (ctrCCell + ctrResultCols) + ")";

                                        we3++;
                                    }
                                    break;
                                case "AP1":
                                    var we4 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(5 + we4);
                                        sLetter = HelperClass.IntToLetters(we4 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('AP1 FCST_Transaction'!" + letter + ":" + letter + ",'AP1 FCST_Transaction'!$B:$B,'Simulation File'!$E$6,'AP1 FCST_Transaction'!$C:$C,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ")";
                                        we4++;
                                    }
                                    break;
                                case "AP2-AP1 BALANCE":
                                    var we5 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we5);
                                        sLetter = HelperClass.IntToLetters(we5 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + letter + ((ctrCCell + 3) + (ctrResultCols)) + "-" + letter + ((ctrCCell + 4) + (ctrResultCols));
                                        we5++;
                                    }
                                    break;
                                case "RTF":
                                    var we6 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we6);
                                        sLetter = HelperClass.IntToLetters(we6 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IF(" + letter + ((ctrCCell) + (ctrResultCols)) + "+" + letter + ((ctrCCell + 1) + (ctrResultCols)) + ">" + letter + ((ctrCCell + 3) + (ctrResultCols)) + "," + letter + ((ctrCCell + 3) + (ctrResultCols)) + "," + letter + ((ctrCCell) + (ctrResultCols)) + "+"+ letter + ((ctrCCell + 1) + (ctrResultCols)) + ")";
                                        we6++;
                                    }
                                    break;
                                case "SP2":
                                    var we7 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we7);
                                        isEditable = IsEditable(int.Parse(w));
                                        sLetter = HelperClass.IntToLetters(we7 + 6);
                                        if (isEditable)
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).Style.Fill.BackgroundColor = XLColor.Orange;
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = item[colHeaders.FindIndex(F => F.ToLower() == w.ToString())];
                                        }
                                        else
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + letter + ((ctrCCell + 8) + (ctrResultCols));
                                        }
                                        we7++;
                                    }
                                    break;
                                case "SP1":
                                    var we8 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we8);
                                        sLetter = HelperClass.IntToLetters(we8 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letter + ":" + letter + ",'Channel Level_Channel PSI Mgmt'!$A:$A,'Simulation File'!$E$6,'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",'Channel Level_Channel PSI Mgmt'!$E:$E,'Simulation File'!$D" + (ctrResultCols1) + ")";
                                        we8++;
                                    }
                                    break;
                                case "SP2-SP1 BALANCE":
                                    var we9 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        sLetter = HelperClass.IntToLetters(we9 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + sLetter + ((ctrResultCols1-2)) + "-" + sLetter + ((ctrResultCols1 - 1));
                                        we9++;
                                    }
                                    break;
                                case "INVENTORY AP2/SP2":
                                    var we10 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(5 + we10);
                                        sLetter = HelperClass.IntToLetters(we10 + 6);
                                        isEditable = IsEditable(int.Parse(w));
                                        if(we10 ==0)
                                                ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "0" ;
                                        else{
                                            if (isEditable)
                                            {
                                                ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + letter + (ctrResultCols1 + 1) ;
                                            }
                                            else
                                            {
                                                ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + sLetter + ((ctrResultCols1) + 1) + "+" + sLetter + ((ctrResultCols1) - 7) + "-" + sLetter + ((ctrResultCols1) - 3);
                                            }

                                        }
                                        
                                        we10++;
                                    }
                                    break;
                                case "INVENTORY AP1/SP1":
                                    var we11 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we11);
                                        sLetter = HelperClass.IntToLetters(we11 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letter + ":" + letter + ",'Channel Level_Channel PSI Mgmt'!$A:$A,'Simulation File'!$E$6,'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",'Channel Level_Channel PSI Mgmt'!$E:$E,'Simulation File'!$D" + (ctrResultCols1) + ")";
                                        we11++;
                                    }
                                    break;
                                case "WOS AP2/SP2 (F4)":
                                    var we12 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we12);
                                        sLetter = HelperClass.IntToLetters(we12 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IFERROR(" + letter + (ctrResultCols1 - 2) + "/AVERAGE(" + HelperClass.IntToLetters(7 + we12) + (ctrResultCols1 - 5) + ":" + HelperClass.IntToLetters((7 + we12) + 4) + (ctrResultCols1 - 5) + ")," + '"' + '"' + ")";
                                        we12++;
                                    }
                                    break;
                                case "WOS AP1/SP1 (F4)":
                                    var we13 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we13);
                                        sLetter = HelperClass.IntToLetters(we13 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IFERROR(" + letter + (ctrResultCols1 - 2) + "/AVERAGE(" + HelperClass.IntToLetters(7 + we13) + (ctrResultCols1 - 5) + ":" + HelperClass.IntToLetters((7 + we13) + 4) + (ctrResultCols1 - 5) + ")," + '"' + '"' + ")";
                                        we13++;
                                    }
                                    break;
                                case "RUN RATE":
                                    var we14 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we14);
                                        sLetter = HelperClass.IntToLetters(we14 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IFERROR(" + letter + (ctrResultCols1 - 6) + "/" + letter + (ctrResultCols1 + 2) + ",0)";
                                        we14++;
                                    }
                                    break;
                                case "FLOORING TARGET":
                                    var we15 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we15);
                                        sLetter = HelperClass.IntToLetters(we15 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Flooring Target_Transaction'!" + letter + ":" + letter + ",'Flooring Target_Transaction'!$H:$H,'Simulation File'!$D" + (ctrResultCols1) + ")";
                                        we15++;
                                    }
                                    break;
                                case "FLOORING ACTUAL":
                                    var we16 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we16);
                                        sLetter = HelperClass.IntToLetters(we16 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Flooring_Site_Transaction'!G:G,'Flooring_Site_Transaction'!$N:$N,'Simulation File'!" + sLetter + "$5" + ",'Flooring_Site_Transaction'!$E:$E,'Simulation File'!$D" + (ctrResultCols1) + ")";
                                        we16++;
                                    }
                                    break;

                                case "N.PRICE":
                                    var we17 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we17);
                                        sLetter = HelperClass.IntToLetters(we17 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Gross & Net Price_Transaction'!L:L,'Gross & Net Price_Transaction'!$H:$H," + '"' + "REF" + '"' + ",'Gross & Net Price_Transaction'!$G:$G,'Sumulation File'!$D" + (ctrResultCols1) + ",'Gross & Net Price_Transaction'!$C:$C,'Simulation File'!" + sLetter + "$2" + ",'Gross & Net Price_Transaction'!$B:$B,'Simulation File'!'" + sLetter + "$1" + ")";//todo
                                        we17++;
                                    }
                                    break;

                            }
                            ctrCOls++;
                            //dr[ctrCOls] = item[ctrCOls];
                        }
                        ctrCOls1++;
                    }

                    //dtSimulationData.Rows.Add(dr);
                    ctrResultCols1++;
                }
                #endregion
                #endregion
                #region BuildKPIFAWorksheet
                var wsKPIFA = wb.Worksheets.Add("KPI_AP1 FA");

                SqlParameter[] paramKPI = {
                        new SqlParameter("@weeks",string.Join(",",HelperClass.GetListOfMonths(dateFrom,dateTo))),
                        new SqlParameter("@pgroup",ProductGroup)
                    };

                DataTable dt = sql.getDTSQLStoredProc("spu_ap2_GET_KPI_AP1_TF", paramKPI);
                var q = (from row in dt.AsEnumerable()
                         group row by row.Field<string>("YearMonth") into grp
                         select grp);//O=>O.FirstOrDefault().Field<string>("YearWeek"));
                List<string> keys = q.Select(S => S.Key).ToList();

                wsKPIFA.Cell("B1").Value = "Year/Month";
                wsKPIFA.Cell("B2").Value = "Year/Week";
                wsKPIFA.Cell("A3").Value = "Product Group";
                wsKPIFA.Cell("B3").Value = "GSCM Account";
                var ctrKPIFA = 3;
                var weekCtr = 4;
                int tryWeek = -1;
                foreach (var month in q)
                {
                    foreach (var week in month)
                    {
                        var colLetter = HelperClass.IntToLetters(ctrKPIFA);
                        wsKPIFA.Cell(colLetter + "1").Value = month.FirstOrDefault().Field<string>("YearMonth");
                        if (!int.TryParse(week.Field<string>("YearWeek"), out tryWeek))
                        {
                            //var colLetterYearWeek = HelperClass.IntToLetters((weekCtr - month.Count()) );
                            wsKPIFA.Cell(colLetter + "2").Value = week.Field<string>("YearWeek");
                        }
                        else
                        {
                            //colLetter = HelperClass.IntToLetters(weekCtr);
                            wsKPIFA.Cell(colLetter + "2").Value = week[1];
                        }


                        ctrKPIFA++;
                        weekCtr++;
                    }
                }

                var q1 = from row in dt.AsEnumerable()
                         group row by new { ProductGroup = row.Field<string>("ProductGroup"), Account = row.Field<string>("GSCM Account") } into grp
                         select grp;

                var ctr1 = 3;
                var rowCtr = 4;
                foreach (var item in q1)
                {
                    foreach (var i in item)
                    {
                        var colLetter = HelperClass.IntToLetters(ctr1);
                        wsKPIFA.Cell(colLetter + "3").Value = "FA(1-8W)";
                        //row
                        wsKPIFA.Cell("A" + (rowCtr.ToString())).Value = i[2];
                        wsKPIFA.Cell("B" + (rowCtr.ToString())).Value = i[3];
                        wsKPIFA.Cell(colLetter + (rowCtr.ToString())).Value = i[4];
                        ctr1++;
                    }
                    rowCtr++;
                }
                wsKPIFA.Columns().AdjustToContents();

                #endregion                #region BuildKPIFAWorksheet
                #region BuildKPIAP2Worksheet
                var wsKPIAp2FA = wb.Worksheets.Add("KPI_AP2 FA");

                SqlParameter[] paramKPIAP2 = {
                        new SqlParameter("@weeks",string.Join(",",HelperClass.GetListOfMonths(dateFrom,dateTo))),
                        new SqlParameter("@pgroup",ProductGroup)
                    };

                DataTable dtap2 = sql.getDTSQLStoredProc("spu_ap2_GET_KPI_AP2_TF", paramKPIAP2);
                var qap2 = (from row in dtap2.AsEnumerable()
                         group row by row.Field<string>("YearMonth") into grp
                         select grp);//O=>O.FirstOrDefault().Field<string>("YearWeek"));
                List<string> keysap2 = q.Select(S => S.Key).ToList();

                wsKPIAp2FA.Cell("B1").Value = "Year/Month";
                wsKPIAp2FA.Cell("B2").Value = "Year/Week";
                wsKPIAp2FA.Cell("A3").Value = "Division";
                var ctrKPIAP2FA = 2;
                var weekCtrAp2 = 3;
                int tryWeekAp2 = -1;
                foreach (var month in qap2)
                {
                    foreach (var week in month)
                    {
                        var colLetter = HelperClass.IntToLetters(ctrKPIAP2FA);
                        wsKPIAp2FA.Cell(colLetter + "1").Value = month.FirstOrDefault().Field<string>("YearMonth");
                        if (!int.TryParse(week.Field<string>("YearWeek"), out tryWeekAp2))
                        {
                            //var colLetterYearWeek = HelperClass.IntToLetters((weekCtr - month.Count()) );
                            wsKPIAp2FA.Cell(colLetter + "2").Value = week.Field<string>("YearWeek");
                        }
                        else
                        {
                            //colLetter = HelperClass.IntToLetters(weekCtr);
                            wsKPIAp2FA.Cell(colLetter + "2").Value = week[1];
                        }


                        ctrKPIAP2FA++;
                        weekCtrAp2++;
                    }
                }

                var qAP2 = from row in dt.AsEnumerable()
                         group row by new { ProductGroup = row.Field<string>("ProductGroup"), Account = row.Field<string>("GSCM Account") } into grp
                         select grp;

                var ctrAP21 = 2;
                var rowCtrAp2 = 3;
                foreach (var item in qAP2)
                {
                    foreach (var i in item)
                    {
                        var colLetter = HelperClass.IntToLetters(ctrAP21);
                        wsKPIAp2FA.Cell(colLetter + "3").Value = "FA(1-8W, Weight)";
                        //row
                        wsKPIAp2FA.Cell("A" + (rowCtrAp2.ToString())).Value = i[2];
                        wsKPIAp2FA.Cell("B" + (rowCtrAp2.ToString())).Value = i[3];
                        wsKPIAp2FA.Cell(colLetter + (rowCtrAp2.ToString())).Value = i[4];
                    }
                    rowCtrAp2++;
                }
                wsKPIAp2FA.Columns().AdjustToContents();

                #endregion
                #region BuildKPIAccountWorksheet
                SqlParameter[] paramKPIAccount = {
                new SqlParameter("@weeks",string.Join(",",HelperClass.GetListOfMonths(dateFrom,dateTo))),
                new SqlParameter("@pgroup",ProductGroup)
                    };

                var wsKPIAccount = wb.Worksheets.Add("KPI_Account Sell-Out FA");
                DataTable dtKPIAccount = sql.getDTSQLStoredProc("spu_ap2_GET_KPI_AP1_Account_TF", paramKPIAccount);
                var qKPIAccount = (from row in dtKPIAccount.AsEnumerable()
                                   group row by row.Field<string>("YearMonth") into grp
                                   select grp);//O=>O.FirstOrDefault().Field<string>("YearWeek"));
                List<string> keysKPIAccount = qKPIAccount.Select(S => S.Key).ToList();

                wsKPIAccount.Cell("B1").Value = "Year/Month";
                wsKPIAccount.Cell("B2").Value = "Year/Week";
                wsKPIAccount.Cell("A3").Value = "Product Group";
                wsKPIAccount.Cell("B3").Value = "GSCM Account";
                var ctrKPIAccount = 3;
                var weekCtrKPIAccount = 4;
                int tryWeekKPIAccount = -1;
                foreach (var month in qKPIAccount)
                {
                    foreach (var week in month)
                    {
                        var colLetter = HelperClass.IntToLetters(ctrKPIAccount);
                        wsKPIAccount.Cell(colLetter + "1").Value = month.FirstOrDefault().Field<string>("YearMonth");
                        if (!int.TryParse(week.Field<string>("YearWeek"), out tryWeekKPIAccount))
                        {
                            //var colLetterYearWeek = HelperClass.IntToLetters((weekCtrKPIAccount - month.Count())) ;
                            wsKPIAccount.Cell(colLetter + "2").Value = week.Field<string>("YearWeek");
                        }
                        else
                        {
                            //colLetter = HelperClass.IntToLetters(weekCtrKPIAccount);
                            wsKPIAccount.Cell(colLetter + "2").Value = week[1];
                        }


                        ctrKPIAccount++;
                        weekCtrKPIAccount++;
                    }
                }

                var q1KPIAccount = from row in dtKPIAccount.AsEnumerable()
                                   group row by new { ProductGroup = row.Field<string>("ProductGroup"), Account = row.Field<string>("GSCM Account") } into grp
                                   select grp;

                var ctr1KPIAccount = 3;
                var rowCtrKPIAccount = 4;
                foreach (var item in q1KPIAccount)
                {
                    foreach (var i in item)
                    {
                        var colLetter = HelperClass.IntToLetters(ctr1KPIAccount);
                        wsKPIAccount.Cell(colLetter + "3").Value = "FA(1-8W, Mean)";
                        //row
                        wsKPIAccount.Cell("A" + (rowCtrKPIAccount.ToString())).Value = i[2];
                        wsKPIAccount.Cell("B" + (rowCtrKPIAccount.ToString())).Value = i[3];
                        wsKPIAccount.Cell(colLetter + (rowCtrKPIAccount.ToString())).Value = i[4];
                        ctr1KPIAccount++;
                    }
                    rowCtrKPIAccount++;
                }
                wsKPIAccount.Columns().AdjustToContents();

                #endregion
                #region OtherSheets
                var ws1 = wb.Worksheets.Add("AP2 PSI_Transaction");
                ws1.Cell("B1").InsertTable(dtAP2Forecast);
                var wsc1 = HelperClass.IntToLetters(dtAP2Forecast.Columns.Count);
                ws1.Range("E2:" + wsc1 + (dtAP2Forecast.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsChannelLevel = wb.Worksheets.Add("Channel Level_Channel PSI Mgmt");
                wsChannelLevel.Cell("B1").InsertTable(dtChannelPSI);
                var wsChannelLevelColAlpha = HelperClass.IntToLetters(dtChannelPSI.Columns.Count);
                wsChannelLevel.Range("G4:" + wsChannelLevelColAlpha + (dtChannelPSI.Rows.Count + 1)).DataType = XLCellValues.Number;

                wb.Worksheets.Add(dtExchangeRate, "Exchange Rate_Transaction");

                var wsWtM = wb.Worksheets.Add("Week-to-Month Ratio");

                wb.Worksheets.Add(dtSellInBilling, "Sell-In_Billing_Transaction");
                wb.Worksheets.Add(dtGrossNetPrice, "Gross & Net Price_Transaction");

                var wsFlooringSite = wb.Worksheets.Add("Flooring_Site_Transaction");
                wsFlooringSite.Cell("A1").InsertTable(dtFlooringSite);
                //var wsFlooringSiteColAlpha = HelperClass.IntToLetters(dtFlooringSite.Columns.Count);
                //wsFlooringSite.Range("F2:" + wsFlooringSiteColAlpha + (dtFlooringSite.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsFlooringTarget = wb.Worksheets.Add("Flooring Target_Transaction");
                wsFlooringTarget.Cell("A1").InsertTable(dtFlooringTarget);
                var wsFlooringTargetColAlpha = HelperClass.IntToLetters(dtFlooringTarget.Columns.Count);
                wsFlooringTarget.Range("I2:" + wsFlooringTargetColAlpha + (dtFlooringTarget.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsAP1Fcst = wb.Worksheets.Add("AP1 FCST_Transaction");
                wsAP1Fcst.Cell("A1").InsertTable(dtAP1Forecast);
                var wsAP1FcstCol = HelperClass.IntToLetters(dtAP1Forecast.Columns.Count);
                wsAP1Fcst.Range("E2:" + wsAP1FcstCol + (dtAP1Forecast.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsWOSTarget = wb.Worksheets.Add("WOS Target_Masterfile");
                wsWOSTarget.Cell("A1").InsertTable(dtWOSTarget);
                var wsWosTargetColAlpha = HelperClass.IntToLetters(dtWOSTarget.Columns.Count);
                wsWOSTarget.Range("E2:" + wsWosTargetColAlpha + (dtWOSTarget.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsST = wb.Worksheets.Add("Sales Target_Transaction");
                wsST.Cell("A1").InsertTable(dtSalesTarget);
                var wsSTCol = HelperClass.IntToLetters(dtSalesTarget.Columns.Count);
                wsST.Range("C2:" + wsSTCol + (dtSalesTarget.Rows.Count + 1)).DataType = XLCellValues.Number;
                #endregion

                var filename = "ap2simulation" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }

                }

                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "AP2"
                });
                DB.SaveChanges();

                //Response.End();
                return new JsonResult()
                {
                    Data = filename
                };
            }
        }
        public ActionResult DownloadSimulationFile(string fileName)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + fileName), FileMode.Open, FileAccess.Read))
                {
                    file.CopyTo(memoryStream);
                }
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }
            Response.End();
            return RedirectToAction("Index");
        }
        public JsonResult GetGSCMAccounts()
        {
            var q = (from pGroup in DB._MF_DealerAndAccountMapping
                     where pGroup.SimulationStatus.ToUpper() =="YES"
                     orderby pGroup.GSCMAccount ascending
                     select new GSCMAccountVM { GSCMAccount = pGroup.Dealer, AP1 = pGroup.AP1 }).Distinct().GroupBy(G => G.AP1);
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductGroup()
        {
            var q = from pGroup in DB._ProductGroup
                    orderby pGroup.GroupName ascending
                    select pGroup.GroupName;
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductGroupAttributes(string pGroup)
        {
            var q = from attri in DB._MF_ProductGroupAttributes
                    where attri.ProductGroup == pGroup
                    select new
                    {
                        Description = attri.Description,
                        Attribute = attri.AttributeColumn
                    };
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductTypeList(string pGroup)
        {
            var prodType = from pt in DB._ProductType
                           where pGroup.Contains(pt.ProductGroup)
                           select new
                           {
                               Type = pt.ProductType
                           };

            return Json(prodType, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetModelListByType(string pType)
        {
            var modelList = (from m in DB._MF_ModelManagementMapping
                             where m.ProductType == pType
                             orderby m.Model ascending
                             select new
                             {
                                 Model = m.Model,
                                 ID = m.ID
                             });

            return Json(modelList, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        private void SetNonWeekColumns(DataColumn cols, DataRow dr, ModelVM item, string cats, string pGroup)
        {
            switch (cols.ColumnName)
            {
                case "Type":
                    dr[cols.Ordinal] = item.Type;
                    break;
                case "Item":
                    dr[cols.Ordinal] = item.Model;
                    break;
                case "Category":
                    dr[cols.Ordinal] = cats;
                    break;
                default:

                    var q1 = from pAttri in DB._MF_ProductGroupAttributes
                             where pAttri.ProductGroup == pGroup && pAttri.Description == cols.Caption
                             select pAttri.AttributeColumn;
                    DataAccessLayer sql = new DataAccessLayer();
                    string cmd = "SELECT " + q1.FirstOrDefault() + " FROM MF_ModelAttributes WHERE Model=@model AND ProductGroup=@group";
                    SqlParameter[] param = {
                                                new SqlParameter("@model",item.Model),
                                                new SqlParameter("@group",pGroup)
                                                   };
                    var result = sql.getSQLDataReader(cmd, param);
                    dr[cols.Ordinal] = result;

                    break;
            }
        }
    
        //current and future
        public bool IsEditable(int yearWeek)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());
            if (currentWeek <= yearWeek)
                return true;
            else
                return false;
        }
        //future only
        public bool IsEditableFuture(int yearWeek)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());
            if (currentWeek < yearWeek)
                return true;
            else
                return false;
        }
        public bool IsEditableJS(int yearWeek,string category)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());

            if (currentWeek == yearWeek && category.ToUpper() == "DEMAND")
                return false;
            if (currentWeek == yearWeek && category.ToUpper() == "BOH")
                return false;

            if (currentWeek <= yearWeek)
                return true;
            else
                return false;
        }
        public DataTable GetExportableData_4Keys(string ProductGroup, List<string> listOfWeeks, string sp, string key1, string key2, string key3, string key4)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { ProductGroup = row.Field<string>(key3),Account = row.Field<string>(key4), Item = row.Field<string>(key1), Category = row.Field<string>(key2) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_WOS(string ProductGroup, List<string> listOfWeeks, string sp, string key1, string key2, string key3, string key4)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { ProductGroup = row.Field<string>(key4), Account = row.Field<string>(key2), Dealer = row.Field<string>(key3), AP1 = row.Field<string>(key1) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_FT(string ProductGroup, List<string> listOfWeeks, string sp, string storename, string dealer, string gscmaccount, string ap1, string pgroup, string model)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { StoreName = row.Field<string>(storename), Account = row.Field<string>(gscmaccount), Dealer = row.Field<string>(dealer), AP1 = row.Field<string>(ap1), ProductGroup = row.Field<string>(pgroup), Model = row.Field<string>(model) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_5Keys(string ProductGroup, List<string> listOfWeeks, string sp, string key1, string key2, string key3, string key4, string key5)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { ProductGroup = row.Field<string>(key3), AP1 = row.Field<string>(key4), Account = row.Field<string>(key5) ,Item = row.Field<string>(key1), Category = row.Field<string>(key2) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_Keys(string ProductGroup,  List<string> listOfWeeks, string sp, string key1, string key2)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { Item = row.Field<string>(key1), Category = row.Field<string>(key2) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        
        public DataTable GetExportableData(string ProductGroup, List<string> listOfWeeks, string sp, string key)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by row.Field<string>("Week") into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by row.Field<string>(key) into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;

                var list = itemGroup.OrderBy(O => O.Field<string>("Week"));
                foreach (var item in list)
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    ctr++;
                }


                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
    }
}