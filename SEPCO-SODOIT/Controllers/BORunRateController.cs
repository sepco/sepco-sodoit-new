﻿using SEPCO_SODOIT.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using SEPCO_SODOIT.Models;
using System.Globalization;
using System.Collections;
using Newtonsoft.Json;
using SEPCO_MVC.Models;
namespace SEPCO_SODOIT.Controllers
{
    public class BORunRateController : Controller
    {
        // GET: BORunRate
        myClass mclass = new myClass();
        public ActionResult Index()
        {
            List<ProductGroup> PGList = new List<ProductGroup>();
            DataTable dt = mclass.getSQLDataTableNoParam("Select Distinct GroupName from ProductGroup ORDER BY ID ASC");
            foreach (DataRow row in dt.Rows)
            {
                PGList.Add(new ProductGroup()
                {
                    GroupName = row["GroupName"].ToString()
                });
            }

            List<MF_DealerAndAccountStoreMapping> AP1 = new List<MF_DealerAndAccountStoreMapping>();
            DataTable dt1 = mclass.getSQLDataTableNoParam("Select Distinct AP1 from MF_DealerAndAccountStoreMapping");
            foreach (DataRow row in dt1.Rows)
            {
                AP1.Add(new MF_DealerAndAccountStoreMapping()
                {
                    AP1 = row["AP1"].ToString()
                });
            }

            List<MF_YearDateMapping> Month = new List<MF_YearDateMapping>();
            DataTable MonthDT = mclass.getSQLDataTableNoParam("select DISTINCT MonthMapping , MonthNumber = " +
                                                                " CASE " +
                                                                " WHEN MonthMapping =  'JAN' Then 1" +
                                                                " WHEN MonthMapping =  'FEB' Then 2" +
                                                                " WHEN MonthMapping =  'MAR' Then 3" +
                                                                " WHEN MonthMapping =  'APR' Then 4" +
                                                                " WHEN MonthMapping =  'MAY' Then 5" +
                                                                " WHEN MonthMapping =  'JUN' Then 6" +
                                                                " WHEN MonthMapping =  'JUL' Then 7" +
                                                                " WHEN MonthMapping =  'AUG' Then 8" +
                                                                " WHEN MonthMapping =  'SEP' Then 9" +
                                                                " WHEN MonthMapping =  'OCT' Then 10" +
                                                                " WHEN MonthMapping =  'NOV' Then 11" +
                                                                " WHEN MonthMapping =  'DEC' Then 12" +
                                                                " END" +
                                                                " FROM MF_YearDateMapping" +
                                                                " ORDER BY MonthNumber");
            foreach (DataRow row in MonthDT.Rows)
            {
                Month.Add(new MF_YearDateMapping()
                {
                    MonthMapping = row["MonthMapping"].ToString()
                });
            }

            List<MF_YearDateMapping> Year = new List<MF_YearDateMapping>();
            DataTable YearDT = mclass.getSQLDataTableNoParam("Select Distinct YearMapping from MF_YearDateMapping");
            foreach (DataRow row in YearDT.Rows)
            {
                Year.Add(new MF_YearDateMapping()
                {
                    YearMapping = row["YearMapping"].ToString()
                });
            }
            ViewBag.ProductGroup = new SelectList(PGList, "GroupName", "GroupName");
            ViewBag.AP1 = new SelectList(AP1, "AP1", "AP1");
            //ViewBag.WeekMappingFrom1 = new SelectList(Week, "WeekMapping", "WeekMapping");
            ViewBag.MonthMappingFrom1 = new SelectList(Month, "MonthMapping", "MonthMapping");
            ViewBag.YearMappingFrom1 = new SelectList(Year, "YearMapping", "YearMapping");

            //ViewBag.WeekMappingTo1 = new SelectList(Week, "WeekMapping", "WeekMapping");
            ViewBag.MonthMappingTo1 = new SelectList(Month, "MonthMapping", "MonthMapping");
            ViewBag.YearMappingTo1 = new SelectList(Year, "YearMapping", "YearMapping");
            return View();
        }
        public ActionResult getGSCMAccountListBasedOnAP1(string AP1)
        {

            List<MF_DealerAndAccountStoreMapping> GSCMAccontListStore = new List<MF_DealerAndAccountStoreMapping>();
            var query = string.Format("Select Distinct GSCMAccount from MF_DealerAndAccountStoreMapping where AP1 in ({0})", AP1);
            DataTable dt = mclass.getSQLDataTableNoParam(query);
            foreach (DataRow row in dt.Rows)
            {
                GSCMAccontListStore.Add(new MF_DealerAndAccountStoreMapping()
                {
                    GSCMAccount = row["GSCMAccount"].ToString()
                });
            }

            return Json(GSCMAccontListStore, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getDealerListByGSCMAccount(string gscmaccount)
        {
            List<MF_DealerAndAccountStoreMapping> dealerlist = new List<MF_DealerAndAccountStoreMapping>();
            var query = string.Format("Select distinct Dealer from MF_DealerAndAccountStoreMapping where GSCMAccount in ({0})", gscmaccount);
            DataTable dt = mclass.getSQLDataTableNoParam(query);
            foreach (DataRow row in dt.Rows)
            {
                dealerlist.Add(new MF_DealerAndAccountStoreMapping()
                {
                    Dealer = row["Dealer"].ToString()
                });
            }
            return Json(dealerlist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult getSiteNameListByDealer(string dealer)
        {
            List<MF_DealerAndAccountStoreMapping> SiteNameList = new List<MF_DealerAndAccountStoreMapping>();
            var query = string.Format("Select Distinct SiteName from MF_DealerAndAccountStoreMapping where Dealer in ({0})", dealer);
            DataTable dt = mclass.getSQLDataTableNoParam(query);
            foreach (DataRow row in dt.Rows)
            {
                SiteNameList.Add(new MF_DealerAndAccountStoreMapping()
                {
                    SiteName = row["SiteName"].ToString()
                });
            }

            return Json(SiteNameList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult getProductByProductGroup(string ProductGroup)
        {
            List<MF_ModelManagementMapping> Product = new List<MF_ModelManagementMapping>();
            SqlParameter[] sql = { 
                                    new SqlParameter("@pgroup",ProductGroup)
                                 };
            DataTable myProductDT = mclass.getSQLDataTable("Select Distinct ProductType,ProductGroup from MF_ModelManagementMapping where ProductGroup=@pgroup", sql);
            foreach (DataRow row in myProductDT.Rows)
            {
                Product.Add(new MF_ModelManagementMapping()
                {
                    ProductType = row["ProductType"].ToString()
                });
            }
            return Json(Product, JsonRequestBehavior.AllowGet);

        }

        public ActionResult getModelByProduct(string Product)
        {
            List<MF_ModelManagementMapping> myModel = new List<MF_ModelManagementMapping>();


            SqlParameter[] sql = { 
                                    new SqlParameter("@product",Product)
                                 };
            DataTable myProductDT = mclass.getSQLDataTable("Select Distinct Model from MF_ModelManagementMapping where ProductType=@product", sql);
            foreach (DataRow row in myProductDT.Rows)
            {
                myModel.Add(new MF_ModelManagementMapping()
                {
                    Model = row["Model"].ToString()
                });
            }
            return Json(myModel, JsonRequestBehavior.AllowGet);

        }
    }
}