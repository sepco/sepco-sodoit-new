﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPCO_SODOIT.Controllers
{
    public class DealerController : Controller
    {
        ApplicationDbContext DB = new ApplicationDbContext();
        public ActionResult Index()
        {
            var dealerList = DB._SYS_DEALER.ToList();
            return View(dealerList);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Dealer dealer)
        {
            if (ModelState.IsValid)
            {
                DB._SYS_DEALER.Add(dealer);
                DB.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var dealertoedit = DB._SYS_DEALER.Find(id);
            return View(dealertoedit);
        }
        [HttpPost]
        public ActionResult Edit(Dealer dealer)
        {
            if (ModelState.IsValid)
            {
                DB.Entry(dealer).State = EntityState.Modified;
                DB.SaveChanges();
            }
            return RedirectToAction("Index");

        }
        [HttpGet]
        [ActionName("Delete")]
        public ActionResult DeleteGet(int id)
        {
            var dealertodelete = DB._SYS_DEALER.Find(id);
            return View(dealertodelete);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            var dealertodelete = DB._SYS_DEALER.Find(id);
            DB._SYS_DEALER.Remove(dealertodelete);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}