﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPCO_SODOIT.Controllers
{
    public class GSCMAccountController : Controller
    {
        ApplicationDbContext DB = new ApplicationDbContext();
        public ActionResult Index()
        {
            var gscmaccountlist = DB._SYS_GSCMACCOUNT.ToList();
            return View(gscmaccountlist);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(GSCMAccount gscm)
        {
            if (ModelState.IsValid)
            {
                DB._SYS_GSCMACCOUNT.Add(gscm);
                DB.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var gscmtoEdit = DB._SYS_GSCMACCOUNT.Find(id);
            return View(gscmtoEdit);
        }
        [HttpPost]
        public ActionResult Edit(GSCMAccount gscm)
        {
            if (ModelState.IsValid)
            {
                DB.Entry(gscm).State = EntityState.Modified;
                DB.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        [ActionName("Delete")]
        public ActionResult DeleteGet(int id)
        {
            var gscmtodelete = DB._SYS_GSCMACCOUNT.Find(id);
            return View(gscmtodelete);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            var gscmtodelete = DB._SYS_GSCMACCOUNT.Find(id);
            DB._SYS_GSCMACCOUNT.Remove(gscmtodelete);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}