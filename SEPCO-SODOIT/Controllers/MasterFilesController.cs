﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.IO;
using SEPCO_SODOIT.Helpers;
using System.Data;
using SEPCO;

namespace SEPCO_SODOIT.Controllers
{
    public class MasterFilesController : Controller
    {
        int DefaultPageSize = 10;
        ApplicationDbContext DB = new ApplicationDbContext();
        // GET: TransactionFiles
        public ActionResult Index(int? page)
        {
            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            var OnePageOfItems = DB._SYS_Transactions.Where(W => W.FileType.ToUpper() == "MASTER FILE").OrderByDescending(O => O.ID);

            ViewBag.OnePageOfItems = OnePageOfItems.ToPagedList(pageNumber, DefaultPageSize); // will only contain 25 products max because of the pageSize
            return View();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportExcel(string TransactionFileList, HttpPostedFileBase upload)
        {
            var user = "Administrator";
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var FileType = TransactionFileList;
                    Stream stream = upload.InputStream;
                    DataTable breakDownDT = new DataTable();
                    switch (FileType)
                    {
                        case "DealerAndAccountMappingMF":
                            #region
                            try
                            {
                                for (int i = 1; i <= 2; i++)
                                {
                                    if (i == 1)
                                    {
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, i);
                                        DataColumn dc = new DataColumn("SoldToParty", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SoldToPartyName", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SiteGroupID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SiteGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Dealer", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("GSCMAccountID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("GSCMAccount", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP1ID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP1", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SimulationStatus", typeof(String));
                                        breakDownDT.Columns.Add(dc);

                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            DataRow row = breakDownDT.NewRow();
                                            var ctrCol = 0;
                                            foreach (DataColumn col in dt.Columns)
                                            {
                                                row[ctrCol] = item[col.Caption].ToString();
                                                ctrCol++;
                                            }
                                            breakDownDT.Rows.Add(row);
                                            ctr++;
                                        }
                                        if (CheckAndSave(breakDownDT, FileType, user) == -1)
                                        {
                                            //There is empty values
                                            ModelState.AddModelError("File", "Invalid file");
                                        }

                                    }
                                    else
                                    {
                                        FileType = "DealerAndAccountStoreMappingMF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, i);
                                        breakDownDT.Columns.Clear();
                                        breakDownDT.Clear();
                                        DataColumn dc = new DataColumn("SiteID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SiteName", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SiteGroupID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SiteGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Dealer", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("GSCMAccountID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("GSCMAccount", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP1ID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP1", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP2", typeof(String));
                                        breakDownDT.Columns.Add(dc);

                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            DataRow row = breakDownDT.NewRow();
                                            var ctrCol = 0;
                                            foreach (DataColumn col in dt.Columns)
                                            {
                                                row[ctrCol] = item[col.Caption].ToString();
                                                ctrCol++;
                                            }
                                            breakDownDT.Rows.Add(row);
                                            ctr++;
                                        }
                                        if (CheckAndSave(breakDownDT, FileType, user) == -1)
                                        {
                                            //There is empty values
                                            ModelState.AddModelError("File", "Invalid file");
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                //error
                            }

                            #endregion
                            break;
                        case "ModelAttributeMF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                DataColumn dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductType", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Model", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute1", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute2", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute3", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute4", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute5", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute6", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute7", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute8", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute9", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute10", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute11", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute12", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute13", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute14", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute15", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute16", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute17", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute18", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute19", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attribute20", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                var ctr = 0;
                                foreach (DataRow item in dt.Rows)
                                {
                                    DataRow row = breakDownDT.NewRow();
                                    var ctrCol = 0;
                                    foreach (DataColumn col in dt.Columns)
                                    {

                                        row[ctrCol] = item[col.Caption].ToString();
                                        ctrCol++;
                                    }
                                    breakDownDT.Rows.Add(row);
                                    ctr++;
                                }
                                if (CheckAndSave(breakDownDT, FileType, user) == -1)
                                {
                                    //There is empty values
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "ModelManagementMappingMF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                DataColumn dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductType", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Model", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ModelStatus", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ModelDescription", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ModelClassification", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                var ctr = 0;
                                foreach (DataRow item in dt.Rows)
                                {
                                    var ctrCol = 0;
                                    DataRow row = breakDownDT.NewRow();
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        row[ctrCol] = item[col.Caption].ToString();
                                        ctrCol++;
                                    }
                                    breakDownDT.Rows.Add(row);
                                    ctr++;
                                }
                                if (CheckAndSave(breakDownDT, FileType, user) == -1)
                                {
                                    //There is empty values
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "ProductGroupAttributesMF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                DataColumn dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Description", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AttributeColumn", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                var ctr = 0;
                                foreach (DataRow item in dt.Rows)
                                {
                                    var ctrCol = 0;
                                    DataRow row = breakDownDT.NewRow();
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        row[ctrCol] = item[col.Caption].ToString();
                                        ctrCol++;
                                    }
                                    breakDownDT.Rows.Add(row);
                                    ctr++;
                                }
                                if (CheckAndSave(breakDownDT, FileType, user) == -1)
                                {
                                    //There is empty values
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "StoreListAttributesMF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                DataColumn dc = new DataColumn("SiteID", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SiteName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Description", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AttributeColumn", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                var ctr = 0;
                                foreach (DataRow item in dt.Rows)
                                {
                                    var ctrCol = 0;
                                    DataRow row = breakDownDT.NewRow();
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        row[ctrCol] = item[col.Caption].ToString();
                                        ctrCol++;
                                    }
                                    breakDownDT.Rows.Add(row);
                                    ctr++;
                                }
                                if (CheckAndSave(breakDownDT, FileType, user) == -1)
                                {
                                    //There is empty values
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "WeekToMonthRatioMF":
                            #region
                            try
                            {
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "WOSTargetMF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                DataColumn dc = new DataColumn("AP1", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("GSCMAccount", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Dealer", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Value", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Average", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                foreach (DataRow item in dt.Rows)
                                {
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        if (col.Ordinal >= 6)//6th or Above 6th column are the weeks
                                        {
                                            DataRow row = breakDownDT.NewRow();
                                            row[0] = item[0].ToString();
                                            row[1] = item[1].ToString();
                                            row[2] = item[2].ToString();
                                            row[3] = item[3].ToString();
                                            row[6] = item[4];//Value
                                            row[4] = col.ColumnName;//Week
                                            row[5] = item[col.Ordinal];//Value
                                            breakDownDT.Rows.Add(row);
                                        }
                                    }
                                }
                                if (CheckAndSave(breakDownDT, FileType, user) == -1)
                                {
                                    //There is empty values
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "YearDateMappingMF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                DataColumn dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SellInYearMonth", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SellOutYearMonth", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("WeekMapping", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("MonthMapping", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("YearMapping", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                var ctr = 0;
                                foreach (DataRow item in dt.Rows)
                                {
                                    var ctrCol = 0;
                                    DataRow row = breakDownDT.NewRow();
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        row[ctrCol] = item[col.Caption].ToString();
                                        ctrCol++;
                                    }
                                    breakDownDT.Rows.Add(row);
                                    ctr++;
                                }
                                if (CheckAndSave(breakDownDT, FileType, user) == -1)
                                {
                                    //There is empty values
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                    }

                }
            }
            return RedirectToAction("Index");
        }
        public int CheckAndSave(DataTable breakDownDT, string FileType, string user)
        {
            int sCount = 0;
            //if (HelperClass.CheckDataTable(breakDownDT))//valid
            //{
            //populate the transaction code
            var tCode = HelperClass.TransactionFileID(FileType);
            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
            Col.DefaultValue = tCode;
            breakDownDT.Columns.Add(Col);
            Col = new DataColumn("User", System.Type.GetType("System.String"));
            Col.DefaultValue = user;
            breakDownDT.Columns.Add(Col);

            //save in sql
            sCount = DataAccessLayer.SaveData(breakDownDT, FileType, user);
            //}
            //else
            //{
            //    return -1;
            //}
            return sCount;
        }
        public ActionResult Delete(int TransID, string FileType)
        {
            switch (FileType)
            {
                case "DealerAndAccountStoreMappingMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_DealerAndAccountStoreMapping.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_DealerAndAccountStoreMapping.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //error
                    }

                    #endregion
                    break;
                case "DealerAndAccountMappingMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_DealerAndAccountMapping.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_DealerAndAccountMapping.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //error
                    }

                    #endregion
                    break;
                case "ModelAttributeMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_ModelAttributes.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_ModelAttributes.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "ModelManagementMappingMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_ModelManagementMapping.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_ModelManagementMapping.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "ProductGroupAttributesMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_ProductGroupAttributes.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_ProductGroupAttributes.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "StoreListAttributesMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_StoreListAttribute.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_StoreListAttribute.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "WeekToMonthRatioMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_WeekToMonthRatio.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_WeekToMonthRatio.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "WOSTargetMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_WOSTarget.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_WOSTarget.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "YearDateMappingMF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_YearDateMapping.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_YearDateMapping.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
            }
            var toRemoveTrans = DB._SYS_Transactions.ToList().Where(W => W.ID == TransID);
            DB._SYS_Transactions.RemoveRange(toRemoveTrans);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}