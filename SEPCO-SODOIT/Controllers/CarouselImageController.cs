﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPCO_SODOIT.Controllers
{
    public class CarouselImageController : Controller
    {
        ApplicationDbContext DB = new ApplicationDbContext();
        public ActionResult Index()
        {
            var list = DB._SYS_CAROUSELIMAGE.ToList();
            return View(list);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(CarouselImage img, HttpPostedFileBase file)
        {

            var allowedExtensions = new[] {  
            ".Jpg", ".png", ".jpg", "jpeg"  
                };
            //tbl.ID = Convert.ToInt32(fc["Id"]);

            img.Title = file.FileName;

            var fileName = Path.GetFileName(file.FileName);
            var ext = Path.GetExtension(file.FileName);
            if (allowedExtensions.Contains(ext))
            {
                string name = Path.GetFileNameWithoutExtension(fileName);
                string myfile = name + ext;
                //
                var path = Path.Combine(Server.MapPath("~/Images"), myfile);
                img.Path = path;
                DB._SYS_CAROUSELIMAGE.Add(img);
                DB.SaveChanges();
                file.SaveAs(path);
            }
            else
            {
                ViewBag.message = "Please choose only Image file";
            }

            return RedirectToAction("Index");


        }
        [HttpGet]
        [ActionName("Delete")]
        public ActionResult DeleteGet(int id)
        {
            var listtodelete = DB._SYS_CAROUSELIMAGE.Find(id);
            return View(listtodelete);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {

            var listtodelete = DB._SYS_CAROUSELIMAGE.Find(id);
            DB._SYS_CAROUSELIMAGE.Remove(listtodelete);

            string fPath1 = Server.MapPath("~/Images/" + listtodelete.Title);
            System.IO.File.Delete(fPath1);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
      
    }
}