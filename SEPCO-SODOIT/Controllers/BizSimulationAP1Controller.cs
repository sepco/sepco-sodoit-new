﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using SEPCO;
using SEPCO_SODOIT.Helpers;
using SEPCO_SODOIT.Models;
using SEPCO_SODOIT.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;

namespace SEPCO_SODOIT.Controllers
{
    public class BizSimulationAP1Controller : Controller
    {
        ApplicationDbContext DB = new ApplicationDbContext();
        List<string> Category = new List<string>();
        BizSimulationsLogic myLogic = new BizSimulationsLogic();
        //string ProductGroup = "", GSCMAccount = "";
        // GET: BizSimulationAP1
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult LoadSimulationData(string hiddenCategory,string pGroup, string pType, string model, DateTime? dateFrom, DateTime? dateTo, string account, string dealer, string columns)
        {
            Category.Add("Sell-In AP1");
            Category.Add("Sell-in(Act+Fcst)");
            Category.Add("Sell-in Balance");
            Category.Add("Sell-Out SP1");
            Category.Add("Sell-Out(Act+Fcst)");
            Category.Add("Sell-Out Balance");
            Category.Add("Inventory AP/SP1");
            Category.Add("Inventory Act+Fcst");
            Category.Add("Inventory Balance");
            Category.Add("WOS AP/SP1(F4)");
            Category.Add("WOS Act+Fcst(F4)");
            Category.Add("Run Rate");
            Category.Add("Flooring Target");
            Category.Add("Flooring Actual");
            Category.Add("Invoice Price");
            Category.Add("Invoice Revenue");

            List<ModelVM> data = new List<ModelVM>();
            DataTable dt = new DataTable();
            var getWeek = new List<String>();
            var allWeek = "";
            var getModelOmitZero = new List<String>();
            var toFilterModel = new List<String>();
            var allFilteredModel = "";
            DataAccessLayer sql = new DataAccessLayer();
            var getFirstCat = Category.FirstOrDefault(x => hiddenCategory.Contains(x));
            foreach (var col in columns.Split(','))
            {
                if (col != null)
                    dt.Columns.Add(col);
            }
            var weekFrom = int.Parse(dateFrom.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var weekTo = int.Parse(dateTo.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());

            for (var ctr = 0; weekFrom <= weekTo; ctr++)
            {
                var year = weekFrom.ToString().Substring(0, 4);
                var week = int.Parse(weekFrom.ToString().Substring(4, 2));
                var maxWeek = Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxWeek)
                {
                    weekFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxWeek).ToString().PadLeft(2, '0'));
                    dt.Columns.Add(weekFrom.ToString());
                    getWeek.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
                else
                {
                    dt.Columns.Add(weekFrom.ToString());
                    getWeek.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
            }
            // get the all week
            allWeek = string.Join(",", getWeek);
            //get the all Model except for model with EOL model status
            var getAllModel = DB._MF_ModelManagementMapping.Where(items => pGroup.Contains(items.ProductGroup) && (pType.Contains(items.ProductType) || pType == "") && (model.Contains(items.Model) || model == "") && items.ModelStatus != "EOL");
            //get the Model dont have 0 of total Values
            toFilterModel = getAllModel.Select(x => x.Model).AsEnumerable().Where(x =>
            {
                SqlParameter[] param = {
                                            new SqlParameter("@model",x),
                                            new SqlParameter("@allweek", allWeek),
                                            new SqlParameter("@account", account),
                                    };
                string TotalofAllCategegory = sql.getSQLStoredProcString("spu_AP1_GET_TotalValueofAllCategories", param);
                return double.Parse(TotalofAllCategegory.ToString()) >= 1;

            }).ToList();
            //get all Filtered Model
            allFilteredModel = string.Join(",", toFilterModel);

            List<ModelVM> ModelFiltered = new List<ModelVM>();

            //add Filtered Models and Add Total
            ModelFiltered = pType.Split(',').Select(x => new ModelVM { Model = "TOTAL", Type = x }).ToList();
            ModelFiltered.AddRange(
                 DB._MF_ModelManagementMapping.Where(m => (allFilteredModel.Contains(m.Model))).Select(x => new ModelVM
                 {
                     Type = x.ProductType,
                     Model = x.Model
                 }).OrderBy(or => or.Model));


            var hiddenCatList = hiddenCategory.Split(',');
            List<int> hiddenCatsForView = new List<int>();
            #region formulas
            List<object[]> list = new List<object[]>();
            var ctrRowNum = 0;
            var catOrdinal = columns.Split(',').ToList().FindIndex(F=>F == "Category"); 
            foreach (ModelVM item in ModelFiltered)
            {
                foreach (var cats in Category)
                {
                    ctrRowNum++;
                    DataRow dr = dt.NewRow();
                    var ctrCol = columns.Split(',').Length;
                    
                    //hide category
                    if (hiddenCatList.Where(W => W.ToUpper() == cats.ToUpper()).Count() == 0)
                    {
                        hiddenCatsForView.Add(ctrRowNum - 1);
                    }

                    switch (cats)
                    {
                        case "Sell-In AP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AP1Forecast.Where(myItem => myItem.GSCMAccount == account && allFilteredModel.Contains(myItem.Item) && myItem.Week == cols.ColumnName)
                                            .Select(val => val.Value);
                                        dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");

                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AP1Forecast
                                                    where t.GSCMAccount.ToLower() == account.ToLower() && t.Item == item.Model && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                   ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Sell-in(Act+Fcst)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var catAlpha = HelperClass.IntToLetters(catOrdinal);
                                        dr[cols.Ordinal] = "0";
                                       
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Account == account && t.Item == item.Model && t.Category.ToLower() == "Sell-in(AP1)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    } 
                                    ctrCol++;

                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Sell-in Balance":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 2) + "-" + colAlpha + (ctrRowNum - 1);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Sell-Out SP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => allFilteredModel.Contains(myItem.Item) && myItem.Week == cols.ColumnName && myItem.Category == "Sell-out FCST(AP1/CON)")
                                           .Select(val => val.Value);
                                        dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Account == account && t.Item == item.Model && t.Category.ToLower() == "Sell-out FCST(AP1/CON)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Sell-Out(Act+Fcst)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        dr[cols.Ordinal] = "0";
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Account == account && t.Item == item.Model && t.Category.ToLower() == "Sell-out FCST(AP1)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Sell-Out Balance":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 2) + "-" + colAlpha + (ctrRowNum - 1);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Inventory AP/SP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => allFilteredModel.Contains(myItem.Item) && myItem.Week == cols.ColumnName && myItem.Category == "EOH_EX(AP1)")
                                           .Select(val => val.Value);
                                        dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Account == account && t.Item == item.Model && t.Category.ToLower() == "EOH_EX(AP1)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Inventory Act+Fcst":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => allFilteredModel.Contains(myItem.Item) && myItem.Week == cols.ColumnName && myItem.Category == "EOH_EX(AP1)")
                                           .Select(val => val.Value);
                                        dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Account == account && t.Item == item.Model && t.Category.ToLower() == "EOH_EX(AP1)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Inventory Balance":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 2) + "-" + colAlpha + (ctrRowNum - 1);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "WOS AP/SP1(F4)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    int colsOrdinal = cols.Ordinal + 1;
                                    int SellOutRow = ctrRowNum - 6;
                                    int InventoryRow = ctrRowNum - 3;

                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        dr[ctrCol] = myLogic.getWosFormulaTotal(allFilteredModel, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP1/CON)");
                                    }
                                    else
                                    {
                                        dr[ctrCol] = myLogic.getWosFormula(item.Model, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP1/CON)");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "WOS Act+Fcst(F4)":

                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {

                                    if (cols.Ordinal + 1 > ctrCol)
                                    {
                                        int colsOrdinal = cols.Ordinal + 1;
                                        int SellOutRow = ctrRowNum - 6;
                                        int InventoryRow = ctrRowNum - 3;

                                        if (item.Model.ToUpper() == "TOTAL")
                                        {

                                            dr[ctrCol] = myLogic.getWosFormulaTotal(allFilteredModel, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP1)");
                                        }
                                        else
                                        {
                                            dr[ctrCol] = myLogic.getWosFormula(item.Model, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP1)");
                                        }
                                        ctrCol++;
                                    }
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Run Rate":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 7) + "/" + colAlpha + (ctrRowNum + 2);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Flooring Target":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_FlooringTarget.Where(myItem => allFilteredModel.Contains(myItem.Model) && myItem.Week == cols.ColumnName)
                                              .Select(val => val.Value);
                                        dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_FlooringTarget
                                                    where t.Account == account && t.Model == item.Model
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");

                                    } 
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Flooring Actual":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var display = DB._TF_FlooringSite.Where(myItem => allFilteredModel.Contains(myItem.Model) && myItem.Week == cols.ColumnName)
                                              .Select(dis => dis.Display);
                                        dr[cols.Ordinal] = display.Count() == 0 ? "0" : display.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_FlooringSite
                                                    where t.Dealer == account && t.Model == item.Model
                                                    && t.Week == cols.ColumnName
                                                    select t.Display;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Invoice Price":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_GrossNetPrice.Where(myItem => allFilteredModel.Contains(myItem.Model) && myItem.GSCMAccount == account)
                                             .Select(val => val.GrossPrice);
                                        dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_GrossNetPrice
                                                    where t.GSCMAccount == account && t.Model == item.Model
                                                    select t.GrossPrice;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Average().ToString();
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Invoice Revenue":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    var formula = colAlpha + (ctrRowNum - 1) + "*" + colAlpha + (ctrRowNum - 14);
                                    dr[ctrCol] = "=IF(" + formula + "=0," + '"' + "-" + '"' + "," + formula + ")";
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                    }
                    dt.Rows.Add(dr);
                    list.Add(dr.ItemArray);
                }
            }
            #endregion
            List<string> columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToList();
            List<HeadersVM> colsFormat = new List<HeadersVM>();
            foreach (DataColumn colHead in dt.Columns)
            {
                if (colHead.Caption != "Type" && colHead.Caption != "Model" && colHead.Caption != "Category")
                    colsFormat.Add(new HeadersVM
                    {
                        type = "numeric",
                        data = colHead.ColumnName
                    });
                else
                    colsFormat.Add(new HeadersVM
                    {
                        type = "text",
                        data = colHead.ColumnName
                    });
            }

            DataVM dataVM = new DataVM();
            dataVM.FirstCategory = getFirstCat.ToString();
            dataVM.Data = list;
            dataVM.Count = ModelFiltered.Count();
            dataVM.Headers = columnNames;
            dataVM.HiddenRows = hiddenCatsForView;
            dataVM.ColsFormat = colsFormat;

            return Json(dataVM, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadShortSimulationData(string hiddenCategory, string pGroup, string pType, string model, DateTime? dateFrom, DateTime? dateTo, string account, string dealer, string columns)
        {
            Category.Clear();
            Category.Add("Sell-in(Act+Fcst)");
            Category.Add("Sell-Out(Act+Fcst)");
            Category.Add("Inventory Act+Fcst");
            Category.Add("WOS(F4)");
            Category.Add("Run Rate");
            Category.Add("Flooring Target");
            Category.Add("Flooring Actual");
            Category.Add("Invoice Price");
            Category.Add("Invoice Revenue");

            List<ModelVM> data = new List<ModelVM>();
            DataTable dt = new DataTable();

            var getWeek = new List<String>();
            var allWeek = "";
            var getModelOmitZero = new List<String>();
            var toFilterModel = new List<String>();
            var allFilteredModel = "";
            var getFirstCat = Category.FirstOrDefault(x => hiddenCategory.Contains(x));
            DataAccessLayer sql = new DataAccessLayer();

            var hiddenCatList = hiddenCategory.Split(',');
            List<int> hiddenCatsForView = new List<int>();
            foreach (var col in columns.Split(','))
            {
                if (col != null)
                    dt.Columns.Add(col);
            }
            var weekFrom = int.Parse(dateFrom.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var weekTo = int.Parse(dateTo.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());

            for (var ctr = 0; weekFrom <= weekTo; ctr++)
            {
                var year = weekFrom.ToString().Substring(0, 4);
                var week = int.Parse(weekFrom.ToString().Substring(4, 2));
                var maxWeek = Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxWeek)
                {
                    weekFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxWeek).ToString().PadLeft(2, '0'));
                    dt.Columns.Add(weekFrom.ToString());
                    getWeek.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
                else
                {
                    dt.Columns.Add(weekFrom.ToString());
                    getWeek.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
            }
            // get the all week
            allWeek = string.Join(",", getWeek);

            //get the all Model except for model with EOL model status
            var getAllModel = DB._MF_ModelManagementMapping.Where(items => pGroup.Contains(items.ProductGroup) && (pType.Contains(items.ProductType) || pType == "") && (model.Contains(items.Model) || model == "") && items.ModelStatus != "EOL");

            //get the Model dont have 0 of total Values
            toFilterModel = getAllModel.Select(x => x.Model).AsEnumerable().Where(x =>
            {
                SqlParameter[] param = {
                                            new SqlParameter("@model",x),
                                            new SqlParameter("@allweek", allWeek),
                                            new SqlParameter("@dealer", dealer)
                                    };
                string TotalofAllCategegory = sql.getSQLStoredProcString("spu_AP1Short_GET_TotalValueofAllCategories", param);
                return double.Parse(TotalofAllCategegory.ToString()) >= 1;

            }).ToList();
            //get all Filtered Model
            allFilteredModel = string.Join(",", toFilterModel);

            List<ModelVM> ModelFiltered = new List<ModelVM>();

            //add Filtered Models and Add Total
            ModelFiltered = pType.Split(',').Select(x => new ModelVM { Model = "TOTAL", Type = x }).ToList();
            ModelFiltered.AddRange(
                 DB._MF_ModelManagementMapping.Where(m => (allFilteredModel.Contains(m.Model))).Select(x => new ModelVM
                 {
                     Type = x.ProductType,
                     Model = x.Model
                 }).OrderBy(or => or.Model));

            #region formulas
            List<object[]> list = new List<object[]>();
            var ctrRowNum = 0;
            foreach (ModelVM item in ModelFiltered)
            {
                foreach (var cats in Category)
                {
                    ctrRowNum++;
                    DataRow dr = dt.NewRow();
                    var ctrCol = columns.Split(',').Length;

                    if (hiddenCatList.Where(W => W.ToUpper() == cats.ToUpper()).Count() == 0)
                    {
                        hiddenCatsForView.Add(ctrRowNum - 1);
                    }

                    switch (cats)
                    {

                        case "Sell-in(Act+Fcst)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {

                                        dr[ctrCol] = "0";
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_SellInGI
                                                    join d in DB._MF_DealerAndAccountMapping
                                                    on t.SoldToPartyCode equals d.SoldToParty
                                                    where t.Model == item.Model && d.Dealer == dealer
                                                    && t.SalesWeek == cols.ColumnName
                                                    select t.SaleQty;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0"); 
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Sell-Out(Act+Fcst)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        dr[ctrCol] = "0";
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_SellOutAndInventoryWeekly
                                                    where t.Model == item.Model
                                                    && t.Week == cols.ColumnName && t.ChannelGroup == dealer
                                                    select t.Sales;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Inventory Act+Fcst":
                            bool isFirst = true;
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters((cols.Ordinal ));
                                    var colAlpha1 = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    if(isFirst)
                                        dr[ctrCol] = "0";
                                    else
                                        dr[ctrCol] = "=" + colAlpha + (ctrRowNum) + "+" + colAlpha1 + (ctrRowNum - 2) + "-" + colAlpha1 + (ctrRowNum - 1);
                                    ctrCol++;
                                    isFirst = false;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "WOS(F4)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    int colsOrdinal = cols.Ordinal + 1;
                                    int SellOutRow = ctrRowNum - 2;
                                    int InventoryRow = ctrRowNum - 1;

                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        dr[ctrCol] = myLogic.getWosFormulaTotalforAP1Short(dealer, allFilteredModel, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow);
                                    }
                                    else
                                    {
                                        dr[ctrCol] = myLogic.getWosFormulaforAP1Short(dealer, item.Model, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow);
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                             
                            }
                            break;
                        case "Run Rate":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum + 2) + "/" + colAlpha + (ctrRowNum - 3);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Flooring Target":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    dr[ctrCol] = " ";
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;

                        case "Flooring Actual":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = from t in DB._TF_FlooringSite
                                                    where allFilteredModel.Contains(t.Model) && t.Dealer == dealer
                                                    && t.Week == cols.ColumnName
                                                    select t.Display;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_FlooringSite
                                                    where t.Model == item.Model && t.Dealer == dealer
                                                    && t.Week == cols.ColumnName
                                                    select t.Display;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;

                        case "Invoice Price":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {

                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = from t in DB._TF_GrossNetPrice
                                                    where t.Dealer == dealer && allFilteredModel.Contains(t.Model)
                                                    select t.GrossPrice;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Average().ToString();
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_GrossNetPrice
                                                    where t.Dealer == dealer && t.Model == item.Model
                                                    select t.GrossPrice;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Average().ToString();
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Invoice Revenue":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    var formula = colAlpha + (ctrRowNum - 1) + "*" + colAlpha + (ctrRowNum - 14);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 1) + "*" + colAlpha + (ctrRowNum - 8);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;

                    }

                    dt.Rows.Add(dr);
                    list.Add(dr.ItemArray);
                }
            }
            #endregion
            List<string> columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToList();
            List<HeadersVM> colsFormat = new List<HeadersVM>();
            foreach (DataColumn colHead in dt.Columns)
            {
                if (colHead.Caption != "Type" && colHead.Caption != "Model" && colHead.Caption != "Category")
                    colsFormat.Add(new HeadersVM
                    {
                        type = "numeric",
                        data = colHead.ColumnName
                    });
                else
                    colsFormat.Add(new HeadersVM
                    {
                        type = "text",
                        data = colHead.ColumnName
                    });
            }

            DataVM dataVM = new DataVM();
            dataVM.FirstCategory = getFirstCat.ToString();
            dataVM.Data = list;
            dataVM.Count = ModelFiltered.Count();
            dataVM.Headers = columnNames;
            dataVM.HiddenRows = hiddenCatsForView;
            dataVM.ColsFormat = colsFormat;

            return Json(dataVM, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadFilters()
        {

            return PartialView("_LoadFilters");
        }

        public JsonResult GetProductGroup()
        {
            var q = from pGroup in DB._ProductGroup
                    orderby pGroup.GroupName ascending
                    select pGroup.GroupName;
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGSCMAccounts()
        {
            var q = (from pGroup in DB._MF_DealerAndAccountMapping
                     where pGroup.SimulationStatus.ToUpper() == "YES"
                     orderby pGroup.GSCMAccount
                     select new GSCMAccountVM { GSCMAccount = pGroup.GSCMAccount, AP1 = pGroup.AP1 }).Distinct().GroupBy(G => G.AP1);
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductGroupAttributes(string pGroup)
        {
            var q = from attri in DB._MF_ProductGroupAttributes
                    where attri.ProductGroup == pGroup
                    select new
                    {
                        Description = attri.Description,
                        Attribute = attri.AttributeColumn
                    };
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductTypeList(string pGroup)
        {
            var prodType = from pt in DB._ProductType
                           where pGroup.Contains(pt.ProductGroup)
                           select new
                           {
                               Type = pt.ProductType
                           };

            return Json(prodType, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDealersByAccount(string account)
        {
            var q = from d in DB._MF_DealerAndAccountMapping
                    where d.GSCMAccount == account
                    orderby d.Dealer ascending
                    select new
                    {
                        Dealer = d.Dealer,
                        ID = d.ID
                    };
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetModelListByType(string pType)
        {
            var modelList = (from m in DB._MF_ModelManagementMapping
                             where m.ProductType == pType
                             orderby m.Model ascending
                             select new
                             {
                                 Model = m.Model,
                                 ID = m.ID
                             });

            return Json(modelList, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public string GetAP1Version(string account, string dealer)
        {
            dealer = String.IsNullOrEmpty(dealer) ? null : dealer;
            var version = from a in DB._SYS_AP1BizVersionLookup
                          where a.GSCMAccount == account && a.Dealer == dealer
                          select a.AP1Version;

            return version.FirstOrDefault();
        }
        private void SetNonWeekColumns(DataColumn cols, DataRow dr, ModelVM item, string cats, string pGroup)
        {
            switch (cols.ColumnName)
            {
                case "Type":
                    dr[cols.Ordinal] = item.Type;
                    break;
                case "Item":
                    dr[cols.Ordinal] = item.Model;
                    break;
                case "Category":
                    dr[cols.Ordinal] = cats;
                    break;
                default:

                    var q1 = from pAttri in DB._MF_ProductGroupAttributes
                             where pAttri.ProductGroup == pGroup && pAttri.Description == cols.Caption
                             select pAttri.AttributeColumn;
                    DataAccessLayer sql = new DataAccessLayer();
                    string cmd = "SELECT " + q1.FirstOrDefault() + " FROM MF_ModelAttributes WHERE Model=@model AND ProductGroup=@group";
                    SqlParameter[] param = {
                                                new SqlParameter("@model",item.Model),
                                                new SqlParameter("@group",pGroup)
                                                   };
                    var result = sql.getSQLDataReader(cmd, param);
                    dr[cols.Ordinal] = result;

                    break;
            }
        }
        public bool IsEditable(int yearWeek)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());
            if (currentWeek <= yearWeek)
                return true;
            else
                return false;
        }
        public bool IsEditableJS(int yearWeek, string category)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());

            if (currentWeek == yearWeek && category == "Sell-in(Act+Fcst)")
                return false;

            if (currentWeek <= yearWeek)
                return true;
            else
                return false;
        }
        public bool IsEditableFuture(int yearWeek)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());
            if (currentWeek < yearWeek)
                return true;
            else
                return false;
        }
        public List<string> GetListOfMonths(DateTime? dateFrom, DateTime? dateTo)
        {
            List<string> listOfMonth = new List<string>();
            //month
            var monthFrom = int.Parse((dateFrom.Value.Year.ToString()) + ((dateFrom.Value.Month).ToString().PadLeft(2, '0'))); //;+ Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var monthTo = int.Parse((dateTo.Value.Year.ToString()) + ((dateTo.Value.Month).ToString().PadLeft(2, '0'))); //;+ Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());
            for (var ctr = 0; monthFrom <= monthTo; ctr++)
            {
                var year = monthFrom.ToString().Substring(0, 4);
                var week = int.Parse(monthFrom.ToString().Substring(4, 2));
                var maxMonth = 12;//Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxMonth)
                {
                    monthFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxMonth + 1).ToString().PadLeft(2, '0'));
                    listOfMonth.Add(monthFrom.ToString());
                }
                else
                {
                    listOfMonth.Add(monthFrom.ToString());
                    monthFrom += 1;
                }
            }


            return listOfMonth;

        }
        public ActionResult ExportSellIn(string ProductGroup, string GSCMAccount, string Dealer, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            var dtSellInActFcst = new DataTable();
            dtSellInActFcst.Columns.Add("Product Grp");
            dtSellInActFcst.Columns.Add("AP1");
            dtSellInActFcst.Columns.Add("Account");
            dtSellInActFcst.Columns.Add("Item");
            dtSellInActFcst.Columns.Add("Category");
            var getAP1 = DB._MF_DealerAndAccountMapping.FirstOrDefault(myItem => myItem.GSCMAccount == GSCMAccount);
            string AP1Val = getAP1.AP1;
            //get the index of future weeks
            var startOfEditableCells = 0;
            var headCtr = 0;
            var colHeaders = columns.Split(',').ToList();
            bool alreadAdded = false;
            foreach (var header in colHeaders)
            {
                int week;
                if(int.TryParse(header, out week))
                {
                    if (IsEditableFuture(week))
                    {
                        if(startOfEditableCells == 0)
                            startOfEditableCells = headCtr;

                        //add month total header
                        var monthOfCurrentWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)));
                        var monthOfNextWeek = "";
                        if(int.Parse(week.ToString().Substring(4, 2)) == 52)
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)) + 1, 1);
                        else
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)) + 1);

                        dtSellInActFcst.Columns.Add(week.ToString());

                        if (monthOfCurrentWeek != monthOfNextWeek)
                        {
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                            alreadAdded = true;
                        }else
                        {
                            alreadAdded = false;
                        }

                        
                        if((colHeaders.Count - 1) == headCtr && !alreadAdded)
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                    }
                }
                headCtr++;
            }
            //get the sell in (act + fcst)
            if(startOfEditableCells != 0)
            {
                var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    if ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")] == "Sell-in(Act+Fcst)" && (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")] != "TOTAL")
                    {
                        DataRow dr = dtSellInActFcst.NewRow();
                        dr[0] = ProductGroup;
                        dr[1] = AP1Val;
                        dr[2] = GSCMAccount;
                        dr[3] = (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                        dr[4] = "AP1 FCST";

                        var ctr = 0;
                        var ctr1 = 0;
                        var ctr2 = 0;
                        var totalInMonth = 0M;
                        var alreadAddedValue =  false;
                        foreach (var cols in item)
                        {
                            if (ctr1 >= startOfEditableCells)
                            {
                                //total in each month
                                var monthOfCurrentWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)));
                                var monthOfNextWeek = "";
                                if (int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) == 52)
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)) + 1, 1);
                                else
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) + 1);

                                dr[5 + ctr] = item[startOfEditableCells + ctr2];

                                ctr++;
                                
                                //if same month, store sum
                                if (monthOfCurrentWeek == monthOfNextWeek)
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    alreadAddedValue = false;
                                }
                                else//if not same month, put the value and increment the ctr to match the week column
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    dr[5 + ctr] = totalInMonth;
                                    ctr++;
                                    alreadAddedValue = true;
                                    totalInMonth = 0;
                                }


                                //if last item but not last week of month
                                if((item.Count - 1) == ctr1 && !alreadAddedValue)
                                    dr[5 + ctr] = totalInMonth;
                                ctr2++;
                            }
                            ctr1++;
                        }
                        dtSellInActFcst.Rows.Add(dr);
                    }
                }
            }
            
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtSellInActFcst, "Export Button - Sell-In");
                var filename = "ap1simulation_sell_in" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }

                }
                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "AP1_Sell-In"
                });
                DB.SaveChanges();

                return new JsonResult()
                {
                    Data = filename,
                };

            }

        }
        public ActionResult DeleteFile(string filename){
            try
            {
                DB._SYS_SimulationTrx.Remove(DB._SYS_SimulationTrx.Where(W => W.FileLocation == filename).FirstOrDefault());
                DB.SaveChanges();
                System.IO.File.Delete(Server.MapPath("~/Content/SimulationFiles/" + filename));
                return new JsonResult()
                {
                    Data = "Success"
                };
            }
            catch (Exception c)
            {
                return new JsonResult()
                {
                    Data = "Failed"
                };
            }
            
        }
        public ActionResult ExportSellOut(string ProductGroup, string GSCMAccount, string Dealer, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            var dtSellInActFcst = new DataTable();
            dtSellInActFcst.Columns.Add("Product Grp");
            dtSellInActFcst.Columns.Add("AP1");
            dtSellInActFcst.Columns.Add("Account");
            dtSellInActFcst.Columns.Add("Item");
            dtSellInActFcst.Columns.Add("Category");
            var getAP1 = DB._MF_DealerAndAccountMapping.FirstOrDefault(myItem => myItem.GSCMAccount == GSCMAccount);
            string AP1Val = getAP1.AP1;

            //get the index of future weeks
            var startOfEditableCells = 0;
            var headCtr = 0;
            var colHeaders = columns.Split(',').ToList();
            bool alreadAdded = false;
            foreach (var header in colHeaders)
            {
                int week;
                if (int.TryParse(header, out week))
                {
                    if (IsEditable(week))
                    {
                        if (startOfEditableCells == 0)
                            startOfEditableCells = headCtr;

                        //add month total header
                        var monthOfCurrentWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)));
                        var monthOfNextWeek = "";
                        if (int.Parse(week.ToString().Substring(4, 2)) == 52)
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)) + 1, 1);
                        else
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)) + 1);

                        dtSellInActFcst.Columns.Add(week.ToString());

                        if (monthOfCurrentWeek != monthOfNextWeek)
                        {
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                            alreadAdded = true;
                        }
                        else
                        {
                            alreadAdded = false;
                        }


                        if ((colHeaders.Count - 1) == headCtr && !alreadAdded)
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                    }
                }
                headCtr++;
            }
            //get the sell out (act + fcst)
            if (startOfEditableCells != 0)
            {
                var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    if ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")] == "Sell-Out(Act+Fcst)" && (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")] != "TOTAL")
                    {
                        DataRow dr = dtSellInActFcst.NewRow();
                        dr[0] = ProductGroup;
                        dr[1] = AP1Val;
                        dr[2] = GSCMAccount;
                        dr[3] = (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                        dr[4] = "Sell-out FCST(AP1)";

                        var ctr = 0;
                        var ctr1 = 0;
                        var ctr2 = 0;
                        var totalInMonth = 0M;
                        var alreadAddedValue = false;
                        foreach (var cols in item)
                        {
                            if (ctr1 >= startOfEditableCells)
                            {
                                //total in each month
                                var monthOfCurrentWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)));
                                var monthOfNextWeek = "";
                                if (int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) == 52)
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)) + 1, 1);
                                else
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) + 1);

                                dr[5 + ctr] = item[startOfEditableCells + ctr2];

                                ctr++;

                                //if same month, store sum
                                if (monthOfCurrentWeek == monthOfNextWeek)
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    alreadAddedValue = false;
                                }
                                else//if not same month, put the value and increment the ctr to match the week column
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    dr[5 + ctr] = totalInMonth;
                                    ctr++;
                                    alreadAddedValue = true;
                                    totalInMonth = 0;
                                }


                                //if last item but not last week of month
                                if ((item.Count - 1) == ctr1 && !alreadAddedValue)
                                    dr[5 + ctr] = totalInMonth;
                                ctr2++;
                            }
                            ctr1++;
                        }
                        dtSellInActFcst.Rows.Add(dr);
                    }
                }
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtSellInActFcst, "Export Button - Sell-Out");
                var filename = "ap1simulation_sell_out" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }

                }
                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "AP1_Sell-Out"
                });
                DB.SaveChanges();

                return new JsonResult()
                {
                    Data = filename
                };

            }

        }
        public ActionResult ExportSimulation(string ProductGroup, string GSCMAccount, string Dealer, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            List<string> listOfWeeks = new List<string>();
            //weeks
            var weekFrom = int.Parse(dateFrom.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var weekTo = int.Parse(dateTo.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());
            for (var ctr = 0; weekFrom <= weekTo; ctr++)
            {
                var year = weekFrom.ToString().Substring(0, 4);
                var week = int.Parse(weekFrom.ToString().Substring(4, 2));
                var maxWeek = Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxWeek)
                {
                    weekFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxWeek).ToString().PadLeft(2, '0'));
                    listOfWeeks.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
                else
                {
                    listOfWeeks.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
            }



            var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
            var colHeaders = columns.Split(',').ToList();
            DataTable dtSimulationData = new DataTable();
            List<int> notIncludedCols = new List<int>();
            var ctrCols = 0;
            foreach (var item in colHeaders)
            {
                if (item.ToLower() != "type" && item.ToLower() != "item" && item.ToLower() != "category" && !item.ToLower().Contains("20"))
                    notIncludedCols.Add(ctrCols);
                else
                    dtSimulationData.Columns.Add(item);

                ctrCols++;
            }

            //var data = System.Web.Helpers.Json.Decode(simulationData);

            DataTable dtAPIForecast = GetExportableData(ProductGroup, GSCMAccount, listOfWeeks, "spu_getAP1ForecastTF", "Item");
            DataTable dtChannelPSI = GetExportableData_Keys(ProductGroup, GSCMAccount, listOfWeeks, "spu_GET_ChannelPSITF", "Item", "Category");

            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtFlooringSite = sql.getDTSQLStoredProc("spu_GET_FlooringSiteTF", param);
            //DataTable dtFlooringTarget = GetExportableData(ProductGroup, GSCMAccount, listOfWeeks, "spu_GET_FlooringTargetTF", "Model");
            DataTable dtFlooringTarget = GetExportableData_FT(ProductGroup, listOfWeeks, "spu_ap2_GET_FlooringTargetTF", "Store Name", "Dealer", "GSCM Account", "AP1", "Product Group", "Model");

            SqlParameter[] param1 = {
                new SqlParameter("@account",GSCMAccount),
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtGrossNetPrice = sql.getDTSQLStoredProc("spu_GET_GrossNetPriceTF", param1);

            var listOfMonths = GetListOfMonths(dateFrom, dateTo);
            SqlParameter[] paramSellInSellOutA = {
                new SqlParameter("@weeks",string.Join(",",listOfMonths)),
                new SqlParameter("@account",GSCMAccount),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtSellInOutTargetQ = GetExportableData_Keys(ProductGroup, GSCMAccount, listOfWeeks, "spu_GET_SellInOutTargetQTF", "Dealer", "Category");// sql.getDTSQLStoredProc("spu_GET_SellInOutTargetQTF", paramSellInSellOutQ);
            DataTable dtSellInOutTargetA = sql.getDTSQLStoredProc("spu_GET_SellInOutTargetATF", paramSellInSellOutA);//GetExportableDataForAmount(ProductGroup, GSCMAccount, listOfMonths, "spu_GET_SellInOutTargetATF","Dealer");//
            DataTable dtWOSTarget = GetExportableData(ProductGroup, GSCMAccount, listOfWeeks, "spu_GET_WOSTargetMF", "Product Group");
            DataTable dtTIM = GetExportableData_Keys(ProductGroup, GSCMAccount, listOfMonths, "spu_GET_TIMTF", "GSCM Account", "Category");

            using (XLWorkbook wb = new XLWorkbook())
            {
                // Add a DataTable as a worksheet
                var ws = wb.Worksheets.Add("Simulation File");
                #region BuildSimulationSheet

                #region firstbox
                // Merge a range
                ws.Cell("B3").Value = "AP1";
                ws.Range("B3:C4").Merge();

                ws.Cell("D3").Value = "MM1";
                ws.Range("D3:E4").Merge();

                // Merge a row
                ws.Cell("B5").Value = "CHANNEL";
                ws.Range("B5:C5").Row(1).Merge();

                ws.Cell("D5").Value = GSCMAccount.ToUpper();
                ws.Range("D5:E5").Row(1).Merge();

                // Merge a row
                ws.Cell("B6").Value = "PRODUCT";
                ws.Range("B6:C6").Row(1).Merge();

                ws.Cell("D6").Value = ProductGroup.ToUpper();
                ws.Range("D6:E6").Row(1).Merge();

                // Merge a range
                ws.Cell("B7").Value = "PIC";
                ws.Range("B7:C9").Merge();

                // Merge a range
                ws.Cell("B10").Value = "PROFILE";
                ws.Range("B10:C14").Merge();

                ws.Cell("B15").Value = "ISS";
                ws.Range("B15:C15").Row(1).Merge();

                ws.Range("B3:E15").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("B3:E15").Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("B3:E15").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("B3:E15").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                #endregion
                #region secondbox
                ws.Cell("B17").Value = "DA TOTAL";//todo
                ws.Range("B17:B18").Column(1).Merge();
                ws.Cell("B17").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("B17:B18").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;


                ws.Cell("B19").Value = "KPI (SCM)";
                ws.Range("B19:B21").Column(1).Merge();
                ws.Range("B19:B21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C19:C21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D19:D21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E19:E21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("B22").Value = "Flooring";
                ws.Range("B22:B24").Column(1).Merge();
                ws.Range("B22:B24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C22:C24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D22:D24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E22:E24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("B25").Value = "Revenue";
                ws.Range("B25:B34").Column(1).Merge();
                ws.Range("B25:B34").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C25:C31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D25:D31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E25:E31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C32:C34").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D32:D34").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E32:E34").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;


                ws.Cell("C17").Value = ProductGroup;
                ws.Range("C17:E18").Merge();
                ws.Cell("C17").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("C17:E18").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("D22").Value = "TOTAL";
                ws.Range("D22:D24").Column(1).Merge();

                var qModelMix = from PT in DB._ProductType
                                join PG in DB._ProductGroup
                                on PT.ProductGroup equals PG.GroupName
                                where PG.GroupName == ProductGroup
                                select PT;
                var typeCount = qModelMix.Count();

                //after model minx starting row
                //for sell-out target achievement
                var afterModelMixRow = 34 + (typeCount * 4) + 1;
                //simulation starting row
                var startingRow = 34 + (typeCount * 4) + 25;
                //total row count of items in simulation
                var simulationCount = result.Count;
                //simulation ending row
                var endRow = startingRow + simulationCount;

                //get the months
                //set month header
                //generate & set formula for the monthly basis computation
                var ctrM = 0;
                var ctrMonth = 6;
                var monthList = GetListOfMonths(dateFrom, dateTo);
                foreach (var month in monthList)
                {
                    //compute the range week of month
                    //this is used to determine which cell will be the scope of each month
                    var rangeDif = HelperClass.GetWeeksInMonthCount(month,ctrM,dateFrom,dateTo,monthList);
                    string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                    var letter = HelperClass.IntToLetters(ctrMonth);
                    var letterTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));
                    //set month header styles
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "17").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "17").Value = monthName;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "17:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "17").Merge();
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "17:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "19:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "25:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "32:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "34").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                    #region revenue by month
                    //sp target
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "25").FormulaA1 = "=SUMIFS('Sell-In & Sell-Out Target_A'!$F:$F,'Sell-In & Sell-Out Target_A'!$B:$B,'Simulation File'!$D$5,'Sell-In & Sell-Out Target_A'!$D:$D," + '"' + "SP Target - CY" + '"' + ",'Sell-In & Sell-Out Target_A'!$E:$E," + month +")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "25:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "25").Merge();

                    //ly
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "26").FormulaA1 = "=SUMIFS('Sell-In & Sell-Out Target_A'!$F:$F,'Sell-In & Sell-Out Target_A'!$B:$B,'Simulation File'!$D$5,'Sell-In & Sell-Out Target_A'!$D:$D," + '"' + "SP Target - LY" + '"' + ",'Sell-In & Sell-Out Target_A'!$E:$E," + month + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "26:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "26").Merge();

                    //act+ap1

                    var grossPrices = "";
                    for (int i = 1; i <= simulationCount / 16; i++)
                    {
                        grossPrices += HelperClass.IntToLetters(ctrMonth) + (startingRow + (i * 16)) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + (startingRow + (i * 16)) + ",";
                    }
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "27").FormulaA1 = "=SUM(" + grossPrices.Remove(grossPrices.Length - 1) + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "27:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "27").Merge();

                    //actual + simulation
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "28").FormulaA1 = "=SUM(" + letter + "29:" + letterTo  + "29)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "28:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "28").Merge();

                    //target vs actial
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "30").FormulaA1 = "=IFERROR(" + letter + "27/" + letter + "25," + '"' + '"' + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "30:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "30").Merge();

                    //ly vs actual
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "31").FormulaA1 = "=IFERROR("  + letter + "27/"  + letter + "26," + '"' + '"' + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "31:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "31").Merge();

                    //sell-out target
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "32").FormulaA1 = "=SUM(" + letter + (afterModelMixRow + 4) + ":" + letterTo + (afterModelMixRow + 4) + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "32:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "32").Merge();

                    //sell-out target achievement
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "33").FormulaA1 = "=IFERROR(SUM(" + letter + afterModelMixRow + ":" + letterTo + afterModelMixRow + ")/F32," + '"' + '"' + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "33:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "33").Merge();

                    //act+fcst asp
                    var f = "=" + letter + "27/SUM(" + letter + (afterModelMixRow + 1) + "," + letter + (afterModelMixRow + 1) + ":" + letterTo + (afterModelMixRow + 1) + ")";
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "34").FormulaA1 = f;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "34:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "34").Merge();
                    #endregion

                    //increment
                    ctrM++;
                    ctrMonth += rangeDif + 1;
                }


                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                var ctr = 6;
                var index = 3;
                foreach (var week in listOfWeeks)
                {
                    //set the week
                    //set the styles
                    ws.Cell(HelperClass.IntToLetters(ctr) + "18").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Cell(HelperClass.IntToLetters(ctr) + "18").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                    ws.Cell(HelperClass.IntToLetters(ctr) + "18").Value = week;

                    var letters = HelperClass.IntToLetters(index);
                    var kpiLetter = HelperClass.IntToLetters(index + 1);
                    var letter1 = HelperClass.IntToLetters(ctr);
                    //AP1 (sell-in) FA (51%)
                    ws.Cell(HelperClass.IntToLetters(ctr) + "20").FormulaA1 = "=SUMIFS('KPI_AP1 FA'!" + kpiLetter + ":" + kpiLetter + ",'KPI_AP1 FA'!$B:$B,'Simulation File'!$D$5)";

                    //SP1 (sell-out) FA (61%)
                    ws.Cell(HelperClass.IntToLetters(ctr) + "21").FormulaA1 = "=SUMIFS('KPI_Account Sell-Out FA'!" + kpiLetter + ":" + kpiLetter + ",'KPI_Account Sell-Out FA'!$B:$B,'Simulation File'!$D$5)";

                    //Flooring Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + "22").FormulaA1 = "=SUMIF($E$" + (startingRow + 1) + ":$E$" + (startingRow + simulationCount) + ",$E22," + letter1 + "$" + startingRow + ":" + letter1 + "$" + (startingRow + simulationCount) + ")";

                    //Flooring Actual
                    ws.Cell(HelperClass.IntToLetters(ctr) + "23").FormulaA1 = "=SUMIF($E$" + (startingRow + 1) + ":$E$" + (startingRow + simulationCount) + ",$E23," + letter1 + "$" + startingRow + ":" + letter1 + "$" + (startingRow + simulationCount) + ")";

                    //Flooring balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + "24").FormulaA1 = "=" + letter1 + "23-" + letter1 + "22";

                    //Actual + Simulation/week
                    ws.Cell(HelperClass.IntToLetters(ctr) + "29").FormulaA1 = "=SUMIF($E$" + (startingRow + 1) + ":$E$" + (startingRow + simulationCount) + "," + '"' + "Invoice Revenue" + '"' + "," + letter1 + (startingRow + 1) + ":" + letter1 + (startingRow + simulationCount) + ")";

                    ctr++;
                    index++;
                }

                ws.Cell("E19").Value = "TOTAL Ranking";
                ws.Cell("E20").Value = "AP1 (sell-in) FA (51%)";
                ws.Cell("E21").Value = "SP1 (sell-out) FA (61%)";

                ws.Cell("E22").Value = "Flooring Target";
                ws.Cell("E23").Value = "Flooring Actual";
                ws.Cell("E24").Value = "Flooring balance";

                ws.Cell("E25").Value = "SP Target";
                ws.Cell("E26").Value = "LY";
                ws.Cell("E27").Value = "Act+AP1";
                ws.Cell("E28").Value = "Actual + Simulation";
                ws.Cell("E29").Value = "Actual + Simulation/week";
                ws.Cell("E30").Value = "Target vs Actual";
                ws.Cell("E31").Value = "LY vs Actual";
                ws.Cell("E32").Value = "Sell-out target";
                ws.Cell("E33").Value = "Sell-out target achievement";
                ws.Cell("E34").Value = "Act+Fcst ASP";

                //MODEL MIX

                var ctrCCell = 35;

                ws.Cell("B" + ctrCCell).Value = "Model Mix";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell - 1 + (typeCount * 4))).Column(1).Merge();
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell - 1 + (typeCount * 4))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("C" + ctrCCell).Value = "SELL-IN";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for sell-in
                var ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = "=IFERROR(" + monthCell + ((35 + ctrModelMix) + (typeCount * 2)) + "/SUM(" + monthCell + (35 + (typeCount * 2)) + ":" + monthCellTo + (35 + (typeCount * 3) - 1 ) + ")," + '"' + '"' + ")";
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();

                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }

                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }

                    ctrCCell++;
                    ctrModelMix++;
                }

                ws.Cell("C" + ctrCCell).Value = "SELL-OUT";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for sell-out
                ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = "=IFERROR(" + monthCell + ((35 + ctrModelMix) + (typeCount * 3)) + "/SUM(" + monthCell + (35 + (typeCount * 3)) + ":" + monthCellTo + (35 + (typeCount * 4) - 1) + ")," + '"' + '"' + ")";
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();


                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }
                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }
                    ctrModelMix++;
                    ctrCCell++;
                }
                ws.Cell("C" + ctrCCell).Value = "SELL-IN AMOUNT";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for sell-in amount
                ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula
                        //loop each week in month (rangeDif is basically the number of weeks in the month)
                        var f = "=SUM(";
                        for (int i = 0; i <= rangeDif; i++)
                        {
                            //alphabeth
                            var colAlpha = HelperClass.IntToLetters(ctrMonth + i);
                            var sumIfs = "SUMIFS(" + colAlpha + "$" + (startingRow + 1) + ":" + colAlpha + "$" + endRow + "," + "$C$" + (startingRow + 1) + ":" + "$C$" + endRow + ",$E" + ctrCCell + "," + "$E$" + (startingRow + 1) + ":" + "$E$" + endRow + "," + '"' + "Invoice Revenue" + '"' + ")";

                            f += sumIfs + ",";
                        }
                        f = f.Remove(f.Length - 1);
                        f = f + ")";
                        //formula
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = f;
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();

                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }
                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }
                    ctrModelMix++;
                    ctrCCell++;
                }
                ws.Cell("C" + ctrCCell).Value = "SELL-OUT AMOUNT";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for sell-OUT amount
                ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula
                        //loop each week in month (rangeDif is basically the number of weeks in the month)
                        var f = "=SUM(";
                        for (int i = 0; i <= rangeDif; i++)
                        {
                            //alphabeth
                            var colAlpha = HelperClass.IntToLetters(ctrMonth + i);
                            var sumIfs = "SUMIFS(" + colAlpha + "$" + (startingRow + 1) + ":" + colAlpha + "$" + endRow + "," + "$C$" + (startingRow + 1) + ":" + "$C$" + endRow + ",$E" + ctrCCell + "," + "$E$" + (startingRow + 1) + ":" + "$E$" + endRow + "," + '"' + "Invoice Revenue" + '"' + ")";

                            f += sumIfs + ",";
                        }
                        f = f.Remove(f.Length - 1);
                        f = f + ")";
                        //formula
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = f;
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();

                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }
                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }
                    ctrModelMix++;
                    ctrCCell++;
                }

                //after model mix
                ws.Cell("B" + ctrCCell).Value = "Sell In";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + (ctrCCell + 1)).Value = "Sell In(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Sell-In Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-In(Account+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-In Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Sell-In Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 0)).FormulaA1 = "=SUMIFS('Sell-In & Sell-Out Target_Q'!" + letters + ":" + letters + ",'Sell-In & Sell-Out Target_Q'!$B:$B,'Simulation File'!$D$5,'Sell-In & Sell-Out Target_Q'!$D:$D," + '"' + "Sell-In" + '"' + ")";

                    //Sell-in(Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 13);

                    //Sell-in Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }

                ctrCCell += 3;

                ws.Cell("B" + ctrCCell).Value = "Sell Out";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + (ctrCCell + 1)).Value = "Sell-out FCST(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Sell-Out Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-Out(Account+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-Out Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Sell-Out Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 0)).FormulaA1 = "=SUMIFS('Sell-In & Sell-Out Target_Q'!" + letters + ":" + letters + ",'Sell-In & Sell-Out Target_Q'!$B:$B,'Simulation File'!$D$5,'Sell-In & Sell-Out Target_Q'!$D:$D," + '"' + "Sell-Out" + '"' + ")";

                    //Sell-Out (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 13);

                    //Sell-Out Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("B" + ctrCCell).Value = "Inv.";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + ctrCCell).Value = "EOH_EX(AP1)";
                ws.Cell("D" + (ctrCCell + 1)).Value = "EOH_EX(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Inventory Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Inventory Act+Fcst";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Inventory Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Inventory Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 12);

                    //Inventory (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 13);

                    //Inventory Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("B" + ctrCCell).Value = "WOS (F4)";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();
                ws.Range("B" + (ctrCCell - 11) + ":B" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C" + (ctrCCell - 11) + ":C" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + (ctrCCell - 11) + ":D" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + (ctrCCell - 11) + ":E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("D" + (ctrCCell + 1)).Value = "WOS_EX(AP1)";

                ws.Cell("E" + ctrCCell).Value = "WOS Target(F4)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "WOS Act+Fcst(F4)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "WOS Balance(F4)";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 6;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Sell-Out Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "=SUMIFS('WOS Target_Masterfile'!" + letters + ":" + letters + ",'WOS Target_Masterfile'!$D:$D,'Simulation File'!$C$17,'WOS Target_Masterfile'!$B:$B,'Simulation File'!$D$5)";

                    //Sell-Out (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 13);

                    //Sell-Out Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell ) + "/" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1) + "," + '"' + '"' + ")";

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                //after WOS(F4)
                ws.Cell("B" + ctrCCell).Value = "SCM Simulation";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 11)).Column(1).Merge();
                ws.Range("B" + (ctrCCell) + ":B" + (ctrCCell + 11)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C" + (ctrCCell) + ":C" + (ctrCCell + 11)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + (ctrCCell) + ":D" + (ctrCCell + 11)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + (ctrCCell) + ":E" + (ctrCCell + 11)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("C" + ctrCCell).Value = "Sell In";
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + ctrCCell).Value = "Sell In(AP1)";
                ws.Cell("D" + (ctrCCell + 1)).Value = "Sell In(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Sell-In(AP1)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-In(Act+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-In Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 7;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);//for channel
                    var letters1 = HelperClass.IntToLetters(ctr);//for api
                    //Sell-in(AP1)
                    var f1 = "=SUMIFS('AP1 FCST_Transaction'!" + letters1 + ":" + letters1 + ",'AP1 FCST_Transaction'!$B:$B,'Simulation File'!$D$5,'AP1 FCST_Transaction'!$C:$C,'Simulation File'!$C$17,'AP1 FCST_Transaction'!$D:$D," + '"' + "Sub Total" + '"' + ")";
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = f1;

                    //Sell-in(Act+Fcst)
                    var f2 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letters + ":" + letters + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$F:$F,'Simulation File'!$D$" + (ctrCCell + 2) + ",'Channel Level_Channel PSI Mgmt'!$B:$B,'Simulation File'!$C$17,'Channel Level_Channel PSI Mgmt'!$E:$E," + '"' + "Total" + '"' + ")";
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = f2;

                    //Sell-in Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("C" + ctrCCell).Value = "Sell Out";
                ws.Range("C" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + ctrCCell).Value = "Sell-out FCST(AP1/CON)";
                ws.Cell("D" + (ctrCCell + 1)).Value = "Sell-out FCST(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Sell-Out(SP1)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-Out(Act+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-Out Balance";

                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 7;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Sell-out(AP1)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letters + ":" + letters+ ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$F:$F,'Simulation File'!$D$" + (ctrCCell + 1) + ",'Channel Level_Channel PSI Mgmt'!$B:$B,'Simulation File'!$C$17,'Channel Level_Channel PSI Mgmt'!$E:$E," + '"' + "Total" + '"' + ")";

                    //Sell-out(Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letters + ":" + letters + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$F:$F,'Simulation File'!$D$" + (ctrCCell + 2) + ",'Channel Level_Channel PSI Mgmt'!$B:$B,'Simulation File'!$C$17,'Channel Level_Channel PSI Mgmt'!$E:$E," + '"' + "Total" + '"' + ")";

                    //Sell-out Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("C" + ctrCCell).Value = "Inv.";
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + ctrCCell).Value = "EOH_EX(AP1)";
                ws.Cell("D" + (ctrCCell + 1)).Value = "EOH_EX(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Inventory (AP/SP1)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Inventory Act+Fcst";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Inventory Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 7;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    var letters1 = HelperClass.IntToLetters(ctr);
                    //Inventory (AP1)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letters + ":" + letters + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$F:$F,'Simulation File'!$D$" + (ctrCCell + 1) + ",'Channel Level_Channel PSI Mgmt'!$B:$B,'Simulation File'!$C$17,'Channel Level_Channel PSI Mgmt'!$E:$E," + '"' + "Total" + '"' + ")";

                    //Inventory (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letters + ":" + letters + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$F:$F,'Simulation File'!$D$" + (ctrCCell + 2) + ",'Channel Level_Channel PSI Mgmt'!$B:$B,'Simulation File'!$C$17,'Channel Level_Channel PSI Mgmt'!$E:$E," + '"' + "Total" + '"' + ")";

                    //Inventory Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("C" + ctrCCell).Value = "WOS (F4)";
                ws.Range("C" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + (ctrCCell + 1)).Value = "WOS_EX(AP1)";

                ws.Cell("E" + ctrCCell).Value = "WOS AP/SP1(F4)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "WOS Act+Fcst(F4)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "WOS Balance(F4)";

                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 7;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //WOS
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell - 2) + "/AVG(" + HelperClass.IntToLetters(ctr + 1) + (ctrCCell - 5) + ":" +  HelperClass.IntToLetters(ctr + 5) + (ctrCCell - 5)+ "),0)";//todo get month

                    //WOS(Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letters + ":" + letters + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$F:$F,'Simulation File'!$D$" + (ctrCCell + 2) + ",'Channel Level_Channel PSI Mgmt'!$B:$B,'Simulation File'!$C$17,'Channel Level_Channel PSI Mgmt'!$E:$E," + '"' + "Total" + '"' + ")";

                    //WOS Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                //data
                ws.Cell("C" + ctrCCell).Value = "Type";
                ws.Cell("D" + ctrCCell).Value = "Model";
                ws.Cell("C" + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Cell("D" + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                //ws.Cell("C80").InsertData(dtSimulationData.Rows);
                ctrCCell += 1;

                //ws.Columns().AdjustToContents();
                ws.Column("A").Width = 20.43;
                ws.Column("B").Width = 9.86;
                ws.Column("C").Width = 11.29;
                ws.Column("D").Width = 22;
                ws.Column("E").Width = 25.57;
                ws.Column("F").Width = 12;
                ws.Column("G").Width = 12;
                ws.Column("H").Width = 12;
                ws.Column("I").Width = 12;
                ws.Column("J").Width = 12;
                ws.Column("K").Width = 12;
                ws.Column("L").Width = 12;
                ws.Column("M").Width = 12;
                ws.Column("N").Width = 12;
                ws.Column("O").Width = 12;
                ws.Column("P").Width = 12;
                ws.Column("Q").Width = 12;
                ws.Column("R").Width = 12;
                ws.Column("S").Width = 12;
                ws.Column("T").Width = 12;
                ws.Column("U").Width = 12;
                ws.Column("V").Width = 12;
                ws.Column("W").Width = 12;
                ws.Column("X").Width = 12;
                ws.Column("Y").Width = 12;
                ws.Column("z").Width = 12;
                #endregion
                #region simulationdata
                var ctrResultCols = 0;
                var ctrResultCols1 = ctrCCell;
                int catIndex = colHeaders.FindIndex(F => F == "Category");
                var lastModel = "";
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    var ctrCOls = 0;
                    var ctrCOls1 = 0;
                    if (lastModel != (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")])
                    {
                        if (lastModel != "")
                            ctrResultCols += 16;//todo

                        lastModel = item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                    }
                    else
                    {
                        ws.Cell("C" + ctrResultCols1).Style.Font.FontColor = XLColor.White;
                        ws.Cell("D" + ctrResultCols1).Style.Font.FontColor = XLColor.White;
                    }
                    ws.Cell("C" + ctrResultCols1).Value = ((string)item[colHeaders.FindIndex(F => F.ToLower() == "type")]).ToString().Trim();
                    ws.Cell("D" + ctrResultCols1).Value = lastModel.Trim();
                    ws.Cell("E" + ctrResultCols1).Value = ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")]).ToString().Trim();

                    foreach (var cols in item)
                    {
                        if (notIncludedCols.Where(W => W == ctrCOls).Count() == 0)
                        {
                            var letter = "";// HelperClass.IntToLetters(6 + weekIndex);
                            var sLetter = "";// HelperClass.IntToLetters(6 + weekIndex);
                            var isEditable = false;
                            switch ((string)cols)//item[catIndex])
                            {
                                case "Sell-In AP1":
                                    var we = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we);
                                        sLetter = HelperClass.IntToLetters(we + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('AP1 FCST_Transaction'!" + letter + ":" + letter + ",'AP1 FCST_Transaction'!$B:$B,'Simulation File'!$D$5,'AP1 FCST_Transaction'!$D:$D,'Simulation File'!$D" + (ctrCCell + ctrResultCols) + ")";//todo change the D80 to correct position of model
                                        we++;
                                    }
                                    break;
                                case "Sell-in(Act+Fcst)":
                                    var we1 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(7 + we1);
                                        isEditable = IsEditable(int.Parse(w));
                                        sLetter = HelperClass.IntToLetters(we1 + 6);
                                        if (isEditable)
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = item[colHeaders.FindIndex(F => F.ToLower() == w.ToString())];
                                            ws.Cell(sLetter + ctrResultCols1).Style.Fill.BackgroundColor = XLColor.Orange;
                                        }
                                        else
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letter + ":" + letter + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$E:$E,'Simulation File'!$D" + (ctrCCell + ctrResultCols) + ",'Channel Level_Channel PSI Mgmt'!$F:$F," + '"' + "Sell-in(AP1)" + '"' + ")";//todo change the D80 to correct position of model
                                        }

                                        we1++;
                                    }
                                    break;
                                case "Sell-in Balance":
                                    var we2 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we2);
                                        sLetter = HelperClass.IntToLetters(we2 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + letter + (ctrCCell + (ctrResultCols)) + "-" + letter + ((ctrCCell + 1) + (ctrResultCols));
                                        we2++;
                                    }
                                    break;
                                case "Sell-Out SP1":
                                    var we3 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(7 + we3);
                                        sLetter = HelperClass.IntToLetters(we3 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letter + ":" + letter + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$E:$E,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",'Channel Level_Channel PSI Mgmt'!$F:$F," + '"' + "Sell-out FCST(AP1/CON)" + '"' + ")";
                                        we3++;
                                    }
                                    break;
                                case "Sell-Out(Act+Fcst)":
                                    var we4 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(7 + we4);
                                        isEditable = IsEditable(int.Parse(w));
                                        sLetter = HelperClass.IntToLetters(we4 + 6);
                                        if (isEditable)
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).Style.Fill.BackgroundColor = XLColor.Orange;
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = item[colHeaders.FindIndex(F => F.ToLower() == w.ToString())];
                                        }
                                        else
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letter + ":" + letter + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$E:$E,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",'Channel Level_Channel PSI Mgmt'!$F:$F," + '"' + "Sell-out FCST(AP1)" + '"' + ")";//todo change the D80 to correct position of model
                                        }
                                        we4++;
                                    }
                                    break;
                                case "Sell-Out Balance":
                                    var we5 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we5);
                                        sLetter = HelperClass.IntToLetters(we5 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + letter + ((ctrCCell + 3) + (ctrResultCols)) + "-" + letter + ((ctrCCell + 4) + (ctrResultCols));
                                        we5++;
                                    }
                                    break;
                                case "Inventory AP/SP1":
                                    var we6 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(7 + we6);
                                        sLetter = HelperClass.IntToLetters(we6 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letter + ":" + letter + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$E:$E,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",'Channel Level_Channel PSI Mgmt'!$F:$F," + '"' + "EOH_EX(AP1)" + '"' + ")";
                                        we6++;
                                    }
                                    break;
                                case "Inventory Act+Fcst":
                                    var we7 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(7 + we7);
                                        isEditable = false;//IsEditable(int.Parse(w));
                                        sLetter = HelperClass.IntToLetters(we7 + 6);
                                        if (isEditable)
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).Style.Fill.BackgroundColor = XLColor.Orange;
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = item[colHeaders.FindIndex(F => F.ToLower() == w.ToString())];
                                        }
                                        else
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letter + ":" + letter + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$E:$E,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",'Channel Level_Channel PSI Mgmt'!$F:$F," + '"' + "EOH_EX(AP1)" + '"' + ")";//todo change the D80 to correct position of model
                                        }
                                        we7++;
                                    }
                                    break;
                                case "Inventory Balance":
                                    var we8 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we8);
                                        sLetter = HelperClass.IntToLetters(we8 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + letter + ((ctrCCell + 6) + (ctrResultCols)) + "-" + letter + ((ctrCCell + 7) + (ctrResultCols));
                                        we8++;
                                    }
                                    break;
                                case "WOS AP/SP1(F4)":
                                    var we9 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we9);
                                        var wosLetter = HelperClass.IntToLetters(6 + (we9 + 1));
                                        var wosLetter1 = HelperClass.IntToLetters(6 + (we9 + 4));
                                        sLetter = HelperClass.IntToLetters(we9 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IFERROR(" + letter + ((ctrCCell + 6) + (ctrResultCols)) + "/AVERAGE(" + wosLetter + "" + ((ctrCCell + 3) + (ctrResultCols)) + ":" + wosLetter1 + "" + ((ctrCCell + 3) + (ctrResultCols)) + "),0)";
                                        we9++;
                                    }
                                    break;
                                case "WOS Act+Fcst(F4)":
                                    var we10 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(7 + we10);
                                        sLetter = HelperClass.IntToLetters(we10 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Channel Level_Channel PSI Mgmt'!" + letter + ":" + letter + ",'Channel Level_Channel PSI Mgmt'!$D:$D,'Simulation File'!$D$5,'Channel Level_Channel PSI Mgmt'!$E:$E,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",'Channel Level_Channel PSI Mgmt'!$F:$F," + '"' + "WOS_EX(AP1)" + '"' + ")";
                                        we10++;
                                    }
                                    break;
                                case "Run Rate":
                                    var we11 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we11);
                                        sLetter = HelperClass.IntToLetters(we11 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IFERROR(" + letter + ((ctrCCell + 4) + (ctrResultCols)) + "/" + letter + ((ctrCCell + 13) + (ctrResultCols)) + "," + '"' + '"' + ")";
                                        we11++;
                                    }
                                    break;
                                case "Flooring Target":
                                    var we12 = 0;
                                    letter = HelperClass.IntToLetters(6 + we12);
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(10 + we12);
                                        sLetter = HelperClass.IntToLetters(we12 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Flooring Target_Transaction'!" + letter + ":" + letter + ",'Flooring Target_Transaction'!$D:$D,'Simulation File'!$D$5,'Flooring Target_Transaction'!$H:$H,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ")";
                                        we12++;
                                    }
                                    break;
                                case "Flooring Actual":
                                    var we13 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we13);
                                        sLetter = HelperClass.IntToLetters(we13 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS(Flooring_Site_Transaction!$G:$G,Flooring_Site_Transaction!$P:$P,'Simulation File'!$D$5,Flooring_Site_Transaction!$E:$E,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",Flooring_Site_Transaction!$N:$N,'Simulation File'!" + letter + "$18)";
                                        we13++;
                                    }
                                    break;
                                case "Invoice Price":
                                    var we14 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we14);
                                        sLetter = HelperClass.IntToLetters(we14 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=AVERAGEIFS('Gross & Net Price_Transaction'!$K:$K,'Gross & Net Price_Transaction'!$E:$E,'Simulation File'!$D$5,'Gross & Net Price_Transaction'!$G:$G,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ")";
                                        we14++;
                                    }
                                    break;
                                case "Invoice Revenue":
                                    var we15 = 0;
                                    ws.Range("C" + (ctrResultCols1 - 15) + ":C" + ctrResultCols1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                                    ws.Range("D" + (ctrResultCols1 - 15) + ":D" + ctrResultCols1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                                    ws.Range("E" + (ctrResultCols1 - 15) + ":E" + ctrResultCols1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we15);
                                        sLetter = HelperClass.IntToLetters(we15 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + letter + ((ctrCCell + 14) + (ctrResultCols)) + "*" + letter + ((ctrCCell + 1) + (ctrResultCols));
                                        ws.Range(sLetter + (ctrResultCols1 - 15) + ":" + sLetter + ctrResultCols1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                                        we15++;
                                    }
                                    break;

                            }
                            ctrCOls++;
                            //dr[ctrCOls] = item[ctrCOls];
                        }
                        ctrCOls1++;
                    }

                    //dtSimulationData.Rows.Add(dr);
                    ctrResultCols1++;
                }
                #endregion
                #endregion
                #region BuildKPIFAWorksheet
                var wsKPIFA = wb.Worksheets.Add("KPI_AP1 FA");

                SqlParameter[] paramKPI = {
                new SqlParameter("@weeks",string.Join(",",GetListOfMonths(dateFrom,dateTo))),
                new SqlParameter("@pgroup",ProductGroup),
                new SqlParameter("@account",GSCMAccount)
                    };

                DataTable dt = sql.getDTSQLStoredProc("spu_GET_KPI_AP1_TF", paramKPI);
                var q = (from row in dt.AsEnumerable()
                         group row by row.Field<string>("YearMonth") into grp
                         select grp);//O=>O.FirstOrDefault().Field<string>("YearWeek"));
                List<string> keys = q.Select(S => S.Key).ToList();

                wsKPIFA.Cell("B1").Value = "Year/Month";
                wsKPIFA.Cell("B2").Value = "Year/Week";
                wsKPIFA.Cell("A3").Value = "Product Group";
                wsKPIFA.Cell("B3").Value = "GSCM Account";
                var ctrKPIFA = 3;
                var weekCtr = 4;
                int tryWeek = -1;
                foreach (var month in q)
                {
                    foreach (var week in month)
                    {
                        var colLetter = HelperClass.IntToLetters(ctrKPIFA);
                        wsKPIFA.Cell(colLetter + "1").Value = month.FirstOrDefault().Field<string>("YearMonth");
                        if (!int.TryParse(week.Field<string>("YearWeek"), out tryWeek))
                        {
                            //var colLetterYearWeek = HelperClass.IntToLetters((weekCtr - month.Count()) );
                            wsKPIFA.Cell(colLetter + "2").Value = week.Field<string>("YearWeek");
                        }
                        else
                        {
                            //colLetter = HelperClass.IntToLetters(weekCtr);
                            wsKPIFA.Cell(colLetter + "2").Value = week[1];
                        }


                        ctrKPIFA++;
                        weekCtr++;
                    }
                }

                var q1 = from row in dt.AsEnumerable()
                         group row by new { ProductGroup = row.Field<string>("ProductGroup"), Account = row.Field<string>("GSCM Account") } into grp
                         select grp;

                var ctr1 = 3;
                var rowCtr = 4;
                foreach (var item in q1)
                {
                    foreach (var i in item)
                    {
                        var colLetter = HelperClass.IntToLetters(ctr1);
                        wsKPIFA.Cell(colLetter + "3").Value = "FA(1-8W)";
                        //row
                        wsKPIFA.Cell("A" + (rowCtr.ToString())).Value = i[2];
                        wsKPIFA.Cell("B" + (rowCtr.ToString())).Value = i[3];
                        wsKPIFA.Cell(colLetter + (rowCtr.ToString())).Value = i[4];
                        ctr1++;
                    }
                    rowCtr++;
                }
                wsKPIFA.Columns().AdjustToContents();

                #endregion
                #region BuildKPIAccountWorksheet
                SqlParameter[] paramKPIAccount = {
                new SqlParameter("@weeks",string.Join(",",GetListOfMonths(dateFrom,dateTo))),
                new SqlParameter("@pgroup",ProductGroup),
                new SqlParameter("@account",GSCMAccount)
                    };

                var wsKPIAccount = wb.Worksheets.Add("KPI_Account Sell-Out FA");
                DataTable dtKPIAccount = sql.getDTSQLStoredProc("spu_GET_KPI_AP1_Account_TF", paramKPIAccount);
                var qKPIAccount = (from row in dtKPIAccount.AsEnumerable()
                                   group row by row.Field<string>("YearMonth") into grp
                                   select grp);//O=>O.FirstOrDefault().Field<string>("YearWeek"));
                List<string> keysKPIAccount = qKPIAccount.Select(S => S.Key).ToList();

                wsKPIAccount.Cell("B1").Value = "Year/Month";
                wsKPIAccount.Cell("B2").Value = "Year/Week";
                wsKPIAccount.Cell("A3").Value = "Product Group";
                wsKPIAccount.Cell("B3").Value = "GSCM Account";
                var ctrKPIAccount = 3;
                var weekCtrKPIAccount = 4;
                int tryWeekKPIAccount = -1;
                foreach (var month in qKPIAccount)
                {
                    foreach (var week in month)
                    {
                        var colLetter = HelperClass.IntToLetters(ctrKPIAccount);
                        wsKPIAccount.Cell(colLetter + "1").Value = month.FirstOrDefault().Field<string>("YearMonth");
                        if (!int.TryParse(week.Field<string>("YearWeek"), out tryWeekKPIAccount))
                        {
                            //var colLetterYearWeek = HelperClass.IntToLetters((weekCtrKPIAccount - month.Count())) ;
                            wsKPIAccount.Cell(colLetter + "2").Value = week.Field<string>("YearWeek");
                        }
                        else
                        {
                            //colLetter = HelperClass.IntToLetters(weekCtrKPIAccount);
                            wsKPIAccount.Cell(colLetter + "2").Value = week[1];
                        }


                        ctrKPIAccount++;
                        weekCtrKPIAccount++;
                    }
                }

                var q1KPIAccount = from row in dtKPIAccount.AsEnumerable()
                                   group row by new { ProductGroup = row.Field<string>("ProductGroup"), Account = row.Field<string>("GSCM Account") } into grp
                                   select grp;

                var ctr1KPIAccount = 3;
                var rowCtrKPIAccount = 4;
                foreach (var item in q1KPIAccount)
                {
                    foreach (var i in item)
                    {
                        var colLetter = HelperClass.IntToLetters(ctr1KPIAccount);
                        wsKPIAccount.Cell(colLetter + "3").Value = "FA(1-8W, Mean)";
                        //row
                        wsKPIAccount.Cell("A" + (rowCtrKPIAccount.ToString())).Value = i[2];
                        wsKPIAccount.Cell("B" + (rowCtrKPIAccount.ToString())).Value = i[3];
                        wsKPIAccount.Cell(colLetter + (rowCtrKPIAccount.ToString())).Value = i[4];
                        ctr1KPIAccount++;
                    }
                    rowCtrKPIAccount++;
                }
                wsKPIAccount.Columns().AdjustToContents();

                #endregion
                #region OtherSheets
                var wsAP1FCST = wb.Worksheets.Add("AP1 FCST_Transaction");
                wsAP1FCST.Cell("B2").InsertTable(dtAPIForecast);
                var wsAP1FCSTColAlpha = HelperClass.IntToLetters(dtAPIForecast.Columns.Count);
                wsAP1FCST.Range("F3:" + wsAP1FCSTColAlpha + (dtAPIForecast.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsChannelLevel = wb.Worksheets.Add("Channel Level_Channel PSI Mgmt");
                wsChannelLevel.Cell("B2").InsertTable(dtChannelPSI);
                var wsChannelLevelColAlpha = HelperClass.IntToLetters(dtChannelPSI.Columns.Count);
                wsChannelLevel.Range("G4:" + wsChannelLevelColAlpha + (dtChannelPSI.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsFlooringSite = wb.Worksheets.Add(dtFlooringSite, "Flooring_Site_Transaction");
                //var wsFlooringSiteColAlpha = HelperClass.IntToLetters(dtFlooringSite.Columns.Count);
                //wsFlooringSite.Range("E2:" + wsFlooringSiteColAlpha + (dtFlooringSite.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsFlooringTarget = wb.Worksheets.Add(dtFlooringTarget, "Flooring Target_Transaction");
                var wsFlooringTargetColAlpha = HelperClass.IntToLetters(dtFlooringTarget.Columns.Count);
                wsFlooringTarget.Range("I2:" + wsFlooringTargetColAlpha + (dtFlooringTarget.Rows.Count + 1)).DataType = XLCellValues.Number;

                wb.Worksheets.Add(dtGrossNetPrice, "Gross & Net Price_Transaction");


                var wsSISOTQ = wb.Worksheets.Add(dtSellInOutTargetQ, "Sell-In & Sell-Out Target_Q");
                var wsSISOTQColAlpha = HelperClass.IntToLetters(dtSellInOutTargetQ.Columns.Count);
                wsSISOTQ.Range("I2:" + wsSISOTQColAlpha + (dtSellInOutTargetQ.Rows.Count + 1)).DataType = XLCellValues.Number;

                wb.Worksheets.Add(dtSellInOutTargetA, "Sell-In & Sell-Out Target_A");

                #region sellinselloutargeta
                ////with merge column
                //var wsSellInSellOutTargetA = wb.Worksheets.Add("Sell-In & Sell-Out Target_A");//dtSellInOutTargetA, 
                //List<string> listColumnHeaders = new List<string>();
                //listColumnHeaders = GenerateGenericColumnsList(listColumnHeaders,dtSellInOutTargetA);
                //var ctrSISOTA = 1;
                //wsSellInSellOutTargetA.Range("A1:A2").Column(1).Merge();
                //wsSellInSellOutTargetA.Range("B1:B2").Column(1).Merge();
                //wsSellInSellOutTargetA.Range("C1:C2").Column(1).Merge();
                //foreach (var item in listColumnHeaders)
                //{
                //    var colsAlpha = HelperClass.IntToLetters(ctrSISOTA);
                //    wsSellInSellOutTargetA.Cell(colsAlpha + "1").Value = item;
                //    ctrSISOTA++;
                //}
                //var qSellInOutTargetA = from row in dtSellInOutTargetA.AsEnumerable()
                //                        group row by new { Type = row.Field<string>("Type") } into grp
                //                        select grp;

                //var weekInTypeCtr = 0;
                //var typeCtr = 1;
                //foreach (var itemGroup in qSellInOutTargetA)
                //{
                //    var typeGroup = itemGroup.GroupBy(G => G.Field<string>("Week"));
                //    var typeGroupCount = typeGroup.Count();
                //    var colsAlpha = HelperClass.IntToLetters(4 + (typeCtr * typeGroupCount));
                //    var colsAlphaFrom = HelperClass.IntToLetters((4 + (typeCtr * typeGroupCount))-12);

                //    wsSellInSellOutTargetA.Range(colsAlphaFrom + "1:" + colsAlpha + "1").Merge();
                //    wsSellInSellOutTargetA.Range(colsAlphaFrom + "1:" + colsAlpha + "1").Value = itemGroup.Key.Type;
                //    foreach (var itemsInGroup in typeGroup)
                //    {
                //        var weekColAlpha = HelperClass.IntToLetters( 4 + weekInTypeCtr);
                //        wsSellInSellOutTargetA.Cell("A").Value = itemsInGroup.FirstOrDefault().Field<string>("Dealer");
                //        wsSellInSellOutTargetA.Cell("B").Value = itemsInGroup.FirstOrDefault().Field<string>("GSCMAccount");
                //        wsSellInSellOutTargetA.Cell("C").Value = itemsInGroup.FirstOrDefault().Field<string>("AP1");
                //        wsSellInSellOutTargetA.Cell(weekColAlpha + "2").Value = itemsInGroup.FirstOrDefault().Field<string>("Week");
                //        wsSellInSellOutTargetA.Cell(weekColAlpha + "2").Value = itemsInGroup.FirstOrDefault().Field<string>("Value");
                //        weekInTypeCtr++;
                //    }
                //    typeCtr++;
                //}
                #endregion
                var wsWOSTarget = wb.Worksheets.Add(dtWOSTarget, "WOS Target_Masterfile");
                var wsWosTargetColAlpha = HelperClass.IntToLetters(dtWOSTarget.Columns.Count);
                wsWOSTarget.Range("I2:" + wsWosTargetColAlpha + (dtWOSTarget.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsTIM = wb.Worksheets.Add(dtTIM, "TIM_Transaction");
                var wsTIMColAlpha = HelperClass.IntToLetters(dtTIM.Columns.Count);
                wsTIM.Range("I2:" + wsFlooringTargetColAlpha + (dtTIM.Rows.Count + 1)).DataType = XLCellValues.Number;
                #endregion

                #region hidesheets
                var wCtr = 0;
                foreach (IXLWorksheet wss in wb.Worksheets)
                {
                    if(wCtr != 0)
                        wss.Visibility = XLWorksheetVisibility.Hidden;

                    wCtr++;
                }
                #endregion
                
                var filename = "ap1simulation" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";
                //Response.Clear();
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.AddHeader("content-disposition", "attachment;filename=\"" + filename + "\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        //byte[] bytes = new byte[memoryStream.Length];
                        //memoryStream.Read(bytes, 0, (int)memoryStream.Length);
                        memoryStream.WriteTo(file);
                    }

                    //memoryStream.WriteTo(Response.OutputStream);
                    //memoryStream.Close();
                }

                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "AP1"
                });
                DB.SaveChanges();

                //Response.End();
                return new JsonResult()
                {
                    Data = filename
                };
            }
        }
        public ActionResult ExportShortSimulation(string ProductGroup, string GSCMAccount, string Dealer, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            List<string> listOfWeeks = new List<string>();
            //weeks
            var weekFrom = int.Parse(dateFrom.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var weekTo = int.Parse(dateTo.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());
            for (var ctr = 0; weekFrom <= weekTo; ctr++)
            {
                var year = weekFrom.ToString().Substring(0, 4);
                var week = int.Parse(weekFrom.ToString().Substring(4, 2));
                var maxWeek = Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxWeek)
                {
                    weekFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxWeek).ToString().PadLeft(2, '0'));
                    listOfWeeks.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
                else
                {
                    listOfWeeks.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
            }



            var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
            var colHeaders = columns.Split(',').ToList();
            DataTable dtSimulationData = new DataTable();
            List<int> notIncludedCols = new List<int>();
            var ctrCols = 0;
            foreach (var item in colHeaders)
            {
                if (item.ToLower() != "type" && item.ToLower() != "item" && item.ToLower() != "category" && !item.ToLower().Contains("20"))
                    notIncludedCols.Add(ctrCols);
                else
                    dtSimulationData.Columns.Add(item);

                ctrCols++;
            }

            //var data = System.Web.Helpers.Json.Decode(simulationData);

            DataAccessLayer sql = new DataAccessLayer();

            SqlParameter[] paramRawDataSOAndInvWeekly = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@dealer",Dealer),
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtRawDataSOAndInvWeekly = sql.getDTSQLStoredProc("spu_GET_RawDataSO&Inv(Weekly)", paramRawDataSOAndInvWeekly);//GetExportableData(ProductGroup, GSCMAccount, listOfWeeks, "spu_GET_SellInOutTargetQTF", "Item");

            SqlParameter[] paramSellIn = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks))
                    };
            DataTable dtSellIn = sql.getDTSQLStoredProc("spu_GET_RawDataDealerSellInTF", paramSellIn);//GetExportableData(ProductGroup, GSCMAccount, listOfWeeks, "spu_GET_SellInOutTargetQTF", "Item");

            DataTable dtChannelPSI = GetExportableData_Keys(ProductGroup, GSCMAccount, listOfWeeks, "spu_GET_ChannelPSITF", "Item", "Category");

            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtFlooringSite = sql.getDTSQLStoredProc("spu_GET_FlooringSiteTF", param);
            DataTable dtFlooringTarget = GetExportableData_FT(ProductGroup, listOfWeeks, "spu_ap2_GET_FlooringTargetTF", "Store Name", "Dealer", "GSCM Account", "AP1", "Product Group", "Model");
            //DataTable dtFlooringTarget = GetExportableData(ProductGroup, Dealer, listOfWeeks, "spu_GET_FlooringTargetTF", "Model");

            SqlParameter[] param1 = {
                new SqlParameter("@dealer",Dealer),
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtGrossNetPrice = sql.getDTSQLStoredProc("spu_GET_GrossNetPriceShortTF", param1);

            var listOfMonths = GetListOfMonths(dateFrom, dateTo);
            SqlParameter[] paramSellInSellOutA = {
                new SqlParameter("@weeks",string.Join(",",listOfMonths)),
                new SqlParameter("@account",GSCMAccount),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtSellInOutTargetQ = GetExportableData_Keys(ProductGroup, GSCMAccount, listOfWeeks, "spu_GET_SellInOutTargetQTF", "Dealer", "Category");// sql.getDTSQLStoredProc("spu_GET_SellInOutTargetQTF", paramSellInSellOutQ);
            DataTable dtSellInOutTargetA = sql.getDTSQLStoredProc("spu_GET_SellInOutTargetATF", paramSellInSellOutA);//GetExportableDataForAmount(ProductGroup, GSCMAccount, listOfMonths, "spu_GET_SellInOutTargetATF","Dealer");//
            DataTable dtWOSTarget = GetExportableData(ProductGroup, Dealer, listOfWeeks, "spu_GET_WOSTargetShortMF", "Product Group");
            DataTable dtTIM = GetExportableData_Keys(ProductGroup, Dealer, listOfMonths, "spu_GET_TIMShortTF", "Dealer", "Category");


            using (XLWorkbook wb = new XLWorkbook())
            {
                // Add a DataTable as a worksheet
                var ws = wb.Worksheets.Add("Simulation File");
                #region BuildSimulationSheet

                #region firstbox
                // Merge a range
                ws.Cell("B3").Value = "AP1";
                ws.Range("B3:C4").Merge();

                ws.Cell("D3").Value = GSCMAccount.ToUpper();
                ws.Range("D3:E4").Merge();

                // Merge a row
                ws.Cell("B5").Value = "CHANNEL";
                ws.Range("B5:C5").Row(1).Merge();

                ws.Cell("D5").Value = Dealer.ToUpper();
                ws.Range("D5:E5").Row(1).Merge();

                // Merge a row
                ws.Cell("B6").Value = "PRODUCT";
                ws.Range("B6:C6").Row(1).Merge();

                ws.Cell("D6").Value = ProductGroup.ToUpper();
                ws.Range("D6:E6").Row(1).Merge();

                // Merge a range
                ws.Cell("B7").Value = "PIC";
                ws.Range("B7:C9").Merge();

                // Merge a range
                ws.Cell("B10").Value = "PROFILE";
                ws.Range("B10:C14").Merge();

                ws.Cell("B15").Value = "ISS";
                ws.Range("B15:C15").Row(1).Merge();

                ws.Range("B3:E15").Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                ws.Range("B3:E15").Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                ws.Range("B3:E15").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Range("B3:E15").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                #endregion
                #region secondbox
                ws.Cell("B17").Value = "DA TOTAL";//todo
                ws.Range("B17:B18").Column(1).Merge();
                ws.Cell("B17").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("B17:B18").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;


                ws.Cell("B19").Value = "KPI (SCM)";
                ws.Range("B19:B21").Column(1).Merge();
                ws.Range("B19:B21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C19:C21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D19:D21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E19:E21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("B22").Value = "Flooring";
                ws.Range("B22:B24").Column(1).Merge();
                ws.Range("B22:B24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C22:C24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D22:D24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E22:E24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("B25").Value = "Revenue";
                ws.Range("B25:B34").Column(1).Merge();
                ws.Range("B25:B34").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C25:C31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D25:D31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E25:E31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C30:E31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;


                ws.Cell("C17").Value = ProductGroup;
                ws.Range("C17:E18").Merge();
                ws.Cell("C17").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("C17:E18").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("D22").Value = "TOTAL";
                ws.Range("D22:D24").Column(1).Merge();

                var qModelMix = from PT in DB._ProductType
                                join PG in DB._ProductGroup
                                on PT.ProductGroup equals PG.GroupName
                                where PG.GroupName == ProductGroup
                                select PT;
                var typeCount = qModelMix.Count();

                //simulation starting row
                var startingRow = 31 + (typeCount * 4) + 14;
                //total row count of items in simulation
                var simulationCount = result.Count;
                //simulation ending row
                var endRow = startingRow + simulationCount;

                //get the months
                //set month header
                //generate & set formula for the monthly basis computation
                var ctrM = 0;
                var ctrMonth = 6;
                var monthList = GetListOfMonths(dateFrom, dateTo);
                foreach (var month in monthList)
                {
                    //compute the range week of month
                    //this is used to determine which cell will be the scope of each month
                    var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                    string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                    var letters = HelperClass.IntToLetters(ctrMonth);
                    //set month header styles
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "17").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "17").Value = monthName;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "17:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "17").Merge();
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "17:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "17").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "19:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "24").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "25:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "29").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "25:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "29").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "30:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "31").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                    #region revenue by month
                    //sp target
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "25").FormulaA1 = "=SUMIFS('Sell-In & Sell-Out Target_A'!$F:$F,'Sell-In & Sell-Out Target_A'!$B:$B,'Simulation File'!$D$5,'Sell-In & Sell-Out Target_A'!$D:$D," + '"' + "SP Target - CY" + '"' + ",'Sell-In & Sell-Out Target_A'!$E:$E," + month +")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "25:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "25").Merge();

                    //ly
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "26").FormulaA1 = "=SUMIFS('Sell-In & Sell-Out Target_A'!$F:$F,'Sell-In & Sell-Out Target_A'!$B:$B,'Simulation File'!$D$5,'Sell-In & Sell-Out Target_A'!$D:$D," + '"' + "SP Target - LY" + '"' + ",'Sell-In & Sell-Out Target_A'!$E:$E," + month + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "26:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "26").Merge();

                    //act+ap1

                    var grossPrices = "";
                    for (int i = 1; i <= simulationCount / 9; i++)
                    {
                        grossPrices += HelperClass.IntToLetters(ctrMonth) + (startingRow + (i * 8)) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + (startingRow + (i * 8)) + ",";
                    }
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "27").FormulaA1 = "=SUM(" + grossPrices.Remove(grossPrices.Length - 1) + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "27:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "27").Merge();

                    //% Achievement
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "28").FormulaA1 = "=IFERROR(" + letters + "27/" + letters + "25," + '"' + '"' + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "28:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "28").Merge();

                    //sell-out target
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "30").FormulaA1 = "=SUM(" + HelperClass.IntToLetters(ctrMonth) + (31 + (typeCount * 4) + 5) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + (31 + (typeCount * 4) + 5) + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "30:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "30").Merge();

                    //sell-out target achievement
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "31").FormulaA1 = "=IFERROR(SUM(" + HelperClass.IntToLetters(ctrMonth) + (31 + (typeCount * 4) + 5) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + (31 + (typeCount * 4) + 5) + ")/" + HelperClass.IntToLetters(ctrMonth) + "30," + '"' + '"' + ")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "31:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "31").Merge();

                    #endregion

                    //increment
                    ctrM++;
                    ctrMonth += rangeDif + 1;
                }


                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                var ctr = 6;
                var index = 3;
                foreach (var week in listOfWeeks)
                {
                    //set the week
                    //set the styles
                    ws.Cell(HelperClass.IntToLetters(ctr) + "18").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Cell(HelperClass.IntToLetters(ctr) + "18").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                    ws.Cell(HelperClass.IntToLetters(ctr) + "18").Value = week;

                    var letters = HelperClass.IntToLetters(ctr);
                    //AP1 (sell-in) FA (51%)
                    //ws.Cell(HelperClass.IntToLetters(ctr) + "20").FormulaA1 = "=SUMIFS('KPI_AP1 FA'!" + letters + ":" + letters + ",'KPI_AP1 FA'!$B:$B,'Simulation File'!$D$5)";
                    ws.Cell(HelperClass.IntToLetters(ctr) + "20").Style.Fill.BackgroundColor = XLColor.FromArgb(128, 128, 128);

                    //SP1 (sell-out) FA (61%)
                    //ws.Cell(HelperClass.IntToLetters(ctr) + "21").FormulaA1 = "=SUMIFS('KPI_Account Sell-Out FA'!" + letters + ":" + letters + ",'KPI_Account Sell-Out FA'!$B:$B,'Simulation File'!$D$5)";
                    ws.Cell(HelperClass.IntToLetters(ctr) + "21").Style.Fill.BackgroundColor = XLColor.FromArgb(128, 128, 128);

                    //Flooring Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + "22").FormulaA1 = "=SUMIF($E$" + startingRow + ":$E$" + ((startingRow + simulationCount) - 1) + ",$E22," + letters + "$" + startingRow + ":" + letters + "$" + ((startingRow + simulationCount) - 1) + ")";

                    //Flooring Actual
                    ws.Cell(HelperClass.IntToLetters(ctr) + "23").FormulaA1 = "=SUMIF($E$" + startingRow + ":$E$" + ((startingRow + simulationCount) - 1) + ",$E23," + letters + "$" + startingRow + ":" + letters + "$" + ((startingRow + simulationCount) - 1) + ")";

                    //Flooring balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + "24").FormulaA1 = "=" + letters + "23-" + letters +"22";

                    //Revenue per week
                    ws.Cell(HelperClass.IntToLetters(ctr) + "29").FormulaA1 = "=SUMIFS(" + letters + "$" + startingRow + ":" + letters + "$" + ((startingRow + simulationCount) - 1) + ",$E" + startingRow + ":$E" + (endRow-1) + "," + '"' + "Invoice Revenue" + '"' + ")";

                    ctr++;
                    index++;
                }

                ws.Cell("E19").Value = "TOTAL Ranking";
                ws.Cell("E20").Value = "AP1 (sell-in) FA (51%)";
                ws.Cell("E21").Value = "SP1 (sell-out) FA (61%)";

                ws.Cell("E22").Value = "Flooring Target";
                ws.Cell("E23").Value = "Flooring Actual";
                ws.Cell("E24").Value = "Flooring balance";

                ws.Cell("E25").Value = "SP Target";
                ws.Cell("E26").Value = "LY";
                ws.Cell("E27").Value = "Act+Fcst";
                ws.Cell("E28").Value = "% Achievement";
                ws.Cell("E29").Value = "Revenue per week";
                ws.Cell("E30").Value = "Sell-out target";
                ws.Cell("E31").Value = "Sell-out target achievement";

                //MODEL MIX

                var ctrCCell = 32;

                ws.Cell("B" + ctrCCell).Value = "Model Mix";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell - 1 + (typeCount * 4))).Column(1).Merge();
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell - 1 + (typeCount * 4))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("C" + ctrCCell).Value = "SELL-IN";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for sell-in
                var ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = "=IFERROR(" + monthCell + ((32 + ctrModelMix) + (typeCount * 2)) + "/SUM(" + monthCell + (32 + (typeCount * 2)) + ":" + monthCellTo + (32 + (typeCount * 3)  -1) + ")," + '"' + '"' + ")";
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();

                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }

                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }

                    ctrCCell++;
                    ctrModelMix++;
                }

                ws.Cell("C" + ctrCCell).Value = "SELL-OUT";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for sell-out
                ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = "=IFERROR(" + monthCell + ((33 + ctrModelMix) + (typeCount * 3)) + "/SUM(" + monthCell + (32 + (typeCount * 3)) + ":" + monthCellTo + (32 + (typeCount * 4)  - 1) + ")," + '"' + '"' + ")";
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();


                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }
                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }
                    ctrModelMix++;
                    ctrCCell++;
                }
                ws.Cell("C" + ctrCCell).Value = "SELL-IN AMOUNT";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for sell-in amount
                ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula
                        //loop each week in month (rangeDif is basically the number of weeks in the month)
                        var f = "=SUM(";
                        for (int i = 0; i <= rangeDif; i++)
                        {
                            //alphabeth
                            var colAlpha = HelperClass.IntToLetters(ctrMonth + i);
                            var sumIfs = "SUMIFS(" + colAlpha + "$" + startingRow + ":" + colAlpha + "$" + (endRow - 1) + "," + "$C$" + startingRow + ":" + "$C$" + (endRow - 1) + ",$E" + ctrCCell + "," + "$E$" + startingRow + ":" + "$E$" + (endRow - 1) + "," + '"' + "Invoice Revenue" + '"' + ")";

                            f += sumIfs + ",";
                        }
                        f = f.Remove(f.Length - 1);
                        f = f + ")";
                        //formula
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = f;
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();

                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }
                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }
                    ctrModelMix++;
                    ctrCCell++;
                }
                ws.Cell("C" + ctrCCell).Value = "SELL-OUT AMOUNT";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for sell-OUT amount
                ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif - 1));

                        //todo
                        //formula
                        //loop each week in month (rangeDif is basically the number of weeks in the month)
                        var f = "=SUM(";
                        //for (int i = 0; i <= rangeDif; i++)
                        //{
                        //    //alphabeth
                        //    var colAlpha = HelperClass.IntToLetters(ctrMonth + i);
                        //    var sumIfs = "SUMIFS(" + colAlpha + "$" + startingRow + ":" + colAlpha + "$" + endRow + "," + "$C$" + startingRow + ":" + "$C$" + endRow + ",$E" + ctrCCell + "," + "$E$" + startingRow + ":" + "$E$" + endRow + "," + '"' + "Gross Revenue" + '"' + ")";

                        //    f += sumIfs + ",";
                        //}
                        f = f.Remove(f.Length - 1);
                        f = f + ")";

                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = f;
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif - 1)) + ctrCCell).Merge();

                        //if (ctrModelMix == typeCount - 1)
                        //{
                        //    ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif - 1)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        //}
                        ctrM++;
                        ctrMonth += rangeDif;
                    }
                    ctrModelMix++;
                    ctrCCell++;
                }

                //after model mix
                ws.Cell("B" + ctrCCell).Value = "Sell In";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + (ctrCCell + 1)).Value = "Sell In(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Sell-In Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-In(Account+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-In Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    var letters1 = HelperClass.IntToLetters(ctr);
                    //Sell-In Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 0)).FormulaA1 = "=SUMIFS('Sell-In & Sell-Out Target_Q'!" + letters + ":" + letters + ",'Sell-In & Sell-Out Target_Q'!$A:$A,'Simulation File'!$D$5,'Sell-In & Sell-Out Target_Q'!$D:$D," + '"' + "Sell-In" + '"' + ")";

                    //Sell-in(Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS(" + letters + "$" + startingRow + ":" + letters + "$" + (endRow-1) + ",$" + letters1 + startingRow + ":$" + letters1  + (endRow -1 ) + ",$E" + ctrCCell + ")";

                    //Sell-in Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }

                ctrCCell += 3;

                ws.Cell("B" + ctrCCell).Value = "Sell Out";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + (ctrCCell + 1)).Value = "Sell-out FCST(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Sell-Out Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Sell-Out(Act+Fcst)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Sell-Out Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    var letters1 = HelperClass.IntToLetters(ctr);
                    //Sell-Out Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 0)).FormulaA1 = "=SUMIFS('Sell-In & Sell-Out Target_Q'!" + letters + ":" + letters + ",'Sell-In & Sell-Out Target_Q'!$A:$A,'Simulation File'!$D$5,'Sell-In & Sell-Out Target_Q'!$D:$D," + '"' + "Sell-Out" + '"' + ")";

                    //Sell-Out (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS(" + letters + "$" + startingRow + ":F" + "$" + letters1 + (endRow-1) + ",$" + startingRow + ":$" + letters1 + (endRow-1) + ",$E" + ctrCCell + ")";

                    //Sell-Out Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 0) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("B" + ctrCCell).Value = "Inv.";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();

                ws.Cell("D" + ctrCCell).Value = "EOH_EX(AP1)";
                ws.Cell("D" + (ctrCCell + 1)).Value = "EOH_EX(AP1)";

                ws.Cell("E" + ctrCCell).Value = "Inventory Target";
                ws.Cell("E" + (ctrCCell + 1)).Value = "Inventory Act+Fcst";
                ws.Cell("E" + (ctrCCell + 2)).Value = "Inventory Balance";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 5;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    var letters1 = HelperClass.IntToLetters(ctr);
                    //Inventory Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "";//"=" + HelperClass.IntToLetters(ctr) + (ctrCCell + 12);

                    //Inventory (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=SUMIFS(" + letters + "$" + startingRow + ":" + letters + "$" + (endRow - 1) + ",$" + letters1 + startingRow + ":$" + letters1 + (endRow - 1) + ",$E" + ctrCCell + ")";

                    //Inventory Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=" + HelperClass.IntToLetters(ctr) + (ctrCCell) + "-" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1);

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                ws.Cell("B" + ctrCCell).Value = "WOS (F4)";
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell + 2)).Column(1).Merge();
                ws.Range("B" + (ctrCCell - 11) + ":B" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C" + (ctrCCell - 11) + ":C" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + (ctrCCell - 11) + ":D" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + (ctrCCell - 11) + ":E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("D" + (ctrCCell + 1)).Value = "WOS_EX(AP1)";

                ws.Cell("E" + ctrCCell).Value = "WOS Target(F4)";
                ws.Cell("E" + (ctrCCell + 1)).Value = "WOS Act+Fcst(F4)";
                ws.Cell("E" + (ctrCCell + 2)).Value = "WOS Balance(F4)";
                ws.Range("E" + ctrCCell + ":" + "E" + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                ctr = 6;
                index = 6;
                foreach (var week in listOfWeeks)
                {
                    var letters = HelperClass.IntToLetters(index);
                    //Sell-Out Target
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell - 3) + "/AVERAGE(" + HelperClass.IntToLetters(ctr + 1) + (ctrCCell - 5) + ":" + HelperClass.IntToLetters(ctr + 4) + (ctrCCell - 5) + ")," + '"' + '"' + ")";

                    //Sell-Out (Act+Fcst)
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 1)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell - 2) + "/AVERAGE(" + HelperClass.IntToLetters(ctr + 1) + (ctrCCell - 5) + ":" + HelperClass.IntToLetters(ctr + 4) + (ctrCCell - 5) + ")," + '"' + '"' + ")";

                    //Sell-Out Balance
                    ws.Cell(HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).FormulaA1 = "=IFERROR(" + HelperClass.IntToLetters(ctr) + (ctrCCell + 1) + "/" + HelperClass.IntToLetters(ctr) + (ctrCCell) + "," + '"' + '"' + ")";

                    ws.Range(HelperClass.IntToLetters(ctr) + (ctrCCell) + ":" + HelperClass.IntToLetters(ctr) + (ctrCCell + 2)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ctr++;
                    index++;
                }
                ctrCCell += 3;

                //data
                ws.Cell("C" + ctrCCell).Value = "Type";
                ws.Cell("D" + ctrCCell).Value = "Model";
                ws.Cell("C" + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Cell("D" + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                //ws.Cell("C80").InsertData(dtSimulationData.Rows);
                ctrCCell += 1;

                //ws.Columns().AdjustToContents();
                ws.Column("A").Width = 20.43;
                ws.Column("B").Width = 9.86;
                ws.Column("C").Width = 11.29;
                ws.Column("D").Width = 22;
                ws.Column("E").Width = 25.57;
                ws.Column("F").Width = 12;
                ws.Column("G").Width = 12;
                ws.Column("H").Width = 12;
                ws.Column("I").Width = 12;
                ws.Column("J").Width = 12;
                ws.Column("K").Width = 12;
                ws.Column("L").Width = 12;
                ws.Column("M").Width = 12;
                ws.Column("N").Width = 12;
                ws.Column("O").Width = 12;
                ws.Column("P").Width = 12;
                ws.Column("Q").Width = 12;
                ws.Column("R").Width = 12;
                ws.Column("S").Width = 12;
                ws.Column("T").Width = 12;
                ws.Column("U").Width = 12;
                ws.Column("V").Width = 12;
                ws.Column("W").Width = 12;
                ws.Column("X").Width = 12;
                ws.Column("Y").Width = 12;
                ws.Column("z").Width = 12;
                #endregion
                #region simulationdata
                var ctrResultCols = 0;
                var ctrResultCols1 = ctrCCell;
                int catIndex = colHeaders.FindIndex(F => F == "Category");
                var lastModel = "";
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    var ctrCOls = 0;
                    var ctrCOls1 = 0;
                    if (lastModel != (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")])
                    {
                        if (lastModel != "")
                            ctrResultCols += 9;

                        lastModel = item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                    }
                    else
                    {
                        ws.Cell("C" + ctrResultCols1).Style.Font.FontColor = XLColor.White;
                        ws.Cell("D" + ctrResultCols1).Style.Font.FontColor = XLColor.White;
                    }
                    ws.Cell("C" + ctrResultCols1).Value = ((string)item[colHeaders.FindIndex(F => F.ToLower() == "type")]).ToString().Trim();
                    ws.Cell("D" + ctrResultCols1).Value = lastModel.Trim();
                    ws.Cell("E" + ctrResultCols1).Value = ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")]).ToString().Trim();

                    foreach (var cols in item)
                    {
                        if (notIncludedCols.Where(W => W == ctrCOls).Count() == 0)
                        {
                            var letter = "";// HelperClass.IntToLetters(6 + weekIndex);
                            var sLetter = "";// HelperClass.IntToLetters(6 + weekIndex);
                            var isEditable = false;
                            switch ((string)cols)//item[catIndex])
                            {
                                case "Sell-in(Act+Fcst)":
                                    var we1 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we1);
                                        isEditable = IsEditable(int.Parse(w));
                                        sLetter = HelperClass.IntToLetters(we1 + 6);
                                        if (isEditable)
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = item[colHeaders.FindIndex(F => F.ToLower() == w.ToString())];
                                            ws.Cell(sLetter + ctrResultCols1).Style.Fill.BackgroundColor = XLColor.Orange;
                                        }
                                        else
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Raw Data_Dealer_SellIn'!$G:$G" + ",'Raw Data_Dealer_SellIn'!$A:$A,'Simulation File'!$D$5,'Raw Data_Dealer_SellIn'!$F:$F,'Simulation File'!$D" + (ctrCCell + ctrResultCols) + ",'Raw Data_Dealer_SellIn'!$N:$N," + "'Simulation File'!" + letter + "$18" + ")";
                                        }

                                        we1++;
                                    }
                                    break;
                                case "Sell-Out(Act+Fcst)":
                                    var we4 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we4);
                                        isEditable = IsEditable(int.Parse(w));
                                        sLetter = HelperClass.IntToLetters(we4 + 6);
                                        if (isEditable)
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).Style.Fill.BackgroundColor = XLColor.Orange;
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = item[colHeaders.FindIndex(F => F.ToLower() == w.ToString())];
                                        }
                                        else
                                        {
                                            ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=SUMIFS('Raw Data_SO & INV (Weekly)'!$U:$U" + ",'Raw Data_SO & INV (Weekly)'!$L:$L,'Simulation File'!$D$5,'Raw Data_SO & INV (Weekly)'!$E:$E,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",'Raw Data_SO & INV (Weekly)'!$S:$S," + "'Simulation File'!" + letter + "$18" + ")";
                                        }
                                        we4++;
                                    }
                                    break;
                                case "Inventory Act+Fcst":
                                    var we7 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        if (we7 != 0)
                                        {
                                            letter = HelperClass.IntToLetters(6 + we7);
                                            isEditable = false;//IsEditable(int.Parse(w));
                                            sLetter = HelperClass.IntToLetters(we7 + 6);
                                            if (isEditable)
                                            {
                                                ws.Cell(sLetter + ctrResultCols1).Style.Fill.BackgroundColor = XLColor.Orange;
                                                ws.Cell(sLetter + ctrResultCols1).FormulaA1 = item[colHeaders.FindIndex(F => F.ToLower() == w.ToString())];
                                            }
                                            else
                                            {
                                                ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + HelperClass.IntToLetters((6 + we7) - 1) + ((ctrCCell + 2) + (ctrResultCols)) + "+" + letter + ((ctrCCell) + (ctrResultCols)) + "-" + letter + ((ctrCCell + 1) + (ctrResultCols));
                                            }
                                        }
                                        we7++;
                                    }
                                    break;
                                case "WOS(F4)":
                                    var we10 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we10);
                                        sLetter = HelperClass.IntToLetters(we10 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IFERROR(" + letter + ((ctrCCell + 2) + (ctrResultCols)) + "/AVERAGE(" + HelperClass.IntToLetters((6 + we10) + 1) + ((ctrCCell + 1) + (ctrResultCols)) + ":" + HelperClass.IntToLetters((6 + we10) + 4) + ((ctrCCell + 1) + (ctrResultCols)) + ")," + '"' + '"' + ")";
                                        we10++;
                                    }
                                    break;
                                case "Run Rate":
                                    var we11 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we11);
                                        sLetter = HelperClass.IntToLetters(we11 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=IFERROR(" + letter + ((ctrCCell + 6) + (ctrResultCols)) + "/" + letter + ((ctrCCell + 1) + (ctrResultCols)) + "," + '"' + '"' + ")";
                                        we11++;
                                    }
                                    break;
                                case "Flooring Target":
                                    var we12 = 0;
                                    letter = HelperClass.IntToLetters(6 + we12);
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(10 + we12);
                                        sLetter = HelperClass.IntToLetters(we12 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "";
                                        we12++;
                                    }
                                    break;
                                case "Flooring Actual":
                                    var we13 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        //letter = HelperClass.IntToLetters(6 + we13);
                                        sLetter = HelperClass.IntToLetters(we13 + 6);
                                        var f = "=SUMIFS(Flooring_Site_Transaction!$G:$G,Flooring_Site_Transaction!$P:$P,'Simulation File'!$D$5,Flooring_Site_Transaction!$E:$E,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ",Flooring_Site_Transaction!$N:$N,'Simulation File'!" + sLetter + "$18)";
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = f;
                                        we13++;
                                    }
                                    break;
                                case "Invoice Price":
                                    var we14 = 0;
                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we14);
                                        sLetter = HelperClass.IntToLetters(we14 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=AVERAGEIFS('Gross & Net Price_Transaction'!$K:$K,'Gross & Net Price_Transaction'!$F:$F,'Simulation File'!$D$5,'Gross & Net Price_Transaction'!$G:$G,'Simulation File'!$D" + (ctrCCell + (ctrResultCols)) + ")";
                                        we14++;
                                    }
                                    break;
                                case "Invoice Revenue":
                                    var we15 = 0;
                                    ws.Range("C" + (ctrResultCols1 - 8) + ":C" + ctrResultCols1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                                    ws.Range("D" + (ctrResultCols1 - 8) + ":D" + ctrResultCols1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                                    ws.Range("E" + (ctrResultCols1 - 8) + ":E" + ctrResultCols1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                                    foreach (var w in listOfWeeks)
                                    {
                                        letter = HelperClass.IntToLetters(6 + we15);
                                        sLetter = HelperClass.IntToLetters(we15 + 6);
                                        ws.Cell(sLetter + ctrResultCols1).FormulaA1 = "=" + letter + ((ctrCCell + 7) + (ctrResultCols)) + "*" + letter + ((ctrCCell) + (ctrResultCols));
                                        ws.Range(sLetter + (ctrResultCols1 - 15) + ":" + sLetter + ctrResultCols1).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                                        we15++;
                                    }
                                    break;

                            }
                            ctrCOls++;
                            //dr[ctrCOls] = item[ctrCOls];
                        }
                        ctrCOls1++;
                    }

                    //dtSimulationData.Rows.Add(dr);
                    ctrResultCols1++;
                }
                #endregion
                #endregion
                #region BuildKPIFAWorksheet
                var wsKPIFA = wb.Worksheets.Add("KPI_AP1 FA");

                SqlParameter[] paramKPI = {
                new SqlParameter("@weeks",string.Join(",",GetListOfMonths(dateFrom,dateTo))),
                new SqlParameter("@pgroup",ProductGroup),
                new SqlParameter("@account",GSCMAccount)
                    };

                DataTable dt = sql.getDTSQLStoredProc("spu_GET_KPI_AP1_TF", paramKPI);
                var q = (from row in dt.AsEnumerable()
                         group row by row.Field<string>("YearMonth") into grp
                         select grp);//O=>O.FirstOrDefault().Field<string>("YearWeek"));
                List<string> keys = q.Select(S => S.Key).ToList();

                wsKPIFA.Cell("B1").Value = "Year/Month";
                wsKPIFA.Cell("B2").Value = "Year/Week";
                wsKPIFA.Cell("A3").Value = "Product Group";
                wsKPIFA.Cell("B3").Value = "GSCM Account";
                var ctrKPIFA = 3;
                var weekCtr = 4;
                int tryWeek = -1;
                foreach (var month in q)
                {
                    foreach (var week in month)
                    {
                        var colLetter = HelperClass.IntToLetters(ctrKPIFA);
                        wsKPIFA.Cell(colLetter + "1").Value = month.FirstOrDefault().Field<string>("YearMonth");
                        if (!int.TryParse(week.Field<string>("YearWeek"), out tryWeek))
                        {
                            //var colLetterYearWeek = HelperClass.IntToLetters((weekCtr - month.Count()) );
                            wsKPIFA.Cell(colLetter + "2").Value = week.Field<string>("YearWeek");
                        }
                        else
                        {
                            //colLetter = HelperClass.IntToLetters(weekCtr);
                            wsKPIFA.Cell(colLetter + "2").Value = week[1];
                        }


                        ctrKPIFA++;
                        weekCtr++;
                    }
                }

                var q1 = from row in dt.AsEnumerable()
                         group row by new { ProductGroup = row.Field<string>("ProductGroup"), Account = row.Field<string>("GSCM Account") } into grp
                         select grp;

                var ctr1 = 3;
                var rowCtr = 4;
                foreach (var item in q1)
                {
                    foreach (var i in item)
                    {
                        var colLetter = HelperClass.IntToLetters(ctr1);
                        wsKPIFA.Cell(colLetter + "3").Value = "FA(1-8W)";
                        //row
                        wsKPIFA.Cell("A" + (rowCtr.ToString())).Value = i[2];
                        wsKPIFA.Cell("B" + (rowCtr.ToString())).Value = i[3];
                        wsKPIFA.Cell(colLetter + (rowCtr.ToString())).Value = i[4];
                        ctr1++;
                    }
                    rowCtr++;
                }
                wsKPIFA.Columns().AdjustToContents();

                #endregion
                #region BuildKPIAccountWorksheet
                SqlParameter[] paramKPIAccount = {
                new SqlParameter("@weeks",string.Join(",",GetListOfMonths(dateFrom,dateTo))),
                new SqlParameter("@pgroup",ProductGroup),
                new SqlParameter("@account",GSCMAccount)
                    };

                var wsKPIAccount = wb.Worksheets.Add("KPI_Account Sell-Out FA");
                DataTable dtKPIAccount = sql.getDTSQLStoredProc("spu_GET_KPI_AP1_Account_TF", paramKPIAccount);
                var qKPIAccount = (from row in dt.AsEnumerable()
                                   group row by row.Field<string>("YearMonth") into grp
                                   select grp);//O=>O.FirstOrDefault().Field<string>("YearWeek"));
                List<string> keysKPIAccount = qKPIAccount.Select(S => S.Key).ToList();

                wsKPIAccount.Cell("B1").Value = "Year/Month";
                wsKPIAccount.Cell("B2").Value = "Year/Week";
                wsKPIAccount.Cell("A3").Value = "Product Group";
                wsKPIAccount.Cell("B3").Value = "GSCM Account";
                var ctrKPIAccount = 3;
                var weekCtrKPIAccount = 4;
                int tryWeekKPIAccount = -1;
                foreach (var month in qKPIAccount)
                {
                    foreach (var week in month)
                    {
                        var colLetter = HelperClass.IntToLetters(ctrKPIAccount);
                        wsKPIAccount.Cell(colLetter + "1").Value = month.FirstOrDefault().Field<string>("YearMonth");
                        if (!int.TryParse(week.Field<string>("YearWeek"), out tryWeekKPIAccount))
                        {
                            //var colLetterYearWeek = HelperClass.IntToLetters((weekCtrKPIAccount - month.Count())) ;
                            wsKPIAccount.Cell(colLetter + "2").Value = week.Field<string>("YearWeek");
                        }
                        else
                        {
                            //colLetter = HelperClass.IntToLetters(weekCtrKPIAccount);
                            wsKPIAccount.Cell(colLetter + "2").Value = week[1];
                        }


                        ctrKPIAccount++;
                        weekCtrKPIAccount++;
                    }
                }

                var q1KPIAccount = from row in dt.AsEnumerable()
                                   group row by new { ProductGroup = row.Field<string>("ProductGroup"), Account = row.Field<string>("GSCM Account") } into grp
                                   select grp;

                var ctr1KPIAccount = 3;
                var rowCtrKPIAccount = 4;
                foreach (var item in q1)
                {
                    foreach (var i in item)
                    {
                        var colLetter = HelperClass.IntToLetters(ctr1KPIAccount);
                        wsKPIAccount.Cell(colLetter + "3").Value = "FA(1-8W, Mean)";
                        //row
                        wsKPIAccount.Cell("A" + (rowCtrKPIAccount.ToString())).Value = i[2];
                        wsKPIAccount.Cell("B" + (rowCtrKPIAccount.ToString())).Value = i[3];
                        wsKPIAccount.Cell(colLetter + (rowCtrKPIAccount.ToString())).Value = i[4];
                        ctr1KPIAccount++;
                    }
                    rowCtrKPIAccount++;
                }
                wsKPIAccount.Columns().AdjustToContents();

                #endregion
                #region OtherSheets
                wb.Worksheets.Add(dtRawDataSOAndInvWeekly, "Raw Data_SO & INV (Weekly)");
                wb.Worksheets.Add(dtSellIn, "Raw Data_Dealer_SellIn");
                wb.Worksheets.Add(dtChannelPSI, "Channel Level_Channel PSI Mgmt");
                wb.Worksheets.Add(dtFlooringSite, "Flooring_Site_Transaction");
                wb.Worksheets.Add(dtFlooringTarget, "Flooring Target_Transaction");
                wb.Worksheets.Add(dtGrossNetPrice, "Gross & Net Price_Transaction");
                wb.Worksheets.Add(dtSellInOutTargetQ, "Sell-In & Sell-Out Target_Q");
                wb.Worksheets.Add(dtSellInOutTargetA, "Sell-In & Sell-Out Target_A");
                wb.Worksheets.Add(dtWOSTarget, "WOS Target_Masterfile");
                wb.Worksheets.Add(dtTIM, "TIM_Transaction");
                #endregion
                #region hidesheets
                var wCtr = 0;
                foreach (IXLWorksheet wss in wb.Worksheets)
                {
                    if (wCtr != 0)
                        wss.Visibility = XLWorksheetVisibility.Hidden;

                    wCtr++;
                }
                #endregion
                
                var filename = "apisimulation" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";
                //Response.Clear();
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.AddHeader("content-disposition", "attachment;filename=\"" + filename + "\"");

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        //byte[] bytes = new byte[memoryStream.Length];
                        //memoryStream.Read(bytes, 0, (int)memoryStream.Length);
                        memoryStream.WriteTo(file);
                    }

                    //memoryStream.WriteTo(Response.OutputStream);
                    //memoryStream.Close();
                }

                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "AP1"
                });
                DB.SaveChanges();

                //Response.End();
                return new JsonResult()
                {
                    Data = filename
                };
            }
        }
        public ActionResult SimulationTransactions()
        {
            var q = from s in DB._SYS_SimulationTrx
                    where s.Simulation == "AP1"
                    select s;
            return PartialView("_SimulationTrx", q.ToList());
        }
        public ActionResult DownloadSimulationFile(string fileName)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + fileName), FileMode.Open, FileAccess.Read))
                {
                    file.CopyTo(memoryStream);
                }
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }
            Response.End();
            return RedirectToAction("Index");
        }
        public DataTable GetExportableData_KPI(string ProductGroup, string GSCMAccount, List<string> listOfWeeks, string sp, string key)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@account",GSCMAccount),//todo change swap this
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);


            //dtTransposeWeeks = GenerateGenericColumns(dtAP1Forecast.Columns);
            //var q = from row in dtAP1Forecast.AsEnumerable()
            //        group row by row.Field<string>("Week") into grp
            //        select grp;
            //List<string> keys = q.Select(S => S.Key).ToList();
            //dtTransposeWeeks = AddWeekColumns(dtTransposeWeeks, keys);
            //var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            //var weekColsCount = keys.Count;

            //var q1 = from row in dtAP1Forecast.AsEnumerable()
            //         group row by row.Field<string>(key) into grp
            //         select grp;

            //foreach (var itemGroup in q1)
            //{
            //    DataRow dr = dtTransposeWeeks.NewRow();
            //    for (int i = 0; i < genericColsCount; i++)
            //    {
            //        dr[i] = itemGroup.FirstOrDefault()[i];
            //    }
            //    var ctr = 0;

            //    var list = itemGroup.OrderBy(O => O.Field<string>("Week"));
            //    foreach (var item in list)
            //    {
            //        dr[genericColsCount + ctr] = item[genericColsCount + 1];
            //        ctr++;
            //    }


            //    dtTransposeWeeks.Rows.Add(dr);

            //}
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_FT(string ProductGroup, List<string> listOfWeeks, string sp, string storename, string dealer, string gscmaccount, string ap1, string pgroup, string model)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { StoreName = row.Field<string>(storename), Account = row.Field<string>(gscmaccount), Dealer = row.Field<string>(dealer), AP1 = row.Field<string>(ap1), ProductGroup = row.Field<string>(pgroup), Model = row.Field<string>(model) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_Keys(string ProductGroup, string GSCMAccount, List<string> listOfWeeks, string sp, string key1, string key2)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@account",GSCMAccount),//todo change swap this
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { Item = row.Field<string>(key1), Category = row.Field<string>(key2) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData1(string ProductGroup, string GSCMAccount, List<string> listOfWeeks, string sp)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@account",GSCMAccount),//todo change swap this
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week"), Type = row.Field<string>("Type") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by row.Field<string>("Item") into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Type") }))
                {
                    dr[genericColsCount + ctr] = item[genericColsCount + 1];
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData(string ProductGroup, string GSCMAccount, List<string> listOfWeeks, string sp, string key)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@account",GSCMAccount),//todo change swap this
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by row.Field<string>("Week") into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key).ToList();
            dtTransposeWeeks = AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by row.Field<string>(key) into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;

                var list = itemGroup.OrderBy(O => O.Field<string>("Week"));
                foreach (var item in list)
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    ctr++;
                }


                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        //for sellinsellouttargetamount
        public DataTable GetExportableDataForAmount(string ProductGroup, string GSCMAccount, List<string> listOfMonths, string sp, string key)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfMonths)),
                new SqlParameter("@account",GSCMAccount),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week"), Type = row.Field<string>("Type") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by row.Field<string>(key) into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;

                var list = itemGroup.OrderBy(O => O.Field<string>("Week"));
                foreach (var item in list)
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    ctr++;
                }


                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable AddWeekColumns(DataTable dt, List<string> weeks)
        {

            foreach (var itemGroup in weeks)
            {
                dt.Columns.Add(itemGroup);

            }
            return dt;
        }
        public DataTable GenerateGenericColumns(DataColumnCollection dtCols)
        {
            DataTable dt = new DataTable();
            foreach (DataColumn item in dtCols)
            {
                if (item.ColumnName.ToUpper() != "WEEK" && item.ColumnName.ToUpper() != "VALUE")
                    dt.Columns.Add(item.ColumnName);
            }
            return dt;
        }
        public List<string> GenerateGenericColumnsList(List<string> list,DataTable dt)
        {
            foreach (DataColumn item in dt.Columns)
            {
                if (item.ColumnName.ToUpper() != "WEEK" && item.ColumnName.ToUpper() != "VALUE")
                    list.Add(item.ColumnName);
            }


            return list;
        }
    }
}