﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text;
using SEPCO_SODOIT.ViewModels;
using System.Data;
using SEPCO_SODOIT.Helpers;
using SEPCO;
using System.Data.SqlClient;
using ClosedXML.Excel;
using Newtonsoft.Json;
using System.IO;

namespace SEPCO_SODOIT.Controllers
{
    public class BizSimulationGCController : Controller
    {
        BizSimulationsLogic myLogic = new BizSimulationsLogic();
        int DefaultPageSize = 10;
        ApplicationDbContext DB = new ApplicationDbContext();
        // GET: BizSimulationAP2
        List<string> Category = new List<string>();

        public string ProductType { get; private set; }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SimulationTransactions(int? page)
        {
            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            var q = (from s in DB._SYS_SimulationTrx
                     where s.Simulation == "AP2"
                     select s).OrderByDescending(O => O.ID);
            ViewBag.OnePageOfItems = q.ToPagedList(pageNumber, DefaultPageSize); // will only contain 25 products max because of the pageSize
            return PartialView("_SimulationTransactions");
        }
        public ActionResult LoadFilters()
        {

            return PartialView("_LoadFilters");
        }
        public JsonResult LoadSimulationData(string hiddenCategory, string pGroup, string pType, string model, DateTime? dateFrom, DateTime? dateTo, string columns)
        {
            Category.Add("BOH");
            Category.Add("ARRIVAL");
            Category.Add("AGING");
            Category.Add("SELL-IN GC");
            Category.Add("DEMAND");
            Category.Add("AP1");
            Category.Add("GC-AP2 BALANCE");
            Category.Add("AP2-AP1 BALANCE");
            Category.Add("GC RTF");
            Category.Add("RTF");
            Category.Add("SELL-OUT GC");
            Category.Add("SP2");
            Category.Add("SP1");
            Category.Add("GC-SP2 BALANCE");
            Category.Add("GC-SP1 BALANCE");
            Category.Add("INVENTORY GC");
            Category.Add("INVENTORY AP2/SP2");
            Category.Add("INVENTORY AP1/SP1");
            Category.Add("WOS GC (F4)");
            Category.Add("WOS AP2/SP2 (F4)");
            Category.Add("WOS AP1/SP1 (F4)");
            Category.Add("Run Rate");
            Category.Add("Flooring Target");
            Category.Add("Flooring Actual");
            Category.Add("N.Price");
            Category.Add("GC N.PRICE");


            List<ModelVM> data = new List<ModelVM>();
            DataTable dt = new DataTable();
            var getFirstCat = Category.FirstOrDefault(x => hiddenCategory.Contains(x));
            var getWeek = new List<String>();
            var allWeek = "";
            var getModelOmitZero = new List<String>();
            var toFilterModel = new List<String>();
            var allFilteredModel = "";
            DataAccessLayer sql = new DataAccessLayer();

            var hiddenCatList = hiddenCategory.Split(',');
            List<int> hiddenCatsForView = new List<int>();

            foreach (var col in columns.Split(','))
            {
                if (col != null)
                    dt.Columns.Add(col);
            }
            var weekFrom = int.Parse(dateFrom.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var weekTo = int.Parse(dateTo.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());

            for (var ctr = 0; weekFrom <= weekTo; ctr++)
            {
                var year = weekFrom.ToString().Substring(0, 4);
                var week = int.Parse(weekFrom.ToString().Substring(4, 2));
                var maxWeek = Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxWeek)
                {
                    weekFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxWeek).ToString().PadLeft(2, '0'));
                    dt.Columns.Add(weekFrom.ToString());
                    getWeek.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
                else
                {
                    dt.Columns.Add(weekFrom.ToString());
                    getWeek.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
            }
            // get the all week
            allWeek = string.Join(",", getWeek);

            //get the all Model except for model with EOL model status
            var getAllModel = DB._MF_ModelManagementMapping.Where(items => pGroup.Contains(items.ProductGroup) && (pType.Contains(items.ProductType) || pType == "") && (model.Contains(items.Model) || model == "") && items.ModelStatus != "EOL");

            //get the Model dont have 0 of total Values
            toFilterModel = getAllModel.Select(x => x.Model).AsEnumerable().Where(x =>
            {
                SqlParameter[] param = {
                                            new SqlParameter("@model",x),
                                            new SqlParameter("@allweek", allWeek),
                                    };
                string TotalofAllCategegory = sql.getSQLStoredProcString("spu_GC_GET_TotalValueofAllCategories", param);
                return double.Parse(TotalofAllCategegory.ToString()) >= 1;

            }).ToList();
            //get all Filtered Model
            allFilteredModel = string.Join(",", toFilterModel);

            List<ModelVM> ModelFiltered = new List<ModelVM>();

            //add Filtered Models and Add Total
            ModelFiltered = pType.Split(',').Select(x => new ModelVM { Model = "TOTAL", Type = x }).ToList();
            ModelFiltered.AddRange(
                 DB._MF_ModelManagementMapping.Where(m => (allFilteredModel.Contains(m.Model))).Select(x => new ModelVM
                 {
                     Type = x.ProductType,
                     Model = x.Model
                 }).OrderBy(or => or.Model));

            #region formulas
            var list = new List<object[]>();
            var ctrRowNum = 0;
            var ctrLastRow = Category.Count() * ModelFiltered.Count();
            var catOrdinal = columns.Split(',').ToList().FindIndex(F => F == "Category");

            foreach (ModelVM item in ModelFiltered)
            {
                foreach (var cats in Category)
                {
                    ctrRowNum++;
                    DataRow dr = dt.NewRow();
                    var ctrCol = columns.Split(',').Length;
                    if (hiddenCatList.Where(W => W.ToUpper() == cats.ToUpper()).Count() == 0)
                    {
                        hiddenCatsForView.Add(ctrRowNum - 1);
                    }

                    switch (cats)
                    {

                        case "BOH":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var catAlpha = HelperClass.IntToLetters(catOrdinal);
                                        dr[cols.Ordinal] = "0";
                                    }
                                    else
                                    {
                                        if (IsEditableJS(int.Parse(cols.ColumnName), "BOH"))
                                        {
                                            var colAlphaLastWeek = HelperClass.IntToLetters(cols.Ordinal + 1);
                                            dr[ctrCol] = "=" + colAlphaLastWeek + (ctrRowNum) + "+" + colAlphaLastWeek + (ctrRowNum + 1) + "-(IF(" + colAlphaLastWeek + (ctrRowNum + 3) + "<= SUM(" + colAlphaLastWeek + (ctrRowNum) + ":" + colAlphaLastWeek + (ctrRowNum + 1) + ")," + colAlphaLastWeek + (ctrRowNum + 3) + "," + "SUM(" + colAlphaLastWeek + (ctrRowNum) + ":" + colAlphaLastWeek + (ctrRowNum + 1) + ")))";

                                        }
                                        else
                                        {
                                            var value = from t in DB._TF_AP2PSI
                                                        where t.Category == "BOH" && t.Item == item.Model && t.Week == cols.ColumnName
                                                        select t.Value;
                                            dr[cols.Ordinal] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "ARRIVAL":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var catAlpha = HelperClass.IntToLetters(catOrdinal);
                                        dr[cols.Ordinal] = "0";
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AP2PSI
                                                    where t.Category.ToUpper() == "ARRIVAL" && t.Item == item.Model && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "AGING":
                            foreach (DataColumn cols in dt.Columns)
                            {


                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (cols.Ordinal > 9)
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var colAlphaNextWeekFrom = HelperClass.IntToLetters(cols.Ordinal - 6);
                                        dr[ctrCol] = "=IF(SUM(" + colAlphaNextWeekFrom + (ctrRowNum - 1) + ":" + colAlphaNextWeekFrom + (ctrRowNum - 2) + ") - SUM(" + colAlphaNextWeekFrom + (ctrRowNum + 1) + ":" + colAlpha + (ctrRowNum + 1) + ") < 0,' ',SUM(" + colAlphaNextWeekFrom + (ctrRowNum - 1) + ":" + colAlphaNextWeekFrom + (ctrRowNum - 2) + ") - SUM(" + colAlphaNextWeekFrom + (ctrRowNum + 1) + ":" + colAlpha + (ctrRowNum + 1) + "))";
                                        ctrCol++;
                                    }
                                    else
                                    {
                                        dr[ctrCol] = " ";
                                        ctrCol++;
                                    }

                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }

                            }
                            break;
                        case "SELL-IN GC":
                            //todo
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var catAlpha = HelperClass.IntToLetters(catOrdinal);
                                        dr[cols.Ordinal] = "0";
                                    }
                                    else
                                    {
                                        if(IsEditableJS(int.Parse(cols.ColumnName), "SELL-IN GC"))
                                        {
                                            var value = from t in DB._TF_AP2Forecast
                                                        where t.Item == item.Model && t.Week == cols.ColumnName
                                                        select t.Value;
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                        else
                                        {
                                            var value = from t in DB._TF_AccountLevelPSIMgmt
                                                        where t.Category.ToUpper() == "Sell-in(GC)" && t.Item == item.Model && t.Week == cols.ColumnName
                                                        select t.Value;
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "DEMAND":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AP2PSI.Where(myItem => myItem.Category == "GI" &&
                                        myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                          .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AP2PSI
                                                    where t.Category.ToUpper() == "GI" && t.Item == item.Model && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "AP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AP1Forecast.Where(myItem => myItem.Week == cols.ColumnName && myItem.Category == "AP1 FCST" && allFilteredModel.Contains(myItem.Item))
                                          .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = DB._TF_AP1Forecast.Where(myItem => myItem.Week == cols.ColumnName && myItem.Category == "AP1 FCST" && myItem.Item == item.Model)
                                             .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "GC-AP2 BALANCE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 3) + "-" + colAlpha + (ctrRowNum - 2);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "AP2-AP1 BALANCE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 3) + "-" + colAlpha + (ctrRowNum - 2);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "GC RTF":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=IF(" + colAlpha + (ctrRowNum - 5) + "<= SUM(" + colAlpha + (ctrRowNum - 8) + ":" + colAlpha + (ctrRowNum - 7) + ")," + colAlpha + (ctrRowNum - 5) + "," + "SUM(" + colAlpha + (ctrRowNum - 8) + ":" + colAlpha + (ctrRowNum - 7) + "))";
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "RTF":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    { 
                                        var value = DB._TF_AP2PSI.Where(myItem => myItem.Category == "RTF" &&
                                       myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                       .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AP2PSI
                                                    where t.Category.ToUpper() == "RTF" && t.Item == item.Model && t.Week == cols.ColumnName
                                                    select t.Value;

                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "SELL-OUT GC":
                            foreach (DataColumn cols in dt.Columns)
                            {

                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var catAlpha = HelperClass.IntToLetters(catOrdinal);
                                        dr[cols.Ordinal] = "0";
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Category.ToUpper() == "Sell-out FCST(GC)" && t.Item == item.Model && t.Week == cols.ColumnName
                                                    select t.Value;

                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "SP2":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "Sell-out FCST(AP2)" &&
                                        myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                        .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Category.ToUpper() == "Sell-out FCST(AP2)" && t.Item == item.Model && t.Week == cols.ColumnName
                                                    select t.Value;

                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "SP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "Sell-out FCST(AP1)" &&
                                        myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                        .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Item == item.Model && t.Category.ToLower() == "Sell-out FCST(AP1)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "GC-SP2 BALANCE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 3) + "-" + colAlpha + (ctrRowNum - 2);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "GC-SP1 BALANCE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 4) + "-" + colAlpha + (ctrRowNum - 2);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "INVENTORY GC":
                            foreach (DataColumn cols in dt.Columns)
                            {

                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                
                                    if (IsEditable(int.Parse(cols.ColumnName)))
                                    {
                                        var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);
                                        var colAlphaLastWeek = HelperClass.IntToLetters(cols.Ordinal);
                                        if (colAlphaLastWeek.Equals("C"))
                                        {
                                            dr[ctrCol] = "0";
                                        }
                                        else
                                        {
                                            dr[ctrCol] = "=" + colAlphaLastWeek + ctrRowNum + "+" + colAlpha + (ctrRowNum - 12) + "-" + colAlpha + (ctrRowNum - 5);
                                        }

                                    }
                                    else
                                     {
                                        if (item.Model.ToUpper() == "TOTAL")
                                        {
                                            var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "EOH_EX(GC)" && myItem.Week == cols.ColumnName
                                                        && allFilteredModel.Contains(myItem.Item)).Select(v => v.Value);
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                        else
                                        {
                                            var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "EOH_EX(GC)" && myItem.Week == cols.ColumnName
                                                      && myItem.Item == item.Model).Select(v => v.Value);
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                        
                                     }
                                  ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "INVENTORY AP2/SP2":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "EOH_EX(AP2)" &&
                                       myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                       .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");

                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Item == item.Model && t.Category.ToLower() == "EOH_EX(AP2)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "INVENTORY AP1/SP1":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_AccountLevelPSIMgmt.Where(myItem => myItem.Category == "EOH_EX(AP1)" &&
                                     myItem.Week == cols.ColumnName && allFilteredModel.Contains(myItem.Item))
                                     .Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_AccountLevelPSIMgmt
                                                    where t.Item == item.Model && t.Category.ToLower() == "EOH_EX(AP1)"
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "WOS GC (F4)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    int colsOrdinal = cols.Ordinal + 1;
                                    int SellOutRow = ctrRowNum - 8;
                                    int InventoryRow = ctrRowNum - 3;
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {

                                        dr[ctrCol] = myLogic.getWosFormulaTotal(allFilteredModel, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(GC)");
                                    }
                                    else
                                    {
                                        dr[ctrCol] = myLogic.getWosFormula(item.Model, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(GC)");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "WOS AP2/SP2 (F4)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    int colsOrdinal = cols.Ordinal + 1;
                                    int SellOutRow = ctrRowNum - 8;
                                    int InventoryRow = ctrRowNum - 3;
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {

                                        dr[ctrCol] = myLogic.getWosFormulaTotal(allFilteredModel, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP2)");
                                    }
                                    else
                                    {
                                        dr[ctrCol] = myLogic.getWosFormula(item.Model, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP2)");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "WOS AP1/SP1 (F4)":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    int colsOrdinal = cols.Ordinal + 1;
                                    int SellOutRow = ctrRowNum - 8;
                                    int InventoryRow = ctrRowNum - 3;
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {

                                        dr[ctrCol] = myLogic.getWosFormulaTotal(allFilteredModel, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP1)");
                                    }
                                    else
                                    {
                                        dr[ctrCol] = myLogic.getWosFormula(item.Model, colsOrdinal, cols.ColumnName, weekTo, dateTo, SellOutRow, InventoryRow, "Sell-out FCST(AP1)");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Run Rate":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var colAlpha = HelperClass.IntToLetters(cols.Ordinal + 1);

                                    dr[ctrCol] = "=" + colAlpha + (ctrRowNum - 9) + "/" + colAlpha + (ctrRowNum + 2);
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Flooring Target":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (item.Model.ToUpper() == "TOTAL")
                                    {
                                        var value = DB._TF_FlooringTarget.Where(myItem => myItem.Week == cols.ColumnName &&
                                        allFilteredModel.Contains(myItem.Model)).Select(v => v.Value);
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    else
                                    {
                                        var value = from t in DB._TF_FlooringTarget
                                                    where t.Model == item.Model
                                                    && t.Week == cols.ColumnName
                                                    select t.Value;
                                        dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "Flooring Actual":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    if (IsEditable(int.Parse(cols.ColumnName)))
                                    {
                                        var colAlphaLastWeek = HelperClass.IntToLetters(cols.Ordinal);
                                        if (colAlphaLastWeek.Equals("C"))
                                        {
                                            dr[ctrCol] = "0";
                                        }
                                        else
                                        {
                                            dr[ctrCol] = "=" + colAlphaLastWeek + (ctrRowNum);
                                        }


                                    }
                                    else
                                    {
                                        if (item.Model.ToUpper() == "TOTAL")
                                        {

                                            var value = DB._TF_FlooringSite.Where(myItem => myItem.Week == cols.ColumnName &&
                                            allFilteredModel.Contains(myItem.Model)).Select(v => v.Display);
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }
                                        else
                                        {
                                            var value = from t in DB._TF_FlooringSite
                                                        where t.Model == item.Model
                                                        && t.Week == cols.ColumnName
                                                        select t.Display;
                                            dr[ctrCol] = value.Count() == 0 ? "0" : value.Sum().ToString("0");
                                        }

                                    }
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "N.Price":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    var dtY = int.Parse(cols.ColumnName.Substring(0, 4));//year
                                    var dtW = int.Parse(cols.ColumnName.Substring(4, 2));//month
                                    var dtM = HelperClass.MonthOfYearByWeek(dtY, dtW);//month
                                    var value = from t in DB._TF_GrossNetPrice
                                                where t.Model == item.Model && t.ProductGroup.ToUpper() == pGroup
                                                && t.Month.ToUpper() == dtM.ToUpper() && t.Year == dtY
                                                select t.GrossPrice;
                                    var exchangeRate = from t in DB._TF_ExchangeRate
                                                       where t.Week == cols.ColumnName
                                                       select t.DollarExchangeRate;
                                    dr[ctrCol] = value.Count() == 0 ? "0" : (value.Sum() / exchangeRate.FirstOrDefault()).ToString("0");
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                        case "GC N.PRICE":
                            foreach (DataColumn cols in dt.Columns)
                            {
                                if (cols.Ordinal + 1 > ctrCol)
                                {
                                    dr[ctrCol] = "";
                                    ctrCol++;
                                }
                                else
                                {
                                    SetNonWeekColumns(cols, dr, item, cats, pGroup);
                                }
                            }
                            break;
                    }

                    dt.Rows.Add(dr);
                    list.Add(dr.ItemArray);
                }
            }
            #endregion
            List<string> columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToList();
            List<HeadersVM> colsFormat = new List<HeadersVM>();
            foreach (DataColumn colHead in dt.Columns)
            {
                if (colHead.Caption != "Type" && colHead.Caption != "Model" && colHead.Caption != "Category")
                    colsFormat.Add(new HeadersVM
                    {
                        type = "numeric",
                        data = colHead.ColumnName
                    });
                else
                    colsFormat.Add(new HeadersVM
                    {
                        type = "text",
                        data = colHead.ColumnName
                    });
            }

            DataVM dataVM = new DataVM();
            dataVM.FirstCategory = getFirstCat.ToString();
            dataVM.Data = list;
            dataVM.Count = ModelFiltered.Count();
            dataVM.Headers = columnNames;
            dataVM.HiddenRows = hiddenCatsForView;
            dataVM.ColsFormat = colsFormat;

            return Json(dataVM, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductGroup()
        {
            var q = from pGroup in DB._ProductGroup
                    select pGroup.GroupName;
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductGroupAttributes(string pGroup)
        {
            var q = from attri in DB._MF_ProductGroupAttributes
                    where attri.ProductGroup == pGroup
                    select new
                    {
                        Description = attri.Description,
                        Attribute = attri.AttributeColumn
                    };
            return Json(q, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductTypeList(string pGroup)
        {
            var prodType = from pt in DB._ProductType
                    where pGroup.Contains(pt.ProductGroup)
                    select new
                    {
                        Type = pt.ProductType
                    };

            return Json(prodType, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetModelListByType(string pType)
        {
            var modelList = (from m in DB._MF_ModelManagementMapping
                             where m.ProductType == pType
                             select new
                             {
                                 Model = m.Model,
                                 ID = m.ID
                             });

            return Json(modelList, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        private void SetNonWeekColumns(DataColumn cols, DataRow dr, ModelVM item, string cats, string pGroup)
        {
            switch (cols.ColumnName)
            {
                case "Type":
                    dr[cols.Ordinal] = item.Type;
                    break;
                case "Item":
                    dr[cols.Ordinal] = item.Model;
                    break;
                case "Category":
                    dr[cols.Ordinal] = cats;
                    break;
                default:

                    var q1 = from pAttri in DB._MF_ProductGroupAttributes
                             where pAttri.ProductGroup == pGroup && pAttri.Description == cols.Caption
                             select pAttri.AttributeColumn;
                    DataAccessLayer sql = new DataAccessLayer();
                    string cmd = "SELECT " + q1.FirstOrDefault() + " FROM MF_ModelAttributes WHERE Model=@model AND ProductGroup=@group";
                    SqlParameter[] param = {
                                                new SqlParameter("@model",item.Model),
                                                new SqlParameter("@group",pGroup)
                                                   };
                    var result = sql.getSQLDataReader(cmd, param);
                    dr[cols.Ordinal] = result;

                    break;
            }
        }
        public bool IsEditableFuture(int yearWeek)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());
            if (currentWeek < yearWeek)
                return true;
            else
                return false;
        }
        public bool IsEditable(int yearWeek)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());
            if (currentWeek <= yearWeek)
                return true;
            else
                return false;
        }
        public bool IsEditableJS(int yearWeek, string category)
        {
            var currentWeek = int.Parse(DateTime.Today.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(DateTime.Today).ToString());

            if (currentWeek == yearWeek && category.ToUpper() == "SELL-IN GC")
                return false;
            if (currentWeek == yearWeek && category.ToUpper() == "BOH")
                return false;
            if (currentWeek <= yearWeek)
                return true;
            else
                return false;
        }

        public DataTable AddWeekColumns(DataTable dt, List<string> weeks)
        {

            foreach (var itemGroup in weeks)
            {
                dt.Columns.Add(itemGroup);

            }
            return dt;
        }
        public DataTable GenerateGenericColumns(DataColumnCollection dtCols)
        {
            DataTable dt = new DataTable();
            foreach (DataColumn item in dtCols)
            {
                if (item.ColumnName.ToUpper() != "WEEK" && item.ColumnName.ToUpper() != "VALUE")
                    dt.Columns.Add(item.ColumnName);
            }
            return dt;
        }
        public List<string> GenerateGenericColumnsList(List<string> list, DataTable dt)
        {
            foreach (DataColumn item in dt.Columns)
            {
                if (item.ColumnName.ToUpper() != "WEEK" && item.ColumnName.ToUpper() != "VALUE")
                    list.Add(item.ColumnName);
            }


            return list;
        }

        public DataTable GetExportableData(string ProductGroup, List<string> listOfWeeks, string sp, string key)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by row.Field<string>("Week") into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by row.Field<string>(key) into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;

                var list = itemGroup.OrderBy(O => O.Field<string>("Week"));
                foreach (var item in list)
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    ctr++;
                }


                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_Keys(string ProductGroup, List<string> listOfWeeks, string sp, string key1, string key2)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { Item = row.Field<string>(key1), Category = row.Field<string>(key2) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;

        }
        public List<string> GetListOfMonths(DateTime? dateFrom, DateTime? dateTo)
        {
            List<string> listOfMonth = new List<string>();
            //month
            var monthFrom = int.Parse((dateFrom.Value.Year.ToString()) + ((dateFrom.Value.Month).ToString().PadLeft(2, '0'))); //;+ Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var monthTo = int.Parse((dateTo.Value.Year.ToString()) + ((dateTo.Value.Month).ToString().PadLeft(2, '0'))); //;+ Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());
            for (var ctr = 0; monthFrom <= monthTo; ctr++)
            {
                var year = monthFrom.ToString().Substring(0, 4);
                var week = int.Parse(monthFrom.ToString().Substring(4, 2));
                var maxMonth = 12;//Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxMonth)
                {
                    monthFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxMonth + 1).ToString().PadLeft(2, '0'));
                    listOfMonth.Add(monthFrom.ToString());
                }
                else
                {
                    listOfMonth.Add(monthFrom.ToString());
                    monthFrom += 1;
                }
            }


            return listOfMonth;

        }
        public DataTable GetExportableData_4Keys(string ProductGroup, List<string> listOfWeeks, string sp, string key1, string key2, string key3, string key4)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { ProductGroup = row.Field<string>(key3), Account = row.Field<string>(key4), Item = row.Field<string>(key1), Category = row.Field<string>(key2) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_WOS(string ProductGroup, List<string> listOfWeeks, string sp, string key1, string key2, string key3, string key4)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { ProductGroup = row.Field<string>(key4), Account = row.Field<string>(key2), Dealer = row.Field<string>(key3), AP1 = row.Field<string>(key1) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_5Keys(string ProductGroup, List<string> listOfWeeks, string sp, string key1, string key2, string key3, string key4, string key5)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { ProductGroup = row.Field<string>(key3), AP1 = row.Field<string>(key4), Account = row.Field<string>(key5), Item = row.Field<string>(key1), Category = row.Field<string>(key2) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;
                foreach (var item in itemGroup)//.OrderBy(O => new { Week = O.Field<string>("Week"), Type = O.Field<string>("Category") }))
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    //dtTransposeWeeks
                    ctr++;
                }
                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public DataTable GetExportableData_6KeysforFTinGC(string ProductGroup, List<string> listOfWeeks, string sp, string key1, string key2, string key3, string key4, string key5, string key6)
        {
            DataAccessLayer sql = new DataAccessLayer();
            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            DataTable dtTransposeWeeks = new DataTable();
            DataTable dt = sql.getDTSQLStoredProc(sp, param);

            dtTransposeWeeks = HelperClass.GenerateGenericColumns(dt.Columns);
            var q = from row in dt.AsEnumerable()
                    group row by new { Week = row.Field<string>("Week") } into grp
                    select grp;
            List<string> keys = q.Select(S => S.Key.Week).ToList();
            dtTransposeWeeks = HelperClass.AddWeekColumns(dtTransposeWeeks, keys);
            var genericColsCount = dtTransposeWeeks.Columns.Count - keys.Count;
            var weekColsCount = keys.Count;

            var q1 = from row in dt.AsEnumerable()
                     group row by new { StoreName = row.Field<string>(key6), Dealer = row.Field<string>(key5), ProductGroup = row.Field<string>(key2), AP1 = row.Field<string>(key4), Account = row.Field<string>(key3), Model = row.Field<string>(key1), Category = row.Field<string>(key2) } into grp
                     select grp;

            foreach (var itemGroup in q1)
            {
                DataRow dr = dtTransposeWeeks.NewRow();
                for (int i = 0; i < genericColsCount; i++)
                {
                    dr[i] = itemGroup.FirstOrDefault()[i];
                }
                var ctr = 0;

                var list = itemGroup.OrderBy(O => O.Field<string>("Week"));
                foreach (var item in list)
                {
                    dr[genericColsCount + ctr] = decimal.Parse(item[genericColsCount + 1].ToString());
                    ctr++;
                }


                dtTransposeWeeks.Rows.Add(dr);

            }
            return dtTransposeWeeks;
        }
        public ActionResult DownloadSimulationFile(string fileName)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + fileName), FileMode.Open, FileAccess.Read))
                {
                    file.CopyTo(memoryStream);
                }
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }
            Response.End();
            return RedirectToAction("Index");
        }
        public ActionResult DeleteFile(string filename)
        {
            try
            {
                DB._SYS_SimulationTrx.Remove(DB._SYS_SimulationTrx.Where(W => W.FileLocation == filename).FirstOrDefault());
                DB.SaveChanges();
                System.IO.File.Delete(Server.MapPath("~/Content/SimulationFiles/" + filename));
                return new JsonResult()
                {
                    Data = "Success"
                };
            }
            catch (Exception c)
            {
                return new JsonResult()
                {
                    Data = "Failed"
                };
            }
        }
        public ActionResult ExportSimulation(string ProductGroup, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            List<string> listOfWeeks = new List<string>();
            //weeks
            var weekFrom = int.Parse(dateFrom.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var weekTo = int.Parse(dateTo.Value.Year.ToString() + Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());
            for (var ctr = 0; weekFrom <= weekTo; ctr++)
            {
                var year = weekFrom.ToString().Substring(0, 4);
                var week = int.Parse(weekFrom.ToString().Substring(4, 2));
                var maxWeek = Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxWeek)
                {
                    weekFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxWeek).ToString().PadLeft(2, '0'));
                    listOfWeeks.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
                else
                {
                    listOfWeeks.Add(weekFrom.ToString());
                    weekFrom += 1;
                }
            }

            var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
            var colHeaders = columns.Split(',').ToList();
            DataTable dtSimulationData = new DataTable();
            List<int> notIncludedCols = new List<int>();
            var ctrCols = 0;
            foreach (var item in colHeaders)
            {
                if (item.ToLower() != "type" && item.ToLower() != "item" && item.ToLower() != "category" && !item.ToLower().Contains("20"))
                    notIncludedCols.Add(ctrCols);
                else
                    dtSimulationData.Columns.Add(item);

                ctrCols++;
            }
            DataAccessLayer sql = new DataAccessLayer();
            var listOfMonths = HelperClass.GetListOfMonths(dateFrom, dateTo);

            SqlParameter[] param = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };

            SqlParameter[] paramExchange = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks))
                    };

            //var data = System.Web.Helpers.Json.Decode(simulationData);
            DataTable dtAP2PSI = GetExportableData_Keys(ProductGroup, listOfWeeks, "spu_GC_GET_AP2PSITF", "Item", "Category");
            DataTable dtChannelPSI = GetExportableData_5Keys(ProductGroup, listOfWeeks, "spu_GC_GET_ChannelPSITF", "Item", "Category", "Product Grp", "AP1", "Account");
            SqlParameter[] paramSB = {
                new SqlParameter("@YEARMONTH",string.Join(",",listOfMonths)),
                new SqlParameter("@pgroup",ProductGroup),
                new SqlParameter("@ATYPE","PHP")
                    };
            DataTable dtSellInBilling = sql.getDTSQLStoredProc("spu_GC_GET_SellInBillingTF", paramSB);
            SqlParameter[] param1 = {
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtAP2Forecast = GetExportableData_4Keys(ProductGroup, listOfWeeks, "spu_GC_GET_AP2ForecastTF", "Item", "Category", "Product Grp", "Account");
            DataTable dtWOSTarget = GetExportableData_WOS(ProductGroup, listOfWeeks, "spu_GC_GET_WOSTargetMF", "AP1", "GSCM Account", "Dealer", "Product Group");
            SqlParameter[] paramST = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };
            SqlParameter[] paramFT = {
                new SqlParameter("@weeks",string.Join(",",listOfWeeks)),
                new SqlParameter("@pgroup",ProductGroup)
                    };
            DataTable dtSalesTarget = sql.getDTSQLStoredProc("spu_GC_GET_SalesTargetTF", paramST);
            DataTable dtFlooringSite = sql.getDTSQLStoredProc("spu_GC_GET_FlooringSiteTF", param);
            //DataTable dtFlooringTarget = GetExportableData(ProductGroup, listOfWeeks, "spu_ap2_GET_FlooringTargetTF", "Model");
            //DataTable dtFlooringTarget = GetExportableData_6KeysforFTinGC(ProductGroup, listOfWeeks, "spu_GC_GET_FlooringTargetTF", "Model","Product Group", "GSCM Account", "AP1","Dealer","Store Name");



            using (XLWorkbook wb = new XLWorkbook())
            {
                // Add a DataTable as a worksheet
                var ws = wb.Worksheets.Add("Simulation File");
                #region BuildSimulationSheet

                #region secondbox
                ws.Cell("B7").Value = "DA TOTAL";
                ws.Range("B7:B8").Column(1).Merge();
                ws.Cell("B7").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("B7:B8").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Range("C7:C8").Merge();
                ws.Cell("C7").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("C7:C8").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Range("D7:D8").Merge();
                ws.Cell("D7").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("D7:D8").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("E7").Value = ProductGroup;
                ws.Range("E7:E8").Merge();
                ws.Cell("E7").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                ws.Range("E7:E8").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;





                ws.Cell("B9").Value = "Revenue";
                ws.Range("B9:B21").Column(1).Merge();
                ws.Range("B9:B21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("C9:C21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D9:D21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E9:E21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("E9").Value = "LY";
                ws.Cell("E10").Value = "MP";
                ws.Cell("E11").Value = "SP";
                ws.Cell("E12").Value = "sireng";
                ws.Cell("E13").Value = "AP2+actual";
                ws.Cell("E14").Value = "Revenue / Week";
                ws.Cell("E15").Value = "GC";
                ws.Cell("E16").Value = "Revenue / Week";
                ws.Cell("E17").Value = "vs LY";
                ws.Cell("E18").Value = "vs MP";
                ws.Cell("E19").Value = "vs SP";
                ws.Cell("E20").Value = "vs Sireng";
                ws.Cell("E21").Value = "ASP";


                var ctrM = 0;
                var ctrMonth = 6;
                var monthList = GetListOfMonths(dateFrom, dateTo);
                foreach (var month in monthList)
                {
                    //compute the range week of month
                    //this is used to determine which cell will be the scope of each month
                    var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                    string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                    var letters = HelperClass.IntToLetters(ctrMonth);
                    //set month header styles
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "7").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                    ws.Cell(HelperClass.IntToLetters(ctrMonth) + "7").Value = monthName;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "7:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "7").Merge();
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "7:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "7").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "9:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "21").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "8:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "8").Style.Fill.BackgroundColor = XLColor.FromArgb(218, 238, 243);
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "8:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "8").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                    #region revenue by month
                    //LY
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "9").FormulaA1 = "=SUMIFS('Sell-In_Billing_Transaction'!$M:$M,'Sell-In_Billing_Transaction'!$B:$B,REF!F$4,'Sell-In_Billing_Transaction'!$I:$I,"+'"' +"E2"+'"'+")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "9:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "9").Merge();

                    //MP
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "10").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + '"' + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + '"' + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + '"' + ",'Sales Target_Transaction'!$D:$D," + '"' + ProductGroup + '"' + ",'Sales Target_Transaction'!$E:$E,$E3,'Sales Target_Transaction'!$F:$F,REF!"+month+")";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "10:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "10").Merge();

                    //SP
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "11").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "11:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "11").Merge();

                    //sering
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "12").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "12:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "12").Merge();

                    //AP2+actual
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "13").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "13:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "13").Merge();

                    //GC
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "15").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "15:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "15").Merge();

                    //vs LY
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "17").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "17:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "17").Merge();

                    //vs MP
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "18").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "18:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "18").Merge();

                    //vs SP
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "19").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "19:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "19").Merge();

                    //vs Sering
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "20").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "20:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "20").Merge();

                    //ASP
                    //ws.Cell(HelperClass.IntToLetters(ctrMonth) + "21").FormulaA1 = "=SUMIFS('Sales Target_Transaction'!$G:$G,'Sales Target_Transaction'!$A:$A," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$B:$B," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$C:$C," + '"' + "TOTAL" + "'" + ",'Sales Target_Transaction'!$D:$D," + '"' + "REF" + '"' + ",'Sales Target_Transaction'!$E:$E,$E10,'Sales Target_Transaction'!$F:$F,REF!F$5)";
                    ws.Range(HelperClass.IntToLetters(ctrMonth) + "21:" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + "21").Merge();

                    #endregion


                    //increment
                    ctrM++;
                    ctrMonth += rangeDif + 1;
                }



                //get the weeks
                //set week header
                //generate & set formula for the weekly basis computation
                var ctr = 6;
                var index = 3;
                foreach (var week in listOfWeeks)
                {
                    //set the week
                    //set the styles
                    ws.Cell(HelperClass.IntToLetters(ctr) + "8").Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                    ws.Cell(HelperClass.IntToLetters(ctr) + "8").Value = week;
                    ctr++;
                    index++;
                }



                var qModelMix = from PT in DB._ProductType
                                join PG in DB._ProductGroup
                                on PT.ProductGroup equals PG.GroupName
                                where PG.GroupName == ProductGroup
                                select PT;
                var typeCount = qModelMix.Count();

                #endregion
                //after model minx starting row
                //for sell-out target achievement
                var afterModelMixRow = 34 + (typeCount * 4) + 1;
                //simulation starting row
                var startingRow = 34 + (typeCount * 4) + 26;
                //total row count of items in simulation
                var simulationCount = result.Count;
                //simulation ending row
                var endRow = startingRow + simulationCount;


                var ctrCCell = 22;

                ws.Cell("B" + ctrCCell).Value = "Model Mix";
                ws.Cell("C" + ctrCCell).Style.Alignment.WrapText = true;
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("B" + ctrCCell + ":B" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Column(1).Merge();
                ws.Range("C" + ctrCCell + ":C" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell + ":D" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell + ":E" + (ctrCCell - 1 + (typeCount))).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                //loop each product type for Model Mix
                var ctrModelMix = 0;
                foreach (var pt in qModelMix)
                {
                    ws.Cell("E" + ctrCCell).Value = pt.ProductType;
                    ctrM = 0;
                    ctrMonth = 6;
                    foreach (var month in monthList)
                    {
                        var rangeDif = HelperClass.GetWeeksInMonthCount(month, ctrM, dateFrom, dateTo, monthList);
                        string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");

                        var monthCell = HelperClass.IntToLetters(ctrMonth);
                        var monthCellTo = HelperClass.IntToLetters(ctrMonth + (rangeDif));

                        //formula todo
                        ws.Cell(HelperClass.IntToLetters(ctrMonth) + ctrCCell).FormulaA1 = "-";
                        ws.Range(HelperClass.IntToLetters(ctrMonth) + ctrCCell + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Merge();

                        //set the border for sell-in based on product type count
                        if (ctrModelMix == typeCount - 1)
                        {
                            ws.Range(HelperClass.IntToLetters(ctrMonth) + ((ctrCCell - typeCount) + 1) + ":" + HelperClass.IntToLetters(ctrMonth + (rangeDif)) + ctrCCell).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                        }

                        ctrM++;
                        ctrMonth += rangeDif + 1;
                    }

                    ctrCCell++;
                    ctrModelMix++;
                }

                //get the months
                //set month header
                //generate & set formula for the monthly basis computation

                var ctrCCell2 = ctrCCell;
                var ctrCCell3 = ctrCCell;
                var ctrCCell4 = ctrCCell;

                ws.Cell("E" + ctrCCell3++).Value = "Sell-in(GC)";
                ws.Cell("E" + ctrCCell3++).Value = "Sell-in(AP2)";
                ws.Cell("E" + ctrCCell3++).Value = "Sell-in(AP1)";
                ws.Cell("E" + ctrCCell3++).Value = "GC-AP2";
                ws.Cell("E" + ctrCCell3++).Value = "GC-AP1";
                ws.Cell("E" + ctrCCell3++).Value = "Sell-out FCST(GC)";
                ws.Cell("E" + ctrCCell3++).Value = "Sell-out FCST(AP2)";
                ws.Cell("E" + ctrCCell3++).Value = "Sell-out FCST(AP1)";
                ws.Cell("E" + ctrCCell3++).Value = "GC-SP2";
                ws.Cell("E" + ctrCCell3++).Value = "GC-SP1";
                ws.Cell("E" + ctrCCell3++).Value = "EOH_EX(GC)";
                ws.Cell("E" + ctrCCell3++).Value = "EOH_EX(AP2)";
                ws.Cell("E" + ctrCCell3++).Value = "EOH_EX(AP1)";
                ws.Cell("E" + ctrCCell3++).Value = "TARGET";
                ws.Cell("E" + ctrCCell3++).Value = "WOS_EX(GC)";
                ws.Cell("E" + ctrCCell3++).Value = "WOS_EX(AP2)";
                ws.Cell("E" + ctrCCell3++).Value = "WOS_EX(AP1)";
                ws.Cell("E" + ctrCCell3++).Value = "TARGET vs GC";
                ws.Cell("E" + ctrCCell3++).Value = "TARGET vs AP2";
                ws.Cell("E" + ctrCCell3++).Value = "TARGET vs AP1";

                ws.Cell("B" + ctrCCell2).Value = "SCM Simulation";
                ws.Range("B" + ctrCCell2 + ":B" + (ctrCCell2 + 19)).Column(1).Merge();
                ws.Range("B" + ctrCCell2 + ":B" + (ctrCCell2 + 19)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("C" + ctrCCell).Value = "Sell In";
                ws.Range("C" + ctrCCell2 + ":C" + (ctrCCell2 + 4)).Column(1).Merge();
                ws.Range("C" + ctrCCell2 + ":C" + (ctrCCell2 + 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + ctrCCell2 + ":D" + (ctrCCell2 + 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + ctrCCell2 + ":E" + (ctrCCell2 + 4)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("C" + (ctrCCell2 + 5)).Value = "Sell Out";
                ws.Range("C" + (ctrCCell2 + 5) + ":C" + (ctrCCell2 + 9)).Column(1).Merge();
                ws.Range("C" + (ctrCCell2 + 5) + ":C" + (ctrCCell2 + 9)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + (ctrCCell2 + 5) + ":D" + (ctrCCell2 + 9)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + (ctrCCell2 + 5) + ":E" + (ctrCCell2 + 9)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;


                ws.Cell("C" + (ctrCCell2 + 10)).Value = "Inv.";
                ws.Range("C" + (ctrCCell2 + 10) + ":C" + (ctrCCell2 + 12)).Column(1).Merge();
                ws.Range("C" + (ctrCCell2 + 10) + ":C" + (ctrCCell2 + 12)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + (ctrCCell2 + 10) + ":D" + (ctrCCell2 + 12)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + (ctrCCell2 + 10) + ":E" + (ctrCCell2 + 12)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;

                ws.Cell("C" + (ctrCCell2 + 13)).Value = "WOS(F4)";
                ws.Range("C" + (ctrCCell2 + 13) + ":C" + (ctrCCell2 + 19)).Column(1).Merge();
                ws.Range("C" + (ctrCCell2 + 13) + ":C" + (ctrCCell2 + 19)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("D" + (ctrCCell2 + 13) + ":D" + (ctrCCell2 + 19)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
                ws.Range("E" + (ctrCCell2 + 13) + ":E" + (ctrCCell2 + 19)).Style.Border.OutsideBorder = XLBorderStyleValues.Medium;








                //ws.Columns().AdjustToContents();
                ws.Column("A").Width = 20.43;
                ws.Column("B").Width = 13.86;
                ws.Column("C").Width = 20;
                ws.Column("D").Width = 20;
                ws.Column("E").Width = 20;
                ws.Column("F").Width = 12;
                ws.Column("G").Width = 12;
                ws.Column("H").Width = 12;
                ws.Column("I").Width = 12;
                ws.Column("J").Width = 12;
                ws.Column("K").Width = 12;
                ws.Column("L").Width = 12;
                ws.Column("M").Width = 12;
                ws.Column("N").Width = 12;
                ws.Column("O").Width = 12;
                ws.Column("P").Width = 12;
                ws.Column("Q").Width = 12;
                ws.Column("R").Width = 12;
                ws.Column("S").Width = 12;
                ws.Column("T").Width = 12;
                ws.Column("U").Width = 12;
                ws.Column("V").Width = 12;
                ws.Column("W").Width = 12;
                ws.Column("X").Width = 12;
                ws.Column("Y").Width = 12;
                ws.Column("z").Width = 12;

                #endregion

                #region OtherSheets
                var ws1 = wb.Worksheets.Add("AP2 PSI_Transaction");
                ws1.Cell("B1").InsertTable(dtAP2PSI);
                var wsc1 = HelperClass.IntToLetters(dtAP2PSI.Columns.Count);
                ws1.Range("E2:" + wsc1 + (dtAP2PSI.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsAP2Fcst = wb.Worksheets.Add("AP2 FCST_Transaction");
                wsAP2Fcst.Cell("A1").InsertTable(dtAP2Forecast);
                var wsAP2FcstCol = HelperClass.IntToLetters(dtAP2Forecast.Columns.Count);
                wsAP2Fcst.Range("E2:" + wsAP2FcstCol + (dtAP2Forecast.Rows.Count + 1)).DataType = XLCellValues.Number;


                var wsChannelLevel = wb.Worksheets.Add("Channel Level_Channel PSI Mgmt");
                wsChannelLevel.Cell("B1").InsertTable(dtChannelPSI);
                var wsChannelLevelColAlpha = HelperClass.IntToLetters(dtChannelPSI.Columns.Count);
                wsChannelLevel.Range("G4:" + wsChannelLevelColAlpha + (dtChannelPSI.Rows.Count + 1)).DataType = XLCellValues.Number;

                wb.Worksheets.Add(dtSellInBilling, "Sell-In_Billing_Transaction");
                var wsST = wb.Worksheets.Add("Sales Target_Transaction");
                wsST.Cell("A1").InsertTable(dtSalesTarget);
                var wsSTCol = HelperClass.IntToLetters(dtSalesTarget.Columns.Count);
                wsST.Range("H2:" + wsSTCol + (dtSalesTarget.Rows.Count + 1)).DataType = XLCellValues.Number;

                var wsFlooringSite = wb.Worksheets.Add("Flooring_Site_Transaction");
                wsFlooringSite.Cell("A1").InsertTable(dtFlooringSite);

                var wsWOSTarget = wb.Worksheets.Add("WOS Target_Masterfile");
                wsWOSTarget.Cell("A1").InsertTable(dtWOSTarget);
                var wsWosTargetColAlpha = HelperClass.IntToLetters(dtWOSTarget.Columns.Count);
                wsWOSTarget.Range("E2:" + wsWosTargetColAlpha + (dtWOSTarget.Rows.Count + 1)).DataType = XLCellValues.Number;

                //var wsFlooringTarget = wb.Worksheets.Add("Flooring Target_Transaction");
                //wsFlooringTarget.Cell("A1").InsertTable(dtFlooringTarget);
                //var wsFlooringTargetColAlpha = HelperClass.IntToLetters(dtFlooringTarget.Columns.Count);
                //wsFlooringTarget.Range("I2:" + wsFlooringTargetColAlpha + (dtFlooringTarget.Rows.Count + 1));


                #endregion

                var filename = "apisimulation" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }

                }

                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "GC"
                });
                DB.SaveChanges();

                //Response.End();
                return new JsonResult()
                {
                    Data = filename

                };
            }
        }
        public ActionResult ExportSellIn(string ProductGroup, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            var dtSellInActFcst = new DataTable();
            dtSellInActFcst.Columns.Add("Product Grp");
            dtSellInActFcst.Columns.Add("Item");
            dtSellInActFcst.Columns.Add("Category");

            //get the index of future weeks
            var startOfEditableCells = 0;
            var headCtr = 0;
            var colHeaders = columns.Split(',').ToList();
            bool alreadAdded = false;
            foreach (var header in colHeaders)
            {
                int week;
                if (int.TryParse(header, out week))
                {
                    if (IsEditableFuture(week))
                    {
                        if (startOfEditableCells == 0)
                            startOfEditableCells = headCtr;

                        //add month total header
                        var monthOfCurrentWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)));
                        var monthOfNextWeek = "";
                        if (int.Parse(week.ToString().Substring(4, 2)) == 52)
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)) + 1, 1);
                        else
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)) + 1);

                        dtSellInActFcst.Columns.Add(week.ToString());

                        if (monthOfCurrentWeek != monthOfNextWeek)
                        {
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                            alreadAdded = true;
                        }
                        else
                        {
                            alreadAdded = false;
                        }


                        if ((colHeaders.Count - 1) == headCtr && !alreadAdded)
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                    }
                }
                headCtr++;
            }
            //get the sell in (act + fcst)
            if (startOfEditableCells != 0)
            {
                var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    if ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")] == "SELL-IN GC" && (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")] != "TOTAL")
                    {
                        DataRow dr = dtSellInActFcst.NewRow();
                        dr[0] = ProductGroup;
                        dr[1] = (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                        dr[2] = "GC FCST";

                        var ctr = 0;
                        var ctr1 = 0;
                        var ctr2 = 0;
                        var totalInMonth = 0M;
                        var alreadAddedValue = false;
                        foreach (var cols in item)
                        {
                            if (ctr1 >= startOfEditableCells)
                            {
                                //total in each month
                                var monthOfCurrentWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)));
                                var monthOfNextWeek = "";
                                if (int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) == 52)
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)) + 1, 1);
                                else
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) + 1);

                                dr[3 + ctr] = item[startOfEditableCells + ctr2];

                                ctr++;

                                //if same month, store sum
                                if (monthOfCurrentWeek == monthOfNextWeek)
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    alreadAddedValue = false;
                                }
                                else//if not same month, put the value and increment the ctr to match the week column
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    dr[3 + ctr] = totalInMonth;
                                    ctr++;
                                    alreadAddedValue = true;
                                    totalInMonth = 0;
                                }


                                //if last item but not last week of month
                                if ((item.Count - 1) == ctr1 && !alreadAddedValue)
                                    dr[3 + ctr] = totalInMonth;
                                ctr2++;
                            }
                            ctr1++;
                        }
                        dtSellInActFcst.Rows.Add(dr);
                    }
                }
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtSellInActFcst, "Export Button - Sell-In");
                var filename = "gcsimulation_sell_in" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }

                }
                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "GC_Sell-In"
                });
                DB.SaveChanges();

                return new JsonResult()
                {
                    Data = filename,
                };

            }
        }
        public ActionResult ExportSellOut(string ProductGroup, DateTime? dateFrom, DateTime? dateTo, string columns, string simulationData)
        {
            var dtSellInActFcst = new DataTable();
            dtSellInActFcst.Columns.Add("Product Grp");
            dtSellInActFcst.Columns.Add("Item");
            dtSellInActFcst.Columns.Add("Category");

            //get the index of future weeks
            var startOfEditableCells = 0;
            var headCtr = 0;
            var colHeaders = columns.Split(',').ToList();
            bool alreadAdded = false;
            foreach (var header in colHeaders)
            {
                int week;
                if (int.TryParse(header, out week))
                {
                    if (IsEditable(week))
                    {
                        if (startOfEditableCells == 0)
                            startOfEditableCells = headCtr;

                        //add month total header
                        var monthOfCurrentWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)));
                        var monthOfNextWeek = "";
                        if (int.Parse(week.ToString().Substring(4, 2)) == 52)
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)) + 1, 1);
                        else
                            monthOfNextWeek = HelperClass.MonthOfWeek(int.Parse(week.ToString().Substring(0, 4)), int.Parse(week.ToString().Substring(4, 2)) + 1);

                        dtSellInActFcst.Columns.Add(week.ToString());

                        if (monthOfCurrentWeek != monthOfNextWeek)
                        {
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                            alreadAdded = true;
                        }
                        else
                        {
                            alreadAdded = false;
                        }


                        if ((colHeaders.Count - 1) == headCtr && !alreadAdded)
                            dtSellInActFcst.Columns.Add("(-)" + monthOfCurrentWeek);
                    }
                }
                headCtr++;
            }
            //get the sell out (act + fcst)
            if (startOfEditableCells != 0)
            {
                var result = JsonConvert.DeserializeObject<dynamic>(simulationData);
                foreach (var item in result)
                {
                    //DataRow dr = dtSimulationData.NewRow();
                    if ((string)item[colHeaders.FindIndex(F => F.ToLower() == "category")] == "SELL-OUT GC" && (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")] != "TOTAL")
                    {
                        DataRow dr = dtSellInActFcst.NewRow();
                        dr[0] = ProductGroup;
                        dr[1] = (string)item[colHeaders.FindIndex(F => F.ToLower() == "item")];
                        dr[2] = "Sell-out FCST(GC)";

                        var ctr = 0;
                        var ctr1 = 0;
                        var ctr2 = 0;
                        var totalInMonth = 0M;
                        var alreadAddedValue = false;
                        foreach (var cols in item)
                        {
                            if (ctr1 >= startOfEditableCells)
                            {
                                //total in each month
                                var monthOfCurrentWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)));
                                var monthOfNextWeek = "";
                                if (int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) == 52)
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)) + 1, 1);
                                else
                                    monthOfNextWeek = HelperClass.MonthOfYearByWeek(int.Parse(colHeaders[ctr1].ToString().Substring(0, 4)), int.Parse(colHeaders[ctr1].ToString().Substring(4, 2)) + 1);

                                dr[3 + ctr] = item[startOfEditableCells + ctr2];

                                ctr++;

                                //if same month, store sum
                                if (monthOfCurrentWeek == monthOfNextWeek)
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    alreadAddedValue = false;
                                }
                                else//if not same month, put the value and increment the ctr to match the week column
                                {
                                    totalInMonth += decimal.Parse(item[startOfEditableCells + ctr2].ToString());
                                    dr[3 + ctr] = totalInMonth;
                                    ctr++;
                                    alreadAddedValue = true;
                                    totalInMonth = 0;
                                }


                                //if last item but not last week of month
                                if ((item.Count - 1) == ctr1 && !alreadAddedValue)
                                    dr[3 + ctr] = totalInMonth;
                                ctr2++;
                            }
                            ctr1++;
                        }
                        dtSellInActFcst.Rows.Add(dr);
                    }
                }
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dtSellInActFcst, "Export Button - Sell-Out");
                var filename = "gcsimulation_sell_out" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xlsx";

                // Flush the workbook to the Response.OutputStream
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    wb.SaveAs(memoryStream);
                    using (FileStream file = new FileStream(Server.MapPath("~/Content/SimulationFiles/" + filename), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        memoryStream.WriteTo(file);
                    }

                }
                DB._SYS_SimulationTrx.Add(new SYS_SimulationTransactions
                {
                    CreatedBy = "Admin",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    FileLocation = filename,
                    Simulation = "GC_Sell-Out"
                });
                DB.SaveChanges();

                return new JsonResult()
                {
                    Data = filename
                };

            }

        }
    }
}