﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.IO;
using System.Data;
using ClosedXML.Excel;
using System.Data.SqlClient;
using System.Configuration;
using SEPCO_SODOIT.Helpers;
using SEPCO;

namespace SEPCO_SODOIT.Controllers
{
    public class TransactionFilesController : Controller
    {
        int DefaultPageSize = 10;
        ApplicationDbContext DB = new ApplicationDbContext();
        // GET: TransactionFiles
        public ActionResult Index(int? page)
        {
            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            var OnePageOfItems = DB._SYS_Transactions.Where(W => W.FileType.ToUpper() == "TRANSACTION FILE").OrderByDescending(O => O.ID);

            ViewBag.OnePageOfItems = OnePageOfItems.ToPagedList(pageNumber, DefaultPageSize); // will only contain 25 products max because of the pageSize
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ImportExcel(string TransactionFileList, HttpPostedFileBase upload)
        {
            var user = "Administrator";
            if (ModelState.IsValid)
            {

                if (upload != null && upload.ContentLength > 0)
                {
                    var FileType = TransactionFileList;
                    Stream stream = upload.InputStream;
                    DataTable breakDownDT = new DataTable();

                    switch (FileType)
                    {
                        case "AccountLevelChannelPSITF":
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                DataColumn dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AP1", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Account", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Item", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Category", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Value", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                foreach (DataRow item in dt.Rows)
                                {
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        if (col.Ordinal >= 5)//Above 6th column are the weeks
                                        {
                                            DataRow row = breakDownDT.NewRow();
                                            row[0] = item[0].ToString();
                                            row[1] = item[1].ToString();
                                            row[2] = item[2].ToString();
                                            row[3] = item[3].ToString();
                                            row[4] = item[4].ToString();
                                            row[5] = col.ColumnName;//Week
                                            row[6] = item[col.Ordinal];//Week
                                            breakDownDT.Rows.Add(row);
                                        }
                                    }
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception e)
                            {

                            }
                            break;
                        case "AP1ForecastTransactionTF":
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                DataColumn dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Account", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Item", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Category", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Number", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                foreach (DataRow item in dt.Rows)
                                {
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        if (col.Ordinal >= 4)//5th or Above 5th column are the weeks
                                        {
                                            DataRow row = breakDownDT.NewRow();
                                            row[0] = item[1].ToString();
                                            row[1] = item[0].ToString();
                                            row[2] = item[2].ToString();
                                            row[3] = item[3].ToString();
                                            row[4] = col.ColumnName;//Week
                                            row[5] = item[col.Ordinal];//Value
                                            breakDownDT.Rows.Add(row);
                                        }
                                    }
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "AP2PSITF":
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                DataColumn dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Item", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Category", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Value", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                foreach (DataRow item in dt.Rows)
                                {
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        if (col.Ordinal >= 3)
                                        {
                                            DataRow row = breakDownDT.NewRow();
                                            row[0] = item[0].ToString();
                                            row[1] = item[1].ToString();
                                            row[2] = item[2].ToString();
                                            row[3] = col.ColumnName;//Week
                                            row[4] = item[col.Ordinal];//Value
                                            breakDownDT.Rows.Add(row);
                                        }
                                    }
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "FlooringTargetTF":
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                DataColumn dc = new DataColumn("StoreCode", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("StoreName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Dealer", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("GSCMAccount", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AP1", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProdyctType", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Model", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("DisplayTarget", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Value", typeof(Decimal));

                                breakDownDT.Columns.Add(dc);
                                foreach (DataRow item in dt.Rows)
                                {
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        if (col.Ordinal >= 9)//10th or Above 10th column are the weeks
                                        {
                                            DataRow row = breakDownDT.NewRow();
                                            row[0] = item[0].ToString();
                                            row[1] = item[1].ToString();
                                            row[2] = item[2].ToString();
                                            row[3] = item[3].ToString();
                                            row[4] = item[4].ToString();
                                            row[5] = item[5].ToString();
                                            row[6] = item[6].ToString();
                                            row[7] = item[7].ToString();
                                            row[8] = String.IsNullOrEmpty(item[8].ToString()) ? 0M : Decimal.Parse(item[8].ToString());
                                            row[9] = col.ColumnName;//Week
                                            row[10] = String.IsNullOrEmpty(item[col.Ordinal].ToString()) ? 0M : Decimal.Parse(item[col.Ordinal].ToString());
                                            breakDownDT.Rows.Add(row);
                                        }
                                    }

                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "KPIAP1FAProductGroupTF":
                            try
                            {
                                //Export to Data Table data of first 2 rows
                                DataTable dtYearMonthWeek = new DataTable();
                                DataColumn dc1 = new DataColumn("YearMonth", typeof(String));
                                dtYearMonthWeek.Columns.Add(dc1);
                                dc1 = new DataColumn("YearWeek", typeof(String));
                                dtYearMonthWeek.Columns.Add(dc1);
                                dtYearMonthWeek = HelperClass.ExcelToHorizontalDataTable(stream, 0, 2, false, 1);// sheet.Cells.Export(0, 2, 2, totalColumns1 - 2, false, false);

                                //// Exporting the data of the active worksheet to a new DataTable object
                                //DataTable dt = sheet.Cells.Export(3, 0, totalRows1-2, totalColumns1, false, true);
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 2, 0, false, 1);

                                //Bring down data
                                //DataTable breakDownDT = new DataTable();

                                DataColumn dc11 = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc11);
                                dc11 = new DataColumn("GSCMAccount", typeof(String));
                                breakDownDT.Columns.Add(dc11);
                                dc11 = new DataColumn("YearMonth", typeof(String));
                                breakDownDT.Columns.Add(dc11);
                                dc11 = new DataColumn("YearWeek", typeof(String));
                                breakDownDT.Columns.Add(dc11);
                                dc11 = new DataColumn("Value", typeof(Decimal));
                                breakDownDT.Columns.Add(dc11);

                                var yearMonthGroupCtr = 0;
                                foreach (var item in dtYearMonthWeek.AsEnumerable().GroupBy(G => G[0]))
                                {
                                    foreach (DataRow item1 in dt.Rows)
                                    {
                                        if (!String.IsNullOrEmpty(item1[0].ToString()))
                                        {
                                            var yearWeekCtr = 2;
                                            foreach (var item3 in item)
                                            {
                                                DataRow dr = breakDownDT.NewRow();
                                                dr[0] = item1[0].ToString();//Product Group
                                                dr[1] = item1[1].ToString();//Account
                                                dr[2] = item.FirstOrDefault()[0].ToString();//Year Month
                                                dr[3] = item3[1].ToString();//Year Week
                                                dr[4] = item1[yearWeekCtr + yearMonthGroupCtr].ToString();//Value
                                                breakDownDT.Rows.Add(dr);

                                                yearWeekCtr++;
                                            }
                                        }
                                    }
                                    yearMonthGroupCtr += (item.Count());
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "KPIAP2FAProductGroupTF":
                            try
                            {
                                //Export to Data Table data of first 2 rows
                                DataTable dtYearMonthWeek = new DataTable();
                                DataColumn dc1 = new DataColumn("YearMonth", typeof(String));
                                dtYearMonthWeek.Columns.Add(dc1);
                                dc1 = new DataColumn("YearWeek", typeof(String));
                                dtYearMonthWeek.Columns.Add(dc1);
                                dtYearMonthWeek = HelperClass.ExcelToHorizontalDataTable(stream, 0, 1, false, 1);// sheet.Cells.Export(0, 2, 2, totalColumns1 - 2, false, false);

                                //// Exporting the data of the active worksheet to a new DataTable object
                                //DataTable dt = sheet.Cells.Export(3, 0, totalRows1-2, totalColumns1, false, true);
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 2, 0, false, 1);

                                //Bring down data
                                //DataTable breakDownDT = new DataTable();

                                DataColumn dc11 = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc11);
                                dc11 = new DataColumn("YearMonth", typeof(String));
                                breakDownDT.Columns.Add(dc11);
                                dc11 = new DataColumn("YearWeek", typeof(String));
                                breakDownDT.Columns.Add(dc11);
                                dc11 = new DataColumn("Value", typeof(Decimal));
                                breakDownDT.Columns.Add(dc11);

                                var yearMonthGroupCtr = 0;
                                foreach (var item in dtYearMonthWeek.AsEnumerable().GroupBy(G => G[0]))
                                {
                                    foreach (DataRow item1 in dt.Rows)
                                    {
                                        if (!String.IsNullOrEmpty(item1[0].ToString()))
                                        {
                                            var yearWeekCtr = 1;
                                            foreach (var item3 in item)
                                            {
                                                DataRow dr = breakDownDT.NewRow();
                                                dr[0] = item1[0].ToString();//Product Group
                                                dr[1] = item.FirstOrDefault()[0].ToString();//Year Month
                                                dr[2] = item3[1].ToString();//Year Week
                                                dr[3] = item1[yearWeekCtr + yearMonthGroupCtr].ToString();//Value
                                                breakDownDT.Rows.Add(dr);

                                                yearWeekCtr++;
                                            }
                                        }
                                    }
                                    yearMonthGroupCtr += (item.Count());
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "KPIAccountSellOutFATF":
                            try
                            {
                                //Export to Data Table data of first 2 rows
                                DataTable dtYearMonthWeek1 = new DataTable();
                                DataColumn dc12 = new DataColumn("YearMonth", typeof(String));
                                dtYearMonthWeek1.Columns.Add(dc12);
                                dc12 = new DataColumn("YearWeek", typeof(String));
                                dtYearMonthWeek1.Columns.Add(dc12);
                                dtYearMonthWeek1 = HelperClass.ExcelToHorizontalDataTable(stream, 0, 2, false, 1);//sheet.Cells.Export(0, 2, 2, totalColumns1 - 2, false, false);

                                // Exporting the data of the active worksheet to a new DataTable object
                                //DataTable dt = sheet.Cells.Export(3, 0, totalRows1 - 2, totalColumns1, false, true);
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 2, 0, false, 1);

                                //Bring down data

                                DataColumn dc13 = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc13);
                                dc13 = new DataColumn("GSCMAccount", typeof(String));
                                breakDownDT.Columns.Add(dc13);
                                dc13 = new DataColumn("YearMonth", typeof(String));
                                breakDownDT.Columns.Add(dc13);
                                dc13 = new DataColumn("YearWeek", typeof(String));
                                breakDownDT.Columns.Add(dc13);
                                dc13 = new DataColumn("Value", typeof(Decimal));
                                breakDownDT.Columns.Add(dc13);

                                var yearMonthGroupCtr1 = 0;
                                foreach (var item in dtYearMonthWeek1.AsEnumerable().GroupBy(G => G[0]))
                                {
                                    foreach (DataRow item1 in dt.Rows)
                                    {
                                        if (!String.IsNullOrEmpty(item1[0].ToString()))
                                        {
                                            var yearWeekCtr = 2;
                                            foreach (var item3 in item)
                                            {
                                                DataRow dr = breakDownDT.NewRow();
                                                dr[0] = item1[0].ToString();//Product Group
                                                dr[1] = item1[1].ToString();//Account
                                                dr[2] = item.FirstOrDefault()[0].ToString();//Year Month
                                                dr[3] = item3[1].ToString();//Year Week
                                                dr[4] = item1[yearWeekCtr + yearMonthGroupCtr1].ToString();//Value
                                                breakDownDT.Rows.Add(dr);

                                                yearWeekCtr++;
                                            }
                                        }
                                    }
                                    yearMonthGroupCtr1 += (item.Count());
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "SellInBillingTF":
                            try
                            {
                                for (int i = 1; i <= 2; i++)
                                {
                                    if (i == 1)
                                    {

                                        FileType = "SellInBillingTF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, i);
                                        //Bring down data by Week Column
                                        DataColumn dc = new DataColumn("BusinessGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("YearMonth", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SoldToPartyCode", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Area", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Dealer", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP1", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SoldToPartyName", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Model", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Description", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Qty", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Amount", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AmountType", typeof(String));
                                        breakDownDT.Columns.Add(dc);

                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            var ctrCols = 0;
                                            DataRow row = breakDownDT.NewRow();
                                            foreach (DataColumn col in dt.Columns)
                                            {
                                                if (col.ColumnName.ToLower() == "qty" || col.ColumnName.ToLower() == "amount")
                                                {
                                                    decimal data = 0M;
                                                    decimal.TryParse(item[col.ColumnName].ToString().Replace(")", "").Replace("(", ""), out data);
                                                    row[ctrCols] = data; 

                                                }
                                                else
                                                {
                                                    row[ctrCols] = item[col.ColumnName].ToString(); 

                                                }
                                                ctrCols++;

                                            }
                                            row[13] = "PHP";
                                            breakDownDT.Rows.Add(row);
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);

                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                    else
                                    {

                                        FileType = "SellInBillingTF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, i);
                                        //Bring down data by Week Column
                                        breakDownDT.Rows.Clear();

                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            var ctrCols = 0;
                                            DataRow row = breakDownDT.NewRow();
                                            foreach (DataColumn col in dt.Columns)
                                            {
                                                if (col.ColumnName.ToLower() == "qty" || col.ColumnName.ToLower() == "amount")
                                                {
                                                    decimal data = 0M;
                                                    decimal.TryParse(item[col.ColumnName].ToString().Replace(")", "").Replace("(", ""), out data);
                                                    row[ctrCols] = data;
                                                }
                                                else
                                                {
                                                    row[ctrCols] = item[col.ColumnName].ToString();

                                                }
                                                ctrCols++;
                                            }
                                            row[13] = "USD";
                                            breakDownDT.Rows.Add(row);
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            //var tCode = HelperClass.TransactionFileID(FileType);
                                            //DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            //Col.DefaultValue = tCode;
                                            //breakDownDT.Columns.Add(Col);
                                            //Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            //Col.DefaultValue = user;
                                            //breakDownDT.Columns.Add(Col);

                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                }
                                return RedirectToAction("Index");

                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "SalesTargetTF":
                            try
                            {

                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                DataColumn dc = new DataColumn("AP1", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("GSCMAccount", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Dealer", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("TargetName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("YearMonth", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Amount", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);

                                //var ctr = 0;
                                //foreach (DataRow item in dt.Rows)
                                //{
                                //    var ctrCols = 0;
                                //    DataRow row = breakDownDT.NewRow();
                                //    foreach (DataColumn col in dt.Columns)
                                //    {

                                //        row[ctrCols] = item[col.Caption].ToString();
                                //        ctrCols++;
                                //    }
                                //    breakDownDT.Rows.Add(row);
                                //    ctr++;
                                //}
                                if (HelperClass.CheckDataTable(dt))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    dt.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    dt.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(dt, FileType, user);
                                }
                                return RedirectToAction("Index");

                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "SellInSellOutTargetTF":
                            try
                            {
                                for (int i = 1; i <= 2; i++)
                                {
                                    if (i == 1)
                                    {
                                        FileType = "SellInOut_Target(Amount)TF";
                                        DataTable dt = HelperClass.ExcelToDataTableCombineHeader(stream, 1, 0, true, i);
                                        //Bring down data by Week Column
                                        DataColumn dc = new DataColumn("Dealer", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Account", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP1", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Type", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Week", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Value", typeof(Decimal));

                                        breakDownDT.Columns.Add(dc);
                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            if (ctr != 0)
                                            {
                                                foreach (DataColumn col in dt.Columns)
                                                {
                                                    if (col.Ordinal >= 3)//4th or Above 4th column are the weeks
                                                    {
                                                        DataRow row = breakDownDT.NewRow();

                                                        row[0] = item[0].ToString();
                                                        row[1] = item[1].ToString();
                                                        row[2] = item[2].ToString();
                                                        row[3] = col.ColumnName.Split(',')[0];
                                                        row[4] = col.ColumnName.Split(',')[1];//Week
                                                        row[5] = item[col.Ordinal];//Value
                                                        breakDownDT.Rows.Add(row);
                                                    }
                                                }
                                            }
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);

                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                    else
                                    {

                                        FileType = "SellInOut_Target(Quantity)TF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, i);
                                        //Bring down data by Week Column
                                        DataColumn dc = new DataColumn("Dealer", typeof(String));
                                        breakDownDT.Columns.Clear();
                                        breakDownDT.Rows.Clear();
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Account", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP1", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Category", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Week", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Value", typeof(Decimal));

                                        breakDownDT.Columns.Add(dc);
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            foreach (DataColumn col in dt.Columns)
                                            {
                                                if (col.Ordinal >= 4)//4th or Above 4th column are the weeks
                                                {
                                                    DataRow row = breakDownDT.NewRow();
                                                    row[0] = item[0].ToString();
                                                    row[1] = item[1].ToString();
                                                    row[2] = item[2].ToString();
                                                    row[3] = item[3].ToString();
                                                    row[4] = col.ColumnName;//Week
                                                    row[5] = item[col.Ordinal];//Value
                                                    breakDownDT.Rows.Add(row);
                                                }
                                            }
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);

                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                }
                                return RedirectToAction("Index");
                                 
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "WOSTransactionTF":
                            try
                            {
                                for (int i = 1; i <= 2; i++)
                                {
                                    if (i == 1)
                                    {
                                        FileType = "WOSPerDealerTF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, i);
                                        //Bring down data by Week Column
                                        DataColumn dc = new DataColumn("ProductGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Account", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Channel", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Model", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Category", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Week", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Value", typeof(Decimal));

                                        breakDownDT.Columns.Add(dc);
                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            if (ctr != 0)
                                            {
                                                foreach (DataColumn col in dt.Columns)
                                                {
                                                    if (col.Ordinal >= 5)//4th or Above 4th column are the weeks
                                                    {
                                                        DataRow row = breakDownDT.NewRow();

                                                        row[0] = item[0].ToString();
                                                        row[1] = item[1].ToString();
                                                        row[2] = item[2].ToString();
                                                        row[3] = item[3].ToString();
                                                        row[4] = item[4].ToString();
                                                        row[5] = dt.Rows[0][col.Ordinal].ToString();//Week
                                                        row[6] = item[col.Ordinal];//Value
                                                        breakDownDT.Rows.Add(row);
                                                    }
                                                }
                                            }
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);

                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                    else
                                    {

                                        FileType = "WOSPerAP1AndAccountTF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, i);
                                        //Bring down data by Week Column
                                        breakDownDT.Rows.Clear();
                                        breakDownDT.Columns.Clear();
                                        DataColumn dc = new DataColumn("ProductGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AP1", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("GSCMAccount", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Model", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Category", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Week", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Value", typeof(Decimal));

                                        breakDownDT.Columns.Add(dc);
                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            if (ctr != 0)
                                            {
                                                foreach (DataColumn col in dt.Columns)
                                                {
                                                    if (col.Ordinal >= 5)//4th or Above 4th column are the weeks
                                                    {
                                                        DataRow row = breakDownDT.NewRow();

                                                        row[0] = item[0].ToString();
                                                        row[1] = item[1].ToString();
                                                        row[2] = item[2].ToString();
                                                        row[3] = item[3].ToString();
                                                        row[4] = item[4].ToString();
                                                        row[5] = dt.Rows[0][col.Ordinal].ToString();//Week
                                                        row[6] = item[col.Ordinal];//Value
                                                        breakDownDT.Rows.Add(row);
                                                    }
                                                }
                                            }
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);

                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                }
                                return RedirectToAction("Index");

                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "TIMTF":
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                DataColumn dc = new DataColumn("Dealer", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("GSCMAccount", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AP1", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Category", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Value", typeof(String));

                                breakDownDT.Columns.Add(dc);
                                foreach (DataRow item in dt.Rows)
                                {
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        if (col.Ordinal >= 4)
                                        {
                                            DataRow row = breakDownDT.NewRow();
                                            row[0] = item[0].ToString();
                                            row[1] = item[1].ToString();
                                            row[2] = item[2].ToString();
                                            row[3] = item[3].ToString();
                                            row[4] = col.ColumnName;//Week
                                            row[5] = item[col.Ordinal];//Value
                                            breakDownDT.Rows.Add(row);
                                        }
                                    }
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Invalid file");
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                        case "FlooringSiteTF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                DataColumn dc = new DataColumn("Brand", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SubCategory", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Capacity", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Model", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SellOut", typeof(Int32));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Display", typeof(Int32));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Inventory", typeof(Int32));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("StocksRequested", typeof(Int32));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("StocksDelivered", typeof(Int32));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("PromoterName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("StoreCode", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("StoreName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Agency", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Dealer", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Segment", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("TeamHead", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("MidasInCharge", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Area", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ModelStatus", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("BWL", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AP1Tagging", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                var ctr = 0;
                                foreach (DataRow item in dt.Rows)
                                {
                                    var ctrCols = 0;
                                    DataRow row = breakDownDT.NewRow();
                                    foreach (DataColumn col in dt.Columns)
                                    {

                                        row[ctrCols] = item[col.Caption].ToString();
                                        ctrCols++;
                                    }
                                    breakDownDT.Rows.Add(row);
                                    ctr++;
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "GrossNetPriceTF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                DataColumn dc = new DataColumn("Year", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Month", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AP1", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("GSCMAccount", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Dealer", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Model", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductType", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ModelClassification", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("GrossPrice", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("NetPrice", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);

                                var ctr = 0;
                                foreach (DataRow item in dt.Rows)
                                {
                                    var ctrCols = 0;
                                    DataRow row = breakDownDT.NewRow();
                                    foreach (DataColumn col in dt.Columns)
                                    {
                                        row[ctrCols] = item[col.Caption].ToString();
                                        ctrCols++;
                                    }
                                    breakDownDT.Rows.Add(row);
                                    ctr++;
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);

                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "SellOutInventoryTF":
                            #region
                            try
                            {
                                for (int i = 1; i <= 2; i++)
                                {
                                    if (i == 1)
                                    {
                                        FileType = "SellOutAndInventoryWeeklyTF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 2);
                                        //Bring down data by Week Column
                                        DataColumn dc = new DataColumn("DivisionGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Division", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Model", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Brand", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Alias", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Subsidiary", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelCD", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Channel", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelCategory", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Region", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("State", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("City", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("District", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("RRPPriceLoc", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Week", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Purchase", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Sales", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SellThru", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Inventory", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("DisplayQty", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SalePRC", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AMTSRRPLoc", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("InvoiceAmountUSD", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            var ctrCols = 0;
                                            DataRow row = breakDownDT.NewRow();
                                            foreach (DataColumn col in dt.Columns)
                                            {

                                                row[ctrCols] = item[col.Caption].ToString();
                                                ctrCols++;
                                            }
                                            breakDownDT.Rows.Add(row);
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);

                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                    else if (i == 2)
                                    {
                                        FileType = "SellOutAndInventoryMonthlyTF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                        //Bring down data by Week Column
                                        breakDownDT.Rows.Clear();
                                        breakDownDT.Columns.Clear();

                                        DataColumn dc = new DataColumn("DivisionGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Division", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Model", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Brand", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Alias", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Subsidiary", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelCD", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Channel", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelCategory", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Region", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("State", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("City", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("District", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("RRPPriceLoc", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Month", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Purchase", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Sales", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SellThru", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Inventory", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("DisplayQty", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SalePRC", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AMTSRRPLoc", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("InvoiceAmountUSD", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            var ctrCols = 0;
                                            DataRow row = breakDownDT.NewRow();
                                            foreach (DataColumn col in dt.Columns)
                                            {

                                                row[ctrCols] = item[col.Caption].ToString();
                                                ctrCols++;
                                            }
                                            breakDownDT.Rows.Add(row);
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);
                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "SellOutTF":
                            #region
                            try
                            {
                                for (int i = 1; i <= 2; i++)
                                {
                                    if (i == 1)
                                    {
                                        FileType = "SellOutMonthlyTF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                        //Bring down data by Week Column
                                        DataColumn dc = new DataColumn("Subsidiary", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("DivisionGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Division", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Brand", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Model", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Alias", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("PromoterID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Promoter", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("MobileNo", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Region", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("State", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("City", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("District", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("NationCD", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelCode", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Channel", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Contact", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("PhoneNo", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelCategory", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ShipmentType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AggregationtyType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Month", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("RRPPriceLoc", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Vat", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("RRPLoc", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("RRPAmountLoc", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SKUSales", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("IMEISales", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Sales", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            var ctrCols = 0;
                                            DataRow row = breakDownDT.NewRow();
                                            foreach (DataColumn col in dt.Columns)
                                            {

                                                row[ctrCols] = item[col.Caption].ToString();
                                                ctrCols++;
                                            }
                                            breakDownDT.Rows.Add(row);
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);

                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                    else if (i == 2)
                                    {
                                        FileType = "SellOutWeeklyTF";
                                        DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                        //Bring down data by Week Column
                                        breakDownDT.Rows.Clear();
                                        breakDownDT.Columns.Clear();

                                        DataColumn dc = new DataColumn("Subsidiary", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("DivisionGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Division", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ProductType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Brand", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Model", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Alias", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("PromoterID", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Promoter", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("MobileNo", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Region", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("State", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("City", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("District", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("NationCD", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelCode", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Channel", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Contact", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("PhoneNo", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelCategory", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ShipmentType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("AggregationtyType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelType", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("ChannelGroup", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Week", typeof(String));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("RRPPriceLoc", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Vat", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("RRPLoc", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("RRPAmountLoc", typeof(Decimal));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("SKUSales", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("IMEISales", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        dc = new DataColumn("Sales", typeof(Int32));
                                        breakDownDT.Columns.Add(dc);
                                        var ctr = 0;
                                        foreach (DataRow item in dt.Rows)
                                        {
                                            var ctrCols = 0;
                                            DataRow row = breakDownDT.NewRow();
                                            foreach (DataColumn col in dt.Columns)
                                            {

                                                row[ctrCols] = item[col.Caption].ToString();
                                                ctrCols++;
                                            }
                                            breakDownDT.Rows.Add(row);
                                            ctr++;
                                        }
                                        if (HelperClass.CheckDataTable(breakDownDT))//valid
                                        {
                                            //populate the transaction code
                                            var tCode = HelperClass.TransactionFileID(FileType);
                                            DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                            Col.DefaultValue = tCode;
                                            breakDownDT.Columns.Add(Col);
                                            Col = new DataColumn("User", System.Type.GetType("System.String"));
                                            Col.DefaultValue = user;
                                            breakDownDT.Columns.Add(Col);
                                            //save in sql
                                            DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "ExchangeRateTF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                breakDownDT.Rows.Clear();
                                breakDownDT.Columns.Clear();

                                DataColumn dc = new DataColumn("Week", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Month", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Year", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("DollarExchangeRate", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                var ctr = 0;
                                foreach (DataRow item in dt.Rows)
                                {
                                    var ctrCols = 0;
                                    DataRow row = breakDownDT.NewRow();
                                    foreach (DataColumn col in dt.Columns)
                                    {

                                        row[ctrCols] = item[col.Caption].ToString();
                                        ctrCols++;
                                    }
                                    breakDownDT.Rows.Add(row);
                                    ctr++;
                                }
                                if (HelperClass.CheckDataTable(breakDownDT))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    breakDownDT.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    breakDownDT.Columns.Add(Col);
                                    //save in sql
                                    DataAccessLayer.SaveDataTF(breakDownDT, FileType, user);
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        case "SellInGITF":
                            #region
                            try
                            {
                                DataTable dt = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                                //Bring down data by Week Column
                                breakDownDT.Rows.Clear();
                                breakDownDT.Columns.Clear();

                                DataColumn dc = new DataColumn("GC", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AP2", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AP1", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("GSCMAccount", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Model", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SalesQty", typeof(Int32));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AmountUSD", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("AmountKRW", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Currency", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Amount", typeof(Decimal));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("EntryDate", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SalesWeek", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SoldToPartyName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ShipToName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Plant", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Storage", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductCode", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("BillToParty", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SoldToParty", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Company", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SalesOrg", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SalesOffice", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ProductType", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attb01", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attb02", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Attb03", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Buyer", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Type", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ToolName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("BasicName", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("Project", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("ModelGroup", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("DistributionChannel", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("DCDesc", typeof(String));
                                breakDownDT.Columns.Add(dc);
                                dc = new DataColumn("SalesDocType", typeof(String));
                                breakDownDT.Columns.Add(dc);

                                //var ctr = 0;
                                //foreach (DataRow item in dt.Rows)
                                //{
                                //    var ctrCols = 0;
                                //    DataRow row = breakDownDT.NewRow();
                                //    foreach (DataColumn col in dt.Columns)
                                //    {

                                //        row[ctrCols] = item[col.Caption].ToString();
                                //        ctrCols++;
                                //    }
                                //    breakDownDT.Rows.Add(row);
                                //    ctr++;
                                //}
                                if (HelperClass.CheckDataTable(dt))//valid
                                {
                                    //populate the transaction code
                                    var tCode = HelperClass.TransactionFileID(FileType);
                                    DataColumn Col = new DataColumn("TransactionID", System.Type.GetType("System.Int32"));
                                    Col.DefaultValue = tCode;
                                    dt.Columns.Add(Col);
                                    Col = new DataColumn("User", System.Type.GetType("System.String"));
                                    Col.DefaultValue = user;
                                    dt.Columns.Add(Col);
                                    //save in sql
                                    DataAccessLayer.SaveDataTF(dt, FileType, user);
                                }
                            }
                            catch (Exception e)
                            {
                                //Error
                            }
                            #endregion
                            break;
                        default:
                            breakDownDT = HelperClass.ExcelToDataTable(stream, 0, 0, true, 1);
                            break;

                    }

                   
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int TransID, string FileType)
        {
            switch (FileType)
            {
                case "AccountLevelChannelPSITF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_AccountLevelPSIMgmt.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_AccountLevelPSIMgmt.RemoveRange(toRemove);

                    }
                    catch (Exception e)
                    {
                        //error
                    }

                    #endregion
                    break;
                case "AP1ForecastTransactionTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_AP1Forecast.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_AP1Forecast.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //error
                    }

                    #endregion
                    break;
                case "AP2PSITF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_AP2PSI.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_AP2PSI.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "ExchangeRateTF":
                    #region
                    try
                    {
                        //todo
                        //var toRemove = DB.ToList().Where(W => W.TransactionID == TransID);
                        //var toRemoveTrans = DB._SYS_Transactions.ToList().Where(W => W.ID == TransID);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "FlooringTargetTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_FlooringTarget.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_FlooringTarget.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "FlooringSiteTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_FlooringSite.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_FlooringSite.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "GrossNetPriceTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_GrossNetPrice.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_GrossNetPrice.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "KPIAP1FAProductGroupTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_KPIAP1FA.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_KPIAP1FA.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "KPIAP2FAProductGroupTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_KPIAP2FA.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_KPIAP2FA.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "KPIAccountSellOutFATF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_KPIAccountSellOutFA.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_KPIAccountSellOutFA.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "SalesTargetTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_SalesTarget.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_SalesTarget.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "SellInSellOutTargetAmountTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_SellInSellOutTargetAmount.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_SellInSellOutTargetAmount.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "SellInSellOutTargetQtyTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_SellInSellOutTargetQty.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_SellInSellOutTargetQty.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "SellInBillingTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_SellinBilling.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_SellinBilling.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "SellInGITF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_SellInGI.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_SellInGI.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "SellOutInventoryTF":
                    #region
                    try
                    {
                        var toRemove = DB._MF_YearDateMapping.ToList().Where(W => W.TransactionID == TransID);
                        DB._MF_YearDateMapping.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "SellOutWeeklyTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_SellOutWeekly.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_SellOutWeekly.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "SellOutMonthlyTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_SellOutMonthly.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_SellOutMonthly.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "TIMTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_TIM.ToList().Where(W => W.TransactionID == TransID);
                        DB._TF_TIM.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "WOSPerDealerTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_WOSPerDealer.ToList().Where(W => W.TransactionID == TransID);

                        DB._TF_WOSPerDealer.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
                case "WOSPerAP1AndAccountTF":
                    #region
                    try
                    {
                        var toRemove = DB._TF_WOSPerAP1AndAccount.ToList().Where(W => W.TransactionID == TransID);

                        DB._TF_WOSPerAP1AndAccount.RemoveRange(toRemove);
                    }
                    catch (Exception e)
                    {
                        //Error
                    }
                    #endregion
                    break;
            }
            var toRemoveTrans = DB._SYS_Transactions.ToList().Where(W => W.ID == TransID);
            DB._SYS_Transactions.RemoveRange(toRemoveTrans);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
        
    }
}