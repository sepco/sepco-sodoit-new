﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPCO_SODOIT.Controllers
{
    public class MFDealerAccountMappingController : Controller
    {
        ApplicationDbContext DB = new ApplicationDbContext();
        public ActionResult Index()
        {
            var dam = DB._MF_DealerAndAccountStoreMapping.ToList();
            return View(dam);
        }
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Dealer = new SelectList(DB._SYS_DEALER.ToList(), "DealerName", "DealerName");

            ViewBag.GSCMAccount = new SelectList(DB._SYS_GSCMACCOUNT.ToList(), "AccountName", "AccountName");
            ViewBag.SiteGroup = new SelectList(DB._SYS_SITEGROUP.ToList(), "SiteGroupName", "SiteGroupName");
            return View();
        }
        [HttpPost]
        public ActionResult getSiteGroupID(string sitegroup)
        {
            var q = from p in DB._SYS_SITEGROUP
                    where p.SiteGroupName == sitegroup
                    select new
                    {
                        id = p.SiteGroupID
                    };
            return Json(q);
        }
        [HttpPost]
        public ActionResult getGSCMAccountID(string account)
        {
            var q = from p in DB._SYS_GSCMACCOUNT
                    where p.AccountName == account
                    select new
                    {
                        id = p.GSCMAccountID
                    };
            return Json(q);
        }
        [HttpPost]
        public ActionResult Create(MF_DealerAndAccountMapping dam)
        {
            DateTime dc = System.DateTime.Now;
            DateTime dm = System.DateTime.Now;

            dam.DateCreated = dc;
            dam.DateModified = dm;
            dam.CreatedBy = "Administrator";
            dam.ModifiedBy = "Administrator";
            dam.TransactionID = 1;


            DB._MF_DealerAndAccountMapping.Add(dam);
            DB.SaveChanges();

            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {

            MF_DealerAndAccountMapping damtoedit = DB._MF_DealerAndAccountMapping.Find(id);
            ViewBag.Dealer = new SelectList(DB._SYS_DEALER.ToList(), "DealerName", "DealerName", damtoedit.Dealer);
            ViewBag.GSCMAccount = new SelectList(DB._SYS_GSCMACCOUNT.ToList(), "AccountName", "AccountName", damtoedit.GSCMAccount);
            ViewBag.SiteGroupName = new SelectList(DB._SYS_SITEGROUP.ToList(), "SiteGroupName", "SiteGroupName", damtoedit.SiteGroup);

            return View(damtoedit);

        }
        [HttpPost]
        public ActionResult Edit(MF_DealerAndAccountMapping dam)
        {
            DateTime dc = System.DateTime.Now;
            DateTime dm = System.DateTime.Now;

            dam.DateCreated = dc;
            dam.DateModified = dm;
            dam.CreatedBy = "Administrator";
            dam.ModifiedBy = "Administrator";
            dam.TransactionID = 1;
            DB.Entry(dam).State = EntityState.Modified;
            DB.SaveChanges();

            return RedirectToAction("Index");
        }
        [HttpGet]
        [ActionName("Delete")]
        public ActionResult DeleteGet(int id)
        {
            var damtodelete = DB._MF_DealerAndAccountMapping.Find(id);
            return View(damtodelete);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            var damtodelete = DB._MF_DealerAndAccountMapping.Find(id);
            DB._MF_DealerAndAccountMapping.Remove(damtodelete);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}