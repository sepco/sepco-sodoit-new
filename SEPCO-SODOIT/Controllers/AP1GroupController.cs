﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPCO_SODOIT.Controllers
{
    public class AP1GroupController : Controller
    {
        ApplicationDbContext DB = new ApplicationDbContext();
        public ActionResult Index()
        {
            var AP1GroupList = DB._SYS_AP1GROUP.ToList();
            return View(AP1GroupList);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(AP1Group ap1)
        {
            if (ModelState.IsValid)
            {
                DB._SYS_AP1GROUP.Add(ap1);
                DB.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var ap1toedit = DB._SYS_AP1GROUP.Find(id);
            return View(ap1toedit);
        }
        [HttpPost]
        public ActionResult Edit(AP1Group ap1)
        {
            if (ModelState.IsValid)
            {
                DB.Entry(ap1).State = EntityState.Modified;
                DB.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        [ActionName("Delete")]
        public ActionResult DeleteGet(int id)
        {
            var ap1todelete = DB._SYS_AP1GROUP.Find(id);
            return View(ap1todelete);
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            var ap1todelete = DB._SYS_AP1GROUP.Find(id);
            DB._SYS_AP1GROUP.Remove(ap1todelete);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}