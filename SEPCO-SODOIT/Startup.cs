﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SEPCO_SODOIT.Startup))]
namespace SEPCO_SODOIT
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
