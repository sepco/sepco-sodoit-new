﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SEPCO_SODOIT.Helpers
{
    public class myClass
    {
        public DataTable getSQLStoredProcDataTable(string sql, SqlParameter[] myParam)
        {
            var dt = new DataTable();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //var page = HttpContext.Current.CurrentHandler as Page;

                //ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return dt;

        }
        public DataTable getSQLStoredProcDataTableNoParam(string sql)
        {
            var dt = new DataTable();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Connection.Open();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //var page = HttpContext.Current.CurrentHandler as Page;

                //ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return dt;

        }
        public string getSQLStoredProcString(string sql, SqlParameter[] myParam)
        {
            string returnValue = string.Empty;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader dr;
            try
            {



                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {

                    returnValue = dr[0].ToString();

                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                cmd.Connection.Close();
            }
            return returnValue;

        }
        public DataTable getSQLDataTable(string sql, SqlParameter[] myParam)
        {
            var dt = new DataTable();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //var page = HttpContext.Current.CurrentHandler as Page;

                //ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return dt;
        }

        public DataTable getSQLDataTableNoParam(string sql)
        {
            var dt = new DataTable();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();

                cmd.Connection.Open();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //var page = HttpContext.Current.CurrentHandler as Page;

                //ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return dt;
        }


        public string getSQLDataReader(string sql, SqlParameter[] myParam)
        {
            string returnValue = string.Empty;
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            SqlDataReader dr;

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {

                    returnValue = dr[0].ToString();

                }

            }
            catch (Exception ex)
            {
                ////MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //var page = HttpContext.Current.CurrentHandler as Page;

                //ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return returnValue;

        }
        public string Encrypt(string plainText)
        {
            string key = "#SAMSUNGSALESMONITORING#";
            byte[] EncryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            EncryptKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByte = Encoding.UTF8.GetBytes(plainText);
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, des.CreateEncryptor(EncryptKey, IV), CryptoStreamMode.Write);
            cStream.Write(inputByte, 0, inputByte.Length);
            cStream.FlushFinalBlock();
            return Convert.ToBase64String(mStream.ToArray());
        }

        public string Decrypt(string encryptedText)
        {
            string key = "#SAMSUNGSALESMONITORING#";
            byte[] DecryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            byte[] inputByte = new byte[encryptedText.Length];

            DecryptKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByte = Convert.FromBase64String(encryptedText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(DecryptKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByte, 0, inputByte.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }
    }
}