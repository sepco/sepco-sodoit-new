﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using ClosedXML.Excel;
namespace SEPCO_SODOIT.Helpers
{
    public class HelperClass
    {
        public static string WeekOfYearISO8601(DateTime date)
        {
            var day = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(date);
            var week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday).ToString().PadLeft(2, '0');
            return week;
        }
        public static string MonthOfWeek(  int year,int week)
        {
          GregorianCalendar gc = new GregorianCalendar();
          for( DateTime dt = new DateTime(year, 1, 1); dt.Year == year; dt = dt.AddDays(1))
          {
            if( gc.GetWeekOfYear( dt, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday) == week ){
              return dt.ToString("MMM");
            }
          }
          return "JAN";
        }  
        public static string MonthOfYearByWeek(int Year, int Week)
        {
            DateTime tDt = new DateTime(Year, 1, 1);

            tDt.AddDays((Week - 1) * 7);

            for (int i = 0; i <= 365; ++i)
            {
                var day = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(tDt);

                int tWeek = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
                    tDt.AddDays(4 - (day == 0 ? 7 : day)),
                    CalendarWeekRule.FirstFourDayWeek,
                    DayOfWeek.Monday);
                if (tWeek == Week)
                    return tDt.ToString("MMM");

                tDt = tDt.AddDays(1);
            }
            return "JAN";
        }
        public static int GetWeeksInYear(int year)
        {
            DateTime date1 = new DateTime(year, 12, 31);
            var day = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(date1);
            var week = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date1.AddDays(4 - (day == 0 ? 7 : day)), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return 52;
        }

        public static string DataTableToJSONWithJSONNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }
        public static object[,] Convert(DataTable dt)
        {
            var rows = dt.Rows;
            int rowCount = rows.Count;
            int colCount = dt.Columns.Count;
            var result = new object[rowCount, colCount];

            for (int i = 0; i < rowCount; i++)
            {
                var row = rows[i];
                for (int j = 0; j < colCount; j++)
                {
                    result[i, j] = row[j];
                }
            }

            return result;
        }
        public static string IntToLetters(int value)
        {
            string result = string.Empty;
            while (--value >= 0)
            {
                result = (char)('A' + value % 26) + result;
                value /= 26;
            }
            return result;
        }
        public static List<string> GetListOfMonths(DateTime? dateFrom, DateTime? dateTo)
        {
            List<string> listOfMonth = new List<string>();
            //month
            var monthFrom = int.Parse((dateFrom.Value.Year.ToString()) + ((dateFrom.Value.Month).ToString().PadLeft(2, '0'))); //;+ Helpers.HelperClass.WeekOfYearISO8601(dateFrom.Value).ToString());
            var monthTo = int.Parse((dateTo.Value.Year.ToString()) + ((dateTo.Value.Month).ToString().PadLeft(2, '0'))); //;+ Helpers.HelperClass.WeekOfYearISO8601(dateTo.Value).ToString());
            for (var ctr = 0; monthFrom <= monthTo; ctr++)
            {
                var year = monthFrom.ToString().Substring(0, 4);
                var week = int.Parse(monthFrom.ToString().Substring(4, 2));
                var maxMonth = 12;//Helpers.HelperClass.GetWeeksInYear(int.Parse(year));

                if (week > maxMonth)
                {
                    monthFrom = int.Parse((int.Parse(year) + 1).ToString() + (week - maxMonth + 1).ToString().PadLeft(2, '0'));
                    listOfMonth.Add(monthFrom.ToString());
                }
                else
                {
                    listOfMonth.Add(monthFrom.ToString());
                    monthFrom += 1;
                }
            }


            return listOfMonth;

        }
        public static DataTable AddWeekColumns(DataTable dt, List<string> weeks)
        {

            foreach (var itemGroup in weeks)
            {
                dt.Columns.Add(itemGroup);

            }
            return dt;
        }
        public static DataTable GenerateGenericColumns(DataColumnCollection dtCols)
        {
            DataTable dt = new DataTable();
            foreach (DataColumn item in dtCols)
            {
                if (item.ColumnName.ToUpper() != "WEEK" && item.ColumnName.ToUpper() != "VALUE")
                    dt.Columns.Add(item.ColumnName);
            }
            return dt;
        }
        public static bool CheckCellsEmpty(DataTable dt)
        {
            var emptyCount = 0;
            foreach (DataRow row in dt.Rows)
            {
                var itemArray = row.ItemArray;
                if (itemArray == null)
                    return true;

                emptyCount += itemArray.Any(x => string.IsNullOrWhiteSpace(x.ToString())) ? 1 : 0;//check if there's a null cells
            }
            return emptyCount > 0 ? true : false;
        }
        public static Int32 TransactionFileID(string masterfile)
        {
            SqlConnection conn = null;
            try
            {
                string sql = "SELECT TOP 1 ID FROM SYS_Transactions ORDER BY ID DESC";

                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                SqlCommand cmd = new SqlCommand(sql, conn);

                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        //return "MF-" + masterfile + "-" + reader.GetInt32(0).ToString().PadLeft(8, '0');
                        return reader.GetInt32(0).ToString() == "" ? 1 : (reader.GetInt32(0) + 1);
                    }
                    else
                    {
                        // no result
                        //return "MF-" + masterfile + "-" + "00000001";
                        return 1;
                    }
                }

            }
            catch (Exception ex)
            {
                // Log your error
                return 1;
            }
            finally
            {
                if (conn != null) conn.Close();
            }
        }
        public static bool CheckDataTable(DataTable dt)//, string masterFileType, string user)
        {
            //if (CheckCellsEmpty(dt))//theres an empty cells
            //{
                //not valid
            //    return false;
            //}
           // else
           // {
                return true;
           // }
        }
        public static int GetWeeksInMonthCount(string month, int ctrM, DateTime? dateFrom, DateTime? dateTo, List<string> monthList)
        {
            var rangeDif = 0;
            //string monthName = new DateTime(2010, int.Parse(month.Substring(4, 2)), 1).ToString("MMM");
            if (ctrM == 0)//if first then use datefrom else use 1st day of this month
            {
                var minWeekInMonth = dateFrom.Value.Year.ToString() + HelperClass.WeekOfYearISO8601(dateFrom.Value);
                var maxWeekInMonth = "";
                if (month.Substring(4, 2) == "12")
                    maxWeekInMonth = month.Substring(0, 4) + HelperClass.WeekOfYearISO8601(new DateTime(int.Parse(month.Substring(0, 4)) + 1, int.Parse(month.Substring(4, 2)), 1).AddDays(-1));
                else
                {
                    var weekOfDay = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)) + 1, 1).AddDays(-1));
                    if (weekOfDay > 3)
                    {
                        maxWeekInMonth = month.Substring(0, 4) + HelperClass.WeekOfYearISO8601(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)) + 1, 1).AddDays(-1));
                    }
                    else
                    {
                        maxWeekInMonth = month.Substring(0, 4) + HelperClass.WeekOfYearISO8601(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)) + 1, 1).AddDays(-7));
                    }
                }
                rangeDif = int.Parse(maxWeekInMonth) - int.Parse(minWeekInMonth);
            }
            else if (ctrM == monthList.Count() - 1)//if last
            {
                var weekOfDay = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)), 1));

                var minWeekInMonth = weekOfDay > 3 ? dateFrom.Value.Year.ToString() + HelperClass.WeekOfYearISO8601(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)), 1 + (7 - weekOfDay + 1))) : dateFrom.Value.Year.ToString() + HelperClass.WeekOfYearISO8601(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)), 1));
                var maxWeekInMonth = month.Substring(0, 4) + HelperClass.WeekOfYearISO8601(dateTo.Value);
                rangeDif = (int.Parse(maxWeekInMonth) - int.Parse(minWeekInMonth));
            }
            else
            {
                var weekOfDay = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)), 1));

                var minWeekInMonth = weekOfDay > 3 ? dateFrom.Value.Year.ToString() + HelperClass.WeekOfYearISO8601(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)), 1 + (7 - weekOfDay))) : dateFrom.Value.Year.ToString() + HelperClass.WeekOfYearISO8601(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)), 1));
                var maxWeekInMonth = month.Substring(0, 4) + HelperClass.WeekOfYearISO8601(new DateTime(int.Parse(month.Substring(0, 4)), int.Parse(month.Substring(4, 2)) + 1, 1).AddDays(-1));
                rangeDif = int.Parse(maxWeekInMonth) - int.Parse(minWeekInMonth);
            }
            return rangeDif;
        }
        public static DataTable ExcelToDataTableCombineHeader(Stream stream, int startRow, int startCol, bool exportColumnName, int sheetIndex)
        {
            DataTable dt = new DataTable();
            //Open the Excel file using ClosedXML.
            using (XLWorkbook workBook = new XLWorkbook(stream))
            {
                //Read the first Sheet from Excel file.
                IXLWorksheet workSheet = workBook.Worksheet(sheetIndex);
                //Create a new DataTable.

                //Loop through the Worksheet rows.
                bool firstRow = true;
                var rowCtr = 0;
                foreach (IXLRow row in workSheet.Rows())
                {
                    //start from set row index
                    if (startRow <= rowCtr)
                    {
                        //Use the first row to add columns to DataTable.
                        if (firstRow)
                        {
                            if (!exportColumnName)
                            {
                                var ctr = 1;
                                var colCtr = 0;
                                foreach (IXLCell cell in row.Cells())
                                {
                                    //start from set col index
                                    if (startCol <= colCtr)
                                    {
                                        dt.Columns.Add("Column " + ctr);
                                    }
                                    ctr++;
                                    colCtr++;
                                }

                            }
                            else
                            {
                                var colCtr = 0;
                                var lastColTargetType = "";
                                foreach (IXLCell cell in row.Cells())
                                {
                                    //start from set col index
                                    if (startCol <= colCtr)
                                    {
                                        IXLRow row1HeaderCol = workSheet.Row(1);
                                        var headVal = row1HeaderCol.Cell(colCtr + 1).Value.ToString();

                                        if (!String.IsNullOrEmpty(cell.Value.ToString()))
                                        {
                                            if (colCtr >= 3)
                                            {
                                                if (headVal != "")
                                                {
                                                    lastColTargetType = headVal;
                                                }
                                                dt.Columns.Add(lastColTargetType + "," + cell.Value.ToString());
                                            }else
                                            {
                                                dt.Columns.Add(cell.Value.ToString());
                                            }
                                        }
                                        else
                                        {
                                            dt.Columns.Add(headVal);
                                        }
                                    }
                                    colCtr++; 
                                }
                            }

                            firstRow = false;
                        }
                        else
                        {
                            //Add rows to DataTable.
                            dt.Rows.Add();
                            int i = 0;
                            int nullCtr = 0;
                            foreach (IXLCell cell in row.Cells())
                            {
                                if (cell.Value.ToString() == "")
                                    nullCtr++;

                                dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                i++;
                            }
                            if (nullCtr == row.Cells().Count())
                                dt.Rows.RemoveAt(dt.Rows.Count - 1);
                        }
                    }
                    rowCtr++;
                }
            }
            return dt;
        }
        public static DataTable ExcelToDataTable(Stream stream, int startRow, int startCol, bool exportColumnName, int sheetIndex)
        {
            DataTable dt = new DataTable();
            //Open the Excel file using ClosedXML.
            using (XLWorkbook workBook = new XLWorkbook(stream))
            {
                //Read the first Sheet from Excel file.
                IXLWorksheet workSheet = workBook.Worksheet(sheetIndex);
                //Create a new DataTable.

                //Loop through the Worksheet rows.
                bool firstRow = true;
                var rowCtr = 0;
                foreach (IXLRow row in workSheet.Rows())
                {
                    //start from set row index
                    if (startRow <= rowCtr)
                    {
                        //Use the first row to add columns to DataTable.
                        if (firstRow)
                        {
                            if (!exportColumnName)
                            {
                                var ctr = 1;
                                var colCtr = 0;
                                foreach (IXLCell cell in row.Cells())
                                {
                                    //start from set col index
                                    if (startCol <= colCtr)
                                    {
                                        dt.Columns.Add("Column " + ctr);
                                    }
                                    ctr++;
                                    colCtr++;
                                }

                            }
                            else
                            {
                                var colCtr = 0;
                                foreach (IXLCell cell in row.Cells())
                                {
                                    //start from set col index
                                    if (startCol <= colCtr)
                                    {
                                        if (!String.IsNullOrEmpty(cell.Value.ToString()))
                                            dt.Columns.Add(cell.Value.ToString());
                                    }
                                    colCtr++;
                                }
                            }

                            firstRow = false;
                        }
                        else
                        {
                            //Add rows to DataTable.
                            dt.Rows.Add();
                            int i = 0;
                            int nullCtr = 0;
                            foreach (IXLCell cell in row.Cells())
                            {
                                if (cell.Value.ToString() == "")
                                    nullCtr++;

                                dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                i++;
                            }
                            if (nullCtr == row.Cells().Count())
                                dt.Rows.RemoveAt(dt.Rows.Count - 1);
                        }
                    }
                    rowCtr++;
                }
            }
            return dt;
        }
        public static DataTable ExcelToHorizontalDataTable(Stream stream, int startRow, int startCol, bool exportColumnName, int sheetIndex)
        {
            DataTable dt = new DataTable();
            //Open the Excel file using ClosedXML.
            using (XLWorkbook workBook = new XLWorkbook(stream))
            {
                //Read the first Sheet from Excel file.
                IXLWorksheet workSheet = workBook.Worksheet(sheetIndex);

                var cc = 0;
                dt.Columns.Add("Column 1");
                dt.Columns.Add("Column 2");

                foreach (IXLRow row in workSheet.Rows())
                {
                    if (cc == 0)
                    {
                        foreach (IXLCell cell in row.Cells())
                        {
                            if (cell.Address.ColumnNumber - 1 >= startCol)
                            {
                                DataRow dr = dt.NewRow();
                                dr[0] = workSheet.Cell(1, cell.Address.ColumnNumber).Value;
                                dr[1] = workSheet.Cell(2, cell.Address.ColumnNumber).Value;
                                dt.Rows.Add(dr);
                            }

                        }
                    }
                    cc++;
                }


                ////Loop through the Worksheet rows.
                //var rowCtr = 0;
                //foreach (IXLRow row in workSheet.Rows())
                //{
                //    //start from set row index
                //    if (startRow <= rowCtr)
                //    {
                //        //Use the first row to add columns to DataTable.
                //        if (firstRow)
                //        {
                //            if (!exportColumnName)
                //            {
                //                var ctr = 1;
                //                var colCtr = 0;
                //                foreach (IXLCell cell in row.Cells())
                //                {
                //                    //start from set col index
                //                    if (startCol <= colCtr)
                //                    {
                //                        dt.Columns.Add("Column " + ctr);
                //                    }
                //                    ctr++;
                //                    colCtr++;
                //                }

                //            }
                //            else
                //            {
                //                var colCtr = 0;
                //                foreach (IXLCell cell in row.Cells())
                //                {
                //                    //start from set col index
                //                    if (startCol <= colCtr)
                //                    {
                //                        dt.Columns.Add(cell.Value.ToString());
                //                    }
                //                    colCtr++;
                //                }
                //            }

                //            firstRow = false;
                //        }
                //        else
                //        {
                //            //Add rows to DataTable.
                //            dt.Rows.Add();
                //            int i = 0;
                //            int nullCtr = 0;
                //            foreach (IXLCell cell in row.Cells())
                //            {
                //                if (cell.Value.ToString() == "")
                //                    nullCtr++;

                //                dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                //                i++;
                //            }
                //            if (nullCtr == row.Cells().Count())
                //                dt.Rows.RemoveAt(dt.Rows.Count - 1);
                //        }
                //    }
                //    rowCtr++;
                //}
            }
            return dt;
        }
        
    }
}