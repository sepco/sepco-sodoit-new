﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Web.UI;
using System.Text;
using System.Security.Cryptography;

namespace SEPCO
{

    public class DataAccessLayer
    {
        public DataTable getSQLDataTable(string sql, SqlParameter[] myParam)
        {
            var dt = new DataTable();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return dt;
        }
  	public string getSQLStoredProcString(string sql, SqlParameter[] myParam)
        {
            string returnValue = string.Empty;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader dr;
            try
            {



                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();

                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {

                    returnValue = dr[0].ToString();

                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                cmd.Connection.Close();
            }
            return returnValue;

        }

        public DataSet getSQLDataSet(string sql, SqlParameter[] myParam)
        {
            var ds = new DataSet();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                da.Fill(ds);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return ds;
        }
        public DataTable getDTSQLStoredProc(string sql, SqlParameter[] myParam)
        {
            var dt = new DataTable();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.StoredProcedure;
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.Clear();
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //string errorMsg = "System Error: " + ex.InnerException.Message.ToString();
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);


            }
            finally
            {
                cmd.Connection.Close();
            }

            return dt;

        }
        public int getSQLStoredProc(string sql, SqlParameter[] myParam)
        {
            int isSuccess = 0;
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.AddRange(myParam);

                SqlParameter returnParam = new SqlParameter();
                returnParam.ParameterName = "returnValue";
                returnParam.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnParam);

                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

                isSuccess = Convert.ToInt32(cmd.Parameters["returnValue"].Value.ToString());



            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //string errorMsg = "System Error: " + ex.InnerException.Message.ToString();
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);


            }
            finally
            {
                cmd.Connection.Close();
            }

            return isSuccess;

        }

        public bool getSQLiudNoParam(string sql)
        {
            bool isSuccess = false;
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            //SqlTransaction trans = null;

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Connection.Open();
                //trans = con.BeginTransaction();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    isSuccess = true;
                }
                //trans.Commit();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
                //trans.Rollback();

            }
            finally
            {
                cmd.Connection.Close();
            }

            return isSuccess;
        }

        public bool getSQLiud(string sql, SqlParameter[] myParam)
        {
            bool isSuccess = false;
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            //SqlTransaction trans = null;

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                //trans = con.BeginTransaction();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    isSuccess = true;
                }
                //trans.Commit();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //string errorMsg = "System Error: " + ex.InnerException.Message.ToString();
                var page = HttpContext.Current.CurrentHandler as Page;


                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);

                //trans.Rollback();
            }
            finally
            {
                cmd.Connection.Close();
            }

            return isSuccess;
        }

        public DataTable getSQLDataTableNoParam(string sql)
        {
            var dt = new DataTable();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();

                cmd.Connection.Open();
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return dt;
        }

        public DataSet getSQLDataSetNoParam(string sql)
        {
            var ds = new DataSet();
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();

                cmd.Connection.Open();
                da.Fill(ds);

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return ds;
        }

        public int getSQLExeScalar(string sql, SqlParameter[] myParam)
        {
            int returnValue = 0;
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            var da = new SqlDataAdapter(cmd);

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                returnValue = Convert.ToInt32(cmd.ExecuteScalar());

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return returnValue;
        }


        public string getSQLDataReader(string sql, SqlParameter[] myParam)
        {
            string returnValue = string.Empty;
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            SqlDataReader dr;

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();
                cmd.Parameters.AddRange(myParam);
                cmd.Connection.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {

                    returnValue = dr[0].ToString();

                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return returnValue;

        }


        public string getSQLDataReaderNoParam(string sql)
        {
            string returnValue = string.Empty;
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var cmd = new SqlCommand(sql, con);
            SqlDataReader dr;

            try
            {
                if (cmd.Connection.State == ConnectionState.Open) cmd.Connection.Close();

                cmd.Connection.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    returnValue = dr[0].ToString();
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var page = HttpContext.Current.CurrentHandler as Page;

                ScriptManager.RegisterStartupScript(page, page.GetType(), "alert", "alert('" + ex.Message.ToString().Replace("'", "") + "');", true);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return returnValue;
        }


        // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private const string initVector = "1MAKV2SPBNI99212";
        // This constant is used to determine the keysize of the encryption algorithm.
        private const int keysize = 256;
        //Encrypt
        public string EncryptString(string plainText, string passPhrase)
        {
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }
        //Decrypt
        public string DecryptString(string cipherText, string passPhrase)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }

        public string Encrypt(string plainText)
        {
            string key = "#SAMSUNGSALESMONITORING#";
            byte[] EncryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            EncryptKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByte = Encoding.UTF8.GetBytes(plainText);
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, des.CreateEncryptor(EncryptKey, IV), CryptoStreamMode.Write);
            cStream.Write(inputByte, 0, inputByte.Length);
            cStream.FlushFinalBlock();
            return Convert.ToBase64String(mStream.ToArray());
        }

        public string Decrypt(string encryptedText)
        {
            string key = "#SAMSUNGSALESMONITORING#";
            byte[] DecryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            byte[] inputByte = new byte[encryptedText.Length];

            DecryptKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByte = Convert.FromBase64String(encryptedText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(DecryptKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByte, 0, inputByte.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }

        //Function Specific
        public static int SaveData(DataTable dt, string tf, string user)
        {
            int results = 0;
            using (SqlConnection connection1 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                connection1.Open();

                // Start a local transaction.
                SqlTransaction sqlTran = connection1.BeginTransaction();

                try
                {
                    // Execute stored proc command for saving data.
                    using (var command = new SqlCommand("MF_SAVE_" + tf, connection1, sqlTran) { CommandType = CommandType.StoredProcedure })
                    {
                        command.CommandTimeout = 120;
                        command.Parameters.Add(new SqlParameter("@Data", dt));

                        command.ExecuteNonQuery();
                    }

                    // Execute inline command for adding transaction
                    using (var command = new SqlCommand("INSERT INTO SYS_Transactions ([File],[FileType],DateCreated,CreatedBy) VALUES (@f,@type,@dt,@by)", connection1, sqlTran))
                    {
                        command.Parameters.Add(new SqlParameter("@f", tf));
                        command.Parameters.Add(new SqlParameter("@type", "Master File"));
                        command.Parameters.Add(new SqlParameter("@dt", DateTime.Now));
                        command.Parameters.Add(new SqlParameter("@by", user));

                        results = command.ExecuteNonQuery();
                    }
                    sqlTran.Commit();
                }
                catch (Exception ex)
                {
                    try
                    {
                        sqlTran.Rollback();
                    }
                    catch (Exception exRollback)
                    {

                    }
                }
            }
            return results;
        }
        public static int SaveDataTF(DataTable dt, string tf, string user)
        {
            int results = 0;
            using (SqlConnection connection1 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                connection1.Open();

                // Start a local transaction.
                SqlTransaction sqlTran = connection1.BeginTransaction();

                try
                {
                    // Execute stored proc command for saving data.
                    using (var command = new SqlCommand("TF_SAVE_" + tf, connection1, sqlTran) { CommandType = CommandType.StoredProcedure })
                    {
                        command.CommandTimeout = 120;
                        command.Parameters.Add(new SqlParameter("@Data", dt));

                        command.ExecuteNonQuery();
                    }

                    // Execute inline command for adding transaction
                    using (var command = new SqlCommand("INSERT INTO SYS_Transactions ([File],[FileType],DateCreated,CreatedBy) VALUES (@f,@type,@dt,@by)", connection1, sqlTran))
                    {
                        command.Parameters.Add(new SqlParameter("@f", tf));
                        command.Parameters.Add(new SqlParameter("@type", "Transaction File"));
                        command.Parameters.Add(new SqlParameter("@dt", DateTime.Now));
                        command.Parameters.Add(new SqlParameter("@by", user));

                        results = command.ExecuteNonQuery();
                    }
                    sqlTran.Commit();
                }
                catch (Exception ex)
                {
                    try
                    {
                        sqlTran.Rollback();
                    }
                    catch (Exception exRollback)
                    {

                    }
                }
            }
            return results;
        }
    }
}
