﻿using SEPCO_SODOIT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Helpers
{
    public class BizSimulationsLogic
    {
        ApplicationDbContext DB = new ApplicationDbContext();
        public string getWosFormula(string Model, int colsOrdinal, string columnName, int weekTo, DateTime? dateTo, int sellOutRow, int InventoryRow, string SellOutCategory)
        {
            var WosFormula = "";

            #region Week49 to week 52
            if (int.Parse(columnName) >= int.Parse(getWeekofLastItemInNextYear(weekTo, 3).ToString()))
            {

                if ((columnName.Equals((columnName.Substring(0, 4) + "49"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && t.Week.Equals(getWeek1.ToString())
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "50"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && t.Week.Equals(getWeek1.ToString())
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "51"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && t.Week.Equals(getWeek1.ToString())
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 1).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "52"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && t.Week.Equals(getWeek1.ToString())
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 1).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else
                {
                    if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 3)
                    {

                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && t.Week == (getCurrentWeek + 1).ToString()
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 2)
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 1)
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString() || (t.Week == (getCurrentWeek + 3).ToString())))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()))
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && t.Item == Model && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString() || (t.Week == (getCurrentWeek + 3).ToString()) || (t.Week == (getCurrentWeek + 4).ToString())))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }

                }
            }

            else
            {
                var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                var colAlphaNextWeek4 = HelperClass.IntToLetters(colsOrdinal + 4);

                WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                            colAlphaNextWeek2 + sellOutRow + "," +
                                                                            colAlphaNextWeek3 + sellOutRow + "," +
                                                                            colAlphaNextWeek4 + sellOutRow + ")/4)";
            }
            #endregion

            return WosFormula;

        }
        public string getWosFormulaTotal(string Models, int colsOrdinal, string columnName, int weekTo, DateTime? dateTo, int sellOutRow, int InventoryRow, string SellOutCategory)
        {
            var WosFormula = "";

            #region Week49 to week 52
            if (int.Parse(columnName) >= int.Parse(getWeekofLastItemInNextYear(weekTo, 3).ToString()))
            {

                if ((columnName.Equals((columnName.Substring(0, 4) + "49"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && t.Week.Equals(getWeek1.ToString())
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "50"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && t.Week.Equals(getWeek1.ToString())
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "51"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && t.Week.Equals(getWeek1.ToString())
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 1).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "52"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && t.Week.Equals(getWeek1.ToString())
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 1).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else
                {
                    if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 3)
                    {

                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && t.Week == (getCurrentWeek + 1).ToString()
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 2)
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString()))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 1)
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString() || (t.Week == (getCurrentWeek + 3).ToString())))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()))
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_AccountLevelPSIMgmt
                                                   where t.Category.ToUpper() == SellOutCategory && Models.Contains(t.Item) && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString() || (t.Week == (getCurrentWeek + 3).ToString()) || (t.Week == (getCurrentWeek + 4).ToString())))
                                                   select t.Value;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }

                }
            }

            else
            {
                var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                var colAlphaNextWeek4 = HelperClass.IntToLetters(colsOrdinal + 4);

                WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                            colAlphaNextWeek2 + sellOutRow + "," +
                                                                            colAlphaNextWeek3 + sellOutRow + "," +
                                                                            colAlphaNextWeek4 + sellOutRow + ")/4)";
            }
            #endregion

            return WosFormula;

        }
        public string getWosFormulaforAP1Short(string dealer, string Model, int colsOrdinal, string columnName, int weekTo, DateTime? dateTo, int sellOutRow, int InventoryRow)
        {
            var WosFormula = "";

            #region Week49 to week 52
            if (int.Parse(columnName) >= int.Parse(getWeekofLastItemInNextYear(weekTo, 3).ToString()))
            {

                if ((columnName.Equals((columnName.Substring(0, 4) + "49"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && t.Week.Equals(getWeek1) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "50"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && t.Week.Equals(getWeek1) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "51"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && t.Week.Equals(getWeek1)  && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 1).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "52"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && t.Week.Equals(getWeek1) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 1).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else
                {
                    if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 3)
                    {

                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && t.Week == (getCurrentWeek + 1).ToString() && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 2)
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString())) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 1)
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString()) || (t.Week == (getCurrentWeek + 3).ToString())) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()))
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Model.Contains(t.Model)
                                                   && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString()) || (t.Week == (getCurrentWeek + 3).ToString()) || (t.Week == (getCurrentWeek + 4).ToString())) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }

                }
            }

            else
            {
                var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                var colAlphaNextWeek4 = HelperClass.IntToLetters(colsOrdinal + 4);

                WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                            colAlphaNextWeek2 + sellOutRow + "," +
                                                                            colAlphaNextWeek3 + sellOutRow + "," +
                                                                            colAlphaNextWeek4 + sellOutRow + ")/4)";
            }
            #endregion

            return WosFormula;

        }
        public string getWosFormulaTotalforAP1Short(string dealer, string Models, int colsOrdinal, string columnName, int weekTo, DateTime? dateTo, int sellOutRow, int InventoryRow)
        {
            var WosFormula = "";

            #region Week49 to week 52
            if (int.Parse(columnName) >= int.Parse(getWeekofLastItemInNextYear(weekTo, 3).ToString()))
            {

                if ((columnName.Equals((columnName.Substring(0, 4) + "49"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                               where Models.Contains(t.Model)
                                                               && (t.Week.Equals(getWeek1.ToString())) && t.ChannelGroup == dealer
                                                               select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1.ToString()) || t.Week.Equals(getWeek2.ToString())) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "49")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "50"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && t.Week.Equals(getWeek1) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "50")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "51"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && t.Week.Equals(getWeek1) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 1).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "51")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else if ((columnName.Equals((columnName.Substring(0, 4) + "52"))))
                {

                    if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 3).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 2).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && t.Week.Equals(getWeek1) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && ((columnName.Equals(getWeekofLastItemInNextYear(weekTo, 1).ToString())))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();

                        int getSaleOutWeekNextYear = int.Parse(((int.Parse(columnName.Substring(0, 4))) + 1).ToString() + "01");
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if ((columnName.Equals((columnName.Substring(0, 4) + "52")) && (columnName.Equals(getWeekofLastItemInNextYear(weekTo, 0).ToString()))))
                    {
                        string getWeek1 = getWeekCounter(weekTo, 1, columnName).ToString();
                        string getWeek2 = getWeekCounter(weekTo, 2, columnName).ToString();
                        string getWeek3 = getWeekCounter(weekTo, 3, columnName).ToString();
                        string getWeek4 = getWeekCounter(weekTo, 4, columnName).ToString();
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week.Equals(getWeek1) || t.Week.Equals(getWeek2) || t.Week.Equals(getWeek3) || t.Week.Equals(getWeek4)) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }
                }
                else
                {
                    if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 3)
                    {

                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && t.Week == (getCurrentWeek + 1).ToString() && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    colAlphaNextWeek3 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 2)
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString())) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    colAlphaNextWeek2 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";
                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()) - 1)
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString()) || (t.Week == (getCurrentWeek + 3).ToString())) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                                    SellOutValue + ")/4)";

                    }
                    else if (int.Parse(columnName) == int.Parse(weekTo.ToString()))
                    {
                        int getCurrentWeek = int.Parse(weekTo.ToString());
                        var WeekOfSellOutGCValue = from t in DB._TF_SellOutAndInventoryWeekly
                                                   where Models.Contains(t.Model)
                                                   && (t.Week == (getCurrentWeek + 1).ToString() || (t.Week == (getCurrentWeek + 2).ToString()) || (t.Week == (getCurrentWeek + 3).ToString()) || (t.Week == (getCurrentWeek + 4).ToString())) && t.ChannelGroup == dealer
                                                   select t.Sales;
                        var SellOutValue = WeekOfSellOutGCValue.Count() == 0 ? "0" : WeekOfSellOutGCValue.Sum().ToString("0");
                        var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                        WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + SellOutValue + ")/4)";

                    }

                }
            }

            else
            {
                var colAlpha = HelperClass.IntToLetters(colsOrdinal);
                var colAlphaNextWeek1 = HelperClass.IntToLetters(colsOrdinal + 1);
                var colAlphaNextWeek2 = HelperClass.IntToLetters(colsOrdinal + 2);
                var colAlphaNextWeek3 = HelperClass.IntToLetters(colsOrdinal + 3);
                var colAlphaNextWeek4 = HelperClass.IntToLetters(colsOrdinal + 4);

                WosFormula = "=" + colAlpha + InventoryRow + "/(SUM(" + colAlphaNextWeek1 + sellOutRow + "," +
                                                                            colAlphaNextWeek2 + sellOutRow + "," +
                                                                            colAlphaNextWeek3 + sellOutRow + "," +
                                                                            colAlphaNextWeek4 + sellOutRow + ")/4)";
            }
            #endregion

            return WosFormula;

        }
        public int getWeekofLastItemInNextYear(int weekTo, int counter)
        {
            int returnWeek = weekTo;
            int TempWeek = weekTo;
            int YearWeek = int.Parse(weekTo.ToString().Substring(0, 4));
            int resetWeek = int.Parse(((int.Parse(weekTo.ToString().Substring(0, 4)) - 1) + "52").ToString());
            for (int decr = counter; decr >= 1; decr--)
            {
                returnWeek--;
                TempWeek--;
                if (TempWeek == int.Parse((weekTo.ToString().Substring(0, 4) + "00")))
                {
                    returnWeek = resetWeek;
                }
            }
            return returnWeek;
        }
        public int getWeekCounter(int weekTo, int addWeek, string columnName)
        {
            int NextYear = int.Parse(columnName.ToString().Substring(0, 4)) + 1;
            int FirstWeekofTheYear = int.Parse(NextYear.ToString() + "01");
            int Week = weekTo + addWeek;

            if (Week.ToString().Contains("53"))
            {
                Week = FirstWeekofTheYear;
            }
            if (Week.ToString().Contains("54"))
            {
                Week = FirstWeekofTheYear + 1;
            }
            if (Week.ToString().Contains("55"))
            {
                Week = FirstWeekofTheYear + 2;
            }
            if (Week.ToString().Contains("56"))
            {
                Week = FirstWeekofTheYear + 3;
            }
            return Week;
        }

    }
}