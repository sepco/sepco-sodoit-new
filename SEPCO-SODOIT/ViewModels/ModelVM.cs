﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.ViewModels
{
    public class ModelVM
    {
        public string Type { get; set; }
        public string Model { get; set; }
        public string Category { get; set; }
    }
    public class HeadersVM
    {
        public string data { get; set; }
        public string type { get; set; }
    }
    public class DataVM
    {
        public List<object[]> Data { get; set; }
        public int Count { get; set; }
        public List<string> Headers { get; set; }
        public List<int> HiddenRows { get; set; }
        public List<HeadersVM> ColsFormat { get; set; }
        public string FirstCategory { get; set; }

    }
 
}