namespace SEPCO_SODOIT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _SYS_AP1GROUP : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AP1Group",
                c => new
                    {
                        RecID = c.Int(nullable: false, identity: true),
                        AP1GroupID = c.String(nullable: false),
                        AP1GroupName = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.RecID);
            
            CreateTable(
                "dbo.CarouselImages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Path = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.GSCMAccounts",
                c => new
                    {
                        RecID = c.Int(nullable: false, identity: true),
                        GSCMAccountID = c.String(nullable: false),
                        AccountName = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.RecID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.GSCMAccounts");
            DropTable("dbo.CarouselImages");
            DropTable("dbo.AP1Group");
        }
    }
}
