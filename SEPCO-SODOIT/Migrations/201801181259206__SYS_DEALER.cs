namespace SEPCO_SODOIT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _SYS_DEALER : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dealers",
                c => new
                    {
                        RecID = c.Int(nullable: false, identity: true),
                        DealerName = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.RecID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Dealers");
        }
    }
}
