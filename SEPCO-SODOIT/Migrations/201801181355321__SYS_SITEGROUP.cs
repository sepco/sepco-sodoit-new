namespace SEPCO_SODOIT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _SYS_SITEGROUP : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SiteGroups",
                c => new
                    {
                        RecID = c.Int(nullable: false, identity: true),
                        SiteGroupID = c.String(nullable: false),
                        SiteGroupName = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.RecID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SiteGroups");
        }
    }
}
