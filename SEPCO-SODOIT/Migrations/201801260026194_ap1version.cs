namespace SEPCO_SODOIT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ap1version : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SYS_AP1BizVersionLookup",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GSCMAccount = c.String(),
                        Dealer = c.String(),
                        AP1Version = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SYS_AP1BizVersionLookup");
        }
    }
}
