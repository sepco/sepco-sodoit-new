namespace SEPCO_SODOIT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedTFExchangeRate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MF_ExchangeRate",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Week = c.String(),
                        Month = c.String(),
                        Year = c.String(),
                        DollarExchangeRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MF_ExchangeRate");
        }
    }
}
