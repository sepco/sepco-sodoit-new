namespace SEPCO_SODOIT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MF_DealerAndAccountMapping",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SoldToParty = c.String(),
                        SoldToPartyName = c.String(),
                        SiteGroupID = c.String(),
                        SiteGroup = c.String(),
                        Dealer = c.String(),
                        GSCMAccountID = c.String(),
                        GSCMAccount = c.String(),
                        AP1ID = c.String(),
                        AP1 = c.String(),
                        SimulationStatus = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MF_DealerAndAccountStoreMapping",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SiteID = c.String(),
                        SiteName = c.String(),
                        SiteGroupID = c.String(),
                        SiteGroup = c.String(),
                        Dealer = c.String(),
                        GSCMAccountID = c.String(),
                        GSCMAccount = c.String(),
                        AP1ID = c.String(),
                        AP1 = c.String(),
                        AP2 = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MF_ModelAttributes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductGroup = c.String(),
                        Model = c.String(),
                        ProductType = c.String(),
                        Attribute1 = c.String(),
                        Attribute2 = c.String(),
                        Attribute3 = c.String(),
                        Attribute4 = c.String(),
                        Attribute5 = c.String(),
                        Attribute6 = c.String(),
                        Attribute7 = c.String(),
                        Attribute8 = c.String(),
                        Attribute9 = c.String(),
                        Attribute10 = c.String(),
                        Attribute11 = c.String(),
                        Attribute12 = c.String(),
                        Attribute13 = c.String(),
                        Attribute14 = c.String(),
                        Attribute15 = c.String(),
                        Attribute16 = c.String(),
                        Attribute17 = c.String(),
                        Attribute18 = c.String(),
                        Attribute19 = c.String(),
                        Attribute20 = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MF_ModelManagementMapping",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        Model = c.String(),
                        ModelStatus = c.String(),
                        ModelDescription = c.String(),
                        ModelClassification = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MF_ProductGroupAttributes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductGroup = c.String(),
                        Description = c.String(),
                        AttributeColumn = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MF_StoreListAttribute",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SiteID = c.String(),
                        SiteName = c.String(),
                        Description = c.String(),
                        AttributeColumn = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MF_WeekToMonthRatio",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GC = c.String(),
                        AP2 = c.String(),
                        GBM = c.String(),
                        ProductGroup = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MF_WOSTarget",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AP1 = c.String(),
                        GSCMAccount = c.String(),
                        Dealer = c.String(),
                        ProductGroup = c.String(),
                        Average = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MF_YearDateMapping",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Week = c.String(),
                        SellInYearMonth = c.String(),
                        SellOutYearMonth = c.String(),
                        WeekMapping = c.String(),
                        MonthMapping = c.String(),
                        YearMapping = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductGroup",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GroupName = c.String(),
                        Description = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SYS_ProductType",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductGroupID = c.Int(nullable: false),
                        ProductTypeName = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SYS_SimulationTransactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Simulation = c.String(),
                        CreatedBy = c.String(),
                        FileLocation = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SYS_Transactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        File = c.String(),
                        FileType = c.String(),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_AccountLevelPSIMgmt",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductGroup = c.String(),
                        AP1 = c.String(),
                        Account = c.String(),
                        Item = c.String(),
                        Category = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_AP1Forecast",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GSCMAccount = c.String(),
                        ProductGroup = c.String(),
                        Item = c.String(),
                        Category = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_AP2PSI",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductGroup = c.String(),
                        Item = c.String(),
                        Category = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_FlooringSite",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Brand = c.String(),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        Capacity = c.String(),
                        Model = c.String(),
                        SellOut = c.Int(nullable: false),
                        Display = c.Int(nullable: false),
                        Inventory = c.Int(nullable: false),
                        StockRequested = c.Int(nullable: false),
                        StockDelivered = c.Int(nullable: false),
                        PromoterName = c.String(),
                        StoreCode = c.String(),
                        StoreName = c.String(),
                        Week = c.String(),
                        Agency = c.String(),
                        Dealer = c.String(),
                        Segment = c.String(),
                        TeamHead = c.String(),
                        MidasInCharge = c.String(),
                        Area = c.String(),
                        ModelStatus = c.String(),
                        BWL = c.String(),
                        AP1Tagging = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_FlooringTarget",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StoreCode = c.String(),
                        StoreName = c.String(),
                        Dealer = c.String(),
                        Account = c.String(),
                        AP1 = c.String(),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        Model = c.String(),
                        DisplayTarget = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_GrossNetPrice",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Month = c.String(),
                        AP1 = c.String(),
                        GSCMAccount = c.String(),
                        Dealer = c.String(),
                        Model = c.String(),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        ModelClassification = c.String(),
                        GrossPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_KPIAccountSellOutFA",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        YearMonth = c.String(),
                        YearWeek = c.String(),
                        ProductGroup = c.String(),
                        GSCMAccount = c.String(),
                        Value = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_KPIAP1FAProductGroup",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        YearMonth = c.String(),
                        YearWeek = c.String(),
                        ProductGroup = c.String(),
                        GSCMAccount = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_KPIAP2FAProductGroup",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        YearMonth = c.String(),
                        YearWeek = c.String(),
                        ProductGroup = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SalesTarget",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AP1 = c.String(),
                        GSCMAccount = c.String(),
                        Dealer = c.String(),
                        ProductGroup = c.String(),
                        TargetName = c.String(),
                        YearMonth = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SellinBilling",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BusinessGroup = c.String(),
                        YearMonth = c.String(),
                        SoldToPartyCode = c.String(),
                        Area = c.String(),
                        Dealer = c.String(),
                        AP1 = c.String(),
                        SoldToPartyName = c.String(),
                        Model = c.String(),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        Description = c.String(),
                        Qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AmountType = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SellInGI",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GC = c.String(),
                        AP2 = c.String(),
                        AP1 = c.String(),
                        GSCMAccount = c.String(),
                        Model = c.String(),
                        SaleQty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AmountUSD = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AmountKRW = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Currency = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EntryDate = c.DateTime(nullable: false),
                        SalesDate = c.DateTime(nullable: false),
                        SalesWeek = c.String(),
                        SoldToName = c.String(),
                        ShipToName1 = c.String(),
                        ShipToName2 = c.String(),
                        Plant = c.String(),
                        Storage = c.String(),
                        ProductCode = c.String(),
                        BillToParty = c.String(),
                        SoldToPartyCode = c.String(),
                        ShipToPartyCode = c.String(),
                        Company = c.String(),
                        SalesOrg = c.String(),
                        SalesOffice = c.String(),
                        ProductType = c.String(),
                        Attribute1 = c.String(),
                        Attribute2 = c.String(),
                        Attribute3 = c.String(),
                        Buyer = c.String(),
                        Type = c.String(),
                        ToolName = c.String(),
                        BasicName = c.String(),
                        Project = c.String(),
                        ModelGroup = c.String(),
                        DistributionChannel = c.String(),
                        DCDesc = c.String(),
                        SalesDocType = c.String(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SellInSellOutTargetAmount",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Dealer = c.String(),
                        GSCMAccount = c.String(),
                        AP1 = c.String(),
                        Type = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SellInSellOutTargetQty",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Dealer = c.String(),
                        GSCMAccount = c.String(),
                        AP1 = c.String(),
                        Category = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SellOutAndInventoryMonthly",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DivisionGroup = c.String(),
                        Division = c.String(),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        Model = c.String(),
                        Brand = c.String(),
                        Alias = c.String(),
                        Subsidiary = c.String(),
                        ChannelCode = c.String(),
                        Channel = c.String(),
                        ChannelCategory = c.String(),
                        ChannelGroup = c.String(),
                        ChannelType = c.String(),
                        Region = c.String(),
                        State = c.String(),
                        City = c.String(),
                        District = c.String(),
                        RRPPriceLoc = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Month = c.String(),
                        Purchase = c.Int(nullable: false),
                        Sales = c.Int(nullable: false),
                        SellThru = c.Int(nullable: false),
                        Inventory = c.Int(nullable: false),
                        DisplayQty = c.Int(nullable: false),
                        SalesPRC = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AMTSRRPLoc = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InvoiceAmountUSD = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SellOutAndInventoryWeekly",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DivisionGroup = c.String(),
                        Division = c.String(),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        Model = c.String(),
                        Brand = c.String(),
                        Alias = c.String(),
                        Subsidiary = c.String(),
                        ChannelCode = c.String(),
                        Channel = c.String(),
                        ChannelCategory = c.String(),
                        ChannelGroup = c.String(),
                        ChannelType = c.String(),
                        Region = c.String(),
                        State = c.String(),
                        City = c.String(),
                        District = c.String(),
                        RRPPriceLoc = c.Decimal(nullable: false, precision: 18, scale: 4),
                        Week = c.String(),
                        Purchase = c.Int(nullable: false),
                        Sales = c.Int(nullable: false),
                        SellThru = c.Int(nullable: false),
                        Inventory = c.Int(nullable: false),
                        DisplayQty = c.Int(nullable: false),
                        SalesPRC = c.Decimal(nullable: false, precision: 18, scale: 4),
                        AMTSRRPLoc = c.Decimal(nullable: false, precision: 18, scale: 4),
                        InvoiceAmountUSD = c.Decimal(nullable: false, precision: 18, scale: 4),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SellOutMonthly",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Subsidiary = c.String(),
                        DivisionGroup = c.String(),
                        Division = c.String(),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        Brand = c.String(),
                        Model = c.String(),
                        Alias = c.String(),
                        PromoterID = c.String(),
                        Promoter = c.String(),
                        MobileNo = c.String(),
                        Region = c.String(),
                        State = c.String(),
                        City = c.String(),
                        District = c.String(),
                        NationCD = c.String(),
                        ChannelCode = c.String(),
                        Channel = c.String(),
                        Contact = c.String(),
                        PhoneNo = c.String(),
                        ChannelCategory = c.String(),
                        ShipmentType = c.String(),
                        AggregationtyPE = c.String(),
                        ChannelType = c.String(),
                        ChannelGroup = c.String(),
                        Month = c.String(),
                        RRPPriceLoc = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Vat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RRPLoc = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SKUSales = c.Int(nullable: false),
                        IMEISales = c.Int(nullable: false),
                        Sales = c.Int(nullable: false),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_SellOutWeekly",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Subsidiary = c.String(),
                        DivisionGroup = c.String(),
                        Division = c.String(),
                        ProductGroup = c.String(),
                        ProductType = c.String(),
                        Brand = c.String(),
                        Model = c.String(),
                        Alias = c.String(),
                        PromoterID = c.String(),
                        Promoter = c.String(),
                        MobileNo = c.String(),
                        Region = c.String(),
                        State = c.String(),
                        City = c.String(),
                        District = c.String(),
                        NationCD = c.String(),
                        ChannelCode = c.String(),
                        Channel = c.String(),
                        Contact = c.String(),
                        PhoneNo = c.String(),
                        ChannelCategory = c.String(),
                        ShipmentType = c.String(),
                        AggregationtyPE = c.String(),
                        ChannelType = c.String(),
                        ChannelGroup = c.String(),
                        Week = c.String(),
                        RRPPriceLoc = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Vat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RRPLoc = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SKUSales = c.Int(nullable: false),
                        IMEISales = c.Int(nullable: false),
                        Sales = c.Int(nullable: false),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_TIM",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Dealer = c.String(),
                        GSCMAccount = c.String(),
                        AP1 = c.String(),
                        Category = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_WOSPerAP1AndAccount",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductGroup = c.String(),
                        AP1 = c.String(),
                        GSCMAccount = c.String(),
                        Model = c.String(),
                        Category = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TF_WOSPerDealer",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductGroup = c.String(),
                        GSCMAccount = c.String(),
                        Channel = c.String(),
                        Model = c.String(),
                        Category = c.String(),
                        Week = c.String(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.TF_WOSPerDealer");
            DropTable("dbo.TF_WOSPerAP1AndAccount");
            DropTable("dbo.TF_TIM");
            DropTable("dbo.TF_SellOutWeekly");
            DropTable("dbo.TF_SellOutMonthly");
            DropTable("dbo.TF_SellOutAndInventoryWeekly");
            DropTable("dbo.TF_SellOutAndInventoryMonthly");
            DropTable("dbo.TF_SellInSellOutTargetQty");
            DropTable("dbo.TF_SellInSellOutTargetAmount");
            DropTable("dbo.TF_SellInGI");
            DropTable("dbo.TF_SellinBilling");
            DropTable("dbo.TF_SalesTarget");
            DropTable("dbo.TF_KPIAP2FAProductGroup");
            DropTable("dbo.TF_KPIAP1FAProductGroup");
            DropTable("dbo.TF_KPIAccountSellOutFA");
            DropTable("dbo.TF_GrossNetPrice");
            DropTable("dbo.TF_FlooringTarget");
            DropTable("dbo.TF_FlooringSite");
            DropTable("dbo.TF_AP2PSI");
            DropTable("dbo.TF_AP1Forecast");
            DropTable("dbo.TF_AccountLevelPSIMgmt");
            DropTable("dbo.SYS_Transactions");
            DropTable("dbo.SYS_SimulationTransactions");
            DropTable("dbo.SYS_ProductType");
            DropTable("dbo.ProductGroup");
            DropTable("dbo.MF_YearDateMapping");
            DropTable("dbo.MF_WOSTarget");
            DropTable("dbo.MF_WeekToMonthRatio");
            DropTable("dbo.MF_StoreListAttribute");
            DropTable("dbo.MF_ProductGroupAttributes");
            DropTable("dbo.MF_ModelManagementMapping");
            DropTable("dbo.MF_ModelAttributes");
            DropTable("dbo.MF_DealerAndAccountStoreMapping");
            DropTable("dbo.MF_DealerAndAccountMapping");
        }
    }
}
