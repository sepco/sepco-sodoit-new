﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_SellOutWeekly")]
    public class TF_SellOutWeekly
    {
        [Key]
        public int ID { get; set; }
        public string Subsidiary { get; set; }
        public string DivisionGroup { get; set; }
        public string Division { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Alias { get; set; }
        public string PromoterID { get; set; }
        public string Promoter { get; set; }
        public string MobileNo { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string NationCD { get; set; }
        public string ChannelCode { get; set; }
        public string Channel { get; set; }
        public string Contact { get; set; }
        public string PhoneNo { get; set; }
        public string ChannelCategory { get; set; }
        public string ShipmentType { get; set; }
        public string AggregationtyPE { get; set; }
        public string ChannelType { get; set; }
        public string ChannelGroup { get; set; }
        public string Week { get; set; }
        public decimal RRPPriceLoc { get; set; }
        public decimal Vat { get; set; }
        public decimal RRPLoc { get; set; }
        public int SKUSales { get; set; }
        public int IMEISales { get; set; }
        public int Sales { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
    [Table("TF_SellOutMonthly")]
    public class TF_SellOutMonthly
    {
        [Key]
        public int ID { get; set; }
        public string Subsidiary { get; set; }
        public string DivisionGroup { get; set; }
        public string Division { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Alias { get; set; }
        public string PromoterID { get; set; }
        public string Promoter { get; set; }
        public string MobileNo { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string NationCD { get; set; }
        public string ChannelCode { get; set; }
        public string Channel { get; set; }
        public string Contact { get; set; }
        public string PhoneNo { get; set; }
        public string ChannelCategory { get; set; }
        public string ShipmentType { get; set; }
        public string AggregationtyPE { get; set; }
        public string ChannelType { get; set; }
        public string ChannelGroup { get; set; }
        public string Month { get; set; }
        public decimal RRPPriceLoc { get; set; }
        public decimal Vat { get; set; }
        public decimal RRPLoc { get; set; }
        public int SKUSales { get; set; }
        public int IMEISales { get; set; }
        public int Sales { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}