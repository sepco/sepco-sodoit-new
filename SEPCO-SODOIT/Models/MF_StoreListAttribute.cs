﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("MF_StoreListAttribute")]
    public class MF_StoreListAttribute
    {
        [Key]
        public int ID { get; set; }
        public string SiteID { get; set; }
        public string SiteName { get; set; }
        public string Description { get; set; }
        public string AttributeColumn { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}