﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    public class GSCMAccount
    {
        [Key]
        public int RecID { get; set; }
        [Required]
        public string GSCMAccountID { get; set; }
        [Required]
        public string AccountName { get; set; }
        public string Description { get; set; }
    }
}