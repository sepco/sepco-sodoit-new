﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("MF_WOSTarget")]
    public class MF_WOSTarget
    {
        [Key]
        public int ID { get; set; }
        public string AP1 { get; set; }
        public string GSCMAccount { get; set; }
        public string Dealer { get; set; }
        public string ProductGroup { get; set; }
        public decimal Average { get; set; }
        public string Week { get; set; }
        public decimal Value { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }

    }
}