﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using SEPCO_MVC.Models;
using SEPCO_SODOIT.Models.SEPCO_SODOIT.Models;

namespace SEPCO_SODOIT.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public DbSet<ProductGroup> _ProductGroup { get; set; }
        public DbSet<SYS_Transactions> _SYS_Transactions { get; set; }
        public DbSet<ProductTypes> _ProductType { get; set; }
        public DbSet<MF_WOSTarget> _MF_WOSTarget { get; set; }
        public DbSet<MF_ModelManagementMapping> _MF_ModelManagementMapping { get; set; }
        public DbSet<MF_ProductGroupAttributes> _MF_ProductGroupAttributes { get; set; }
        public DbSet<MF_ModelAttributes> _MF_ModelAttributes { get; set; }
        public DbSet<MF_DealerAndAccountStoreMapping> _MF_DealerAndAccountStoreMapping { get; set; }
        public DbSet<MF_DealerAndAccountMapping> _MF_DealerAndAccountMapping { get; set; }
        public DbSet<MF_StoreListAttribute> _MF_StoreListAttribute { get; set; }
        public DbSet<MF_YearDateMapping> _MF_YearDateMapping { get; set; }
        public DbSet<TF_ExchangeRate> _TF_ExchangeRate { get; set; }
        public DbSet<TF_AccountLevelPSIMgmt> _TF_AccountLevelPSIMgmt { get; set; }
        public DbSet<TF_AP1Forecast> _TF_AP1Forecast { get; set; }
        public DbSet<TF_FlooringSite> _TF_FlooringSite { get; set; }
        public DbSet<TF_FlooringTarget> _TF_FlooringTarget { get; set; }
        public DbSet<TF_GrossNetPrice> _TF_GrossNetPrice { get; set; }
        public DbSet<TF_KPIAccountSellOutFA> _TF_KPIAccountSellOutFA { get; set; }
        public DbSet<TF_KPIAP1FA> _TF_KPIAP1FA { get; set; }
        public DbSet<TF_SellInSellOutTargetAmount> _TF_SellInSellOutTargetAmount { get; set; }
        public DbSet<TF_SellInSellOutTargetQty> _TF_SellInSellOutTargetQty { get; set; }
        public DbSet<TF_TIM> _TF_TIM { get; set; }
        public DbSet<SYS_SimulationTransactions> _SYS_SimulationTrx { get; set; }
        public DbSet<TF_KPIAP2FA> _TF_KPIAP2FA { get; set; }
        public DbSet<TF_SellinBilling> _TF_SellinBilling { get; set; }
        public DbSet<TF_AP2PSI> _TF_AP2PSI { get; set; }
        public DbSet<TF_SalesTarget> _TF_SalesTarget { get; set; }
        public DbSet<MF_WeekToMonthRatio> _MF_WeekToMonthRatio { get; set; }
        public DbSet<TF_SellInGI> _TF_SellInGI { get; set; }
        public DbSet<TF_SellOutAndInventory> _TF_SellOutAndInventoryWeekly { get; set; }
        public DbSet<TF_SellOutAndInventoryMonthly> _TF_SellOutAndInventoryMonthly { get; set; }
        public DbSet<TF_SellOutWeekly> _TF_SellOutWeekly { get; set; }
        public DbSet<TF_SellOutMonthly> _TF_SellOutMonthly { get; set; }
        public DbSet<TF_WOSPerDealer> _TF_WOSPerDealer { get; set; }
        public DbSet<TF_WOSPerAP1AndAccount> _TF_WOSPerAP1AndAccount { get; set; }
        public DbSet<Dealer> _SYS_DEALER { get; set; }
        public DbSet<AP1Group> _SYS_AP1GROUP { get; set; }
        public DbSet<CarouselImage> _SYS_CAROUSELIMAGE { get; set; }
        public DbSet<GSCMAccount> _SYS_GSCMACCOUNT { get; set; }
        public DbSet<SiteGroup> _SYS_SITEGROUP { get; set; }
        public DbSet<SYS_AP1BizVersionLookup> _SYS_AP1BizVersionLookup { get; set; }
       
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TF_SellOutAndInventory>().Property(S => S.RRPPriceLoc).HasPrecision(18, 4);
            modelBuilder.Entity<TF_SellOutAndInventory>().Property(S => S.SalesPRC).HasPrecision(18, 4);
            modelBuilder.Entity<TF_SellOutAndInventory>().Property(S => S.AMTSRRPLoc).HasPrecision(18, 4);
            modelBuilder.Entity<TF_SellOutAndInventory>().Property(S => S.InvoiceAmountUSD).HasPrecision(18, 4);
            base.OnModelCreating(modelBuilder);
        }
    }
}