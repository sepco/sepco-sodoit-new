﻿using SEPCO_SODOIT.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SEPCO_SODOIT.Models
{
    public class SEPCODBContext : DbContext
    {
        public SEPCODBContext()
            : base("DefaultConnection")
        {
        }
        public DbSet<MF_WOSTarget> _MF_WOSTarget { get; set; }
        public TF_AccountLevelPSIMgmt _TF_AccountLevelPSIMgmt { get; set; }
        public TF_AP1Forecast _TF_AP1Forecast { get; set; }
        public TF_FlooringSite _TF_FlooringSite { get; set; }
        public TF_FlooringTarget _TF_FlooringTarget { get; set; }
        public TF_GrossNetPrice _TF_GrossNetPrice { get; set; }
        public TF_KPIAccountSellOutFA _TF_KPIAccountSellOutFA { get; set; }
        public TF_KPIAP1FA _TF_KPIAP1FA { get; set; }
        public TF_SellInSellOutTargetAmount _TF_SellInSellOutTargetAmount { get; set; }
        public TF_SellInSellOutTargetQty _TF_SellInSellOutTargetQty { get; set; }
        public TF_TIM _TF_TIM { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        //ADDED BY ARIES FOR MIGRATE
        static SEPCODBContext()
        {
//#if DEBUG
//            //Database.SetInitializer<SARIDBContext> (new DropCreateIfChangeInitializer());
//#else
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SEPCODBContext, Configuration>()); 
//#endif
        }
        public class TrimModelBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext,
            ModelBindingContext bindingContext)
            {
                ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                if (valueResult == null || string.IsNullOrEmpty(valueResult.AttemptedValue))
                    return null;
                return valueResult.AttemptedValue.Trim();
            }
        }
    }
}