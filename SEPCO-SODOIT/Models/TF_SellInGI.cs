﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_SellInGI")]
    public class TF_SellInGI
    {
        [Key]
        public int ID { get; set; }
        public string GC { get; set; }
        public string AP2 { get; set; }
        public string AP1 { get; set; }
        public string GSCMAccount { get; set; }
        public string Model { get; set; }
        public decimal SaleQty { get; set; }
        public decimal AmountUSD { get; set; }
        public decimal AmountKRW { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime SalesDate { get; set; }
        public string SalesWeek { get; set; }
        public string SoldToName { get; set; }
        public string ShipToName1 { get; set; }
        public string ShipToName2 { get; set; }
        public string Plant { get; set; }
        public string Storage { get; set; }
        public string ProductCode { get; set; }
        public string BillToParty { get; set; }
        public string SoldToPartyCode { get; set; }
        public string ShipToPartyCode { get; set; }
        public string Company { get; set; }
        public string SalesOrg { get; set; }
        public string SalesOffice { get; set; }
        public string ProductType { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public string Attribute3 { get; set; }
        public string Buyer { get; set; }
        public string Type { get; set; }
        public string ToolName { get; set; }
        public string BasicName { get; set; }
        public string Project { get; set; }
        public string ModelGroup { get; set; }
        public string DistributionChannel { get; set; }
        public string DCDesc { get; set; }
        public string SalesDocType { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}