﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    namespace SEPCO_SODOIT.Models
    {
        [Table("ProductType")]
        public class ProductTypes
        {
            [Key]
            public int ID { get; set; }
            public string ProductGroup { get; set; }
            public string ProductType { get; set; }
            public string Description { get; set; }
            public int ProductHierarchy { get; set; }
        }
    }
}