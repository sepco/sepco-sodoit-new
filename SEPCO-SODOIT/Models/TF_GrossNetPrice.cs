﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_GrossNetPrice")]

    public class TF_GrossNetPrice
    {
        [Key]
        public int ID { get; set; }
        public int Year { get; set; }
        public string Month { get; set; }
        public string AP1 { get; set; }
        public string GSCMAccount { get; set; }
        public string Dealer { get; set; }
        public string Model { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string ModelClassification { get; set; }
        public decimal GrossPrice { get; set; }
        public decimal NetPrice { get; set; }
        public int TransactionID { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string ModifiedBy { get; set; }

    }
}