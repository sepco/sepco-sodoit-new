﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_FlooringSite")]
    public class TF_FlooringSite
    {
        [Key]
        public int ID { get; set; }
        public string Brand { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string Capacity { get; set; }
        public string Model { get; set; }
        public int SellOut { get; set; }
        public int Display { get; set; }
        public int Inventory { get; set; }
        public int StockRequested { get; set; }
        public int StockDelivered { get; set; }
        public string PromoterName { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public string Week { get; set; }
        public string Agency { get; set; }
        public string Dealer { get; set; }
        public string Segment { get; set; }
        public string TeamHead { get; set; }
        public string MidasInCharge { get; set; }
        public string Area { get; set; }
        public string ModelStatus { get; set; }
        public string BWL { get; set; }
        public string AP1Tagging { get; set; }
        public int TransactionID { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string ModifiedBy { get; set; }

    }
}