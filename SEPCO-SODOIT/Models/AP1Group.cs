﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    public class AP1Group
    {
        [Key]
        public int RecID { get; set; }
        [Required]
        public string AP1GroupID { get; set; }
        [Required]
        public string AP1GroupName { get; set; }
        public string Description { get; set; }
    }
}