﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_AP1Forecast")]
    public class TF_AP1Forecast
    {
        [Key]
        public int ID { get; set; }
        public string GSCMAccount { get; set; }
        public string ProductGroup { get; set; }
        public string Item { get; set; }
        public string Category { get; set; }
        public string Week { get; set; }
        public decimal Value { get; set; }
        public int TransactionID { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string ModifiedBy { get; set; }

    }
}