﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    public class SiteGroup
    {
        [Key]
        public int RecID { get; set; }
        [Required]
        public string SiteGroupID { get; set; }
        [Required]
        public string SiteGroupName { get; set; }
        public string Description { get; set; }
    }
}