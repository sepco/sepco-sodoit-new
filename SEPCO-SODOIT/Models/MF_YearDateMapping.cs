﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("MF_YearDateMapping")]
    public class MF_YearDateMapping
    {
        [Key]
        public int ID { get; set; }
        public string Week { get; set; }
        public string SellInYearMonth { get; set; }
        public string SellOutYearMonth { get; set; }
        public string WeekMapping { get; set; }
        public string MonthMapping { get; set; }
        public string YearMapping { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}