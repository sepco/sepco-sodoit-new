﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_SellinBilling")]
    public class TF_SellinBilling
    {
        [Key]
        public int ID { get; set; }
        public string BusinessGroup { get; set; }
        public string YearMonth { get; set; }
        public string SoldToPartyCode { get; set; }
        public string Area { get; set; }
        public string Dealer { get; set; }
        public string AP1 { get; set; }
        public string SoldToPartyName { get; set; }
        public string Model { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string Description { get; set; }
        public decimal Qty { get; set; }
        public decimal Amount { get; set; }
        public string AmountType { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }

    }
}