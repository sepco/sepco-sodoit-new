﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("MF_DealerAndAccountStoreMapping")]
    public class MF_DealerAndAccountStoreMapping
    {
        [Key]
        public int ID { get; set; }
        public string SiteID { get; set; }
        public string SiteName { get; set; }
        public string SiteGroupID { get; set; }
        public string SiteGroup { get; set; }
        public string Dealer { get; set; }
        public string GSCMAccountID { get; set; }
        public string GSCMAccount { get; set; }
        public string AP1ID { get; set; }
        public string AP1 { get; set; }
        public string AP2 { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
    [Table("MF_DealerAndAccountMapping")]
    public class MF_DealerAndAccountMapping
    {
        [Key]
        public int ID { get; set; }
        public string SoldToParty { get; set; }
        public string SoldToPartyName { get; set; }
        public string SiteGroupID { get; set; }
        public string SiteGroup { get; set; }
        public string Dealer { get; set; }
        public string GSCMAccountID { get; set; }
        public string GSCMAccount { get; set; }
        public string AP1ID { get; set; }
        public string AP1 { get; set; }
        public string SimulationStatus { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}