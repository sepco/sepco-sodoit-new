﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_KPIAP2FAProductGroup")]
    public class TF_KPIAP2FA
    {
        [Key]
        public int ID { get; set; }
        public string YearMonth { get; set; }
        public string YearWeek { get; set; }
        public string ProductGroup { get; set; }
        public decimal Value { get; set; }
        public int TransactionID { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}