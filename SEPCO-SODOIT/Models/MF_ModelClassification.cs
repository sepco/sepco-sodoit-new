﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SEPCO_MVC.Models
{
    [Table("MF_ModelManagementMapping")]
    public class MF_ModelManagementMapping
    {
        [Key]
        public int ID { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string Model { get; set; }
        public string ModelStatus { get; set; }
        public string ModelDescription { get; set; }
        public string ModelClassification { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }

    }
}