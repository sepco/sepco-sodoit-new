﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("MF_WeekToMonthRatio")]
    public class MF_WeekToMonthRatio
    {
        [Key]
        public int ID { get; set; }
        public string GC { get; set; }
        public string AP2 { get; set; }
        public string GBM { get; set; }
        public string ProductGroup { get; set; }
        public string Week { get; set; }
        public decimal Value { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}