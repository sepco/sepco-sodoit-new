﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("SYS_SimulationTransactions")]
    public class SYS_SimulationTransactions
    {
        [Key]
        public int ID { get; set; }
        public string Simulation { get; set; }
        public string CreatedBy { get; set; }
        public string FileLocation { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}