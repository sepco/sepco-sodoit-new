﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_SellOutAndInventoryWeekly")]
    public class TF_SellOutAndInventory
    {
        [Key]
        public int ID { get; set; }
        public string DivisionGroup { get; set; }
        public string Division { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public string Alias { get; set; }
        public string Subsidiary { get; set; }
        public string ChannelCode { get; set; }
        public string Channel { get; set; }
        public string ChannelCategory { get; set; }
        public string ChannelGroup { get; set; }
        public string ChannelType { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public decimal RRPPriceLoc { get; set; }
        public string Week { get; set; }
        public int Purchase { get; set; }
        public int Sales { get; set; }
        public int SellThru { get; set; }
        public int Inventory { get; set; }
        public int DisplayQty { get; set; }
        public decimal SalesPRC { get; set; }
        public decimal AMTSRRPLoc { get; set; }
        public decimal InvoiceAmountUSD { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
    [Table("TF_SellOutAndInventoryMonthly")]
    public class TF_SellOutAndInventoryMonthly
    {
        [Key]
        public int ID { get; set; }
        public string DivisionGroup { get; set; }
        public string Division { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public string Alias { get; set; }
        public string Subsidiary { get; set; }
        public string ChannelCode { get; set; }
        public string Channel { get; set; }
        public string ChannelCategory { get; set; }
        public string ChannelGroup { get; set; }
        public string ChannelType { get; set; }
        public string Region { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public decimal RRPPriceLoc { get; set; }
        public string Month { get; set; }
        public int Purchase { get; set; }
        public int Sales { get; set; }
        public int SellThru { get; set; }
        public int Inventory { get; set; }
        public int DisplayQty { get; set; }
        public decimal SalesPRC { get; set; }
        public decimal AMTSRRPLoc { get; set; }
        public decimal InvoiceAmountUSD { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}