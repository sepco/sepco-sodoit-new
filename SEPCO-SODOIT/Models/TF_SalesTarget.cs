﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SEPCO_SODOIT.Models
{
    [Table("TF_SalesTarget")]
    public class TF_SalesTarget
    {
        [Key]
        public int ID { get; set; }
        public string AP1 { get; set; }
        public string GSCMAccount { get; set; }
        public string Dealer { get; set; }
        public string ProductGroup { get; set; }
        public string TargetName { get; set; }
        public string YearMonth { get; set; }
        public decimal Amount { get; set; }
        public int TransactionID { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateModified { get; set; }
        public string ModifiedBy { get; set; }
    }
}