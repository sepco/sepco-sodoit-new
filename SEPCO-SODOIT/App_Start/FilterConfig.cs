﻿using System.Web;
using System.Web.Mvc;

namespace SEPCO_SODOIT
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
