﻿using System.Web;
using System.Web.Optimization;

namespace SEPCO_SODOIT
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.js",
                        "~/Scripts/jquery-ui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/chosenjs").Include(
                      "~/Scripts/chosen.jquery.js"));
            bundles.Add(new ScriptBundle("~/bundles/dragtable").Include(
                      "~/Scripts/jquery.dragtable.js"));

            bundles.Add(new ScriptBundle("~/bundles/handsontable").Include(
                      "~/Scripts/handsontable.full.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/SumoSelect").Include(
                      "~/SumoSelect/jquery.sumoselect.min.js"));



            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/component.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/handsontable").Include(
                      "~/Content/handsontable.min.css"));

            bundles.Add(new StyleBundle("~/Content/chosenjs").Include(
                      "~/Content/chosen.css"));

            bundles.Add(new StyleBundle("~/Content/dragtable").Include(
                      "~/Content/dragtable.css"));

            bundles.Add(new StyleBundle("~/Content/fontawesome").Include(
                      "~/Content/font-awesome.css"));
            bundles.Add(new ScriptBundle("~/Content/SumoSelect").Include(
                      "~/SumoSelect/sumoselect.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
