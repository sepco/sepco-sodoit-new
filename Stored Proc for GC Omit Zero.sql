USE [SEPCO-DB]
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_TotalValueofAllCategories]    Script Date: 3/3/2018 8:26:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_TotalValueofAllCategories]
(
	@model nvarchar(MAX),
	@allWeek nvarchar(MAX)
)	
AS
BEGIN
	
	DECLARE @TF_AP2PSIValue decimal(18,2), @TF_AccountLevelPSIMgmtValue decimal(18,2), @TF_AP1ForecastValue decimal(18,2), @TF_FlooringTargetValue decimal(18,2),
	 @TF_FlooringSite decimal(18,2), @TotalValueforAllCategory decimal(18,2)
	
	Select @TF_AP2PSIValue = ISNULL(sum(Value),0) from TF_AP2PSI		
	where Week in ('BOH','ARRIVAL','GI','RTF') and Item = @model and Category in  (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AccountLevelPSIMgmtValue = ISNULL(sum(Value),0) from TF_AccountLevelPSIMgmt
	where Category in ('Sell-in(GC)','Sell-out FCST(GC)','Sell-out FCST(AP2)','Sell-out FCST(AP1)','EOH_EX(GC)','EOH_EX(AP2)','EOH_EX(AP1)') and Item = @model and Week in (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AP1ForecastValue =  ISNULL(sum(Value),0) from TF_AP1Forecast 
	where Category = 'Sell-in(AP1)' and Item = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	select @TF_FlooringTargetValue =  ISNULL(sum(Value),0) from TF_FlooringTarget
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))
	
	select @TF_FlooringSite=  ISNULL(sum(Display),0) from TF_FlooringSite
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	set @TotalValueforAllCategory = @TF_AP2PSIValue + @TF_AccountLevelPSIMgmtValue + @TF_AP1ForecastValue + @TF_FlooringTargetValue + @TF_FlooringSite
	select @TotalValueforAllCategory
END

GO
