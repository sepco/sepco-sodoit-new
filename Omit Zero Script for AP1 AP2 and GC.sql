USE [SEPCO-DB]
GO
/****** Object:  StoredProcedure [dbo].[spu_AP1Short_GET_TotalValueofAllCategories]    Script Date: 3/6/2018 12:37:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_AP1Short_GET_TotalValueofAllCategories]
(
	@model nvarchar(MAX),
	@allWeek nvarchar(MAX),
	@dealer nvarchar(MAX)
)	
AS
BEGIN
	
	DECLARE @TF_SellInGI decimal(18,2), @TF_SellOutAndInventoryWeekly decimal(18,2), @TF_FlooringTargetValue decimal(18,2),
	 @TF_FlooringSite decimal(18,2), @TotalValueforAllCategory decimal(18,2)

	
	Select @TF_SellInGI = ISNULL(sum(TF_SellInGI.SaleQty),0) from TF_SellInGI INNER JOIN MF_DealerAndAccountMapping
	ON TF_SellInGI.SoldToPartyCode = MF_DealerAndAccountMapping.SoldToParty
	where MF_DealerAndAccountMapping.Dealer = @dealer and  TF_SellInGI.Model = @model and TF_SellInGI.SalesWeek in (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_SellOutAndInventoryWeekly =  ISNULL(sum(Sales),0) from TF_SellOutAndInventoryWeekly 
	where ChannelGroup= @dealer  and Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	--select @TF_FlooringTargetValue =  ISNULL(sum(Value),0) from TF_FlooringTarget
	--where  Account = @account  and Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))
	
	select @TF_FlooringSite=  ISNULL(sum(Display),0) from TF_FlooringSite
	where  Dealer = @dealer  and Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	set @TotalValueforAllCategory = @TF_SellOutAndInventoryWeekly + @TF_SellOutAndInventoryWeekly + @TF_FlooringSite
	select @TotalValueforAllCategory
END

GO
/****** Object:  StoredProcedure [dbo].[spu_ap2_GET_SalesTargetTF]    Script Date: 3/6/2018 12:37:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_ap2_GET_SalesTargetTF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		ProductGroup 'Product Grp',
		TargetName,
		[YearMonth] ,
		[Amount]
	FROM TF_SalesTarget
	WHERE [YearMonth] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) 
	      AND ProductGroup  =@PGROUP  
	ORDER BY [YearMonth] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[spu_AP2_GET_TotalValueofAllCategories]    Script Date: 3/6/2018 12:37:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_AP2_GET_TotalValueofAllCategories]
(
	@model nvarchar(MAX),
	@allWeek nvarchar(MAX)
)	
AS
BEGIN
	
	DECLARE @TF_AP2PSIValue decimal(18,2), @TF_AccountLevelPSIMgmtValue decimal(18,2), @TF_AP1ForecastValue decimal(18,2), @TF_FlooringTargetValue decimal(18,2),
	 @TF_FlooringSite decimal(18,2), @TotalValueforAllCategory decimal(18,2)
	
	Select @TF_AP2PSIValue = ISNULL(sum(Value),0) from TF_AP2PSI		
	where Week in ('BOH','ARRIVAL','GI') and Item = @model and Category in  (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AccountLevelPSIMgmtValue = ISNULL(sum(Value),0) from TF_AccountLevelPSIMgmt
	where Category in ('Sell-out FCST(GC)','Sell-out FCST(AP2)','Sell-out FCST(AP1)','EOH_EX(GC)','EOH_EX(AP2)','EOH_EX(AP1)') and Item = @model and Week in (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AP1ForecastValue =  ISNULL(sum(Value),0) from TF_AP1Forecast 
	where Item = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	select @TF_FlooringTargetValue =  ISNULL(sum(Value),0) from TF_FlooringTarget
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))
	
	select @TF_FlooringSite=  ISNULL(sum(Display),0) from TF_FlooringSite
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	set @TotalValueforAllCategory = @TF_AP2PSIValue + @TF_AccountLevelPSIMgmtValue + @TF_AP1ForecastValue + @TF_FlooringTargetValue + @TF_FlooringSite
	select @TotalValueforAllCategory
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_TotalValueofAllCategories]    Script Date: 3/6/2018 12:37:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_TotalValueofAllCategories]
(
	@model nvarchar(MAX),
	@allWeek nvarchar(MAX)
)	
AS
BEGIN
	
	DECLARE @TF_AP2PSIValue decimal(18,2), @TF_AccountLevelPSIMgmtValue decimal(18,2), @TF_AP1ForecastValue decimal(18,2), @TF_FlooringTargetValue decimal(18,2),
	 @TF_FlooringSite decimal(18,2), @TotalValueforAllCategory decimal(18,2)
	
	Select @TF_AP2PSIValue = ISNULL(sum(Value),0) from TF_AP2PSI		
	where Week in ('BOH','ARRIVAL','GI','RTF') and Item = @model and Category in  (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AccountLevelPSIMgmtValue = ISNULL(sum(Value),0) from TF_AccountLevelPSIMgmt
	where Category in ('Sell-in(GC)','Sell-out FCST(GC)','Sell-out FCST(AP2)','Sell-out FCST(AP1)','EOH_EX(GC)','EOH_EX(AP2)','EOH_EX(AP1)') and Item = @model and Week in (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AP1ForecastValue =  ISNULL(sum(Value),0) from TF_AP1Forecast 
	where Item = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	select @TF_FlooringTargetValue =  ISNULL(sum(Value),0) from TF_FlooringTarget
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))
	
	select @TF_FlooringSite=  ISNULL(sum(Display),0) from TF_FlooringSite
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	set @TotalValueforAllCategory = @TF_AP2PSIValue + @TF_AccountLevelPSIMgmtValue + @TF_AP1ForecastValue + @TF_FlooringTargetValue + @TF_FlooringSite
	select @TotalValueforAllCategory
END

GO
