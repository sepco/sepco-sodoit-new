USE [SEPCO-DB]
GO
/****** Object:  StoredProcedure [dbo].[spu_GET_TIMShortTF]    Script Date: 02/06/2018 12:35:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_TIMShortTF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[Dealer]
      ,[GSCMAccount] 'GSCM Account'
      ,[AP1]
      ,[Category]
      ,[Week]
      ,[Value]
	FROM TF_TIM
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND Dealer = @ACCOUNT
	ORDER BY [Week] ASC
END


GO
/****** Object:  StoredProcedure [dbo].[spu_GET_WOSTargetShortMF]    Script Date: 02/06/2018 12:35:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_WOSTargetShortMF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[AP1]
      ,[GSCMAccount] 'GSCM Account'
      ,[Dealer]
      ,[ProductGroup] 'Product Group'
      ,[Week]
      ,[Value]
	FROM MF_WOSTarget
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND Dealer = @ACCOUNT AND ProductGroup = @PGROUP
	ORDER BY [Week] ASC
END


GO
