USE [SEPCO-DB]
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_AP2ForecastTF]    Script Date: 3/1/2018 9:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_AP2ForecastTF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		GSCMAccount 'Account',
		ProductGroup 'Product Grp',
		Item,
		Category,
		[Week] ,
		[Value]
	FROM TF_AP2Forecast
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) 
	      AND ProductGroup  =@PGROUP  
	ORDER BY [Week] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_AP2PSITF]    Script Date: 3/1/2018 9:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_AP2PSITF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		ProductGroup 'Product Grp',
		Item,
		Category,
		[Week] ,
		[Value]
	FROM TF_AP2PSI
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) 
	      AND ProductGroup = @PGROUP
	ORDER BY [Week] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_ChannelPSITF]    Script Date: 3/1/2018 9:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_ChannelPSITF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		ProductGroup 'Product Grp',
		AP1,
		Account 'Account',
		Item,
		Category,
		[Week] ,
		[Value]
	FROM TF_AccountLevelPSIMgmt
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) 
	      AND ProductGroup = @PGROUP
	ORDER BY [Week] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_FlooringSiteTF]    Script Date: 3/1/2018 9:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_FlooringSiteTF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Brand,
		ProductGroup 'Product Group',
		ProductType 'Sub Category',
		Capacity ,
		Model,
		[SellOut] 'Sell Out'
      ,[Display]
      ,[Inventory]
      ,[StockRequested] 'Stock Requested'
      ,[StockDelivered] 'Stock Delivered'
      ,[PromoterName] 'Promoter Name'
      ,[StoreCode] 'Store Code'
      ,[StoreName] 'Store Name'
      ,[Week]
      ,[Agency]
      ,[Dealer]
      ,[Segment]
      ,[TeamHead] 'Team Head'
      ,[MidasInCharge] 'Midas In Charge'
      ,[Area]
      ,[ModelStatus] 'Model Status'
      ,[BWL]
      ,[AP1Tagging] 'AP1 Tagging' 
	FROM TF_FlooringSite
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND ProductGroup = @PGROUP 
	ORDER BY [Week] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_FlooringTargetTF]    Script Date: 3/1/2018 9:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_FlooringTargetTF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	   [StoreCode] 'Store Code'
      ,[StoreName] 'Store Name'
      ,[Dealer]
      ,[Account] 'GSCM Account'
      ,[AP1] 
      ,[ProductGroup] 'Product Group' 
      ,[ProductType] 'Product Type'
      ,[Model] 'Model'
      ,[DisplayTarget] 'Display Target'
      ,[Week]
      ,[Value]
	FROM TF_FlooringTarget
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND ProductGroup = @PGROUP 
	ORDER BY [Week] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_SalesTargetTF]    Script Date: 3/1/2018 9:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_SalesTargetTF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		ProductGroup 'Product Grp',
		TargetName,
		[YearMonth] ,
		[Amount]
	FROM TF_SalesTarget
	WHERE [YearMonth] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) 
	      AND ProductGroup  =@PGROUP  
	ORDER BY [YearMonth] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_SellInBillingTF]    Script Date: 3/1/2018 9:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spu_GC_GET_SellInBillingTF]
	@YEARMONTH NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX),
	@ATYPE NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		BusinessGroup 'Business Group(N:Division)',
		YearMonth 'Year/Month',
		SoldToPartyCode 'Sold-To-Party',
		Area,
		Dealer,
		AP1,
		SoldToPartyName,
		Model 'Material',
		ProductGroup 'DIV',
		ProductType 'SUB CAT',
		Description,
		Qty,
		Amount
	FROM TF_SellinBilling
	WHERE [YearMonth] IN (SELECT val FROM dbo.f_split(@YEARMONTH, ',')) 
	      AND ProductGroup = @PGROUP
		  AND AmountType = @ATYPE
	ORDER BY [YearMonth] ASC
END
GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_WOSTargetMF]    Script Date: 3/1/2018 9:34:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_WOSTargetMF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[AP1]
      ,[GSCMAccount] 'GSCM Account'
      ,[Dealer]
      ,[ProductGroup] 'Product Group'
      ,[Week]
      ,[Value]
	FROM MF_WOSTarget
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) 
	AND ProductGroup = @PGROUP
	ORDER BY [Week] ASC
END
GO
