USE [SEPCO-MVC]
GO
/****** Object:  UserDefinedTableType [dbo].[MF_DealerAndAccountMapping]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[MF_DealerAndAccountMapping] AS TABLE(
	[SoldToParty] [nvarchar](max) NULL,
	[SoldToPartyName] [nvarchar](max) NULL,
	[SiteGroupID] [nvarchar](max) NULL,
	[SiteGroup] [nvarchar](max) NULL,
	[Dealer] [nvarchar](max) NULL,
	[GSCMAccountID] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[AP1ID] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[SimulationStatus] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_DealerAndAccountStoreMapping]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[MF_DealerAndAccountStoreMapping] AS TABLE(
	[SiteID] [nvarchar](max) NULL,
	[SiteName] [nvarchar](max) NULL,
	[SiteGroupID] [nvarchar](max) NULL,
	[SiteGroup] [nvarchar](max) NULL,
	[Dealer] [nvarchar](max) NULL,
	[GSCMAccountID] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[AP1ID] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[AP2] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_ModelAttributes]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[MF_ModelAttributes] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Attribute1] [nvarchar](max) NULL,
	[Attribute2] [nvarchar](max) NULL,
	[Attribute3] [nvarchar](max) NULL,
	[Attribute4] [nvarchar](max) NULL,
	[Attribute5] [nvarchar](max) NULL,
	[Attribute6] [nvarchar](max) NULL,
	[Attribute7] [nvarchar](max) NULL,
	[Attribute8] [nvarchar](max) NULL,
	[Attribute9] [nvarchar](max) NULL,
	[Attribute10] [nvarchar](max) NULL,
	[Attribute11] [nvarchar](max) NULL,
	[Attribute12] [nvarchar](max) NULL,
	[Attribute13] [nvarchar](max) NULL,
	[Attribute14] [nvarchar](max) NULL,
	[Attribute15] [nvarchar](max) NULL,
	[Attribute16] [nvarchar](max) NULL,
	[Attribute17] [nvarchar](max) NULL,
	[Attribute18] [nvarchar](max) NULL,
	[Attribute19] [nvarchar](max) NULL,
	[Attribute20] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_ModelManagementMapping]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[MF_ModelManagementMapping] AS TABLE(
	[Model] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[ModelClassification] [nvarchar](max) NULL,
	[ModelStatus] [nvarchar](max) NULL,
	[ModelDescription] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_ProductGroupAttributes]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[MF_ProductGroupAttributes] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[AttributeColumn] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_StoreListAttributes]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[MF_StoreListAttributes] AS TABLE(
	[SiteID] [nvarchar](max) NULL,
	[SiteName] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[AttributeColumn] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_WOSTarget]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[MF_WOSTarget] AS TABLE(
	[AP1] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[Dealer] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[Week] [nvarchar](max) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[Average] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MF_YearDateMapping]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[MF_YearDateMapping] AS TABLE(
	[Week] [nvarchar](max) NULL,
	[SellInYearMonth] [nvarchar](max) NULL,
	[SellOutYearMonth] [nvarchar](max) NULL,
	[WeekMapping] [nvarchar](max) NULL,
	[MonthMapping] [nvarchar](max) NULL,
	[YearMapping] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_ACCOUNT_LEVEL_CHANNEL_PSI_MGMT]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_ACCOUNT_LEVEL_CHANNEL_PSI_MGMT] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[AP1] [nvarchar](150) NULL,
	[Account] [nvarchar](150) NULL,
	[Item] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_AP1_FORECAST]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_AP1_FORECAST] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[Account] [nvarchar](150) NULL,
	[Item] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_AP2_PSI]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_AP2_PSI] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[Item] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Value] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_FLOORING_SITE]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_FLOORING_SITE] AS TABLE(
	[Brand] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[ProductType] [nvarchar](150) NULL,
	[Capacity] [nvarchar](150) NULL,
	[Model] [nvarchar](150) NULL,
	[SellOut] [int] NULL,
	[Display] [int] NULL,
	[Inventory] [int] NULL,
	[StocksRequested] [int] NULL,
	[StocksDelivered] [int] NULL,
	[PromoterName] [nvarchar](300) NULL,
	[StoreCode] [nvarchar](150) NULL,
	[StoreName] [nvarchar](300) NULL,
	[Week] [nvarchar](150) NULL,
	[Agency] [nvarchar](150) NULL,
	[Dealer] [nvarchar](150) NULL,
	[Segment] [nvarchar](150) NULL,
	[TeamHead] [nvarchar](150) NULL,
	[MidasInCharge] [nvarchar](150) NULL,
	[Area] [nvarchar](150) NULL,
	[ModelStatus] [nvarchar](150) NULL,
	[BWL] [nvarchar](150) NULL,
	[AP1Tagging] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_FLOORING_TARGET]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_FLOORING_TARGET] AS TABLE(
	[StoreCode] [nvarchar](150) NULL,
	[StoreName] [nvarchar](300) NULL,
	[Dealer] [nvarchar](150) NULL,
	[Account] [nvarchar](150) NULL,
	[AP1] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[ProductType] [nvarchar](150) NULL,
	[Model] [nvarchar](150) NULL,
	[DisplayTarget] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [decimal](18, 2) NULL,
	[TransactionID] [nvarchar](250) NULL,
	[User] [nvarchar](25) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_GROSS_NET_PRICE]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_GROSS_NET_PRICE] AS TABLE(
	[Year] [int] NULL,
	[Month] [nvarchar](20) NULL,
	[AP1] [nvarchar](50) NULL,
	[GSCMAccount] [nvarchar](50) NULL,
	[Dealer] [nvarchar](50) NULL,
	[Model] [nvarchar](50) NULL,
	[ProductGroup] [nvarchar](50) NULL,
	[ProductType] [nvarchar](50) NULL,
	[ModelClassification] [nvarchar](50) NULL,
	[GrossPrice] [decimal](18, 2) NULL,
	[NetPrice] [decimal](18, 2) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_KPI_ACCOUNT_SELLOUT_FA]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_KPI_ACCOUNT_SELLOUT_FA] AS TABLE(
	[YearMonth] [nvarchar](150) NULL,
	[YearWeek] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[GSCMAccount] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_KPI_AP1_FA_PRODUCTGROUP]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_KPI_AP1_FA_PRODUCTGROUP] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[GSCMAccount] [nvarchar](150) NULL,
	[YearMonth] [nvarchar](150) NULL,
	[YearWeek] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_KPI_AP2_FA_PRODUCTGROUP]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_KPI_AP2_FA_PRODUCTGROUP] AS TABLE(
	[ProductGroup] [nvarchar](150) NULL,
	[YearMonth] [nvarchar](150) NULL,
	[YearWeek] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SALES_TARGET]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SALES_TARGET] AS TABLE(
	[AP1] [nvarchar](350) NULL,
	[GSCMAccount] [nvarchar](150) NULL,
	[Dealer] [nvarchar](350) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[TargetName] [nvarchar](150) NULL,
	[YearMonth] [nvarchar](150) NULL,
	[Amount] [decimal](18, 2) NULL,
	[TransactionID] [nvarchar](250) NULL,
	[User] [nvarchar](25) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SELLIN_BILLING]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SELLIN_BILLING] AS TABLE(
	[BusinessGroup] [nvarchar](120) NULL,
	[YearMonth] [nvarchar](50) NULL,
	[SoldToPartyCode] [nvarchar](50) NULL,
	[Area] [nvarchar](250) NULL,
	[Dealer] [nvarchar](250) NULL,
	[AP1] [nvarchar](150) NULL,
	[SoldToPartyName] [nvarchar](250) NULL,
	[Model] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[ProductType] [nvarchar](150) NULL,
	[Description] [nvarchar](550) NULL,
	[Qty] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[AmountType] [nvarchar](5) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SELLIN_BILLING_USD]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SELLIN_BILLING_USD] AS TABLE(
	[BusinessGroup] [nvarchar](120) NULL,
	[Year_Month] [nvarchar](50) NULL,
	[SoldToPartyCode] [nvarchar](50) NULL,
	[Area] [nvarchar](250) NULL,
	[DealerName] [nvarchar](250) NULL,
	[AP1] [nvarchar](150) NULL,
	[SellOutToPartyName] [nvarchar](250) NULL,
	[Material] [nvarchar](150) NULL,
	[ProductGroup] [nvarchar](150) NULL,
	[ProductType] [nvarchar](150) NULL,
	[Description] [nvarchar](550) NULL,
	[Qty] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SELLIN_SELLOUT_TARGET_AMOUNT]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SELLIN_SELLOUT_TARGET_AMOUNT] AS TABLE(
	[Dealer] [nvarchar](250) NULL,
	[Account] [nvarchar](250) NULL,
	[AP1] [nvarchar](150) NULL,
	[Type] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SELLIN_SELLOUT_TARGET_QTY]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SELLIN_SELLOUT_TARGET_QTY] AS TABLE(
	[Dealer] [nvarchar](250) NULL,
	[Account] [nvarchar](250) NULL,
	[AP1] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [decimal](18, 1) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellInGI]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SellInGI] AS TABLE(
	[GC] [nvarchar](max) NULL,
	[AP2] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[SaleQty] [decimal](18, 2) NOT NULL,
	[AmountUSD] [decimal](18, 2) NOT NULL,
	[AmountKRW] [decimal](18, 2) NOT NULL,
	[Currency] [nvarchar](max) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[EntryDate] [datetime] NOT NULL,
	[SalesDate] [datetime] NOT NULL,
	[SalesWeek] [nvarchar](max) NULL,
	[SoldToName] [nvarchar](max) NULL,
	[ShipToName1] [nvarchar](max) NULL,
	[ShipToName2] [nvarchar](max) NULL,
	[Plant] [nvarchar](max) NULL,
	[Storage] [nvarchar](max) NULL,
	[ProductCode] [nvarchar](max) NULL,
	[BillToParty] [nvarchar](max) NULL,
	[SoldToPartyCode] [nvarchar](max) NULL,
	[ShipToPartyCode] [nvarchar](max) NULL,
	[Company] [nvarchar](max) NULL,
	[SalesOrg] [nvarchar](max) NULL,
	[SalesOffice] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Attribute1] [nvarchar](max) NULL,
	[Attribute2] [nvarchar](max) NULL,
	[Attribute3] [nvarchar](max) NULL,
	[Buyer] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NULL,
	[ToolName] [nvarchar](max) NULL,
	[BasicName] [nvarchar](max) NULL,
	[Project] [nvarchar](max) NULL,
	[ModelGroup] [nvarchar](max) NULL,
	[DistributionChannel] [nvarchar](max) NULL,
	[DCDesc] [nvarchar](max) NULL,
	[SalesDocType] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellOutAndInventoryMonthly]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SellOutAndInventoryMonthly] AS TABLE(
	[ChannelGroup] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[RRPPriceLoc] [decimal](18, 2) NOT NULL,
	[Month] [nvarchar](max) NULL,
	[Purchase] [int] NOT NULL,
	[Sales] [int] NOT NULL,
	[SellThru] [int] NOT NULL,
	[Inventory] [int] NOT NULL,
	[DisplayQty] [int] NOT NULL,
	[SalesPRC] [decimal](18, 2) NOT NULL,
	[AMTSRRPLoc] [decimal](18, 2) NOT NULL,
	[InvoiceAmountUSD] [decimal](18, 2) NOT NULL,
	[DivisionGroup] [nvarchar](max) NULL,
	[Division] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[Subsidiary] [nvarchar](max) NULL,
	[ChannelCode] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[ChannelCategory] [nvarchar](max) NULL,
	[ChannelType] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellOutAndInventoryWeekly]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SellOutAndInventoryWeekly] AS TABLE(
	[ChannelGroup] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[RRPPriceLoc] [decimal](18, 4) NOT NULL,
	[Week] [nvarchar](max) NULL,
	[Purchase] [int] NOT NULL,
	[Sales] [int] NOT NULL,
	[SellThru] [int] NOT NULL,
	[Inventory] [int] NOT NULL,
	[DisplayQty] [int] NOT NULL,
	[SalesPRC] [decimal](18, 4) NOT NULL,
	[AMTSRRPLoc] [decimal](18, 4) NOT NULL,
	[InvoiceAmountUSD] [decimal](18, 4) NOT NULL,
	[DivisionGroup] [nvarchar](max) NULL,
	[Division] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[Subsidiary] [nvarchar](max) NULL,
	[ChannelCode] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[ChannelCategory] [nvarchar](max) NULL,
	[ChannelType] [nvarchar](max) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellOutMonthly]    Script Date: 01/14/2018 4:16:50 PM ******/
CREATE TYPE [dbo].[TF_SellOutMonthly] AS TABLE(
	[Subsidiary] [nvarchar](max) NULL,
	[DivisionGroup] [nvarchar](max) NULL,
	[Division] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[PromoterID] [nvarchar](max) NULL,
	[Promoter] [nvarchar](max) NULL,
	[MobileNo] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[NationCD] [nvarchar](max) NULL,
	[ChannelCode] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[Contact] [nvarchar](max) NULL,
	[PhoneNo] [nvarchar](max) NULL,
	[ChannelCategory] [nvarchar](max) NULL,
	[ShipmentType] [nvarchar](max) NULL,
	[AggregationtyPE] [nvarchar](max) NULL,
	[ChannelType] [nvarchar](max) NULL,
	[ChannelGroup] [nvarchar](max) NULL,
	[Month] [nvarchar](max) NULL,
	[RRPPriceLoc] [decimal](18, 2) NOT NULL,
	[Vat] [decimal](18, 2) NOT NULL,
	[RRPLoc] [decimal](18, 2) NOT NULL,
	[SKUSales] [int] NOT NULL,
	[IMEISales] [int] NOT NULL,
	[Sales] [int] NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_SellOutWeekly]    Script Date: 01/14/2018 4:16:51 PM ******/
CREATE TYPE [dbo].[TF_SellOutWeekly] AS TABLE(
	[Subsidiary] [nvarchar](max) NULL,
	[DivisionGroup] [nvarchar](max) NULL,
	[Division] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](max) NULL,
	[Brand] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Alias] [nvarchar](max) NULL,
	[PromoterID] [nvarchar](max) NULL,
	[Promoter] [nvarchar](max) NULL,
	[MobileNo] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[NationCD] [nvarchar](max) NULL,
	[ChannelCode] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[Contact] [nvarchar](max) NULL,
	[PhoneNo] [nvarchar](max) NULL,
	[ChannelCategory] [nvarchar](max) NULL,
	[ShipmentType] [nvarchar](max) NULL,
	[AggregationtyPE] [nvarchar](max) NULL,
	[ChannelType] [nvarchar](max) NULL,
	[ChannelGroup] [nvarchar](max) NULL,
	[Week] [nvarchar](max) NULL,
	[RRPPriceLoc] [decimal](18, 2) NOT NULL,
	[Vat] [decimal](18, 2) NOT NULL,
	[RRPLoc] [decimal](18, 2) NOT NULL,
	[SKUSales] [int] NOT NULL,
	[IMEISales] [int] NOT NULL,
	[Sales] [int] NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_TIM]    Script Date: 01/14/2018 4:16:51 PM ******/
CREATE TYPE [dbo].[TF_TIM] AS TABLE(
	[Dealer] [nvarchar](150) NULL,
	[GSCMAccount] [nvarchar](150) NULL,
	[AP1] [nvarchar](150) NULL,
	[Category] [nvarchar](150) NULL,
	[Week] [nvarchar](150) NULL,
	[Value] [nvarchar](150) NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](150) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_WOSPerAP1AndAccount]    Script Date: 01/14/2018 4:16:51 PM ******/
CREATE TYPE [dbo].[TF_WOSPerAP1AndAccount] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Category] [nvarchar](max) NULL,
	[Week] [nvarchar](max) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TF_WOSPerDealer]    Script Date: 01/14/2018 4:16:51 PM ******/
CREATE TYPE [dbo].[TF_WOSPerDealer] AS TABLE(
	[ProductGroup] [nvarchar](max) NULL,
	[GSCMAccount] [nvarchar](max) NULL,
	[Channel] [nvarchar](max) NULL,
	[Model] [nvarchar](max) NULL,
	[Category] [nvarchar](max) NULL,
	[Week] [nvarchar](max) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NULL,
	[User] [nvarchar](250) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[MF_SAVE_DealerAndAccountMappingMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MF_SAVE_DealerAndAccountMappingMF]
	-- Add the parameters for the stored procedure here
	@Data MF_DealerAndAccountMapping readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE MF_DealerAndAccountMapping AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[SoldToParty] = Source.[SoldToParty]
		)
	WHEN MATCHED THEN 
		 UPDATE SET SoldToPartyName = Source.SoldToPartyName,
					SiteGroupID = Source.SiteGroupID,
					SiteGroup = Source.SiteGroup,
					Dealer = Source.Dealer,
					GSCMAccountID = Source.GSCMAccountID,
					GSCMAccount = Source.GSCMAccount,
					AP1ID = Source.AP1ID,
					AP1 = Source.AP1,
					SimulationStatus = Source.SimulationStatus,
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([SoldToParty],
				SoldToPartyName,
				SiteGroupID,
				SiteGroup,
				Dealer,
				GSCMAccountID,
				GSCMAccount,
				AP1ID,
				AP1,
				SimulationStatus,
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (
				Source.[SoldToParty],
				Source.[SoldToPartyName],
				Source.[SiteGroupID],
				Source.[SiteGroup],
				Source.[Dealer],
				Source.[GSCMAccountID],
				Source.[GSCMAccount],
				Source.[AP1ID],
				Source.[AP1],
				Source.[SimulationStatus],
				Source.[TransactionID],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[MF_SAVE_DealerAndAccountStoreMappingMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MF_SAVE_DealerAndAccountStoreMappingMF]
	-- Add the parameters for the stored procedure here
	@Data MF_DealerAndAccountStoreMapping readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE MF_DealerAndAccountStoreMapping AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[SiteID] = Source.[SiteID]
		)
	WHEN MATCHED THEN 
		 UPDATE SET SiteName = Source.SiteName,
					SiteGroupID = Source.SiteGroupID,
					SiteGroup = Source.SiteGroup,
					Dealer = Source.Dealer,
					GSCMAccountID = Source.GSCMAccountID,
					GSCMAccount = Source.GSCMAccount,
					AP1ID = Source.AP1ID,
					AP1 = Source.AP1,
					AP2 = Source.AP2,
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([SiteID],
				SiteName,
				SiteGroupID,
				SiteGroup,
				Dealer,
				GSCMAccountID,
				GSCMAccount,
				AP1ID,
				AP1,
				AP2,
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (
				Source.[SiteID],
				Source.[SiteName],
				Source.[SiteGroupID],
				Source.[SiteGroup],
				Source.[Dealer],
				Source.[GSCMAccountID],
				Source.[GSCMAccount],
				Source.[AP1ID],
				Source.[AP1],
				Source.[AP2],
				Source.[TransactionID],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[MF_SAVE_ModelAttributesMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MF_SAVE_ModelAttributesMF]
	-- Add the parameters for the stored procedure here
	@Data MF_ModelAttributes readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE MF_ModelAttributes AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup] AND
			 Target.[ProductType] = Source.[ProductType] AND
			 Target.[Model] = Source.[Model]
		)
	WHEN MATCHED THEN 
		 UPDATE SET [Attribute1] = Source.[Attribute1],
					[Attribute2] = Source.[Attribute2],
					[Attribute3] = Source.[Attribute3],
					[Attribute4] = Source.[Attribute4],
					[Attribute5] = Source.[Attribute5],
					[Attribute6] = Source.[Attribute6],
					[Attribute7] = Source.[Attribute7],
					[Attribute8] = Source.[Attribute8],
					[Attribute9] = Source.[Attribute9],
					[Attribute10] = Source.[Attribute10],
					[Attribute11] = Source.[Attribute11],
					[Attribute12] = Source.[Attribute12],
					[Attribute13] = Source.[Attribute13],
					[Attribute14] = Source.[Attribute14],
					[Attribute15] = Source.[Attribute15],
					[Attribute16] = Source.[Attribute16],
					[Attribute17] = Source.[Attribute17],
					[Attribute18] = Source.[Attribute18],
					[Attribute19] = Source.[Attribute19],
					[Attribute20] = Source.[Attribute20],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([ProductGroup],
				[ProductType],
				[Model],
				[Attribute1],
				[Attribute2],
				[Attribute3],
				[Attribute4],
				[Attribute5],
				[Attribute6],
				[Attribute7],
				[Attribute8],
				[Attribute9],
				[Attribute10] ,
				[Attribute11],
				[Attribute12],
				[Attribute13],
				[Attribute14],
				[Attribute15],
				[Attribute16],
				[Attribute17],
				[Attribute18],
				[Attribute19],
				[Attribute20],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (
				Source.[ProductGroup],
				Source.[ProductType],
				Source.[Model],
				Source.[Attribute1],
				Source.[Attribute2],
				Source.[Attribute3],
				Source.[Attribute4],
				Source.[Attribute5],
				Source.[Attribute6],
				Source.[Attribute7],
				Source.[Attribute8],
				Source.[Attribute9],
				Source.[Attribute10] ,
				Source.[Attribute11],
				Source.[Attribute12],
				Source.[Attribute13],
				Source.[Attribute14],
				Source.[Attribute15],
				Source.[Attribute16],
				Source.[Attribute17],
				Source.[Attribute18],
				Source.[Attribute19],
				Source.[Attribute20],
				Source.[TransactionID],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[MF_SAVE_ModelManagementMappingMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MF_SAVE_ModelManagementMappingMF]
	-- Add the parameters for the stored procedure here
	@Data MF_ModelManagementMapping readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE MF_ModelManagementMapping AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup] AND
			 Target.[ProductType] = Source.[ProductType] AND
			 Target.[Model] = Source.[Model]
		)
	WHEN MATCHED THEN 
		 UPDATE SET [ModelStatus] = Source.[ModelStatus],
					[ModelDescription] = Source.[ModelDescription],
					[ModelClassification] = Source.[ModelClassification],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([ProductGroup],
				[ProductType],
				[Model],
				[ModelStatus],
				[ModelDescription],
				[ModelClassification],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (
				Source.[ProductGroup],
				Source.[ProductType],
				Source.[Model],
				Source.[ModelStatus],
				Source.[ModelDescription],
				Source.[ModelClassification],
				Source.[TransactionID],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[MF_SAVE_ProductGroupAttributesMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MF_SAVE_ProductGroupAttributesMF]
	-- Add the parameters for the stored procedure here
	@Data [MF_ProductGroupAttributes] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE [MF_ProductGroupAttributes] AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup] AND
			 Target.[AttributeColumn] = Source.[AttributeColumn] 

		)
	WHEN MATCHED THEN 
		 UPDATE SET [Description] = Source.[Description],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([ProductGroup],
				[AttributeColumn],
				[Description],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (
				Source.[ProductGroup],
				Source.[AttributeColumn],
				Source.[Description],
				Source.[TransactionID],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[MF_SAVE_StoreListAttributesMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MF_SAVE_StoreListAttributesMF]
	-- Add the parameters for the stored procedure here
	@Data [MF_StoreListAttributes] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE [MF_StoreListAttributes] AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[SiteID] = Source.[SiteID] AND
			 Target.[SiteName] = Source.[SiteName] AND
			 Target.[AttributeColumn] = Source.[AttributeColumn] 
		)
	WHEN MATCHED THEN 
		 UPDATE SET [Description] = Source.[Description],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([SiteID],
				[SiteName],
				[AttributeColumn],
				[Description],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (
				Source.[SiteID],
				Source.[SiteName],
				Source.[AttributeColumn],
				Source.[Description],
				Source.[TransactionID],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[MF_SAVE_WOSTargetMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MF_SAVE_WOSTargetMF]
	-- Add the parameters for the stored procedure here
	@Data [MF_WOSTarget] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE [MF_WOSTarget] AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[AP1] = Source.[AP1]  AND
			 Target.[GSCMAccount] = Source.[GSCMAccount]  AND
			 Target.[Dealer] = Source.[Dealer]  AND
			 Target.[ProductGroup] = Source.[ProductGroup]  AND
			 Target.[Week] = Source.[Week] 
		)
	WHEN MATCHED THEN 
		 UPDATE SET [Average] = Source.[Average],
					[Value] = Source.[Value],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([AP1],
				[GSCMAccount],
				[Dealer],
				[ProductGroup],
				[Week],
				[Average],
				[Value],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (
				Source.[AP1],
				Source.[GSCMAccount],
				Source.[Dealer],
				Source.[ProductGroup],
				Source.[Week],
				Source.[Average],
				Source.[Value],
				Source.[TransactionID],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[MF_SAVE_YearDateMappingMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MF_SAVE_YearDateMappingMF]
	-- Add the parameters for the stored procedure here
	@Data [MF_YearDateMapping] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE [MF_YearDateMapping] AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[Week] = Source.[Week]  
		)
	WHEN MATCHED THEN 
		 UPDATE SET [SellInYearMonth] = Source.[SellInYearMonth],
					[SellOutYearMonth] = Source.[SellOutYearMonth],
					[WeekMapping] = Source.[WeekMapping],
					[MonthMapping] = Source.[MonthMapping],
					[YearMapping] = Source.[YearMapping],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([Week],
				[SellInYearMonth],
				[SellOutYearMonth],
				[WeekMapping],
				[MonthMapping],
				[YearMapping],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (
				Source.[Week],
				Source.[SellInYearMonth],
				Source.[SellOutYearMonth],
				Source.[WeekMapping],
				Source.[MonthMapping],
				Source.[YearMapping],
				Source.[TransactionID],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[spu_GET_ChannelPSITF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_ChannelPSITF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		ProductGroup 'Product Grp',
		AP1,
		Account 'Account',
		Item,
		Category,
		[Week] ,
		[Value]
	FROM TF_AccountLevelPSIMgmt
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND ProductGroup = @PGROUP AND Account = @ACCOUNT 
	ORDER BY [Week] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_FlooringSiteTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_FlooringSiteTF]
	@WEEKS NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		Brand,
		ProductGroup 'Product Group',
		ProductType 'Sub Category',
		Capacity ,
		Model,
		[SellOut] 'Sell Out'
      ,[Display]
      ,[Inventory]
      ,[StockRequested] 'Stock Requested'
      ,[StockDelivered] 'Stock Delivered'
      ,[PromoterName] 'Promoter Name'
      ,[StoreCode] 'Store Code'
      ,[StoreName] 'Store Name'
      ,[Week]
      ,[Agency]
      ,[Dealer]
      ,[Segment]
      ,[TeamHead] 'Team Head'
      ,[MidasInCharge] 'Midas In Charge'
      ,[Area]
      ,[ModelStatus] 'Model Status'
      ,[BWL]
      ,[AP1Tagging] 'AP1 Tagging' 
	FROM TF_FlooringSite
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND ProductGroup = @PGROUP 
	ORDER BY [Week] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_FlooringTargetTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_FlooringTargetTF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[StoreCode] 'Store Code'
      ,[StoreName] 'Store Name'
      ,[Dealer]
      ,[Account] 'GSCM Account'
      ,[AP1] 
      ,[ProductGroup] 'Product Group' 
      ,[ProductType] 'Product Type'
      ,[Model]
      ,[DisplayTarget] 'Display Target'
      ,[Week]
      ,[Value]
	FROM TF_FlooringTarget
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND ProductGroup = @PGROUP AND Account = @ACCOUNT 
	ORDER BY [Week] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_GrossNetPriceTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_GrossNetPriceTF]
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[ID]
      ,[Year]
      ,[Month]
      ,[AP1]
      ,[GSCMAccount] 'GSCM Account'
      ,[Dealer]
      ,[Model]
      ,[ProductGroup] 'Product Group'
      ,[ProductType]
      ,[ModelClassification] 'Model Classification'
      ,[GrossPrice] 'Gross Price'
      ,[NetPrice] 'Net Price'
	FROM TF_GrossNetPrice
	WHERE ProductGroup = @PGROUP AND GSCMAccount = @ACCOUNT 
	ORDER BY [ProductGroup] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_KPI_AP1_Account_TF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_KPI_AP1_Account_TF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	   [YearMonth]
      ,[YearWeek]
      ,[ProductGroup]
      ,[GSCMAccount] 'GSCM Account'
      ,[Value]
	FROM TF_KPIAccountSellOutFA
	WHERE [YearMonth] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND GSCMAccount = @ACCOUNT AND ProductGroup = @PGROUP
	ORDER BY [YearMonth] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_KPI_AP1_TF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_KPI_AP1_TF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	   [YearMonth]
      ,[YearWeek]
      ,[ProductGroup]
      ,[GSCMAccount] 'GSCM Account'
      ,[Value]
	FROM [TF_KPIAP1FAProductGroup]
	WHERE [YearMonth] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND GSCMAccount = @ACCOUNT AND ProductGroup = @PGROUP
	ORDER BY [YearMonth] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_SellInOutTargetATF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_SellInOutTargetATF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[Dealer]
      ,[GSCMAccount] 'GSCM Account'
      ,[AP1] 
      ,[Week]
      ,[Value]
	FROM TF_SellInSellOutTargetAmount
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND GSCMAccount = @ACCOUNT 
	ORDER BY [Week] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_SellInOutTargetQTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_SellInOutTargetQTF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[Dealer]
      ,[GSCMAccount] 'GSCM Account'
      ,[AP1] 
      ,[Category]
      ,[Week]
      ,[Value]
	FROM TF_SellInSellOutTargetQty
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND GSCMAccount = @ACCOUNT 
	ORDER BY [Week] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_TIMTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_TIMTF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[Dealer]
      ,[GSCMAccount] 'GSCM Account'
      ,[AP1]
      ,[Category]
      ,[Week]
      ,[Value]
	FROM TF_TIM
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND GSCMAccount = @ACCOUNT
	ORDER BY [Week] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GET_WOSTargetMF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_GET_WOSTargetMF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[AP1]
      ,[GSCMAccount] 'GSCM Account'
      ,[Dealer]
      ,[ProductGroup] 'Product Group'
      ,[Week]
      ,[Value]
	FROM MF_WOSTarget
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND GSCMAccount = @ACCOUNT AND ProductGroup = @PGROUP
	ORDER BY [Week] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[spu_getAP1ForecastTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spu_getAP1ForecastTF]
	@WEEKS NVARCHAR(MAX),
	@ACCOUNT NVARCHAR(MAX),
	@PGROUP NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		GSCMAccount 'Account',
		ProductGroup 'Product Grp',
		Item,
		Category,
		[Week] ,
		[Value]
	FROM TF_AP1Forecast
	WHERE [Week] IN (SELECT val FROM dbo.f_split(@WEEKS, ',')) AND ProductGroup = @ACCOUNT AND GSCMAccount = @PGROUP
	ORDER BY [Week] ASC
END

GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_AccountLevelChannelPSITF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_AccountLevelChannelPSITF]
	-- Add the parameters for the stored procedure here
	@Data TF_ACCOUNT_LEVEL_CHANNEL_PSI_MGMT readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    MERGE TF_AccountLevelPSIMgmt AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup] AND Target.[AP1] = Source.[AP1]
			  AND Target.[Account] = Source.[Account] AND Target.[Item] = Source.[Item] AND   Target.[Category] = Source.[Category] 
			  AND Target.[Week] = Source.[Week]
		)
	WHEN MATCHED THEN 
		 UPDATE SET [Value] = Source.[Value],
					[DateModified]  = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([TransactionID],[ProductGroup],
					[AP1],
					[Account],
					[Item],
					[Category],
					[Week],
					[Value],
					[DateCreated],
					[CreatedBy])
		 VALUES (Source.[TransactionID],Source.[ProductGroup],
					Source.[AP1],
					Source.[Account],
					Source.[Item],
					Source.[Category],
					Source.[Week],
					Source.[Value],
				GETDATE(),Source.[User]);
END


GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_AP1ForecastTransactionTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_AP1ForecastTransactionTF]
	-- Add the parameters for the stored procedure here
	@Data TF_AP1_FORECAST readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    MERGE TF_AP1Forecast AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup]  AND Target.[GSCMAccount] = Source.[Account] AND Target.[Item] = Source.[Item] AND   Target.[Category] = Source.[Category] 
			  AND Target.[Week] = Source.[Week]
		)
	WHEN MATCHED THEN 
		 UPDATE SET Value = Source.Value,
					[DateModified]  = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([TransactionID],[ProductGroup],
					[GSCMAccount],
					[Item],
					[Category],
					[Week],
					Value,
					[DateCreated],
					[CreatedBy])
		 VALUES (Source.TransactionID,
		            Source.[ProductGroup],
					Source.[Account],
					Source.[Item],
					Source.[Category],
					Source.[Week],
					Source.[Value],
				GETDATE(),Source.[User]);
END


GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_AP2PSITF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_AP2PSITF]
	-- Add the parameters for the stored procedure here
	@Data TF_AP2_PSI readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    MERGE TF_AP2PSI AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup]AND Target.[Item] = Source.[Item] AND   Target.[Category] = Source.[Category] 
			  AND Target.[Week] = Source.[Week]
		)
	WHEN MATCHED THEN 
		 UPDATE SET Value = Source.Value,
					[DateModified]  = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([TransactionID],[ProductGroup],
					[Item],
					[Category],
					[Week],
					Value,
					[DateCreated],
					[CreatedBy])
		 VALUES (Source.TransactionID,
		            Source.[ProductGroup],
					Source.[Item],
					Source.[Category],
					Source.[Week],
					Source.[Value],
				GETDATE(),Source.[User]);
END


GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_FlooringSiteTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_FlooringSiteTF]
	-- Add the parameters for the stored procedure here
	@Data [TF_FLOORING_SITE] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_FlooringSite AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[Brand] = Source.[Brand] AND Target.[ProductGroup] = Source.[ProductGroup] AND Target.[ProductType] = Source.[ProductType] AND
			 Target.[Capacity] = Source.[Capacity] AND Target.[Model] = Source.[Model] AND Target.[StoreCode] = Source.[StoreCode] AND
			 Target.[StoreName] = Source.[StoreName] AND Target.[Week] = Source.[Week] AND Target.[Dealer] = Source.[Dealer]
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[SellOut] = Source.[SellOut],
					[Display] = Source.[Display],
					[Inventory] = Source.[Inventory],
					[StockRequested] = Source.[StocksRequested],
					[StockDelivered] = Source.[StocksDelivered],
					[PromoterName] = Source.[PromoterName],
					[Agency] = Source.[Agency],
					[Segment] = Source.[Segment],
					[TeamHead] = Source.[TeamHead],
					[MidasInCharge] = Source.[MidasInCharge],
					[Area] = Source.[Area],
					[ModelStatus] = Source.[ModelStatus],
					[BWL] = Source.[BWL],
					[AP1Tagging] = Source.[AP1Tagging]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([Brand] ,
				[ProductGroup] ,
				[ProductType] ,
				[Capacity] ,
				[Model] ,
				[SellOut] ,
				[Display] ,
				[Inventory] ,
				[StockRequested] ,
				[StockDelivered] ,
				[PromoterName], 
				[StoreCode] ,
				[StoreName] ,
				[Week] ,
				[Agency], 
				[Dealer] ,
				[Segment] ,
				[TeamHead] ,
				[MidasInCharge], 
				[Area] ,
				[ModelStatus], 
				[BWL] ,
				[AP1Tagging], 
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[Brand] ,
				Source.[ProductGroup] ,
				Source.[ProductType] ,
				Source.[Capacity] ,
				Source.[Model] ,
				Source.[SellOut] ,
				Source.[Display] ,
				Source.[Inventory] ,
				Source.[StocksRequested] ,
				Source.[StocksDelivered] ,
				Source.[PromoterName], 
				Source.[StoreCode] ,
				Source.[StoreName] ,
				Source.[Week] ,
				Source.[Agency], 
				Source.[Dealer] ,
				Source.[Segment] ,
				Source.[TeamHead] ,
				Source.[MidasInCharge], 
				Source.[Area] ,
				Source.[ModelStatus], 
				Source.[BWL] ,
				Source.[AP1Tagging],
				Source.[TransactionID],GETDATE(),Source.[User]);

END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_FlooringTargetTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[TF_SAVE_FlooringTargetTF]
	-- Add the parameters for the stored procedure here
	@Data [TF_FLOORING_TARGET] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_FlooringTarget AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[StoreCode] = Source.[StoreCode] AND Target.[StoreName] = Source.[StoreName] AND Target.[Dealer] = Source.[Dealer] AND
			 Target.[Account] = Source.[Account] AND Target.[AP1] = Source.[AP1] AND Target.[ProductGroup] = Source.[ProductGroup] AND
			 Target.[ProductType] = Source.[ProductType] AND Target.[Model] = Source.[Model] AND Target.[Week] = Source.[Week]
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[DisplayTarget] = Source.[DisplayTarget],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([StoreCode],
				[StoreName],
				[Dealer],
				[Account],
				[AP1],
				[ProductGroup],
				[ProductType],
				[Model],
				[DisplayTarget],
				[Week],
				[Value],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[StoreCode],
				Source.[StoreName],
				Source.[Dealer],
				Source.[Account],
				Source.[AP1],
				Source.[ProductGroup],
				Source.[ProductType],
				Source.[Model],
				Source.[DisplayTarget],
				Source.[Week],
				Source.[Value],
				Source.[TransactionID],GETDATE(),Source.[User]);

END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_GrossNetPriceTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[TF_SAVE_GrossNetPriceTF]
	-- Add the parameters for the stored procedure here
	@Data [TF_GROSS_NET_PRICE] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_GrossNetPrice AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[Year] = Source.[Year]  AND Target.[Month] = Source.[Month]  AND 
			 Target.[AP1] = Source.[AP1]  AND Target.[GSCMAccount] = Source.[GSCMAccount]  AND 
			 Target.[Dealer] = Source.[Dealer]  AND Target.[Model] = Source.[Model]  
		)
	WHEN MATCHED THEN 
		 UPDATE SET [GrossPrice] = Source.[GrossPrice],
					[NetPrice] = Source.[NetPrice],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([TransactionID],[Year],[Month],[AP1],[GSCMAccount],[Dealer],[Model],[ProductGroup]
				,[ProductType],[ModelClassification],[GrossPrice],[NetPrice],[DateCreated],[CreatedBy])
		 VALUES (Source.[TransactionID],Source.[Year],Source.[Month], Source.[AP1], Source.[GSCMAccount], Source.[Dealer],
				 Source.[Model],Source.[ProductGroup],Source.[ProductType],Source.[ModelClassification],
				 Source.[GrossPrice],Source.[NetPrice],GETDATE(),Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_KPIAccountSellOutFATF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_KPIAccountSellOutFATF]
	-- Add the parameters for the stored procedure here
	@Data [TF_KPI_ACCOUNT_SELLOUT_FA] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE [TF_KPIAccountSellOutFA] AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[YearMonth] = Source.[YearMonth] AND Target.[YearWeek] = Source.[YearWeek] AND Target.[ProductGroup] = Source.[ProductGroup] AND
			 Target.[GSCMAccount] = Source.[GSCMAccount] 
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([YearMonth],
				[YearWeek],
				[ProductGroup],
				[Value],
				[GSCMAccount], 
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[YearMonth],
				Source.[YearWeek],
				Source.[ProductGroup],
				Source.[Value],
				Source.[GSCMAccount], 
				Source.[TransactionID],GETDATE(),Source.[User]);

END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_KPIAP1FAProductGroupTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_KPIAP1FAProductGroupTF]
	-- Add the parameters for the stored procedure here
	@Data [TF_KPI_AP1_FA_PRODUCTGROUP] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_KPIAP1FAProductGroup AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[YearMonth] = Source.[YearMonth] AND Target.[YearWeek] = Source.[YearWeek] AND Target.[ProductGroup] = Source.[ProductGroup] AND
			 Target.[GSCMAccount] = Source.[GSCMAccount] 
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([YearMonth],
				[YearWeek],
				[ProductGroup],
				[Value],
				[GSCMAccount], 
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[YearMonth],
				Source.[YearWeek],
				Source.[ProductGroup],
				Source.[Value],
				Source.[GSCMAccount], 
				Source.[TransactionID],GETDATE(),Source.[User]);

END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_KPIAP2FAProductGroupTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_KPIAP2FAProductGroupTF]
	-- Add the parameters for the stored procedure here
	@Data [TF_KPI_AP2_FA_PRODUCTGROUP] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_KPIAP2FAProductGroup AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[YearMonth] = Source.[YearMonth] AND Target.[YearWeek] = Source.[YearWeek] AND Target.[ProductGroup] = Source.[ProductGroup]
			 
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([YearMonth],
				[YearWeek],
				[ProductGroup],
				[Value],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[YearMonth],
				Source.[YearWeek],
				Source.[ProductGroup],
				Source.[Value],
				Source.[TransactionID],GETDATE(),Source.[User]);

END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SALESTARGETTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SALESTARGETTF]
	-- Add the parameters for the stored procedure here
	@Data TF_SALES_TARGET readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    MERGE TF_SalesTarget AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup]AND Target.[AP1] = Source.[AP1] AND   Target.[GSCMAccount] = Source.[GSCMAccount] 
			  AND Target.[Dealer] = Source.[Dealer] AND Target.[TargetName] = Source.[TargetName] AND Target.[YearMonth] = Source.[YearMonth]
		)
	WHEN MATCHED THEN 
		 UPDATE SET Amount = Source.Amount,
					[DateModified]  = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([TransactionID],[ProductGroup],
					[AP1],
					[Dealer],
					[GSCMAccount],
					[TargetName],
					[YearMonth],
					[Amount],
					[DateCreated],
					[CreatedBy])
		 VALUES (Source.TransactionID,
		            Source.[ProductGroup],
					Source.[AP1],
					Source.[Dealer],
					Source.[GSCMAccount],
					Source.[TargetName],
					Source.[YearMonth],
					Source.[Amount],
				GETDATE(),Source.[User]);
END


GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SellInBillingTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SellInBillingTF]
	-- Add the parameters for the stored procedure here
	@Data [TF_SELLIN_BILLING] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_SellinBilling AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[BusinessGroup] = Source.BusinessGroup AND
			 Target.[YearMonth] = Source.[YearMonth] AND
			 Target.[SoldToPartyCode]= Source.[SoldToPartyCode] AND
			 Target.[Area] = Source.[Area] AND
			 Target.[Dealer] = Source.[Dealer] AND
			 Target.[AP1] = Source.[AP1] AND
			 Target.[Model] = Source.[Model] AND
			 Target.[AmountType] = Source.[AmountType] 
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Qty] = Source.[Qty],
					[Amount] = Source.[Amount]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([BusinessGroup],
			 [YearMonth],
			 [SoldToPartyCode],
			 [Area],
			 [Dealer],
			 [AP1],
			 [SoldToPartyName],
			 [Model],
			 [ProductGroup],
			 [ProductType],
			 [Description],
			 [Qty],
			 [Amount],
			 [AmountType],
			 [TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[BusinessGroup],
			 Source.[YearMonth],
			 Source.[SoldToPartyCode],
			 Source.[Area],
			 Source.[Dealer],
			 Source.[AP1],
			 Source.[SoldToPartyName],
			 Source.[Model],
			 Source.[ProductGroup],
			 Source.[ProductType],
			 Source.[Description],
			 Source.[Qty],
			 Source.[Amount],
			 Source.[AmountType],
				Source.[TransactionID],GETDATE(),Source.[User]);

END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SellInGITF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SellInGITF]
	-- Add the parameters for the stored procedure here
	@Data [TF_SellInGI] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_SellInGI AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[GC] = Source.[GC]  AND 
			 Target.[AP2] = Source.[AP2]  AND 
			 Target.[AP1] = Source.[AP1]  AND 
			 Target.[GSCMAccount] = Source.[GSCMAccount]  AND 
			 Target.[Model] = Source.[Model] AND
			 Target.[SoldToName] = Source.[SoldToName] AND
			 Target.[ShipToName1] = Source.[ShipToName1] AND
			 Target.[ShipToName2] = Source.[ShipToName2]

		)
	WHEN MATCHED THEN 
		 UPDATE SET [SaleQty] = Source.[SaleQty],
					[AmountUSD] = Source.[AmountUSD],
					[AmountKRW] = Source.[AmountKRW],
					[Currency] = Source.[Currency],
					[Amount] = Source.[Amount],
					[EntryDate] = Source.[EntryDate],
					[SalesDate] = Source.[SalesDate],
					[SalesWeek] = Source.[SalesWeek],
					[SoldToName] = Source.[SoldToName],
					[ShipToName1] = Source.[ShipToName1],
					[ShipToName2] = Source.[ShipToName2],
					[Plant] = Source.[Plant],
					[Storage] = Source.[Storage],
					[ProductCode] = Source.[ProductCode],
					[BillToParty] = Source.[BillToParty],
					[SoldToPartyCode] = Source.[SoldToPartyCode],
					[ShipToPartyCode] = Source.[ShipToPartyCode],
					[Company] = Source.[Company],
					[SalesOrg] = Source.[SalesOrg],
					[SalesOffice] = Source.[SalesOffice],
					[ProductType] = Source.[ProductType],
					[Attribute1] = Source.[Attribute1],
					[Attribute2] = Source.[Attribute2],
					[Attribute3] = Source.[Attribute3],
					[Buyer] = Source.[Buyer],
					[Type] = Source.[Type],
					[ToolName] = Source.[ToolName],
					[BasicName] = Source.[BasicName],
					[Project] = Source.[Project],
					[ModelGroup] = Source.[ModelGroup],
					[DistributionChannel] = Source.[DistributionChannel],
					[DCDesc] = Source.[DCDesc],
					[SalesDocType] = Source.[SalesDocType],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([GC],
				[AP2],
				[AP1],
				[GSCMAccount],
				[Model],
				[SaleQty],
				[AmountUSD],
				[AmountKRW],
				[Currency],
				[Amount],
				[EntryDate],
				[SalesDate],
				[SalesWeek],
				[SoldToName],
				[ShipToName1],
				[ShipToName2],
				[Plant],
				[Storage],
				[ProductCode],
				[BillToParty],
				[SoldToPartyCode],
				[ShipToPartyCode],
				[Company],
				[SalesOrg],
				[SalesOffice],
				[ProductType],
				[Attribute1],
				[Attribute2],
				[Attribute3],
				[Buyer],
				[Type],
				[ToolName],
				[BasicName],
				[Project],
				[ModelGroup],
				[DistributionChannel],
				[DCDesc],
				[SalesDocType],
				[TransactionID],
				[DateCreated],
				[CreatedBy])
		 VALUES (Source.[GC],
				Source.[AP2],
				Source.[AP1],
				Source.[GSCMAccount],
				Source.[Model],
				Source.[SaleQty],
				Source.[AmountUSD],
				Source.[AmountKRW],
				Source.[Currency],
				Source.[Amount],
				Source.[EntryDate],
				Source.[SalesDate],
				Source.[SalesWeek],
				Source.[SoldToName],
				Source.[ShipToName1],
				Source.[ShipToName2],
				Source.[Plant],
				Source.[Storage],
				Source.[ProductCode],
				Source.[BillToParty],
				Source.[SoldToPartyCode],
				Source.[ShipToPartyCode],
				Source.[Company],
				Source.[SalesOrg],
				Source.[SalesOffice],
				Source.[ProductType],
				Source.[Attribute1],
				Source.[Attribute2],
				Source.[Attribute3],
				Source.[Buyer],
				Source.[Type],
				Source.[ToolName],
				Source.[BasicName],
				Source.[Project],
				Source.[ModelGroup],
				Source.[DistributionChannel],
				Source.[DCDesc],
				Source.[SalesDocType],
				Source.[TransactionID],
				GETDATE(),
				Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SellInOut_Target(Amount)TF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SellInOut_Target(Amount)TF]
	-- Add the parameters for the stored procedure here
	@Data [TF_SELLIN_SELLOUT_TARGET_AMOUNT] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_SellInSellOutTargetAmount AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[Dealer] = Source.[Dealer] AND Target.[AP1] = Source.[AP1] AND Target.[Week] = Source.[Week] AND
			 Target.[GSCMAccount] = Source.[Account] AND Target.[Type] = Source.[Type]
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([Dealer],
				[AP1],
				[GSCMAccount], 
				[Type],
				[Week],
				[Value],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[Dealer],
				Source.[AP1],
				Source.[Account],
				Source.[Type],
				Source.[Week],
				Source.[Value], 
				Source.[TransactionID],GETDATE(),Source.[User]);

END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SellInOut_Target(Quantity)TF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SellInOut_Target(Quantity)TF]
	-- Add the parameters for the stored procedure here
	@Data [TF_SELLIN_SELLOUT_TARGET_QTY] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_SellInSellOutTargetQty AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[Dealer] = Source.[Dealer] AND Target.[AP1] = Source.[AP1] AND Target.[Week] = Source.[Week] AND
			 Target.[GSCMAccount] = Source.[Account] AND Target.[Category] = Source.[Category]
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([Dealer],
				[AP1],
				[GSCMAccount], 
				[Category], 
				[Week],
				[Value],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[Dealer],
				Source.[AP1],
				Source.[Account],
				Source.[Category],
				Source.[Week],
				Source.[Value], 
				Source.[TransactionID],GETDATE(),Source.[User]);

END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SellOutAndInventoryMonthlyTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SellOutAndInventoryMonthlyTF]
	-- Add the parameters for the stored procedure here
	@Data [TF_SellOutAndInventoryMonthly] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE [TF_SellOutAndInventoryMonthly] AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[DivisionGroup] = Source.[DivisionGroup]  AND 
			 Target.[Division] = Source.[Division]  AND 
			 Target.[ProductGroup] = Source.[ProductGroup]  AND 
			 Target.[ProductType] = Source.[ProductType]  AND 
			 Target.[Model] = Source.[Model] AND
			 Target.[Brand] = Source.[Brand] AND
			 Target.[Alias] = Source.[Alias] AND
			 Target.[Subsidiary] = Source.[Subsidiary] AND
			 Target.[ChannelCode] = Source.[ChannelCode] AND
			 Target.[Channel] = Source.[Channel] AND
			 Target.[ChannelCategory] = Source.[ChannelCategory] AND
			 Target.[ChannelGroup] = Source.[ChannelGroup] AND
			 Target.[ChannelType] = Source.[ChannelType] AND
			 Target.[Region] = Source.[Region] AND
			 Target.[State] = Source.[State] AND
			 Target.[City] = Source.[City] AND
			 Target.[District] = Source.[District] AND
			 Target.[Month] = Source.[Month] 

		)
	WHEN MATCHED THEN 
		 UPDATE SET [RRPPriceLoc] = Source.[RRPPriceLoc],
					[Purchase] = Source.[Purchase],
					[Sales] = Source.[Sales],
					[SellThru] = Source.[SellThru],
					[Inventory] = Source.[Inventory],
					[DisplayQty] = Source.[DisplayQty],
					[SalesPrc] = Source.[SalesPrc],
					[AmtSRRPLoc] = Source.[AmtSRRPLoc],
					[InvoiceAmountUsd] = Source.[InvoiceAmountUsd],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([DivisionGroup],
				 [Division],
				 [ProductGroup],
				 [ProductType] ,
				 [Model],
				 [Brand],
				 [Alias],
				 [Subsidiary],
				 [ChannelCode],
				 [Channel],
				 [ChannelCategory],
				 [ChannelGroup] ,
				 [ChannelType],
				 [Region],
				 [State],
				 [City],
				 [District], 
				 [Month],
				 [RRPPriceLoc],
				[Purchase],
				[Sales],
				[SellThru],
				[Inventory],
				[DisplayQty],
				[SalesPrc],
				[AmtSRRPLoc],
				[InvoiceAmountUsd],
				[TransactionID],
				[DateCreated],
				[CreatedBy])
		 VALUES (Source.[DivisionGroup],
				 Source.[Division],
				 Source.[ProductGroup],
				 Source.[ProductType] ,
				 Source.[Model],
				 Source.[Brand],
				 Source.[Alias],
				 Source.[Subsidiary],
				 Source.[ChannelCode],
				 Source.[Channel],
				 Source.[ChannelCategory],
				 Source.[ChannelGroup] ,
				 Source.[ChannelType],
				 Source.[Region],
				 Source.[State],
				 Source.[City],
				 Source.[District], 
				 Source.[Month],
				 Source.[RRPPriceLoc],
				Source.[Purchase],
				Source.[Sales],
				Source.[SellThru],
				Source.[Inventory],
				Source.[DisplayQty],
				Source.[SalesPrc],
				Source.[AmtSRRPLoc],
				Source.[InvoiceAmountUsd],
				Source.[TransactionID],
				GETDATE(),
				Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SellOutAndInventoryWeeklyTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SellOutAndInventoryWeeklyTF]
	-- Add the parameters for the stored procedure here
	@Data [TF_SellOutAndInventoryWeekly] readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE [TF_SellOutAndInventoryWeekly] AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[DivisionGroup] = Source.[DivisionGroup]  AND 
			 Target.[Division] = Source.[Division]  AND 
			 Target.[ProductGroup] = Source.[ProductGroup]  AND 
			 Target.[ProductType] = Source.[ProductType]  AND 
			 Target.[Model] = Source.[Model] AND
			 Target.[Brand] = Source.[Brand] AND
			 Target.[Alias] = Source.[Alias] AND
			 Target.[Subsidiary] = Source.[Subsidiary] AND
			 Target.[ChannelCode] = Source.[ChannelCode] AND
			 Target.[Channel] = Source.[Channel] AND
			 Target.[ChannelCategory] = Source.[ChannelCategory] AND
			 Target.[ChannelGroup] = Source.[ChannelGroup] AND
			 Target.[ChannelType] = Source.[ChannelType] AND
			 Target.[Region] = Source.[Region] AND
			 Target.[State] = Source.[State] AND
			 Target.[City] = Source.[City] AND
			 Target.[District] = Source.[District] AND
			 Target.[Week] = Source.[Week] 

		)
	WHEN MATCHED THEN 
		 UPDATE SET [RRPPriceLoc] = Source.[RRPPriceLoc],
					[Purchase] = Source.[Purchase],
					[Sales] = Source.[Sales],
					[SellThru] = Source.[SellThru],
					[Inventory] = Source.[Inventory],
					[DisplayQty] = Source.[DisplayQty],
					[SalesPrc] = Source.[SalesPrc],
					[AmtSRRPLoc] = Source.[AmtSRRPLoc],
					[InvoiceAmountUsd] = Source.[InvoiceAmountUsd],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([DivisionGroup],
				 [Division],
				 [ProductGroup],
				 [ProductType] ,
				 [Model],
				 [Brand],
				 [Alias],
				 [Subsidiary],
				 [ChannelCode],
				 [Channel],
				 [ChannelCategory],
				 [ChannelGroup] ,
				 [ChannelType],
				 [Region],
				 [State],
				 [City],
				 [District], 
				 [Week],
				 [RRPPriceLoc],
				[Purchase],
				[Sales],
				[SellThru],
				[Inventory],
				[DisplayQty],
				[SalesPrc],
				[AmtSRRPLoc],
				[InvoiceAmountUsd],
				[TransactionID],
				[DateCreated],
				[CreatedBy])
		 VALUES (Source.[DivisionGroup],
				 Source.[Division],
				 Source.[ProductGroup],
				 Source.[ProductType] ,
				 Source.[Model],
				 Source.[Brand],
				 Source.[Alias],
				 Source.[Subsidiary],
				 Source.[ChannelCode],
				 Source.[Channel],
				 Source.[ChannelCategory],
				 Source.[ChannelGroup] ,
				 Source.[ChannelType],
				 Source.[Region],
				 Source.[State],
				 Source.[City],
				 Source.[District], 
				 Source.[Week],
				 Source.[RRPPriceLoc],
				Source.[Purchase],
				Source.[Sales],
				Source.[SellThru],
				Source.[Inventory],
				Source.[DisplayQty],
				Source.[SalesPrc],
				Source.[AmtSRRPLoc],
				Source.[InvoiceAmountUsd],
				Source.[TransactionID],
				GETDATE(),
				Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SellOutMonthlyTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SellOutMonthlyTF]
	-- Add the parameters for the stored procedure here
	@Data TF_SellOutMonthly readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_SellOutMonthly AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[Subsidiary] = Source.[Subsidiary] AND
			 Target.[DivisionGroup] = Source.[DivisionGroup]  AND 
			 Target.[Division] = Source.[Division]  AND 
			 Target.[ProductGroup] = Source.[ProductGroup]  AND 
			 Target.[ProductType] = Source.[ProductType]  AND 
			 Target.[Model] = Source.[Model] AND
			 Target.[Brand] = Source.[Brand] AND
			 Target.[Alias] = Source.[Alias] AND
			 Target.[PromoterID] = Source.[Alias] AND
			 Target.[Promoter] = Source.[Alias] AND
			 Target.[MobileNo] = Source.[Alias] AND
			 Target.[Region] = Source.[Region] AND
			 Target.[State] = Source.[State] AND
			 Target.[City] = Source.[City] AND
			 Target.[District] = Source.[District] AND
			 Target.[NationCD] = Source.[District] AND
			 Target.[ChannelCode] = Source.[ChannelCode] AND
			 Target.[Channel] = Source.[Channel] AND
			 Target.[Contact] = Source.[Channel] AND
			 Target.[PhoneNo] = Source.[Channel] AND
			 Target.[ChannelCategory] = Source.[ChannelCategory] AND
			 Target.[ShipmentType] = Source.[Channel] AND
			 Target.[AggregationtyPE] = Source.[Channel] AND
			 Target.[ChannelGroup] = Source.[ChannelGroup] AND
			 Target.[ChannelType] = Source.[ChannelType] AND
			 Target.[Month] = Source.[Month] 

		)
	WHEN MATCHED THEN 
		 UPDATE SET [RRPPriceLoc] = Source.[RRPPriceLoc],
					[Vat] = Source.[Vat],
					[RRPLoc] = Source.[RRPLoc],
					[SKUSales] = Source.[SKUSales],
					[IMEISales] = Source.[IMEISales],
					[Sales] = Source.[Sales],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([Subsidiary] ,
				[DivisionGroup],
				[Division],
				[ProductGroup],
				[ProductType],
				[Model],
				[Brand],
				[Alias],
				[PromoterID],
				[Promoter],
				[MobileNo],
				[Region],
				[State],
				[City],
				[District],
				[NationCD],
				[ChannelCode],
				[Channel],
				[Contact],
				[PhoneNo],
				[ChannelCategory],
				[ShipmentType],
				[AggregationtyPE],
				[ChannelGroup],
				[ChannelType],
				[Month],
				[RRPPriceLoc],
				[Vat],
				[RRPLoc],
				[SKUSales],
				[IMEISales],
				[Sales],
				[TransactionID],
				[DateCreated],
				[CreatedBy])
		 VALUES (Source.[Subsidiary] ,
				Source.[DivisionGroup],
				Source.[Division],
				Source.[ProductGroup],
				Source.[ProductType],
				Source.[Model],
				Source.[Brand],
				Source.[Alias],
				Source.[PromoterID],
				Source.[Promoter],
				Source.[MobileNo],
				Source.[Region],
				Source.[State],
				Source.[City],
				Source.[District],
				Source.[NationCD],
				Source.[ChannelCode],
				Source.[Channel],
				Source.[Contact],
				Source.[PhoneNo],
				Source.[ChannelCategory],
				Source.[ShipmentType],
				Source.[AggregationtyPE],
				Source.[ChannelGroup],
				Source.[ChannelType],
				Source.[Month],
				Source.[RRPPriceLoc],
				Source.[Vat],
				Source.[RRPLoc],
				Source.[SKUSales],
				Source.[IMEISales],
				Source.[Sales],
				Source.[TransactionID],
				GETDATE(),
				Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_SellOutWeeklyTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_SellOutWeeklyTF]
	-- Add the parameters for the stored procedure here
	@Data TF_SellOutWeekly readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_SellOutWeekly AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[Subsidiary] = Source.[Subsidiary] AND
			 Target.[DivisionGroup] = Source.[DivisionGroup]  AND 
			 Target.[Division] = Source.[Division]  AND 
			 Target.[ProductGroup] = Source.[ProductGroup]  AND 
			 Target.[ProductType] = Source.[ProductType]  AND 
			 Target.[Model] = Source.[Model] AND
			 Target.[Brand] = Source.[Brand] AND
			 Target.[Alias] = Source.[Alias] AND
			 Target.[PromoterID] = Source.[Alias] AND
			 Target.[Promoter] = Source.[Alias] AND
			 Target.[MobileNo] = Source.[Alias] AND
			 Target.[Region] = Source.[Region] AND
			 Target.[State] = Source.[State] AND
			 Target.[City] = Source.[City] AND
			 Target.[District] = Source.[District] AND
			 Target.[NationCD] = Source.[District] AND
			 Target.[ChannelCode] = Source.[ChannelCode] AND
			 Target.[Channel] = Source.[Channel] AND
			 Target.[Contact] = Source.[Channel] AND
			 Target.[PhoneNo] = Source.[Channel] AND
			 Target.[ChannelCategory] = Source.[ChannelCategory] AND
			 Target.[ShipmentType] = Source.[Channel] AND
			 Target.[AggregationtyPE] = Source.[Channel] AND
			 Target.[ChannelGroup] = Source.[ChannelGroup] AND
			 Target.[ChannelType] = Source.[ChannelType] AND
			 Target.[Week] = Source.[Week] 

		)
	WHEN MATCHED THEN 
		 UPDATE SET [RRPPriceLoc] = Source.[RRPPriceLoc],
					[Vat] = Source.[Vat],
					[RRPLoc] = Source.[RRPLoc],
					[SKUSales] = Source.[SKUSales],
					[IMEISales] = Source.[IMEISales],
					[Sales] = Source.[Sales],
					[DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([Subsidiary] ,
				[DivisionGroup],
				[Division],
				[ProductGroup],
				[ProductType],
				[Model],
				[Brand],
				[Alias],
				[PromoterID],
				[Promoter],
				[MobileNo],
				[Region],
				[State],
				[City],
				[District],
				[NationCD],
				[ChannelCode],
				[Channel],
				[Contact],
				[PhoneNo],
				[ChannelCategory],
				[ShipmentType],
				[AggregationtyPE],
				[ChannelGroup],
				[ChannelType],
				[Week],
				[RRPPriceLoc],
				[Vat],
				[RRPLoc],
				[SKUSales],
				[IMEISales],
				[Sales],
				[TransactionID],
				[DateCreated],
				[CreatedBy])
		 VALUES (Source.[Subsidiary] ,
				Source.[DivisionGroup],
				Source.[Division],
				Source.[ProductGroup],
				Source.[ProductType],
				Source.[Model],
				Source.[Brand],
				Source.[Alias],
				Source.[PromoterID],
				Source.[Promoter],
				Source.[MobileNo],
				Source.[Region],
				Source.[State],
				Source.[City],
				Source.[District],
				Source.[NationCD],
				Source.[ChannelCode],
				Source.[Channel],
				Source.[Contact],
				Source.[PhoneNo],
				Source.[ChannelCategory],
				Source.[ShipmentType],
				Source.[AggregationtyPE],
				Source.[ChannelGroup],
				Source.[ChannelType],
				Source.[Week],
				Source.[RRPPriceLoc],
				Source.[Vat],
				Source.[RRPLoc],
				Source.[SKUSales],
				Source.[IMEISales],
				Source.[Sales],
				Source.[TransactionID],
				GETDATE(),
				Source.[User]);
END



GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_TIMTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_TIMTF]
	-- Add the parameters for the stored procedure here
	@Data TF_TIM readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_TIM AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[Dealer] = Source.[Dealer] AND Target.[AP1] = Source.[AP1] AND Target.[Week] = Source.[Week] AND
			 Target.[GSCMAccount] = Source.[GSCMAccount] AND Target.[Category] = Source.[Category] 
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([Dealer],
				[AP1],
				[GSCMAccount], 
				[Week],
				[Value],
				[Category],
				[TransactionID],[DateCreated],[CreatedBy])
		 VALUES (Source.[Dealer],
				Source.[AP1],
				Source.[GSCMAccount],
				Source.[Week],
				Source.[Value], 
				SOurce.[Category],
				Source.[TransactionID],GETDATE(),Source.[User]);

END




GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_WOSPerAP1AndAccountTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_WOSPerAP1AndAccountTF]
	-- Add the parameters for the stored procedure here
	@Data TF_WOSPerAP1AndAccount readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_WOSPerAP1AndAccount AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup] AND 
			 Target.[AP1] = Source.[AP1] AND
			 Target.[GSCMAccount] = Source.[GSCMAccount] AND 
			 Target.[Model] = Source.[Model] AND 
			 Target.[Category] = Source.[Category] AND
			 Target.[Week] = Source.[Week]  
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([ProductGroup],
				[AP1], 
				[GSCMAccount],
				[Model], 
				[Category], 
				[Week],
				[Value],
				[TransactionID],
				[DateCreated],
				[CreatedBy])
		 VALUES (Source.[ProductGroup],
				Source.[AP1],
				Source.[GSCMAccount],
				Source.[Model],
				Source.[Category], 
				SOurce.[Week],
				SOurce.[Value],
				Source.[TransactionID],
				GETDATE(),
				Source.[User]);

END




GO
/****** Object:  StoredProcedure [dbo].[TF_SAVE_WOSPerDealerTF]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TF_SAVE_WOSPerDealerTF]
	-- Add the parameters for the stored procedure here
	@Data TF_WOSPerDealer readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	MERGE TF_WOSPerDealer AS Target
	USING (SELECT *
		   FROM @Data mi
		   )AS Source  
	ON (
			 Target.[ProductGroup] = Source.[ProductGroup] AND 
			 Target.[GSCMAccount] = Source.[GSCMAccount] AND 
			 Target.[Channel] = Source.[Channel] AND
			 Target.[Model] = Source.[Model] AND 
			 Target.[Category] = Source.[Category] AND
			 Target.[Week] = Source.[Week]  
		)
	WHEN MATCHED THEN 
		 UPDATE SET [DateModified] = GETDATE(),
					[ModifiedBy] = Source.[User],
					[Value] = Source.[Value]
	WHEN NOT MATCHED BY TARGET THEN
		 INSERT ([ProductGroup],
				[GSCMAccount],
				[Channel], 
				[Model], 
				[Category], 
				[Week],
				[Value],
				[TransactionID],
				[DateCreated],
				[CreatedBy])
		 VALUES (Source.[ProductGroup],
				Source.[GSCMAccount],
				Source.[Channel],
				Source.[Model],
				Source.[Category], 
				SOurce.[Week],
				SOurce.[Value],
				Source.[TransactionID],
				GETDATE(),
				Source.[User]);

END




GO
/****** Object:  UserDefinedFunction [dbo].[f_split]    Script Date: 01/14/2018 4:16:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[f_split]
(
@param nvarchar(max), 
@delimiter char(1)
)
returns @t table (val nvarchar(max), seq int)
as
begin
set @param += @delimiter

;with a as
(
select cast(1 as bigint) f, charindex(@delimiter, @param) t, 1 seq
union all
select t + 1, charindex(@delimiter, @param, t + 1), seq + 1
from a
where charindex(@delimiter, @param, t + 1) > 0
)
insert @t
select substring(@param, f, t - f), seq from a
option (maxrecursion 0)
return
end
GO
