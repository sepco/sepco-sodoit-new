USE [SEPCO-DB]
GO
/****** Object:  Table [dbo].[TF_AccountLevelPSIMgmt]    Script Date: 3/13/2018 2:14:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TF_AccountLevelPSIMgmt](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[AP1] [nvarchar](max) NULL,
	[Account] [nvarchar](50) NULL,
	[Item] [nvarchar](50) NULL,
	[Category] [nvarchar](100) NULL,
	[Week] [nvarchar](50) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.TF_AccountLevelPSIMgmt] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TF_AP1Forecast]    Script Date: 3/13/2018 2:14:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TF_AP1Forecast](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GSCMAccount] [nvarchar](50) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[Item] [nvarchar](50) NULL,
	[Category] [nvarchar](100) NULL,
	[Week] [nvarchar](50) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.TF_AP1Forecast] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TF_AP2PSI]    Script Date: 3/13/2018 2:14:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TF_AP2PSI](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[Item] [nvarchar](50) NULL,
	[Category] [nvarchar](100) NULL,
	[Week] [nvarchar](50) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.TF_AP2PSI] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TF_FlooringSite]    Script Date: 3/13/2018 2:14:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TF_FlooringSite](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Brand] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](50) NULL,
	[Capacity] [nvarchar](max) NULL,
	[Model] [nvarchar](50) NULL,
	[SellOut] [int] NOT NULL,
	[Display] [int] NOT NULL,
	[Inventory] [int] NOT NULL,
	[StockRequested] [int] NOT NULL,
	[StockDelivered] [int] NOT NULL,
	[PromoterName] [nvarchar](max) NULL,
	[StoreCode] [nvarchar](max) NOT NULL,
	[StoreName] [nvarchar](100) NULL,
	[Week] [nvarchar](50) NULL,
	[Agency] [nvarchar](max) NULL,
	[Dealer] [nvarchar](50) NULL,
	[Segment] [nvarchar](max) NULL,
	[TeamHead] [nvarchar](max) NULL,
	[MidasInCharge] [nvarchar](max) NULL,
	[Area] [nvarchar](max) NULL,
	[ModelStatus] [nvarchar](max) NULL,
	[BWL] [nvarchar](max) NULL,
	[AP1Tagging] [nvarchar](max) NULL,
	[TransactionID] [int] NOT NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.TF_FlooringSite] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TF_FlooringTarget]    Script Date: 3/13/2018 2:14:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TF_FlooringTarget](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StoreCode] [nvarchar](max) NULL,
	[StoreName] [nvarchar](100) NULL,
	[Dealer] [nvarchar](50) NULL,
	[Account] [nvarchar](50) NULL,
	[AP1] [nvarchar](max) NULL,
	[ProductGroup] [nvarchar](max) NULL,
	[ProductType] [nvarchar](50) NULL,
	[Model] [nvarchar](50) NULL,
	[DisplayTarget] [nvarchar](max) NULL,
	[Week] [nvarchar](50) NULL,
	[Value] [decimal](18, 2) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[DateCreated] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.TF_FlooringTarget] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AccounrLevelPSIMgmtNonClustered]    Script Date: 3/13/2018 2:14:29 PM ******/
CREATE NONCLUSTERED INDEX [AccounrLevelPSIMgmtNonClustered] ON [dbo].[TF_AccountLevelPSIMgmt]
(
	[Week] ASC,
	[Account] ASC,
	[Category] ASC,
	[Item] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AP1ForecastNonClustered]    Script Date: 3/13/2018 2:14:29 PM ******/
CREATE NONCLUSTERED INDEX [AP1ForecastNonClustered] ON [dbo].[TF_AP1Forecast]
(
	[Week] ASC,
	[Category] ASC,
	[Item] ASC,
	[GSCMAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [AP2PSINonClustered]    Script Date: 3/13/2018 2:14:29 PM ******/
CREATE NONCLUSTERED INDEX [AP2PSINonClustered] ON [dbo].[TF_AP2PSI]
(
	[Week] ASC,
	[Category] ASC,
	[Item] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FlooringSiteNonClustered]    Script Date: 3/13/2018 2:14:29 PM ******/
CREATE NONCLUSTERED INDEX [FlooringSiteNonClustered] ON [dbo].[TF_FlooringSite]
(
	[Week] ASC,
	[Model] ASC,
	[StoreName] ASC,
	[Dealer] ASC,
	[ProductType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [FlooringTargetNonClustered]    Script Date: 3/13/2018 2:14:29 PM ******/
CREATE NONCLUSTERED INDEX [FlooringTargetNonClustered] ON [dbo].[TF_FlooringTarget]
(
	[Week] ASC,
	[Model] ASC,
	[StoreName] ASC,
	[Dealer] ASC,
	[Account] ASC,
	[ProductType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spu_AP1_GET_TotalValueofAllCategories]    Script Date: 3/13/2018 2:14:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_AP1_GET_TotalValueofAllCategories]
(
	@model nvarchar(MAX),
	@allWeek nvarchar(MAX),
	@account nvarchar(MAX)
)	
AS
BEGIN
	
	DECLARE @TF_AccountLevelPSIMgmtValue decimal(18,2), @TF_AP1ForecastValue decimal(18,2), @TF_FlooringTargetValue decimal(18,2),
	 @TF_FlooringSite decimal(18,2), @TotalValueforAllCategory decimal(18,2)
	
	Select @TF_AccountLevelPSIMgmtValue = ISNULL(sum(Value),0) from TF_AccountLevelPSIMgmt  WITH(INDEX(AccounrLevelPSIMgmtNonClustered))
	where Category in ('Sell-in(AP1)','Sell-out FCST(AP1/CON)','Sell-out FCST(AP1)','EOH_EX(AP1)') and Account = @account and Item = @model and Week in (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AP1ForecastValue =  ISNULL(sum(Value),0) from TF_AP1Forecast  WITH(INDEX(AP1ForecastNonClustered))
	where GSCMAccount = @account  and Item = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	select @TF_FlooringTargetValue =  ISNULL(sum(Value),0) from TF_FlooringTarget  WITH(INDEX(FlooringTargetNonClustered))
	where  Account = @account  and Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))
	
	select @TF_FlooringSite=  ISNULL(sum(Display),0) from TF_FlooringSite WITH(INDEX(FlooringSiteNonClustered))
	where  Dealer = @account  and Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	set @TotalValueforAllCategory = @TF_AccountLevelPSIMgmtValue + @TF_AP1ForecastValue + @TF_FlooringTargetValue + @TF_FlooringSite
	select @TotalValueforAllCategory
END

GO
/****** Object:  StoredProcedure [dbo].[spu_AP1Short_GET_TotalValueofAllCategories]    Script Date: 3/13/2018 2:14:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_AP1Short_GET_TotalValueofAllCategories]
(
	@model nvarchar(MAX),
	@allWeek nvarchar(MAX),
	@dealer nvarchar(MAX)
)	
AS
BEGIN
	
	DECLARE @TF_SellInGI decimal(18,2), @TF_SellOutAndInventoryWeekly decimal(18,2), @TF_FlooringTargetValue decimal(18,2),
	 @TF_FlooringSite decimal(18,2), @TotalValueforAllCategory decimal(18,2)

	
	Select @TF_SellInGI = ISNULL(sum(TF_SellInGI.SaleQty),0) from TF_SellInGI INNER JOIN MF_DealerAndAccountMapping
	ON TF_SellInGI.SoldToPartyCode = MF_DealerAndAccountMapping.SoldToParty
	where MF_DealerAndAccountMapping.Dealer = @dealer and  TF_SellInGI.Model = @model and TF_SellInGI.SalesWeek in (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_SellOutAndInventoryWeekly =  ISNULL(sum(Sales),0) from TF_SellOutAndInventoryWeekly 
	where ChannelGroup= @dealer  and Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	--select @TF_FlooringTargetValue =  ISNULL(sum(Value),0) from TF_FlooringTarget
	--where  Account = @account  and Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))
	
	select @TF_FlooringSite=  ISNULL(sum(Display),0) from TF_FlooringSite
	where  Dealer = @dealer  and Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	set @TotalValueforAllCategory = @TF_SellOutAndInventoryWeekly + @TF_SellOutAndInventoryWeekly + @TF_FlooringSite
	select @TotalValueforAllCategory
END

GO
/****** Object:  StoredProcedure [dbo].[spu_AP2_GET_TotalValueofAllCategories]    Script Date: 3/13/2018 2:14:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_AP2_GET_TotalValueofAllCategories]
(
	@model nvarchar(MAX),
	@allWeek nvarchar(MAX)
)	
AS
BEGIN
	
	DECLARE @TF_AP2PSIValue decimal(18,2), @TF_AccountLevelPSIMgmtValue decimal(18,2), @TF_AP1ForecastValue decimal(18,2), @TF_FlooringTargetValue decimal(18,2),
	 @TF_FlooringSite decimal(18,2), @TotalValueforAllCategory decimal(18,2)
	
	Select @TF_AP2PSIValue = ISNULL(sum(Value),0) from TF_AP2PSI WITH(INDEX(AP2PSINonClustered))		
	where Week in ('BOH','ARRIVAL','GI') and Item = @model and Category in  (Select Value from funcListToTableString(@allWeek,','))
		
	Select @TF_AccountLevelPSIMgmtValue = ISNULL(sum(Value),0) from TF_AccountLevelPSIMgmt WITH(INDEX(AccounrLevelPSIMgmtNonClustered))
	where Category in ('Sell-out FCST(GC)','Sell-out FCST(AP2)','Sell-out FCST(AP1)','EOH_EX(GC)','EOH_EX(AP2)','EOH_EX(AP1)') and Item = @model and Week in (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AP1ForecastValue =  ISNULL(sum(Value),0) from TF_AP1Forecast  WITH(INDEX(AP1ForecastNonClustered))
	where Item = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	select @TF_FlooringTargetValue =  ISNULL(sum(Value),0) from TF_FlooringTarget WITH(INDEX(FlooringTargetNonClustered))
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))
	
	select @TF_FlooringSite=  ISNULL(sum(Display),0) from TF_FlooringSite WITH(INDEX(FlooringSiteNonClustered))
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,',')) 

	set @TotalValueforAllCategory = @TF_AP2PSIValue + @TF_AccountLevelPSIMgmtValue + @TF_AP1ForecastValue + @TF_FlooringTargetValue + @TF_FlooringSite
	select @TotalValueforAllCategory
END

GO
/****** Object:  StoredProcedure [dbo].[spu_GC_GET_TotalValueofAllCategories]    Script Date: 3/13/2018 2:14:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spu_GC_GET_TotalValueofAllCategories]
(
	@model nvarchar(MAX),
	@allWeek nvarchar(MAX)
)	
AS
BEGIN
	
	DECLARE @TF_AP2PSIValue decimal(18,2), @TF_AccountLevelPSIMgmtValue decimal(18,2), @TF_AP1ForecastValue decimal(18,2), @TF_FlooringTargetValue decimal(18,2),
	 @TF_FlooringSite decimal(18,2), @TotalValueforAllCategory decimal(18,2)
	
	Select @TF_AP2PSIValue = ISNULL(sum(Value),0) from TF_AP2PSI WITH(INDEX(AP2PSINonClustered))		
	where Week in ('BOH','ARRIVAL','GI','RTF') and Item = @model and Category in  (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AccountLevelPSIMgmtValue = ISNULL(sum(Value),0) from TF_AccountLevelPSIMgmt WITH(INDEX(AccounrLevelPSIMgmtNonClustered))
	where Category in ('Sell-in(GC)','Sell-out FCST(GC)','Sell-out FCST(AP2)','Sell-out FCST(AP1)','EOH_EX(GC)','EOH_EX(AP2)','EOH_EX(AP1)') and Item = @model and Week in (Select Value from funcListToTableString(@allWeek,','))
	
	Select @TF_AP1ForecastValue =  ISNULL(sum(Value),0) from TF_AP1Forecast WITH(INDEX(AP1ForecastNonClustered))
	where Item = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	select @TF_FlooringTargetValue =  ISNULL(sum(Value),0) from TF_FlooringTarget WITH(INDEX(FlooringTargetNonClustered))
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))
	
	select @TF_FlooringSite=  ISNULL(sum(Display),0) from TF_FlooringSite WITH(INDEX(FlooringSiteNonClustered))
	where  Model = @model and Week in  (Select Value from funcListToTableString(@allWeek,','))

	set @TotalValueforAllCategory = @TF_AP2PSIValue + @TF_AccountLevelPSIMgmtValue + @TF_AP1ForecastValue + @TF_FlooringTargetValue + @TF_FlooringSite
	select @TotalValueforAllCategory
END

GO
