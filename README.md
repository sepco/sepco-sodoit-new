# README #

### What is this repository for? ###

* For Samsung Sales Simulation and Reports
* A EF ASP.net MVC Web Application with Code First Approach
* Features
    1. Upload Excel Files
         a. Master Files
         b. Transactional Files
    2. Biz Simulation
         a. AP1
         b. AP2
         c. GC
    3. Download Excel File
         a. Master Files and Transactional Files Template
         b. Biz Simulation File
    4. Graphical Reports
* Database Version is SQL 2012
* Development Version

### How do I setup? ###

* Clone this
* Open the Solution in Visual Studio 2013 up.
* Run "update-database" on Package Manager Console
* Run the script "sepcodb-stored-and-userDefinedTable-01-14-2018.sql"
* Then Merge your work

### Who do I talk to? ###

* If you have any questions, please ask Aries Tagudin or Rogel Labanan.
